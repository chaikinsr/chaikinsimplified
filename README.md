# Chaikinsimplified

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.7.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4600/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## chaikinsimplified repo name 
1. Get a clone of "chaikinsimplified" repo and switch master branch to  development branch .
2. To install project dependency use - npm install 

## POWER PULSE PROJECT
1. To install project dependency use - npm install 
2. To start project   -  npm start
   To start this project we have to set valid token on environment.ts file. We can get this token from cookies. Token name in the cookies would be dev-token , app-token.
3. Build command - ng build --prod --base-href ./


## PGR-WIDGET
Widget run command  : npm start pgr-widget
Widget build Command :  ng build pgr-widget --prod --base-href ./
We have to change "pgr-widget.html" to "index.html" in a build bundle (dist folder) for deployment. 

## list-utility
list-utility npm start command  : npm start list-utility
list-utility build Command :  ng build list-utility --prod --base-href ./
We have to change "list-utility.html" to "index.html" in a build bundle (dist folder) for deployment. 



