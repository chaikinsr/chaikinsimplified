import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginGuard } from './core/guard/login.guard';
import { ProjectResolver } from './resolver/project.resolver';

const routes: Routes = [
  {
    path: '',
    resolve: {
      data: ProjectResolver
    },
  
    loadChildren: () => import('./layout/layout.module').then(m => m.LayoutModule),
    data: {
      isPublic: true
    }
  }
];


@NgModule({
  imports: [
    // [RouterModule.forRoot(routes,{useHash: true})]
    RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled',
    anchorScrolling: 'enabled',
    onSameUrlNavigation: 'reload', useHash: true,
    relativeLinkResolution: 'legacy'
},),
  ],
  exports: [RouterModule],
  providers: [ProjectResolver]
  
})
export class AppRoutingModule {}