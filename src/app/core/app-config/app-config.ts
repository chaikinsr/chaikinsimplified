import { TemplateRef } from "@angular/core";

export class AppConfig {

    // public static RedirectComponentPath = {
    //     LOGIN: '/login',
    //     HEALTHCHECK: '/home/health-check',
    //     RESEARCH: '/home/research',
    //     ROBOSCREENS: '/home/robo-screens',
    //     REPORTS: '/home/reports'
    // };

    public static PriceDataInterval = {
        INTERVEL: 72000
    };

    public static Toast = {
        DELAY: 4000
    };

    public static SignalMap = {
        0: 'Oversold Buy',
        1: 'Overbought Sell',
        2: 'Momentum Breakout',
        3: 'Momentum Breakdown',
        4: 'Reversal Buy',
        5: 'Reversal Sell',
        6: 'Money Flow Buy',
        7: 'Money Flow Sell',
        8: 'Rel. Strength Buy',
        9: 'Rel. Strength Sell',
        10: 'Rel. Strength Breakout',
        11: 'Rel. Strength Breakdown'
    }
}


