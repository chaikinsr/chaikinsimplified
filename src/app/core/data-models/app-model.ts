import { map } from 'rxjs/operators';
import { AppConst } from '../app-constants/app-const';
import {
  AnnualEP,
  CalendarEarningData,
  EarningData,
  EarningsContextSummary,
  EtfData,
  ExpertOpnionsContextSummary,
  FinancialContextSummary,
  NavigationListDataWithLabel,
  PgrContextSummary,
  PowerbarData,
  PriceVolumeContextSummary,
} from './service-model';

export class SortingModel {
  sortData: Array<any>;
  sortOrder: string;
  sortKey: string;
  ignoreDataKeys: Array<any>;
  ignoreData: Array<any>;
  sortLevelKey: Array<string>;
  isExtended: boolean;
  extendedData: SortingModel;
}

export class SortObjectModel {
  sortingKey: string;
  sortingOrder: string;
  ignoreKey: Array<any>;
}

export class HeaderDataModel {
  pgrRating: number;
  rawPgrRating: number;
  pgr: number;
  ratingType: string;
  ratingTypeColorLabel: string;
  ratingTypeColorBg: string;
  symbol: string;
  name: string;
  last: number;
  lastFormattedString: string;
  change: number;
  listRating: number;
  changeFormattedString: string;
  percentageChange: number;
  percentageChangeFormattedString: string;
  industry: string;
  isEtf: boolean;
  pgrImageSrc: string;
  pgrImageSrcGradient: string;
  reportDate: string;
  generalDate: string;
  red: number;
  yellow: number;
  green: number;
  neutralPerCountStyle: number;
  bearishPerCountStyle: number;
  bullishPerCountStyle: number;
  percentageChangeColor: string;
  financialRating: number;
  earningRating: number;
  technicalRating: number;
  expertRating: number;
  financialDebtToEquity: number;
  financialPriceToBook: number;
  financialReturnOnEquity: number;
  financialPriceToSales: number;
  financialFreeCashFlow: number;
  earningGrowth: number;
  earningSurprise: number;
  earningTrend: number;
  earningProfitToEarning: number;
  earningConsistency: number;
  technicalRelativeStrengthVsMarket: number;
  technicalChaikinMoneyFlow: number;
  technicalPriceStrength: number;
  technicalPriceTrendRoc: number;
  technicalVolumeTrend: number;
  expertEstimateTrend: number;
  expertShortInterest: number;
  expertInsiderActivity: number;
  expertRatingTrend: number;
  expertIndustrialRelStrength: number;

  lastReportDate: string;
  nextReportDate: string;
  lastQuarterConsensusEstimate: string;
  epsDiffDescription: string;
  currentQuarter: string;
  currentQuarterConsensusEstimate: string;
  earningsReportInfo: number;
  preReportInfo: number;
  estimateRevisionInfo: number;
  industryColor: string;
  imgSrcBind: string;
  svgColorSet: string;
}

export class ContextSummaryDataModel {
  pgrContextSummary: PgrContextSummary;
  financialContextSummary: FinancialContextSummary;
  earningsContextSummary: EarningsContextSummary;
  priceVolumeContextSummary: PriceVolumeContextSummary;
  expertOpnionsContextSummary: ExpertOpnionsContextSummary;
  financialRatingLableColorClass: string;
  financialRatingBgColorClass: string;
  earningRatingLableColorClass: string;
  earningRatingBgColorClass: string;
  priceVolumeRatingLableColorClass: string;
  priceVolumeRatingBgColorClass: string;
  expertRatingLableColorClass: string;
  expertRatingBgColorClass: string;

  constructor() {
    this.pgrContextSummary = new PgrContextSummary();
    this.financialContextSummary = new FinancialContextSummary();
    this.earningsContextSummary = new EarningsContextSummary();
    this.priceVolumeContextSummary = new PriceVolumeContextSummary();
    this.expertOpnionsContextSummary = new ExpertOpnionsContextSummary();
  }
}

export class FinancialsDataModel {
  ticker: string;
  returnOneMonth: string;
  returnThreeMonth: string;
  returnOnEquity: string;
  returnOnInvestment: string;
  yield: string;
  dividendPerShare: string;
  payoutRatio: string;
  growthRate5Year: string;
  peg: string;
  priceToSale: string;
  priceEarning: string;
  priceToBook: string;
  bookValue: string;
  current_ratio: string;
  debtEquity: string;
  marketCap: string;
  revenue: string;

  returnOneMonthFormatted: string;
  returnThreeMonthFormatted: string;
  returnOnEquityFormatted: string;
  returnOnInvestmentFormatted: string;
  yieldFormatted: string;
  dividendPerShareFormatted: string;
  payoutRatioFormatted: string;
  growthRate5YearFormatted: string;
  pegFormatted: string;
  priceToSaleFormatted: string;
  priceEarningFormatted: string;
  priceToBookFormatted: string;
  bookValueFormatted: string;
  current_ratioFormatted: string;
  debtEquityFormatted: string;
  marketCapFormatted: string;
  revenueFormatted: string;
}

export class TechnicalDataModel {
  ticker: string;
  chg24weekRelativeToSPY: string;
  chg4weekRelativeToSPY: string;
  volatility: string;
  beta: string;
  avgVolume90Days: string;
  avgVolume20Days: string;
  high52Week: string;
  low52Week: string;

  chg24weekRelativeToSPYFormatted: string;
  chg4weekRelativeToSPYFormatted: string;
  volatilityFormatted: string;
  betaFormatted: string;
  avgVolume90DaysFormatted: string;
  avgVolume20DaysFormatted: string;
  high52WeekFormatted: string;
  low52WeekFormatted: string;
}

export class ExpertDataModel {
  nextQuarterPercentChange: string;
  currentQuarter30Dago: string;
  nextQuarterCurrent: string;
  currentQuarterPercentChange: string;
  nextQuarter7Dago: string;
  currentQuarterCurrent: string;
  fiveweekago: string;
  meanLastWeek: string;
  meanThisWeek: string;
  fiveweekagoColor: string;
  meanLastWeekColor: string;
  meanThisWeekColor: string;
  shortIntrest: number;
  shortIntrestHighColor: string;
  shortIntrestMediumColor: string;
  shortIntrestLowColor: string;
}

export class EtfStatDataModel {
  dividentYield: string;
  ticker: string;
  assets: string;
  management: string;
  expenseRatio: string;
  holdings: string;
  strategy: string;
  avgDailyVolume: string;
  etfGroup: string;
  beta: string;

  assetsFormatted: string;
  expenseRatioFormatted: string;
  dividentYieldFormatted: string;
  avgDailyVolumeFormatted: string;
}

export class QuaterlyEpsData {
  category: string;
  first: number;
  second: number;
  third: number;
  four: number;
}
export class AnnualEps {
  year: string;
  eps: number;
}

export class AnnualRevenueData {
  'revenue': number;
  'year': string;
}

export class EarningAnnouncementData {
  actualEps: number;
  estimatedEps: number;
  quarter: string;
  weight: number;
  color: string;
  constructor() {
    this.weight = 2;
    this.color = '#d8854f';
  }
}

export class StockEarningsDataModel {
  quaterlyEpsData: QuaterlyEpsData[];
  annualEPS: AnnualEps[];
  annualRevenue: AnnualRevenueData[];
  earningAnnouncement: EarningAnnouncementData[];
}

export class UISetting {
  histPrice: boolean;
  includeTermAvg: boolean;
  includeBenchmark: boolean;
  overViewScrollTopPos: number;
  riskcrollTopPos: number;
  ratingsScrollTopPos: number;
  holdingScrollTopPos: number;
  selectedPerformanceBenchmark: string;
  selectedRelstrengthBenchmark: string;
  printChartData: any;
  constructor() {
    this.includeTermAvg = true;
    this.includeBenchmark = true;
    this.histPrice = true;
    this.selectedPerformanceBenchmark = 'SPY';
    this.selectedRelstrengthBenchmark = 'SPY';
    this.printChartData = [];
  }
}

export class HistoricPriceChartData {
  dates: string[];
  closingprices: string[];
  symbol: string;
  chainkinTrends: string[];
  interval: string;
  spyrelativeStrengths: string[];
  powerGauges: string[];
  moneyFlow: string[];
}

export class DividendChartData {
  pay_date: string;
  ticker: string;
  dividend: string;
  interval: string;
}

export class BenchMarkRelativeStrengthChartData {
  dates: string[];
  relative_strength: string[];
  symbol: string;
  benchmark: string;
  interval: string;
}

export class MarketIndicesDataModel {
  listId: number;
  hasChart: boolean;
  symbol: string;
  isExtenable: boolean;
  description: string;
  listName: string;
  listType: string;
  change: number;
  last: number;
  percentage: number;
  pgr: number;
  isEtf: boolean;
  rawPgr: number;

  colorClass: string;
  lastInString: string;
  percentageChangeInString: string;
  arrowDirection: string;
}

export class ListSymbolsPriceForSelectedTimeFrameModel {
  change: number;
  corrected_pgr: number;
  is_etf: boolean;
  last: number;
  percent_change: number;
  raw_pgr: number;
  symbol: string;
  imgSrc: string;
  changeBarWidth: number;
  modifiedChangeValue: string;
  calculateWidth: number;
  sliderValue: boolean;
  selectedTimeFrame: string;
  companyName: string;
  alertTrue: boolean;
  finalRating: number;
  alertType: string;
  listPriceClass: string;
}

export class AlertDataModel {
  date: string;
  symbol: string;
  pgr: number;
  label: string;
  alertName: string;
  alertInfo: string;
  formattedDate: string;
  rawPGR: number;
  isEtf: boolean;
  value: number;
  oldValue: string;
  newValue: string;
  estimateEps: string;
  actualEps: string;
  changeInPer: number;
  changeInPerFormatted: string;
  classForPerChange: string;
  rank: number;
  close: string;
  ratingClass: any;
}

export class SignalDataModel {
  symbol: string;
  signal: string[];
  signalDate: string[];
  close: string[];
}

export class EarningsDataModel {
  data: EarningData;
  surpriseModifiedData: SurpriseModifiedDataModel;
  estimateModifiedData: EstimateModifiedDataModel;
  constructor() {
    this.data = new EarningData();
    this.surpriseModifiedData = new SurpriseModifiedDataModel();
    this.estimateModifiedData = new EstimateModifiedDataModel();
  }
}

export class SurpriseModifiedDataModel {
  [key: string]: EarningSurpriseData[];
}

export class EstimateModifiedDataModel {
  [key: string]: EarningEstimateData[];
}

export class EarningsCalendarDataModel {
  earningSurprise: EarningSurpriseData[];
  earningEstimate: EarningEstimateData[];
  constructor() {
    this.earningSurprise = [];
    this.earningEstimate = [];
  }
}

export class EarningSurpriseData {
  symbol: string;
  percentChange: number;
  epsYearAgo: string;
  change: number;
  consensusEstimate: string;
  percentageSurprise: string;
  isEtf: boolean;
  pgrRating: number;
  rawPgrRating: number;
  reportDate: string;
  company: string;
  actualEPS: number;
  quarter: number;
  lastPrice: number;
  reportingTime: number;

  percentChangeFormatted: string;
  percentChangeFormattedClass: boolean;
  changeFormatted: string;
  rating: number;
  changeFormattedClass: boolean;
  imageSrc: string;
  reportDateFormatted: string;
  actualEPSFormatted: string;
  quaterFormatted: string;
  lastPriceFormatted: string;
}

export class EarningEstimateData {
  estimateTrend: string;
  symbol: string;
  percentChange: number;
  epsYearAgo: string;
  change: number;
  epsEstimateCurrent: number;
  isEtf: boolean;
  pgrRating: number;
  rawPgrRating: number;
  reportingTime: number;
  nextReportDate: string;
  company: string;
  quarter: number;
  lastPrice: number;
  estimateTrendClass: string;
  percentChangeFormatted: string;
  changeFormatted: string;
  rating: number;
  imageSrc: string;
  nextReportDateFormatted: string;
  changeFormattedClass: boolean;
  quaterFormatted: string;
  lastPriceFormatted: string;
  epsEstimateCurrentFormatted: string;
}

export class AlertModel {
  data: AlertDataModel[];
  filterData: AlertDataModel[];
  count: BullishBearishCount;
  isBullishSelected: boolean;
  filterApply: string;
  constructor() {
    this.count = new BullishBearishCount();
  }
}

export class AlertSortingDataModel {
  symbol = 'asc';
  date = 'desc';
  exposure = 'asc';
  defaultValueIsTrue = true;
  defaultSorting = 'symbol';
  sortingKey = 'symbol';
  sortingOrder = 'asc';
  errorMsg: string;
}

export class BullishBearishCount {
  bullish: number;
  bearish: number;
  constructor() {
    this.bullish = 0;
    this.bearish = 0;
  }
}

export class AlertFilterType {
  value: string;
  viewValue: string;
  comment: string;
}

export class PriceMovementSorting {
  defaultSortingValue = {
    symbol: 'asc',
    percent_change: 'asc',
  };
  ignoreKeys = {
    symbol: [],
    percent_change: [],
  };
  defaultSorting = 'percent_change,symbol';
  sortingKey: string;
  isDefaultSorting = true;
  sortingOrder: string;
  sortingOrderPgr: string;
  sortingOrderSymbols: string;
  sortingOrderPrice: string;
  finalSorting: string;
  constructor() {}
}

export class GetListDataModel {
  data: NavigationListExtendedDataModel[];
  parentId: number;
  activeListId: number;
  listType: string;
}

export class NavigationListExtendedDataModel {
  description: string;
  hasChart: boolean;
  listRating: string;
  listId: number;
  listName: string;
  listType: string;
  powerBar: PowerbarData;
  priorityList: boolean;
  constructor() {
    this.powerBar = new PowerbarData();
  }
}
export class GlobalListWithPowbarModel {
  data: NavigationListWithPowbarModel[] = [];
  title: string;
}

export class NavigationListWithPowbarModel {
  listId: number;
  hasChart: boolean;
  isExtenable: boolean;
  description: string;
  listName: string;
  listType: string;
  symbolCount: number;
  powerBar: PowerbarData;
  extendedData: GetListDataModel;
  powerBarBullish: number;
  listNameFormatted: string;
  priorityList: boolean;
  constructor() {
    this.powerBar = new PowerbarData();
  }
}

export class GlobalListWithLabelModel {
  data: NavigationListDataWithLabel;
  title: string;
}

export class SymbolPriceModel {
  last: number;
  rawPgr: number;
  symbol: string;
  pgrRating: number;
  name: string;
  change: number;
  percentage: number;
  isEtf: boolean;
  pgr: number;
  lastFormattedString: string;
  ratingType: string;
  ratingTypeColorLabel: string;
  ratingTypeColorBg: string;
  changeFormattedString: string;
  percentageChangeFormattedString: string;
  percentageChangeColor: string;
  pgrImageSrc: string;

  Change: number;
  Last: number;
  PGR: number;
  Percentage: number;
  SummaryRating: number;
  TechnicalRating: number;
  div_yield: number;
  filter: number;
  industry_ListID: number;
  industry_name: string;
  isNoteExist: boolean;
  is_etf_symbol: boolean;
  list_rating: number;
  market_cap: number;

  raw_PGR: number;
  signals: string;
}
export class specialList {
  listName: string;
  listId: number;
  symbolCount: number;
}

export class ProductMapping {
  report: boolean;
  dashboard: boolean;
  checklist: boolean;
}

///Start: Recently Viewed
export class RecentlyViewedInfo {
  symbols: string[] = null;

  constructor() {
    this.symbols = JSON.parse(
      localStorage.getItem('PGR_RecentlyViewedSymbols')
    ) as unknown as [];
    if (!this.symbols) {
      this.symbols = [];
      // this.setSymbol('AMZN');
    }
  }

  clearSymbolsMetoInfo() {
    localStorage.removeItem('PGR_RecentlyViewedSymbols');
    this.symbols = [];
  }

  getSymbols(): string[] {
    return this.symbols;
  }

  getSymbol(symbol: string): string {
    let symbolsInfo = this.getSymbols();
    return symbolsInfo.find(
      (item: string) => item.toLowerCase() === symbol.toLowerCase()
    );
  }

  setSymbol(symbol: string) {
    let symbolsInfo = this.getSymbols();
    let idx = symbolsInfo.findIndex(
      (item: string) => item.toLowerCase() == symbol.toLowerCase()
    );
    // let data = ((symbolsInfo)).map(symbolsInfo1 => symbolsInfo1);
    // let idx;
    // console.log(symbolsInfo[0],symbol,data);
    // if (symbolsInfo){
    //  // console.log(data,symbol);
    //  data.forEach((item , i) => {
    //    console.log(item);
    //    if(item.toLowerCase() == symbol.toLowerCase()){
    //     idx = i;
    //    }
    //  });
    // let idx = symbolsInfo.findIndex((item: string) => item.toLowerCase() === symbol.toLowerCase());
    //Remove symbol if already exist.
    if (idx >= 0) {
      this.symbols.splice(idx, 1);
    }

    // Add symbolInfo in 0th index or on top
    this.symbols.splice(0, 0, symbol);

    //Always keep max 20 elements
    if (this.symbols.length > 20) {
      this.symbols = this.symbols.slice(0, 20);
    }

    localStorage.setItem(
      'PGR_RecentlyViewedSymbols',
      JSON.stringify(this.symbols)
    );
  }
}

export class ChecklistModel {
  relativeStrength: string;
  ltTrend: string;
  pgr: string;
  industry: string;
  overboughtOversold: string;
  moneyFlow: string;
  stockStatus: string;
  sentence1: string;
  sentence2: string;
  sentence3a: string;
  sentence3b: string;
  lastPrice: string;
  change: string;
  changePercentage: string;
  symbol: string;
  pgrRating: number;
  rawPgrRating: number;
  sentence4: string;
  strengthCount: number;
  timingCount: number;
  strengthCountClass: string;
  timingCountClass: string;
  pgrColor: string;
  pgrIconColor: string;
  pgrIconClass: string;
  relativeStrengthColor: string;
  relativeStrengthIconColor: string;
  relativeStrengthIconClass: string;
  industryColor: string;
  industryIconClass: string;
  industryIconColor: string;

  overboughtOversoldColor: string;
  overboughtOversoldIconClass: string;
  overboughtOversoldIconColor: string;
  ltTrendColor: string;
  ltTrendIconClass: string;
  ltTrendIconColor: string;
  moneyFlowColor: string;
  moneyFlowIconClass: string;
  moneyFlowIconColor: string;
  strengthOverallColor: string;
  timingOverallColor: string;
  sentenceBreakFirst: string;
  sentenceBreakSecond: string;
  sentenceBreakThird: string;
  sentenceBreakFourth: string;
  sentenceBreakFifth: string;
  sentenceBreakSecondColor: string;
  sentenceBreakFouthColor: string;
}

export class ProductAccessModel {
  checklist = false;
  report = false;
  dashboard = false;
  powerGaugeAnalytics = false;
  totalProductAccessArray = [];
  productsSubscriptionList = [];
}

export class CalendarEarningsDataModel {
  data: CalendarEarningData;
  surpriseModifiedData: SurpriseModifiedDataModel;
  estimateModifiedData: EstimateModifiedDataModel;
  constructor() {
    this.data = new CalendarEarningData();
    this.surpriseModifiedData = new SurpriseModifiedDataModel();
    this.estimateModifiedData = new EstimateModifiedDataModel();
  }
}

export class AlertComponentData {
  alertModel: AlertModel;
  alertDataModel: AlertDataModel[];
  totalAlertCount: number;
}

export class IndustryExposureDataModel {
  strength: string;
  powerBarRating: string;
  industry: string;
  symbolCount: number;
  symbolList: IndustryExposureSymbolListDataModel[];
  strengthClass: string;
  sliderwidth: number;
  svgColorSet: string;
  imgSrcBind: string;
}

export class IndustryExposureSymbolListDataModel {
  pgr: number;
  rawPgr: number;
  symbol: string;
  pgrImg: string;
  rating: number;
}

export class StrengthCountObject {
  strong = 0;
  weak = 0;
  neutral = 0;
  strongSliderWidth = 0;
  weakSliderWidth = 0;
}

export class IndustryExposureSortingDataModel {
  powerBarRating = 'asc';
  industry = 'desc';
  exposure = 'asc';
  defaultValueIsTrue = true;
  defaultSorting = 'powerBarRating';
  sortingKey = 'powerBarRating';
  sortingOrder = 'desc';
  errorMsg: string;
}

// export class SubExtendedListDataModel {
//   sub_extended_list_name: string;
// }
export class SubExtendedListDataModel {
  name: string;
  value: number;
  green: number;
  list_id: number;
  red: number;
  sub_extended_list_name: string;
  symbolCount: number;
  yellow: number;
  widthOfGreenSlider: number;
  widthOfRedSlider: number;
  extendedListName: string;
  rating: string;
  color: string;
  stripClass: string;
  shortName: string;
  image: string;
  nodeSettings: any;
  value1: number;
}

// export class ExtendedListDataModel {
//   subExtendedList: SubExtendedListDataModel[];
//   listName: string;
//   // value: number;
// }

// export class MarketScannerDataDataModel {
//   extendedList: ExtendedListDataModel[];
//   listName: string;
//   // value: number;
// }

export class ExtendedListDataModel {
  children: SubExtendedListDataModel[] = [];
  name: string;
  value: number;
  color: string;
  splitName: string;
  shortName: string;
  description: string;
  nodeSettings: any;
  value1: number;
}

export class MarketScannerDataDataModel {
  children: ExtendedListDataModel[] = [];
  name: string;
  value: number;
  color: string;
  shortName: string;
  nodeSettings: any;
  value1: number;
}

export class GetListSymbolsDataModel {
  symbol: string;
  rawPGR: number;
  industryName: string;
  isNoteExist: boolean;
  change: number;
  filter: number;
  last: number;
  isEtfSymbol: boolean;
  signals: string;
  marketCap: number;
  divYield: number;
  name: string;
  listRating: number;
  pgr: number;
  technicalRating: number;
  percentage: number;
  industryListID: number;
  summaryRating: number;
  pgrImage: string;
  pgrName: string;
  changeFormatted: string;
  lastFormatted: string;
  percentageFormatted: string;
  percentageFormattedClass: string;
  changeFormattedClass: string;
  rating: number;
  modifiedName: string;
}

export class UserAccessibilityModel {
  tickerBanner: boolean;
  checklist: boolean;
  dashboard: boolean;
  alerts: boolean;
  earning: boolean;
  welcomePopup: boolean;
  industryExposure: boolean;
  marketScanner: boolean;
  advancedSearch: boolean;
  tabs: boolean;
  reportPage: boolean;
  chaikinList: boolean;
  powerFeed: boolean;
  issuesAndUpdates: boolean;
  majorIndex: boolean;
  alertpanel: boolean;
  alertTable: boolean;
  internalTabOnDashboard: boolean;
  constructor(role: string, device: string) {
    let prepareRoleText = 'USERPERMISSIONFOR' + role + '_' + device;
    let userPermissions = AppConst[prepareRoleText];
    this.tickerBanner = userPermissions.tickerBanner;
    this.checklist = userPermissions.checklist;
    this.dashboard = userPermissions.dashboard;
    this.alerts = userPermissions.alerts;
    this.earning = userPermissions.earning;
    this.welcomePopup = userPermissions.welcomePopup;
    this.industryExposure = userPermissions.industryExposure;
    this.marketScanner = userPermissions.marketScanner;
    this.advancedSearch = userPermissions.advancedSearch;
    this.tabs = userPermissions.tabs;
    this.reportPage = userPermissions.reportPage;
    this.chaikinList = userPermissions.chaikinList;
    this.powerFeed = userPermissions.powerFeed;
    this.majorIndex = userPermissions.majorIndex;
    this.alertTable = userPermissions.alertTable;
    this.alertpanel = userPermissions.alertpanel;
    this.internalTabOnDashboard = userPermissions.internalTabOnDashboard;
    this.issuesAndUpdates = userPermissions.issuesAndUpdates;
  }
}

export class UserRoleMappingModel {
  userRole: string;
  deviceType: string;
  userPermission: UserAccessibilityModel;
  constructor(role: string, device: string) {
    this.userPermission = new UserAccessibilityModel(role, device);
    this.userRole = role;
    this.deviceType = device;
  }
}

export class ChaikinAdvanceIdeaLists {
  listId: number;
  listName: string;
  description: string;
}

export class AdvanceSearch {
  activeListId: number;
}

export class ChaikinAdvanceListData {
  symbol: string;
  rawPgr: number;
  industryName: string;
  change: number;
  last: number;
  isEtf: boolean;
  name: string;
  pgr: number;
  percentage: number;

  rating: number;
  imgSrc: string;
  lastFormattedString: string;
  changeFormattedString: string;
  percentageChangeFormattedString: string;
  percentageChangeColor: string;
  alert: AlertDataModel;
  add: string;
}

export class AdvancedSearchSorting {
  ratingSortingOrder = 'desc';
  symbolSortingOrder = 'desc';
  nameSortingOrder = 'desc';
  changeSortingOrder = 'asc';
  percentageSortingOrder = 'asc';
  lastSortingOrder = 'asc';
  selectedKey = 'rating';
}

export class TabbedStructureData {
  activeTabForHome = true;
  activeTabForMarket = false;
  activeTabForAdvancedSearch = false;
}

export class PowerFeedInsightData {
  postDate: string;
  postContent: string;
  postTitle: string;
  postName: string;
  postModified: string;
  guid: string;
  formattedDate: string;
  postAuthor: string;
}

export class MarketPanelData {
  change: number;
  last: number;
  percentage: number;
  symbol: string;
  company: string;
  correctedPgr: number;
  isEtf: boolean;
  name: string;
  rawPgr: number;
  type: string;
  colorClass: string;
  lastInString: string;
  percentageChangeInString: string;
  arrowDirection: string;
  stripeClass: string;
  summarySentence: string;
}

export class ScreenerFactorModel {
  factorId: number;
  defaultStatus: number;
  name: string;
}

export class ScreenerValueModel {
  valueId: number;
  defaultStatus: number;
  name: string;
}

export class ScreenerDataModel {
  id: number;
  defaultStatus: number;
  name: string;
}

export class ScreenerFirstModel {
  factor: ScreenerDataModel[];
  value: ScreenerDataModel[];
}

export class screenerInitialDataModel {
  [key: string]: ScreenerFirstModel;
}

export class ScreenerResultSymbolModel {
  symbol: string;
  rawPgr: number;
  percentage: number;
  change: number;
  isEtf: boolean;
  last: number;
  signals: string;
  name: string;
  pgr: number;
  technicalRating: string;
  industryListId: string;
  summaryRating: string;
  pgrImage: string;
  pgrName: string;
  changeFormattedClass: string;
  percentageFormattedClass: string;
  lastFormatted: string;
  percentageFormatted: string;
  rating: number;
  ratingTypeColorLabel: string;
}
