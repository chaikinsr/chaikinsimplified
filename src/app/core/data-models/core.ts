export class KeyValuePair {
    key: string;
    value: string;

    constructor(key: string, value: any) {
        this.key = key;
        this.value = value;
    }
}
