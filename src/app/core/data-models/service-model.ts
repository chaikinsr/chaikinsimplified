import { AlertComponentData, ExtendedListDataModel, GetListDataModel, ListSymbolsPriceForSelectedTimeFrameModel } from "./app-model";

export class PGRs {
  VERY_BEARISH = 1;
  BEARISH = 2;
  NEUTRAL = 3;
  BULLISH = 4;
  VERY_BULLISH = 5;
}

export class WordpressIdMapping {
  Industry = ['industry'];
  Power_Gauge_Rating = '[power_gauge_rating]';
  Chaikin_Power_Bar = '[chaikin_power_bar]';
  Historic_Price_Chart = '[historic_price_chart]';
  Money_Flow = '[money_flow]';
  Rel_Strength = '[rel_strength]';
  Financials = '[financials]';
  Earnings = '[earnings]';
  Technicals = '[technicals]';
  Experts = '[experts]';
}

export class AuthorizationModel {
  existingSession: string;
  existingSessionStatus: string;
  loggedInStatus: string;
  sessionId: string;
}

export class LoginDetailModel {
    ListID: string;
    ListType: string;
    Status: string;
    UID: string;
    email: string;
    enableDiscoveryEngineBanner: boolean;
    enableOptionsPlayDisclaimer: boolean;
    enableOptionsPlayVideo: boolean;
    enablePortfoliowiseWelcomeGuide: boolean;
    enableWelcomeGuide: boolean;
    isAmeritradeUser: boolean;
    isMarketAlertsEnabled: boolean;
    isScreenerAlertsEnabled: boolean;
    isUserListAlertsEnabled: boolean;
    showCustomerSurveyPopup: boolean;
    showDefaultPopUpOfMailSupport: boolean;
    showDefaultPopUpOfNote: boolean;
    showPortfoliowiseProfilePopup: boolean;
    enablePowerGaugeAnalyticsWelcomeGuide: boolean;
    stocksFilterValues: any;
    userEmailedAlerts: Array<string>;
    userType: string;
  }

  export class SymbolDataRootObject {
    EPSData: EPSData;
    metaInfo: MetaInfo[];
    pgr: any;
    status: string;
}
// Chart

export interface SectorsPerformanceChart {
  etf_ticker_full_name: string;
  symbol: string;
  data: Data;
  containerId: string;
  period: string;
}

export interface FormattedDate {
  date: string;
  formatted_date: string;
}

export interface Data {
  formatted_dates: FormattedDate[];
  dates: string[];
  points: number[];
}

export class SymbolInfo {
  EPSData: EPSData;
  metaInfo: SymbolMetaInfo;
  pgr: Pgr;
  fundamentalData: FundamentalData;
  status: string;
  constructor() {
      this.EPSData = new EPSData();
      this.metaInfo = new SymbolMetaInfo();
      this.pgr = new Pgr();
      this.fundamentalData = new FundamentalData();
  }
}

export class Financial {
  Value: number;
  LTDebtToEquity?: number;//LT Debt to Equity
  PriceToBook?: number;//Price to Book
  ReturnOnEquity?: number;//Return on Equity
  PriceToSales?: number;//Price to Sales
  FreeCashFlow?: number;//Free Cash Flow
}

export class Earning {
  Value: number;
  EarningsGrowth?: number; // Earnings Growth
  EarningsSurprise?: number; // Earnings Surprise
  EarningsTrend?: number; // Earnings Trend
  ProjectedPERatio?: number; // Projected P/E
  EarningsConsistency?: number; // Earnings Consistency
}

export class Technical {
  Value: number;
  RelStrengthvsMarket?: number;//Rel Strength vs Market
  ChaikinMoneyFlow?: number;//Chaikin Money Flow
  PriceStrength?: number;//Price Strength
  PriceTrendROC?: number;//Price Trend ROC
  VolumeTrend?: number;//Volume Trend
}

export class Expert {
  Value: number;
  EstimateTrend?: number; // Estimate Trend
  ShortInterest?: number; // Short Interest
  InsiderActivity?: number; //  Insider Activity
  AnalystRatingTrend?: number; // Analyst Rating Trend
  IndustryRelStrength?: number; // Industry Rel Strength
}

export class Pgr {
  PGRValue: number; //"PGR Value"
  Financials: Financial;
  Earnings: Earning;
  Technicals: Technical;
  Experts: Expert;
  CorrectedPGRValue?: number; //"Corrected PGR Value"
  relative_strength: string;
  weight_avg_Pgr: string;
  divident_yield: string;
  bucketed_avg_pgr: string;
  momentum: string;
  is_etf_symbol?: boolean;
  assets: string;
  bucketed_weighted_pgr: string;
  management: string;
  technical_rank: string;
  expense_ratio: string;
  weighted_pgr: string;
  avg_daily_volume: string;
  holdings: string;
  power_bar: number[];
  strategy: string;
  beta: string;
  formatedAvgDailyVolume: string;
  constructor() {
      this.Financials = new Financial();
      this.Earnings = new Earning();
      this.Technicals = new Technical();
      this.Experts = new Expert();
  }
}

export class FundamentalData {
  no_of_stocks_in_industry: string;
  EPS_current: string; // EPS_current ('20) // EPS_current (curr FY)
  Address: string;
  next_div_date: string;
  PERatio: string; //  P/E
  AnalystRevisionsPerc: string; //Analyst Revisions (%)
  payout: string;
  is_optionable: boolean;
  liquidity: string;
  volatility: string;
  liquidity_bucket: number;
  PriceBookRatio: string; //  Price/Book
  FiftyTwoWkPerformancePerc: string; // 52 Wk Performance (%)
  FiveYrEPSGrowthPerc: string; // 5 Yr EPS Growth (%)
  growth_rate: string;
  PriceSalesRatio: string; // Price/Sales
  industry_rank: string;
  beta: string;
  FiftyTwoWkLo: string; // 52 Wk Lo
  FiftyTwoWkHi: string; //52 Wk Hi
  DebtEquityRatio: string; // Debt/Equity
  ROE: string;
  RelativetoSAndPPerc: string; //Relative to S&P (%)
  ROI: string;
  CompanyTextBlurb: string; // Company Text Blurb
  volatility_bucket: number;
  MktCapitalization: string; // Mkt Capitalization
  Revenue: string;
  Yield: string;
  EPS_Previous: string; // EPS_Previous ('19) // EPS_Previous (prev FY)
  beta_bucket: number;
  dividend_per_share: string;
  formatedMktCapitalization: string;
  formatedRevenue: string;
  formatedYield: string;
  formatedPERatio: string;
  formatedROE: string;
  formatedPriceSalesRatio: string;
  formatedPriceBookRatio: string;
  formatedEPS_Previous: string;
  formatedEPS_current: string;
  formatedFiveYrEPSGrowthPerc: string;
  formatedFiftyTwoWkLo: string;
  formatedFiftyTwoWkHi: string;
  formatedbeta: string;
  epsCurrentYear: string;
  epsPreviousYear: string;
  one_month_pct_change: string;
  one_week_pct_change: string;
  three_month_pct_change: string;
  six_month_pct_change: string;
  ytd_pct_change: string;
  year_pct_change: string;
  three_year_pct_change: string;
  five_year_pct_change: string;
  pct_change_from_trade_date:string;

}


export class SymbolMetaInfo {
  symbol: string;
  Change: number;
  name: string;
  PGR: number;
  Percentage: number;
  etf_group_name: string;
  Last: number;
  group_rank: number;
  is_etf_symbol: boolean;
  is_selected: boolean;
  raw_PGR: number;
  marketCap: number;
  marketCapText: string;
  marketCapBucketText: string;
  industry_name: string;
  isNoteExist: boolean;
  liquidity: number;
  filter: number;
  signals: string;//"000000000000"
  technicalRating: number;//"TechnicalRating ": 1
  listRating: number;
  industry_ListID: number;//"industry_ListID ": 1377812
  summaryRating: number;//"SummaryRating ": 1
  status: string;
  etf_data: ETFData;
  finalPgr: number;
  isClosedEndFund: boolean;
}

export class ETFData {
  etf_group: string;
  has_constituents: boolean;
  list_id: string;
  list_name: string;
  parent_list_id: string;
}

export class PgrStatusCount {
  veryBullish = 0;
  bullish = 0;
  neutral = 0;
  bearish = 0;
  veryBearish = 0;
  bullishPer = 0;
  neutralPer = 0;
  bearishPer = 0;
  bearishPerStyle = 0;
  bullishPerStyle = 0;
  neutralPerStyle = 0;


  setCount(veryBullish: number, bullish: number, neutral: number, bearish: number, veryBearish: number) {
      this.veryBullish = veryBullish;
      this.bullish = bullish;
      this.neutral = neutral;
      this.bearish = bearish;
      this.veryBearish = veryBearish;
      this.setPercentage();

  }

  setPercentage() {
      // let pgrCount = element.pgrStatusCount;
      let totalCount = this.veryBullish + this.bullish + this.neutral + this.bearish + this.veryBearish;
      let totalBearishCount = this.bearish + this.veryBearish;
      let bearishPer = Math.round(totalCount === 0 ? 0 : (totalBearishCount / totalCount) * 100);
      let totalNeutralCount = this.neutral;
      let neutralPer = Math.round(totalCount === 0 ? 0 : (totalNeutralCount / totalCount) * 100);
      let totalBullishCount = this.veryBullish + this.bullish;
      let bullishPer = Math.round(totalCount === 0 ? 0 : (totalBullishCount / totalCount) * 100);
      // let neutralPerStyle = neutralPer;
      // if (6 - bearishPer > 0) {
      //     neutralPerStyle = neutralPerStyle - (6 - bearishPer);
      // }
      // if (6 - bullishPer > 0) {
      //     neutralPerStyle = neutralPerStyle - (6 - bullishPer);
      // }

      this.bearishPer = bearishPer;
      this.neutralPer = neutralPer;
      this.bullishPer = bullishPer;
      this.bearishPerStyle = (6 - bearishPer > 0) ? 6 : bearishPer;
      this.bullishPerStyle = (6 - bullishPer > 0) ? 6 : bullishPer;
      this.neutralPerStyle = (100 - this.bearishPerStyle - this.bullishPerStyle);
  }
}

// End Chart

export class EPSData {
  symbol: string;
  time_icon: number;
  earnings_report_info: number;
  eps_12mo_pct_chg_category: number;
  next_report_date: string;
  revenue_12mo_pct_chg: string;
  eps_12mo_pct_chg: string;
  last_report_date: string;
  warning_factor: string;
  revenue_12mo_pct_chg_category: number;
  estimate_revision_info: number;
  pre_report_info: number;
  current_revision_estimate: string;
  warning_period: string;
  warning_impact: string;
  last_quarter_sales: string;
  last_quarter_reported_eps: string;
  eps_diff_description: string;
  current_quarter_consensus_estimate: string;
  warning_direction: number;
  eps_diff_category: number;
  warning_type: number;
  last_quarter_consensus_estimate: string;
  time_icon_description: string;
  revision_quarter: string;
  previous_revision_estimate: string;
  earings_report_category: number;
  eps_year_ago: string;
  estimate_revision_category: number;
  current_quarter: string;
  last_quarter: string;
}

export class MetaInfo {
  symbol: string;
  raw_PGR: number;
  marketCap: number;
  industry_name: string;
  isNoteExist: boolean;
  etf_group_name: string;
  liquidity: number;
  Change: number;
  filter: number;
  Last: number;
  signals: string;
  is_etf_symbol: boolean;
  name: string;
  PGR: number;
  TechnicalRating : number;
  listRating: number;
  'Percentage ': number;
  industry_ListID : number;
  SummaryRating : number;
}

export class PriceVolumeContextSummary {
  explanatorySentence: string;
  generalSentence: string;
  status: string;

  priceVolumeRatingLableColorClass?: string;
  priceVolumeRatingBgColorClass?: string;
}

export class FinancialContextSummary {
  explanatorySentence: string;
  generalSentence: string;
  status: string;

  financialRatingLableColorClass?: string;
  financialRatingBgColorClass?: string;
}

export class ExpertOpnionsContextSummary {
  explanatorySentence: string;
  generalSentence: string;
  status: string;

  expertRatingLableColorClass?: string;
  expertRatingBgColorClass?: string;
}

export class PgrData {
  volume_trend: number;
  price_trend_roc: number;
  earnings_trend_rate: number;
  pgr_date: string;
  financial_strength: number;
  corrected_pgr: number;
  earnings_stability: number;
  debt_equity_ratio_rate: number;
  earnings_consistency: number;
  chaikin_volume_accumulation: number;
  relative_strength_vs_industry_rate: number;
  earnings_consistency_rate: number;
  price_volume_activity: number;
  price_to_sales_ratio: number;
  short_interest_rate: number;
  earnings_performance: number;
  debt_equity_ratio: number;
  price_to_sales_ratio_rate: number;
  price_volume_activity_rate: number;
  earning_estimates_revision: number;
  earnings_stability_rate: number;
  analyst_options_rate: number;
  price_trend_roc_rate: number;
  earnings_trend: number;
  price_to_book_value: number;
  insider_activity: number;
  relative_strength_vs_market: number;
  insider_activity_rate: number;
  price_trend_rate: number;
  pgr_value_rate: number;
  analyst_options: number;
  chaikin_volume_accumulation_rate: number;
  pgr_value: number;
  price_trend: number;
  relative_strength_vs_industry: number;
  volume_trend_rate: number;
  return_on_investment_rate: number;
  eps_surprise_rate: number;
  projected_pe_ratio: number;
  isPgrCorrected: boolean;
  investor_sentiment: number;
  financial_strength_rate: number;
  return_on_investment: number;
  short_interest: number;
  price_to_book_value_rate: number;
  business_value_rate: number;
  projected_pe_ratio_rate: number;
  corrected_pgr_rate: number;
  eps_surprise: number;
  relative_strength_vs_market_rate: number;
  investor_sentiment_rate: number;
  earnings_performance_rate: number;
  earning_estimates_revision_rate: number;
  business_value: number;
}

export class EarningsContextSummary {
  explanatorySentence: string;
  generalSentence: string;
  status: string;

  earningRatingLableColorClass?: string;
  earningRatingBgColorClass?: string;
}

export class PgrContextSummary {
  additonalSentence: string;
  neutralSentence: string;
  mainSentence: string;
  status: string;
}

export class ContextSummaryRootObject {
  priceVolumeContextSummary: PriceVolumeContextSummary[];
  financialContextSummary: FinancialContextSummary[];
  expertOpnionsContextSummary: ExpertOpnionsContextSummary[];
  pgrData: PgrData[];
  earningsContextSummary: EarningsContextSummary[];
  pgrContextSummary: PgrContextSummary[];
}

export class Valuation {
  peg: string;
  priceToSale: string;
  priceEarning: string;
  priceToBook: string;
}

export class AssetAndLiability {
  bookValue: string;
  current_ratio: string;
  debtEquity: string;
  marketCap: string;
  revenue: string;
}

export class Dividends {
  yield: string;
  dividendPerShare: string;
  payoutRatio: string;
  growthRate5Year: string;
}

export class Returns {
  returnOneMonth: string;
  returnThreeMonth: string;
  returnOnEquity: string;
  returnOnInvestment: string;
}

export class StockFinancialsDataObject {
  msg: string;
  ticker: string;
  valuation: Valuation;
  assetAndLiability: AssetAndLiability;
  dividends: Dividends;
  returns: Returns;
  status: boolean;
}

export class PriceActivity {
  high52Week: string;
  low52Week: string;
}

export class VolumeActivity {
  avgVolume90Days: string;
  avgVolume20Days: string;
}

export class VolatilityRelativeToMarket {
  volatility: string;
  beta: string;
}

export class PriceChangePtc {
  chg24weekRelativeToSPY: string;
  chg4weekRelativeToSPY: string;
}

export class StockTechnicalDataObject {
  msg: string;
  priceActivity: PriceActivity;
  ticker: string;
  volumeActivity: VolumeActivity;
  volatilityRelativeToMarket: VolatilityRelativeToMarket;
  priceChangePtc: PriceChangePtc;
  status: boolean;
}
  
export class EarningEstimateRevision {
  nextQuarterPercentChange: string;
  currentQuarter30Dago: string;
  nextQuarterCurrent: string;
  currentQuarterPercentChange: string;
  nextQuarter30Dago: string;
  currentQuarterCurrent: string;
}

export class ExpertContextSummary {
  explanatorySentence: string;
  generalSentence: string;
  status: string;
}

export class Recomandations {
  fiveweekago: string;
  meanLastWeek: string;
  meanThisWeek: string;
}

export class Experts {
  expert: number;
  industryRelStrength: number;
  insiderActivity: number;
  shortInterest: number;
  estTrend: number;
  analystRatingTrend: number;
}

export class StockExpertDataObject {
  msg: string;
  earningEstimateRevision: EarningEstimateRevision;
  shortIntrest: number;
  expertOpnionsContextSummary: ExpertContextSummary;
  recomandations: Recomandations;
  experts: Experts;
  status: string;
}

export class SymbolLookUp {
  CompanyName: string;
  raw_PGR: number;
  Symbol: string;
  PGR: number;
  is_etf: boolean;
  etf_data?: EtfData;
  rating?: number;
  imageSrc?: string;
}

export class EtfData {
  list_id: string;
  etf_group: string;
  zacks_asset_type: string;
  parent_list_id: string;
  list_name: string;
  has_constituents: boolean;
  zacks_region: string;
  zacks_etf_etn: string;
}

export class EtfStatData {
  msg: string;
  dividentYield: string;
  ticker: string;
  assets: string;
  management: string;
  expenseRatio: string;
  holdings: string;
  strategy: string;
  avgDailyVolume: string;
  etfGroup: string;
  beta: string;
  status: boolean;
}

export class AnnualEP {
  year: string;
  eps: string;
}

export class EarningAnnouncement {
  estimatedEps: number;
  quarter: number;
  actualEps: string;
}

export class QuarterEP {
  q1: string;
  q2: string;
  q3: string;
  q4: string;
  year: string;
}

export class EarningData {
  earnings: number;
  earningsGrowth: number;
  earningsTrend: number;
  earningsSurprise: number;
  projectedRatio: number;
  earningsConsistency: number;
}

export class AnnualRevenue {
  revenue: string;
  year: string;
}

export class StockEarningsDataObject {
  msg: string;
  annualEPS: AnnualEP[];
  earningAnnouncement: EarningAnnouncement[];
  earningsContextSummary: EarningsContextSummary;
  quarterEPS: QuarterEP[];
  earningData: EarningData;
  annualRevenue: AnnualRevenue[];
  status: string;
}


export class MarketQuickViewBarData {
  listId: number;
  hasChart: boolean;
  symbol: string;
  isExtenable: boolean;
  description: string;
  listName: string;
  listType: string;
}

export class SymbolsCsvPriceData {
  Change: number;
  Last: number;
  percentage: number;
  'Percentage ': number;
  Symbol: string;
  corrected_pgr: number;
  is_etf: boolean;
  raw_pgr: number;
}

export class AlertsData {
  CTI: any[];
  EarningEstimate: EarningEstimateData[];
  EarningSurprise: EarningSurpriseData[];
  PGR: PgrAlertData[];
}

export class EarningEstimateData {
  AlertDate: string;
  Date: string;
  DateTime: string;
  ESTChange: string;
  ESTPercentageChange: string;
  MeanESTCurrentDay: string;
  MeanESTPreviousDay: string;
  QuarterEndDate: string;
  Symbol: string;
  Text: string;
  Value: number;
  is_etf: boolean;
  pgrRating: number;
  rawPgrRating: number;
}

export class EarningSurpriseData {
  ActualEPS: string;
  AlertDate: string;
  CompanyName: string;
  ConsensusEstimate: string;
  EPSChange: string;
  PercentageSurprise: string;
  QuarterEndDate: string;
  ReportDate: string;
  Symbol: string;
  Text: string;
  Value: number;
  is_etf: boolean;
  pgrRating: number;
  rawPgrRating: number;
}

export class PgrAlertData {
  AlertDate: string;
  Symbol: string;
  Text: string;
  Value: number;
  is_etf: boolean;
}

export class NavigationListWithPowbar {
  listId: number;
  hasChart: boolean;
  isExtenable: boolean;
  description: string;
  listName: string;
  listType: string;
  symbolCount: number;
  powerBar: PowerbarData;
  extendedData: GetListDataModel;
  priorityList: boolean;
  constructor() {
    this.powerBar = new PowerbarData();
  }
}

export class PowerbarData {
  red: number;
  green: number;
  yellow: number;
}

export class NavigationListDataWithLabel {
  data: ListWithLabel;
  label: string[];
}

export class ListWithLabel {
  [key: string]: NavigationListWithPowbar;
}

export class GetListSymbolsData {
PowerBar: string
listRating: string
list_id: string
symbols: GetSymbolsDataAccToList[]
}

export class GetSymbolsDataAccToList {
  Change: number
  Last: number
  PGR: number
  Percentage: number
  SummaryRating: number
  TechnicalRating: number
  div_yield: number
  filter: number
  industry_ListID : number
  industry_name: string
  isNoteExist: boolean
  is_etf_symbol: boolean
  list_rating: number
  market_cap: number
  name: string
  raw_PGR: number
  signals: string
  symbol: string

  lastFormattedString: string;
  percentageChangeColor: string;
  percentageChangeFormattedString: string;
  ratingType: string;
  ratingTypeColorLabel: string;
  ratingTypeColorBg: string;
  pgrImageSrc: string;
  pgr: number;
 
}

export class SymbolPriceObject {
  Last: number;
  raw_pgr: number;
  Symbol: string;
  corrected_pgr: number;
  company: string;
  Change: number;
  Percentage : number;
  is_etf: boolean;
}

export class ProductCodesAndPreferenceObject {
  msg: string;
  uid: string;
  productPreference: string;
  expiry: Date;
  productCodes: string[];
  email: string;
  status: string;
  isb2b:string;
}

export class ChecklistObject {
  relativeStrength: string;
  ltTrend: string;
  pgr: string;
  industry: string;
  overboughtOversold: string;
  moneyFlow: string;
  stockStatus: string;
  sentence1: string;
  sentence2: string;
  sentence3a: string;
  sentence3b: string;
  lastPrice: string;
  change: string;
  changePercentage: string;
  symbol: string;
  pgrRating: number;
  rawPgrRating: number;
  sentence4: string;
  strengthCount: number;
  timingCount: number;
}

export class CompanyProfileObject {
Address1: string;
ChineseProfile: string;
CompanyName: string;
EmployeesCount: string;
Profile: string;
Sector: string;
msg: string;
status: string;
formattedEmployeesCount: number;
}

export class CookiesDataObject {
  email : string;
  token : string;
  pgrToken : string;
  role:string;
  pgrRole:string;
  isMobile: boolean;
  beaconStreetJwtToken:string;

}

export class EarningsGlance{
 nextWeekEarningCount: number;
thisWeekEarningCount: number;
todayEarningCount: number;
}

export class CalendarEarningData {
  earningSurprise: EarningSurprise[];
  earningEstimate: EarningEstimate[];
}

export class EarningSurprise {
  symbol: string;
  percentChange: number;
  epsYearAgo: string;
  change: number;
  consensusEstimate: string;
  percentageSurprise: string;
  isEtf: boolean;
  pgrRating: number;
  rawPgrRating: number;
  reportDate: string;
  company: string;
  actualEPS: number;
  quarter: number;
  lastPrice: number;
  reportingTime: number;
}

export class EarningEstimate {
  estimateTrend: string;
  symbol: string;
  percentChange: number;
  epsYearAgo: string;
  change: number;
  company: string;
  epsEstimateCurrent: number;
  isEtf: boolean;
  pgrRating: number;
  rawPgrRating: number;
  reportingTime: number;
  nextReportDate: string;
  quarter: number;
  lastPrice: number;
}

export class DashboardData {
 alertComponentData: AlertComponentData;
 priceMovementComponentData:ListSymbolsPriceForSelectedTimeFrameModel[];
}

export class IndustryExposureData {
  Strength: string;
  rating: string;
  listName: string;
  symbolCount: number;
  symbolList: IndustryExposureSymbolListData[];
}

export class IndustryExposureSymbolListData {
  pgr: number;
  raw_PGR: number;
  symbol: string;
}

export class IndustryExposureDataObject {
  msg: string;
  data: IndustryExposureData[];
  status: string;
}

export class ChaikinAdvanceLists {
  list_id: number;
  list_name: string;
  description: string;
}

export class ChaikinAdvanceListObject {
  msg: string;
  list_data: ChaikinAdvanceLists[];
  status: boolean;
}

export class ChaikinAdvanceListDataSymbols {
  symbol: string;
  raw_PGR: number;
  industry_name: string;
  isNoteExist: boolean;
  Change: number;
  filter: number;
  Last: number;
  is_etf_symbol: boolean;
  signals: string;
  market_cap: number;
  div_yield: number;
  name: string;
  list_rating: number;
  PGR: number;
  TechnicalRating : number;
  Percentage : number;
  industry_ListID : number;
  SummaryRating : number;
}

export class ChaikinAdvanceListDataObject {
  list_id: string;
  PowerBar: string;
  listRating: string;
  symbols: ChaikinAdvanceListDataSymbols[];
}

// export class SubExtendedList {
//   sub_extended_list_name: string;
// }

// export class ExtendedList {
//   subExtendedList: SubExtendedList[];
//   listName: string;
// }

export class MarketScannerData {
  extendedList: ExtendedListDataModel[];
  listName: string;
}

export class MarketScannerDataObject {
  msg: string;
  data: MarketScannerData;
  status: string;
}

export class GetListSymbols {
  list_id : string;
  PowerBar: string;
  listRating:string;
  symbols : GetListSymbolsDataObject[];
}

export class GetListSymbolsDataObject {
  symbol: string;
  raw_PGR: number;
 industry_name: string;
 isNoteExist: boolean;
 Change: number;
 filter: number;
 Last: number;
 is_etf_symbol: boolean;
 signals: string;
 market_cap: number;
 div_yield: number;
 name: string;
 list_rating: number;
 PGR:number;
 TechnicalRating : number;
 Percentage : number;
 industry_ListID : number;
 SummaryRating : number;
}

export class GetPowerFeedInsightDataObject {
    ID: number;
    post_author: string;
    post_date: string;
    post_date_gmt: string;
    post_content: string;
    post_title: string;
    post_excerpt : string;
    post_status: string;
    comment_status: string;
    ping_status: string;
    post_password: string;
    post_name: string;
    to_ping: string;
    pinged: string;
    post_modified: string;
    post_modified_gmt: string;
    post_content_filtered: string;
    post_parent: number;
    guid: string;
    menu_order: number;
    post_type: string;
    post_mime_type: string;
    comment_count: string;
    filter: string;
}

export class GetMarketPanelDataObject {
data: MarketPanelDataObject;
isMarketOpen: boolean;
summarySentance : string;
msg: string;
status: string;
}

export class MarketPanelDataObject {
Change: number;
Last: number;
Percentage: number;
Symbol: string;
company: string;
corrected_pgr: number;
is_etf: boolean;
name: string;
raw_pgr: number;
type: string;
}

export class ScrennerResultSymbolObject {
  symbol: string;
  raw_PGR: number;
  Percentage: number;
  Change: number;
  is_etf: boolean;
  Last: number;
  signals: string;
  eps_data: ScreenerEpsDataObject;
  name: string;
  PGR: number;
  'TechnicalRating ': string;
  'industry_ListID ': string;
  'SummaryRating ': string;
}

export class ScreenerEpsDataObject {
  time_icon: string;
  earnings_report_info: string;
  eps_12mo_pct_chg_category: string;
  next_report_date: string;
  revenue_12mo_pct_chg: string;
  eps_12mo_pct_chg: string;
  last_report_date: string;
  warning_factor: string;
  revenue_12mo_pct_chg_category: string;
  estimate_revision_info: string;
  pre_report_info: string;
  current_revision_estimate: string;
  warning_period: string;
  warning_impact: string;
  last_quarter_sales: string;
  last_quarter_reported_eps: string;
  eps_diff_description: string;
  current_quarter_consensus_estimate: string;
  warning_direction: string;
  eps_diff_category: string;
  warning_type: string;
  last_quarter_consensus_estimate: string;
  time_icon_description: string;
  revision_quarter: string;
  previous_revision_estimate: string;
  earings_report_category: string;
  eps_year_ago: string;
  estimate_revision_category: string;
  current_quarter: string;
  last_quarter: string;
}

export class ScreenerResultCountObject {
  'Price Levels': number;
  'Chaikin Money Flow': number;
  'Div Yield': number;
  'Price Return': number;
  'Max Price': number;
  'Stock has triggered': number;
  'Rel Return': number;
  'Rating Components & Factors3': number;
  'Min Price': number;
  'Beta': number;
  'Chaikin Rel Strength': number;
  'Price Levels 2': number;
  'Rating Components & Factors2': number;
  'Price Return2': number;
  'Rating Components & Factors1': number;
  'Relative Volume': number;
  'Earnings Report Date': number;
  'Starting Universe': number;
  'Power Gauge Rating': number;
  'Market Cap': number;
  'defaultFilter'?: number;
}


export class ScreenerResultDataObject {
  symbols: ScrennerResultSymbolObject[];
  powerBar: number[];
  hasMoreStock?: boolean;
  lastUpdateTime: string;
  results: ScreenerResultCountObject[];
}


export class ScreenCriteria {
    factorId: any;
    valueId: any;
    header: string;
    value2Id?: number;
}

export class InvestorListData {
  guru: string;
  associatedPubCodes: string[];
  screenCriteria: ScreenCriteria[];
  limit: number;
  description: string;
  listName: string;
  updationMode: string;
  isActive: boolean;
  symbols: string[];
  listType: string;
  primarySort: string;
  primarySortOrder: string;
  secondarySort: string;
  secondarySortOrder: string;
}


export class InvestorIdeaListDataObject {
  data: InvestorListData[];
  message: string;
  status: boolean;
}



