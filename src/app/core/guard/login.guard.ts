import { Injectable } from '@angular/core';
import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  ActivatedRoute,
} from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

import { AppConfig } from '../app-config/app-config';
import {
  CookiesDataObject,
  LoginDetailModel,
} from '../data-models/service-model';
import { AuthService } from '../http-services/auth.service';
import { CookieService } from 'ngx-cookie-service';

import {
  LocalStorage,
  RedirectParameter,
  SharedUtil,
} from '../services/utilily/utility';
import { APIService } from '../http-services/api.service';
import { DataService } from '../services/data.service';
import { AppDataService } from '../services/app-data.service';
import {
  ProductAccessModel,
  UserRoleMappingModel,
} from '../data-models/app-model';

@Injectable({
  providedIn: 'root',
})
export class LoginGuard implements CanActivate {
  private checked: boolean;
  env: string = environment.env;
  paramsObject: any;
  routingParamObjectAvailable = false;
  showPopOnCAMobile = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private apiService: APIService,
    private cookieService: CookieService,
    private dataService: DataService,
    private route: ActivatedRoute,
    public appDataService: AppDataService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return new Observable<boolean>((observer) => {
      // setTimeout(() => {
      this.onMobileLogin();
      this.setCookiesData();
      this.getParamsFromUrl();
      const token = SharedUtil.getToken(this.cookieService);
      let email = '';
      if (this.env != 'local') {
        email = localStorage.getItem('email');
        if (!email) {
          email = this.cookieService.get('app-email');
        }
      } else {
        email = environment.email;
        localStorage.setItem('email', email);
      }

      if (email) {
        if (this.env === 'local') {
          this.authService.getLoggedInstate(email).subscribe((resp) => {
            if (resp) {
              observer.next(true);
              observer.complete();
            } else {
              this.authService
                .getAuth(environment.email, environment.password)
                .subscribe((authResp) => {
                  if (authResp) {
                    observer.next(true);
                    observer.complete();
                  } else {
                    observer.next(false);
                    observer.complete();
                  }
                });
            }
          });
        } else if (
          this.cookieService.get('pgr-role') === 'Power Gauge Report'
        ) {
          let acquireSessionForcibly = true;
          // Fix patch plese look after build how would we get token from cookies.
         // let token = this.cookieService.get('app-token');
          this.appDataService.cookiesData.token = token
           
          // this.apiService.getToken()
          
          this.authCall(acquireSessionForcibly, 'Power Pulse Report', token, email)
          .subscribe(flag => {
            if (flag) {
              observer.next(true);
              observer.complete();
            } else {
              this.logOut();
              observer.next(false);
              observer.complete();
            }
          });
   
        } else if (this.cookieService.get('role') === 'Power Pulse') {
          let acquireSessionForcibly = true;
          // Fix patch plese look after build how would we get token from cookies.
          // this.apiService.getToken()
          this.authCall(acquireSessionForcibly, 'Power Pulse', token, email)
          .subscribe(flag => {
            if (flag) {
              observer.next(true);
              observer.complete();
            } else {
              this.logOut();
              observer.next(false);
              observer.complete();
            }
          });
        } else if (
          this.cookieService.get('role') === 'Power Gauge Analytics' ||
          (this.cookieService.get('role') === 'Chaikin Analytics' &&
            this.showPopOnCAMobile) ||
          this.cookieService.get('role') === 'Chaikin Power Tactics' ||
          this.cookieService.get('role') === 'Chaikin Power Trader'
        ) {
          let acquireSessionForcibly = true;
          let productName = this.cookieService.get('role');
          // Fix patch plese look after build how would we get token from cookies.
          // this.apiService.getToken()
          this.authCall(acquireSessionForcibly, productName, token, email)
          .subscribe(flag => {
            if (flag) {
              observer.next(true);
              observer.complete();
            } else {
              this.logOut();
              observer.next(false);
              observer.complete();
            }
          });
        } else {
          this.route.queryParams.subscribe((params) => {
            if (Object.keys(params).length > 0) {
              this.routingParamObjectAvailable = true;
              this.paramsObject = JSON.parse('{' + params.data + '}');
            }
          });
          this.logOut();
          observer.next(false);
          observer.complete();
        }
      } else {
        this.logOut();
      }
    });
  }

  setCookiesData(): any {
    let email: string;
    let token: string;
    if (this.env != 'local') {
      token = SharedUtil.getToken(this.cookieService);
      email = localStorage.getItem('email');
      if (!email) {
        email = this.cookieService.get('app-email');
      }
    } else if (this.env == 'local') {
      token = environment.token;
      email = environment.email;
      localStorage.setItem('email', email);
    }
    let cookiesData = new CookiesDataObject();
    cookiesData.email = email;
    cookiesData.pgrRole = this.cookieService.get('pgr-role');
    cookiesData.pgrToken = this.cookieService.get('app-token');
    cookiesData.role = this.cookieService.get('role');
    cookiesData.token = token;
    cookiesData.beaconStreetJwtToken = SharedUtil.getBeaconStreetJwtToken(
      this.cookieService
    );

    cookiesData.isMobile = this.showPopOnCAMobile;
    this.appDataService.cookiesData = cookiesData;
  }

  getParamsFromUrl(): any {
    // this.route.queryParams.subscribe(params => {
    //   if (Object.keys(params).length > 0){
    //     this.paramsObject = JSON.parse(("{"+params.data+"}"));
    //     if ( this.paramsObject.page === 'alert'  ||  this.paramsObject.page === 'post'){
    //     this.routingParamObjectAvailable = true;
    //     this.dataService.recentlyViewedData.setSymbol((this.paramsObject.symbol).toUpperCase());
    //     LocalStorage.setItem('mainSymbol', this.paramsObject.symbol);
    //     const navigationExtras =   RedirectParameter.getNavigationExtrasDataForResearch((this.paramsObject.symbol).toUpperCase(),
    //     this.paramsObject.page, null, null);
    //     this.router.navigate(['/'], navigationExtras);
    //     }
    //   }
    //       });
    //if (!this.paramsObject) {
    let decodedUrl = decodeURI(window.location.href).split('=');
    if (decodedUrl[1]) {
      let object = decodedUrl[1];
      let objectParse = JSON.parse(JSON.stringify(object));
      this.paramsObject = eval('({' + objectParse + '})');
      if (
        this.paramsObject.page === 'alert' ||
        this.paramsObject.page === 'post'
      ) {
        LocalStorage.setItem('mainSymbol', this.paramsObject.symbol);
        this.routingParamObjectAvailable = true;
      }
    }
    //  }
  }

  logOut(): void {
    if (this.env != 'local') {
      // this.authService.killsessions(this.appDataService.cookiesData.email).subscribe((resp) => {
      //  if (resp["status"] === 'success') {
      //      this.appDataService.removeAllData();
      if (this.routingParamObjectAvailable) {
        window.location.replace(
          window.location.protocol +
            '//' +
            window.location.host +
            '/login/#/pulse-login'
        );
      } else {
       
        window.location.replace(
          window.location.protocol + '//' + window.location.host + '/login/'
        );
      }
    }
  }

  onMobileLogin(): void {
    let _isNotMobile = (function () {
      let check = false;
      (function (a) {
        if (
          /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(
            a
          ) ||
          /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
            a.substr(0, 4)
          )
        )
          check = true;
      })(navigator.userAgent || navigator.vendor);
      return !check;
    })();

    if (!_isNotMobile) {
      this.showPopOnCAMobile = true;
    } else {
      //  this.cookieService.set('role', 'Chaikin Analytics', { path: '/', domain: '.chaikinanalytics.com' });
      // window.location.replace(window.location.protocol + "//" + window.location.host + "/");
      this.showPopOnCAMobile = false;
    }
  }

  authCall(
    acquireSessionForcibly: any,
    product: string,
    token: string,
    email
  ): Observable<boolean> {
    return new Observable<boolean>((observer) => {
      this.apiService
        .getAuthorizationCall(acquireSessionForcibly, product, token)
        .subscribe((authResp) => {
          if (authResp.loggedInStatus === 'true') {
            let sessionId = authResp.sessionId;
            window.localStorage.setItem('sessionId', sessionId);
            this.authService.getLoggedInstate(email).subscribe((resp) => {
              if (resp) {
                observer.next(true);
                observer.complete();
              } else {
                observer.next(false);
                observer.complete();
              }
            });
            
          } else {
              observer.next(false);
              observer.complete();
          }
        });
    });
  }
}
