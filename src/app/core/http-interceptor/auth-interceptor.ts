import {
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { throwError } from 'rxjs';
import { catchError, takeUntil } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { AppConfig } from '../app-config/app-config';
import { AuthService } from '../http-services/auth.service';
import { HttpCancelService } from '../http-services/http-cancel.service';
import { AppDataService } from '../services/app-data.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  env: string = environment.env;
  constructor(
    private httpCancelService: HttpCancelService,
    private router: Router,
    public appDataService: AppDataService,
    public auth: AuthService,
    private cookieService: CookieService
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    return next.handle(req).pipe(
      takeUntil(this.httpCancelService.onCancelPendingRequests()),
      catchError((err) => {
        if (err.status === 403) {
          console.log('Session expired 403');
          this.logOut();
        }
        return throwError(err);
      })
    );
  }

  logOut() {
    const pathName = window.location.pathname;
    if (this.env != 'local') {
      
      this.cookieService.deleteAll('/', '.chaikinanalytics.com');
      this.appDataService.removeAllData();
      window.location.replace(
        window.location.protocol + '//' + window.location.host + '/login/'
      );
     
    }
  }
}
