import { HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { KeyValuePair } from '../data-models/core';

export class APIServiceBase {
  constructor() {}
  prepareHeaderForWidget(): HttpHeaders {
    let headers;
    headers = new HttpHeaders({
      'Content-Type': 'application/json',
      environment: 'desktop',
      version: '1.3.4',
      token: 'FXC43177DE04OQSX3082F97BAS77489Z'
    });
    return headers
  }
  prepareHeader(): HttpHeaders {
    let headers;
    const sessionId = window.localStorage.getItem("sessionId");
    const email = window.localStorage.getItem("email");
    if (sessionId && email) {
      headers = new HttpHeaders({
        'Content-Type': 'application/json',
        environment: 'desktop',
        version: '1.3.4',
        jsessionId: sessionId,
        uuid: email
      });
    } else {
      headers = new HttpHeaders({
        'Content-Type': 'application/json',
        environment: 'desktop',
        version: '1.3.4',
      });
    }
    
    return headers;
  }

  prepareHeaderPost(): HttpHeaders {
    let headers;
    const sessionId = window.localStorage.getItem("sessionId");
    const email = window.localStorage.getItem("email");
    if (sessionId && email) {
      headers = new HttpHeaders({
        'Content-Type': 'application/json',
        Accept: 'application/json',
        jsessionId: sessionId,
        uuid: email
      });
    } else {
      headers = new HttpHeaders({
        'Content-Type': 'application/json',
        Accept: 'application/json',
      });
    }
   
    return headers;
  }

  prepareHttpOptionsPost(
    rptProgress: boolean = null,
    withCreds: boolean = null
  ): any {
    const hdrs: HttpHeaders = this.prepareHeaderPost();
    return {
      headers: hdrs,
      params: new HttpParams(),
      reportProgress: rptProgress,
      responseType: 'json',
      withCredentials: withCreds
    };
  }

  prepareParams(data: KeyValuePair[]): HttpParams {
    const obj = {};

    data.forEach((dataItem) => {
      obj[`${dataItem.key}`] = dataItem.value;
    });

    const params = new HttpParams({ fromObject: obj });
    return params;
  }

  prepareHttpOptions(
    rptProgress: boolean = null,
    withCreds: boolean = null
  ): any {
    const hdrs: HttpHeaders = this.prepareHeader();
    return {
      headers: hdrs,
      params: new HttpParams(),
      reportProgress: rptProgress,
      responseType: 'json',
      withCredentials: withCreds
    };
  }

  prepareHttpOptionsForwidget(
    rptProgress: boolean = null,
    withCreds: boolean = null
  ): any {
    const hdrs: HttpHeaders = this.prepareHeaderForWidget();
    return {
      headers: hdrs,
      params: new HttpParams(),
      reportProgress: rptProgress,
      responseType: 'json',
      withCredentials: withCreds
    };
  }

  get APIUrl(): string {
    const url = environment.APIUrl;
    return url;
  }

  get getAuthorizationJWT(): string {
    const url = this.APIUrl + '/authenticate/getJWTAuthorization';
    return url;
  }

  get getAuthorizationUrl(): string {
    const url = this.APIUrl + '/authenticate/getAuthorization';
    return url;
  }

  get getLoginUrl(): string {
    const url = this.APIUrl + '/user/login';
    return url;
  }

  get getSymbolDataUrl(): string {
    const url = this.APIUrl + '/portfolio/getSymbolData';
    return url;
  }

  get getSymbolDataForWidgetUrl(): string {
    const url =
      'https://app.chaikinanalytics.com/CPTRestSecure/app/portfolio/getSymbolData';
    // const url =  'https://dev.chaikinanalytics.com/CPTRestSecure/app/portfolio/getSymbolData';
    return url;
  }

  get getContextSummaryDataUrl(): string {
    const url =
      this.APIUrl + '/researchReportServices/getPgrDataAndContextSummary';
    return url;
  }

  get getContextSummaryDataForWidgetUrl(): string {
    const url =
      'https://app.chaikinanalytics.com/CPTRestSecure/app/researchReportServices/getPgrDataAndContextSummary';
    // const url = 'https://dev.chaikinanalytics.com/CPTRestSecure/app/researchReportServices/getPgrDataAndContextSummary';
    return url;
  }

  get getStockFinancialsDataUrl(): string {
    const url = this.APIUrl + '/stocks/getStockFinancialsData';
    return url;
  }

  get getStockTechnicalDataUrl(): string {
    const url = this.APIUrl + '/stocks/getStockTechnicalData';
    return url;
  }

  get getStockExpertDataUrl(): string {
    const url = this.APIUrl + '/stocks/getStockExpertData';
    return url;
  }

  get symbolLookupURL(): string {
    const url = this.APIUrl + '/stocks/symbol-lookupV1';
    return url;
  }

  get getEtfTickerDataUrl(): string {
    const url = this.APIUrl + '/stocks/getEtfTickerData';
    return url;
  }

  get getStockEarningsDataUrl(): string {
    const url = this.APIUrl + '/stocks/getStockEarningsData';
    return url;
  }

  // Chart
  get getChartDataURL(): string {
    const url = this.APIUrl + '/chart/getChart';
    return url;
  }
  get getDividendDataForTickerURL(): string {
    const url = this.APIUrl + '/portfolioWise/getDividendDataForTicker';
    return url;
  }
  get getBenchMarkRelativeStrengthChartURL(): string {
    const url = this.APIUrl + '/chart/getBenchMarkRelativeStrengthChart';
    return url;
  }
  get getSectorsPerformanceChartURL(): string {
    const url = this.APIUrl + '/etfSectors/getSectorsPerformanceChart';
    return url;
  }
  //End Chart

  get getInsightContentDataURL(): string {
    const url = '/insights/';
    return url;
  }

  get getProductCodesAndPreferenceDataURL(): string {
    const url = this.APIUrl + '/user/getProductCodesAndPreference';
    return url;
  }

  get getUpdateProductPreferanceDataURL(): string {
    const url = this.APIUrl + '/user/updateProductPreferance';
    return url;
  }

  get getMarketQuickViewBarDataUrl(): string {
    const url = this.APIUrl + '/cpt/portfolio/getMarketQuickViewBarData';
    return url;
  }

  get getSymbolsCSVPriceUrl(): string {
    const url = this.APIUrl + '/price/getSymbolsCSVPrice';
    return url;
  }

  get getListSymbolsPriceForSelectedTimeFrameUrl(): string {
    const url = this.APIUrl + '/price/getListSymbolsPriceForSelectedTimeFrame';
    return url;
  }

  get getAllAlertsUrl(): string {
    const url = this.APIUrl + '/alerts/getAllAlerts';
    return url;
  }

  get getSignalDataForListUrl(): string {
    const url = this.APIUrl + '/signals/getSignalDataForList';
    return url;
  }

  get getDeleteSymbolFromListUrl(): string {
    const url = this.APIUrl + '/portfolio/deleteSymbolFromList';
    return url;
  }

  get getUserWatchlistDataUrl(): string {
    const url = this.APIUrl + '/cpt/portfolio/getUserWatchlistData';
    return url;
  }
  get getPowerPulseEnabledAlertListUrl(): string {
    const url =
      this.APIUrl + '/dailyEmailedAlerts/getPowerPulseEnabledAlertList';
    return url;
  }

  get deleteForEnabledPowerPulseAlertListUrl(): string {
    const url =
      this.APIUrl + '/dailyEmailedAlerts/deleteForEnabledPowerPulseAlertList';
    return url;
  }

  get addListForEnablePowerPulseAlertUrl(): string {
    const url =
      this.APIUrl + '/dailyEmailedAlerts/addListForEnablePowerPulseAlert';
    return url;
  }

  get getListSymbolsUrl(): string {
    const url = this.APIUrl + '/portfolio/getListSymbols';
    return url;
  }

  get getcheckListNameExistenceUrl(): string {
    const url = this.APIUrl + '/portfolio/checkListNameExistence';
    return url;
  }

  get getAddListToPortfolioUrl(): string {
    const url = this.APIUrl + '/portfolio/addListToPortfolio';
    return url;
  }

  get getUploadListDataToRedisUrl(): string {
    const url = this.APIUrl + '/portfolio/uploadListDataToRedis';
    return url;
  }
  get getDeleteListDataToRediseUrl(): string {
    const url = this.APIUrl + '/portfolio/deleteListDataToRedis';
    return url;
  }

  get getAddStockIntoListUrl(): string {
    const url = this.APIUrl + '/portfolio/addStockIntoList';
    return url;
  }
  get getUploadFileDataUrl(): string {
    const url = this.APIUrl + '/portfolio/uploadFileData';
    return url;
  }

  get getFormattedFileDataUrl(): string {
    const url = this.APIUrl + '/portfolio/getFormattedFileData';
    return url;
  }

  get validateAndUploadTickersForListUrl(): string {
    const url = this.APIUrl + '/portfolio/validateAndUploadTickersForList';
    return url;
  }

  get clearRawListDataUrl(): string {
    const url = this.APIUrl + '/portfolio/clearRawListData';
    return url;
  }

  get getEditSymbolListUrl(): string {
    const url = this.APIUrl + '/symbolsList/editList';
    return url;
  }

  get getDeleteStockListUrl(): string {
    const url = this.APIUrl + '/portfolio/deleteStockList';
    return url;
  }

  get checkSandboxPortfolioListNameExistenceURL(): string {
    const url =
      this.APIUrl + '/portfolio/checkSandboxPortfolioListNameExistence';
    return url;
  }

  get getChecklistStocksUrl(): string {
    const url = this.APIUrl + '/portfolio/getChecklistStocks';
    return url;
  }

  get getCompanyInformationUrl(): string {
    const url = this.APIUrl + '/stocks/getCompanyInformation';
    return url;
  }

  get getKillSessionURL() {
    const url = this.APIUrl + '/session/killsessions';
    return url;
  }
  get getEarningsGlanceURL() {
    const url = this.APIUrl + '/portfolio/getEarningsGlance';
    return url;
  }

  get getEarningCalenderDataUrl(): string {
    const url = this.APIUrl + '/alerts/getEarningCalenderData';
    return url;
  }

  get getPulseChaikinListUrl(): string {
    const url = this.APIUrl + '/portfolio/getPulseChaikinList';
    return url;
  }
  get disablePowerGaugeAnalyticsWelcomeGuideUrl(): string {
    const url = this.APIUrl + '/welcome/disablePowerGaugeAnalyticsWelcomeGuide';
    return url;
  }

  get getIndustryExposureUrl(): string {
    const url = this.APIUrl + '/powerGauge/getIndustryExposure';
    return url;
  }

  get getMarketScannerChartUrl(): string {
    const url = this.APIUrl + '/powerGauge/getMarketScannerData';
    return url;
  }

  get getChaikinAdvanceListUrl(): string {
    const url = this.APIUrl + '/portfolio/getAllDashboardLists';
    return url;
  }

  get getChaikinAdvanceListDataUrl(): string {
    const url = this.APIUrl + '/portfolio/getDashboardListData';
    return url;
  }

  get getPowerFeedInsightUrl(): string {
    const url = '/insights/';
    return url;
  }

  get getMarketPanelDataUrl(): string {
    const url = this.APIUrl + '/powerGauge/getPowerFeedMarketview';
    return url;
  }

  get getScreenerInitialDataUrl(): string {
    const url = this.APIUrl + '/screener/getInitialData';
    return url;
  }

  get getEtfScreenerInitialDataUrl(): string {
    const url = this.APIUrl + '/screener/getEtfInitialData';
    return url;
  }

  get getScreenerResultsUrl(): string {
    const url = this.APIUrl + '/screener/getResults';
    return url;
  }

  get getInvestorListsDetailsUrl(): string {
    const url = this.APIUrl + '/investorIdeas/getListsDetails';
    return url;
  }

  get getInvestorAddNewListDetailsUrl(): string {
    const url = this.APIUrl + '/investorIdeas/addNewListDetails';
    return url;
  }

  get getInvestorDeleteListDetailsUrl(): string {
    const url = this.APIUrl + '/investorIdeas/deleteListDetails';
    return url;
  }

  get getInvestorUpdateListDetailsUrl(): string {
    const url = this.APIUrl + '/investorIdeas/updateListDetails';
    return url;
  }

  get getIssuesAndUpdatesUrl(): string {
    return `${this.APIUrl}/wordPress/getPublicationData`;
  }
}
