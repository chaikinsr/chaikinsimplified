import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { KeyValuePair } from '../data-models/core';
import { LoginDetailModel } from '../data-models/service-model';
import { LocalStorage } from '../services/utilily/utility';
import { APIServiceBase } from './api.service.base';

@Injectable({ providedIn: 'root' })
export class APIService extends APIServiceBase {
  userDetail: LoginDetailModel;

  constructor(private http: HttpClient, private cookieService: CookieService) {
    super();
    if (!this.userDetail) {
      this.userDetail = this.getUserDetail();
    }
  }

  uid: any;
  getToken(): any {
    let hostname = window.location.hostname.split(".")[0];
    if (hostname+'.chaikinanalytics.com' === window.location.hostname) {
      this.uid = this.cookieService.get(hostname+'-uid');
      return this.cookieService.get(hostname+'-token');
    }
    // if (
    //   'dev-login.chaikinanalytics.com' === window.location.hostname ||
    //   'dev.chaikinanalytics.com' === window.location.hostname
    // ) {
    //   this.uid = this.cookieService.get('dev-uid');
    //   return this.cookieService.get('dev-token');
    // } else if ('localhost' === window.location.hostname) {
    //   this.uid = this.cookieService.get('local-uid');
    //   return this.cookieService.get('local-token');
    // } else if ('qa.chaikinanalytics.com' === window.location.hostname) {
    //   this.uid = this.cookieService.get('qa-uid');
    //   return this.cookieService.get('qa-token');
    // } else if ('staging.chaikinanalytics.com' === window.location.hostname) {
    //   this.uid = this.cookieService.get('staging-uid');
    //   return this.cookieService.get('staging-token');
    // } else if ('app.chaikinanalytics.com' === window.location.hostname) {
    //   this.uid = this.cookieService.get('app-uid');
    //   return this.cookieService.get('app-token');
    // }
  }
  getUserDetail(): LoginDetailModel {
    const userDetail: LoginDetailModel = LocalStorage.getItem('userDetail');
    return userDetail;
  }

  getAuthorization(
    acquireSessionForcibly: boolean,
    email: string,
    password: string
  ): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair(
        'acquireSessionForcibly',
        acquireSessionForcibly ? 'Yes' : 'No'
      ),
      new KeyValuePair('email', email),
      new KeyValuePair('password', password),
      new KeyValuePair('productName', 'Chaikin Analytics'),
    ]);
    return this.http.get(this.getAuthorizationUrl, options as any);
  }

  getLogin(email: string): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([new KeyValuePair('email', email)]);
    return this.http.get(this.getLoginUrl, options as any);
  }

  getSymbolData(symbol: string, componentParams: string, uid): Observable<any> {
    if (!this.userDetail) {
      this.userDetail = this.getUserDetail();
    }
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('uid', uid),
      new KeyValuePair('symbol', symbol),
      new KeyValuePair('components', componentParams),
    ]);
    return this.http.get(this.getSymbolDataUrl, options as any);
  }

  getSymbolDataForWidget(
    symbol: string,
    componentParams: string,
    token
  ): Observable<any> {
    if (!this.userDetail) {
      this.userDetail = this.getUserDetail();
    }
    const options = this.prepareHttpOptionsForwidget(true, true);
    options.params = this.prepareParams([
      // new KeyValuePair('token', token),
      new KeyValuePair('symbol', symbol),
      new KeyValuePair('components', componentParams),
    ]);
    return this.http.get(this.getSymbolDataForWidgetUrl, options as any);
  }

  getContextSummaryDataForWidget(
    symbol: string,
    industry: string,
    token: string
  ): Observable<any> {
    const options = this.prepareHttpOptionsForwidget(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('symbol', symbol),
      new KeyValuePair('industry', industry),
      // new KeyValuePair('token', token),
    ]);
    return this.http.get(
      this.getContextSummaryDataForWidgetUrl,
      options as any
    );
  }

  getContextSummaryData(symbol: string, industry: string): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('symbol', symbol),
      new KeyValuePair('industry', industry),
    ]);
    return this.http.get(this.getContextSummaryDataUrl, options as any);
  }

  getStockFinancialsData(symbol: string): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([new KeyValuePair('symbol', symbol)]);
    return this.http.get(this.getStockFinancialsDataUrl, options as any);
  }

  getStockTechnicalData(symbol: string): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([new KeyValuePair('symbol', symbol)]);
    return this.http.get(this.getStockTechnicalDataUrl, options as any);
  }

  getStockExpertData(symbol: string): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([new KeyValuePair('symbol', symbol)]);
    return this.http.get(this.getStockExpertDataUrl, options as any);
  }

  symbolLookup(question: string, searchColumn: string): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('q', question),
      new KeyValuePair('searchColumn', searchColumn),
    ]);

    return this.http.get(this.symbolLookupURL, options as any);
  }

  getEtfTickerData(symbol: string): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([new KeyValuePair('symbol', symbol)]);
    return this.http.get(this.getEtfTickerDataUrl, options as any);
  }

  getStockEarningsData(symbol: string): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([new KeyValuePair('symbol', symbol)]);
    return this.http.get(this.getStockEarningsDataUrl, options as any);
  }
  // Chart
  getChartData(symbol: string, interval: string, uid): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('symbol', symbol),
      new KeyValuePair('uid', uid),
      new KeyValuePair('interval', interval),
      new KeyValuePair('type', 1),
      new KeyValuePair('numBars', 320),
      new KeyValuePair('version', 1.1),
    ]);
    return this.http.get(this.getChartDataURL, options as any);
  }

  getBenchMarkRelativeStrengthChart(
    symbol: string,
    benchmark: string,
    interval: string
  ): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('symbol', symbol),
      new KeyValuePair('benchmark', benchmark),
      new KeyValuePair('interval', interval),
      new KeyValuePair('numBars', 320),
    ]);
    return this.http.get(
      this.getBenchMarkRelativeStrengthChartURL,
      options as any
    );
  }

  getSectorsPerformanceChart(symbolCSV: string, period: string, uid) {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('symbolCSV', symbolCSV),
      new KeyValuePair('period', period),
      new KeyValuePair('uid', uid),
    ]);

    return this.http.get(this.getSectorsPerformanceChartURL, options as any);
  }

  //End Chart

  getInsightContent(id: number) {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('json', 'secursive.get_product_updates'),
      new KeyValuePair('dev', 1),
      new KeyValuePair('id', id),
    ]);
    return this.http.get(this.getInsightContentDataURL, options as any);
  }

  getProductCodesAndPreference(token) {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([new KeyValuePair('jwtToken', token)]);
    return this.http.get(
      this.getProductCodesAndPreferenceDataURL,
      options as any
    );
  }

  getAuthorizationCall(
    acquireSessionForcibly: boolean,
    productName: string,
    token: string
  ): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair(
        'acquireSessionForcibly',
        acquireSessionForcibly ? 'Yes' : 'No'
      ),
      new KeyValuePair('productName', productName),
      new KeyValuePair('jwtToken', token),
    ]);
    return this.http.get(this.getAuthorizationJWT, options as any);
  }

  updateProductPreferance(value: string, uid): Observable<any> {
    if (!this.userDetail) {
      this.userDetail = this.getUserDetail();
    }
    this.userDetail = this.getUserDetail();
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('uid', uid),
      new KeyValuePair('preference', value),
    ]);
    return this.http.get(
      this.getUpdateProductPreferanceDataURL,
      options as any
    );
  }

  getMarketQuickViewBarData(symbolCsv: string): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('SymbolCsv', symbolCsv),
    ]);
    return this.http.get(this.getMarketQuickViewBarDataUrl, options as any);
  }

  getSymbolsCSVPrice(symbolCsv: string, uid): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('uid', uid),
      new KeyValuePair('tickerCSV', symbolCsv),
    ]);
    return this.http.get(this.getSymbolsCSVPriceUrl, options as any);
  }

  getListSymbolsPriceForSelectedTimeFrame(
    timeFrame,
    listId,
    uid
  ): Observable<any> {
    const v = Math.random();
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('listId', listId),
      new KeyValuePair('timeFrame', timeFrame),
      new KeyValuePair('uid', uid),
    ]);
    return this.http.get(
      this.getListSymbolsPriceForSelectedTimeFrameUrl,
      options as any
    );
  }

  getAllAlerts(listId: string, uid: string): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    // console.log(this.userDetail.ListID);
    // console.log(typeof(this.userDetail.ListID));
    // console.log(+this.userDetail.ListID);
    // if (!listId) {
    //   listId = +this.userDetail.ListID;
    // }
    // console.log("getAllAlerts ", listId);
    // console.log("getAllAlerts ", this.userDetail);
    options.params = this.prepareParams([
      new KeyValuePair('uid', uid),
      new KeyValuePair('period', 3),
      new KeyValuePair('list_id', listId),
    ]);
    return this.http.get(this.getAllAlertsUrl, options as any);
  }

  getSignalDataForList(listId: any, uid: any): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    // if (!listId) {
    //   listId = +this.userDetail.ListID;
    // }
    options.params = this.prepareParams([
      new KeyValuePair('uid', uid),
      new KeyValuePair('period', 30),
      new KeyValuePair('list_ID', listId),
    ]);
    return this.http.get(this.getSignalDataForListUrl, options as any);
  }

  deleteSymbolFromList(csvSymbols: string, uid): Observable<any> {
    let listId = LocalStorage.getItem('listId');
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('uid', uid),
      new KeyValuePair('symbol', csvSymbols),
      new KeyValuePair('listId', listId),
    ]);
    return this.http.get(this.getDeleteSymbolFromListUrl, options as any);
  }

  getUserWatchlistData(uid): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([new KeyValuePair('uid', uid)]);
    return this.http.get(this.getUserWatchlistDataUrl, options as any);
  }

  getPowerPulseEnabledAlertList(uid): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([new KeyValuePair('uid', uid)]);
    return this.http.get(this.getPowerPulseEnabledAlertListUrl, options as any);
  }

  deleteForEnabledPowerPulseAlertList(listId, uid): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('uid', uid),
      new KeyValuePair('listId', listId),
    ]);
    return this.http.delete(
      this.deleteForEnabledPowerPulseAlertListUrl,
      options as any
    );
  }

  addListForEnablePowerPulseAlert(listId): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    return this.http.post(
      this.addListForEnablePowerPulseAlertUrl +
        '?listId=' +
        listId +
        '&uid=' +
        this.userDetail.UID,
      options as any
    );
  }

  getListSymbols(listId, uid): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('uid', uid),
      new KeyValuePair('listId', listId),
    ]);
    return this.http.get(this.getListSymbolsUrl, options as any);
  }

  checkListNameExistence(listName: string, uid): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('uid', uid),
      new KeyValuePair('listName', listName),
    ]);
    return this.http.get(this.getcheckListNameExistenceUrl, options as any);
  }

  addListToPortfolio(listName: string, uid): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('uid', uid),
      new KeyValuePair('listName', listName),
    ]);
    return this.http.get(this.getAddListToPortfolioUrl, options as any);
  }

  uploadListDataToRedis(
    listId: number,
    listName: string,
    uid
  ): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    return this.http.post(
      this.getUploadListDataToRedisUrl +
        '?listId=' +
        listId +
        '&uid=' +
        uid +
        '&listName=' +
        encodeURIComponent(listName),
      options as any
    );
  }

  // deleteSymbolFromList(listId: number, csvSymbols: string): Observable<any> {
  //   if (!listId) {
  //     listId = +this.userDetail.ListID;
  //   }
  //   const options = this.prepareHttpOptions(true, true);
  //   options.params = this.prepareParams([
  //     new KeyValuePair('uid', this.userDetail.UID),
  //     new KeyValuePair('symbol', csvSymbols),
  //     new KeyValuePair('listId', listId),
  //   ]);
  //   return this.http.get(this.getDeleteSymbolFromListUrl, options as any);
  // }

  deleteListDataToRedis(listId: number, uid): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    return this.http.delete(
      this.getDeleteListDataToRediseUrl + '?listId=' + listId + '&uid=' + uid,
      options as any
    );
  }

  addStockIntoList(symbolCsv: string, listId: number, uid): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('symbol', symbolCsv),
      new KeyValuePair('uid', uid),
      new KeyValuePair('listId', listId),
    ]);
    return this.http.get(this.getAddStockIntoListUrl, options as any);
  }

  uploadFileData(fileData: any, listName: string, uid) {
    const options = this.prepareHttpOptions(true, true);
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      environment: 'desktop',
      version: '1.3.4',
    });
    options.headers = headers;

    let data = {
      fileData: fileData,
      listName: listName,
      uid: uid,
    };

    let buffer = [];
    for (let name in data) {
      if (!data.hasOwnProperty(name)) {
        continue;
      }
      let value = data[name];
      buffer.push(
        encodeURIComponent(name) +
          '=' +
          encodeURIComponent(value == null ? '' : value)
      );
    }
    let formData = buffer.join('&').replace(/%20/g, '+');

    return this.http.post(this.getUploadFileDataUrl, formData, options as any);
  }

  getFormattedFileData(uid) {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('delimiter', 44),
      new KeyValuePair('uid', uid),
    ]);
    return this.http.get(this.getFormattedFileDataUrl, options as any);
  }

  validateAndUploadTickersForList(
    tickerCol: number,
    tickerRow: number,
    limit: boolean,
    listName: string,
    uid
  ) {
    const options = this.prepareHttpOptions(true, true);
    const headers = new HttpHeaders({
      environment: 'desktop',
      version: '1.3.4',
    });
    options.headers = headers;
    if (limit) {
      options.params = this.prepareParams([
        new KeyValuePair('delimiter', 44),
        new KeyValuePair('uid', uid),
        new KeyValuePair('tickerCol', tickerCol),
        new KeyValuePair('tickerRow', tickerRow),
        new KeyValuePair('limit', limit),
      ]);
    } else {
      options.params = this.prepareParams([
        new KeyValuePair('delimiter', 44),
        new KeyValuePair('uid', uid),
        new KeyValuePair('tickerCol', tickerCol),
        new KeyValuePair('tickerRow', tickerRow),
      ]);
    }

    return new Observable<any>((observer) => {
      this.http
        .get(this.validateAndUploadTickersForListUrl, options as any)
        .subscribe((resp: any) => {
          if (resp['updationStatus']) {
            this.uploadListDataToRedis(resp.listID, listName, uid).subscribe();
          }
          observer.next(resp);
          observer.complete();
        });
    });

    // return this.http.get(this.validateAndUploadTickersForListURL, options as any);
  }

  clearRawListData(uid) {
    const options = this.prepareHttpOptions(true, true);
    const headers = new HttpHeaders({
      environment: 'desktop',
      version: '1.3.4',
    });
    options.headers = headers;
    options.params = this.prepareParams([new KeyValuePair('uid', uid)]);
    return this.http.get(this.clearRawListDataUrl, options as any);
  }

  editSymbolList(listId: number, revisedName: string, uid) {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('uid', uid),
      new KeyValuePair('listId', listId),
      new KeyValuePair('revisedName', revisedName),
      new KeyValuePair('updateListName', true),
      new KeyValuePair('updateDefaultList', false),
    ]);
    return this.http.get(this.getEditSymbolListUrl, options as any);
  }

  deleteStockList(listId: number, uid) {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('uid', uid),
      new KeyValuePair('listId', listId),
    ]);
    return this.http.get(this.getDeleteStockListUrl, options as any);
  }

  checkSandboxPortfolioListNameExistence(
    listName: string,
    uid
  ): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('listName', listName),
      new KeyValuePair('uid', uid),
    ]);
    return this.http.get(
      this.checkSandboxPortfolioListNameExistenceURL,
      options as any
    );
  }

  getChecklistStocks(symbolName: string): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('symbol', symbolName),
    ]);
    return this.http.get(this.getChecklistStocksUrl, options as any);
  }

  getCompanyInformation(symbolName: string): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('symbol', symbolName),
    ]);
    return this.http.get(this.getCompanyInformationUrl, options as any);
  }

  getEarningCalenderData(
    listId: number,
    startDate: string,
    endDate: string
  ): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    // if (!listId) {
    //  listId = +this.userDetail.ListID;
    // }
    options.params = this.prepareParams([
      new KeyValuePair('listId', listId),
      new KeyValuePair('startDate', startDate),
      new KeyValuePair('endDate', endDate),
    ]);
    return this.http.get(this.getEarningCalenderDataUrl, options as any);
  }

  getEarningsGlanceData(listId): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([new KeyValuePair('listId', listId)]);
    return this.http.get(this.getEarningsGlanceURL, options as any);
  }

  getPulseChaikinList(): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([]);
    return this.http.get(this.getPulseChaikinListUrl, options as any);
  }

  disablePowerGaugeAnalyticsWelcomeGuide(uid, enable): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('uid', uid),
      new KeyValuePair('enable', enable),
    ]);
    return this.http.get(
      this.disablePowerGaugeAnalyticsWelcomeGuideUrl,
      options as any
    );
  }

  // getIndustryExposure(listId): Observable<any> {
  //   const options = this.prepareHttpOptions(true, true);
  //   options.params = this.prepareParams([
  //     new KeyValuePair('listId', listId)
  //   ]);
  //   return this.http.get(this.getIndustryExposureUrl, options as any);
  // }

  getChaikinAdvanceList(uid: string): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([new KeyValuePair('uid', uid)]);
    return this.http.get(this.getChaikinAdvanceListUrl, options as any);
  }

  getChaikinAdvanceListData(listId: number): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([new KeyValuePair('listId', listId)]);
    return this.http.get(this.getChaikinAdvanceListDataUrl, options as any);
  }
  getIndustryExposure(listId): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([new KeyValuePair('listId', listId)]);
    return this.http.get(this.getIndustryExposureUrl, options as any);
  }

  getMarketScannerChartData(): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([]);
    return this.http.get(this.getMarketScannerChartUrl, options as any);
  }

  getPowerFeedInsightData(): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('json', 'secursive.get_product_updates'),
      new KeyValuePair('dev', 1),
      new KeyValuePair('id', '2,10,67'),
    ]);
    return this.http.get(this.getPowerFeedInsightUrl, options as any);
  }

  getMarketPanelData(): Observable<any> {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([]);
    return this.http.get(this.getMarketPanelDataUrl, options as any);
  }

  getScreenerInitialData(uid: string) {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([new KeyValuePair('uid', uid)]);
    return this.http.get(this.getScreenerInitialDataUrl, options as any);
  }

  getEtfScreenerInitialData(uid: string) {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([new KeyValuePair('uid', uid)]);
    return this.http.get(this.getEtfScreenerInitialDataUrl, options as any);
  }

  getScreenerResults(uid: string, rules) {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('uid', uid),
      new KeyValuePair('environment', 'desktop'),
      new KeyValuePair('ruleSet', rules),
      new KeyValuePair('subEnvironment', 'chaikinAnalytics'),
      new KeyValuePair('version', '1.3.4'),
    ]);
    return this.http.get(this.getScreenerResultsUrl, options as any);
  }

  getEtfScreenerResults(uid: string, rules: string) {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('uid', uid),
      new KeyValuePair('environment', 'desktop'),
      new KeyValuePair('ruleSet', rules),
      new KeyValuePair('screenType', 'etf'),
      new KeyValuePair('subEnvironment', 'chaikinAnalytics'),
      new KeyValuePair('version', '1.3.4'),
    ]);
    return this.http.get(this.getScreenerResultsUrl, options as any);
  }

  getInvestorListsDetails() {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([]);
    return this.http.get(this.getInvestorListsDetailsUrl, options as any);
  }

  investorUpdateListDetails(data) {
    const options = this.prepareHttpOptionsPost(true, true);
    data.screenCriteria = data.screenCriteria
      ? JSON.stringify(data.screenCriteria)
      : '';
    // let data2 = {
    //   "listName": "ff Special",
    //   "description": "This cccc special stock list",
    //   "guru": "cccc",
    //   "screenCriteria": "",
    //   "symbols": ["AAPL", "F"],
    //   "limit": 26,
    //   "associatedPubCodes": ["Pwd", "CHA"],
    //   "updationMode": "mannual",
    //   "isActive": false
    // }

    // let data1 = {
    //   "guru": "qqqqqq",
    //   "associatedPubCodes": [
    //     "Pwd",
    //     "CHA"
    //   ],
    //   "screenCriteria": "",
    //   "limit": 10,
    //   "description": "This qqqqq Special stock list",
    //   "listName": "qqqqq Special",
    //   "isActive": false,
    //   "symbols": [
    //     "F",
    //     "FB",
    //     "INFY"
    //   ],
    //   "updationMode": "mannual"
    // }

    return this.http.put(
      this.getInvestorUpdateListDetailsUrl,
      data,
      options as any
    );
  }

  addNewListDetails(data) {
    const options = this.prepareHttpOptionsPost(true, true);
    data.screenCriteria = data.screenCriteria
      ? JSON.stringify(data.screenCriteria)
      : '';
    // let data = {
    //   "listName": "ff Special",
    //   "description": "This cccc special stock list",
    //   "guru": "cccc",
    //   "screenCriteria": "",
    //   "symbols": ["AAPL", "F"],
    //   "limit": 26,
    //   "associatedPubCodes": ["Pwd", "CHA"],
    //   "updationMode": "mannual",
    //   "isActive": true
    // }

    return this.http.post(
      this.getInvestorAddNewListDetailsUrl,
      data,
      options as any
    );
  }

  deleteListDetails(listName: string) {
    const options = this.prepareHttpOptionsPost(true, true);
    return this.http.delete(
      this.getInvestorDeleteListDetailsUrl + '/' + listName,
      options as any
    );
  }

  getIssuesAndUpdates(pubCSV: string) {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([new KeyValuePair('pubCSV', pubCSV)]);
    return this.http.get(this.getIssuesAndUpdatesUrl, options as any);
  }
}
