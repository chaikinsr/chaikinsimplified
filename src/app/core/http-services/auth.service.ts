import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { KeyValuePair } from '../data-models/core';
import { LoginDetailModel } from '../data-models/service-model';
import { LocalStorage } from '../services/utilily/utility';
import { APIServiceBase } from './api.service.base';

@Injectable({ providedIn: 'root' })
export class AuthService extends APIServiceBase {
  userDetail: LoginDetailModel;

  constructor(private http: HttpClient) {
    super();
    if (!this.userDetail) {
      this.userDetail = this.getUserDetail();
    }
  }

  getLoginDetail(deviceId: string) {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair('deviceId', deviceId),
    ]);
    return this.http
      .get(this.getLoginUrl, options as any)
      .toPromise()
      .then((res) => {
        let userDetail: LoginDetailModel = res as any;
        this.setUserDetail(userDetail);
        return res;
      })
      .catch(this.handleError);
  }

  getLoggedInstate(deviceId: string): Observable<any> {
    return new Observable<any>((observer) => {
      this.getLoginDetail(deviceId)
        .then((loginResp) => {
          observer.next(loginResp);
          observer.complete();
        })
        .catch((e) => {
          observer.next(false);
          observer.complete();
        });
    });
  }

  getAuthorization(
    acquireSessionForcibly: boolean,
    email: string,
    password: string
  ) {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair(
        'acquireSessionForcibly',
        acquireSessionForcibly ? 'Yes' : 'No'
      ),
      new KeyValuePair('email', email),
      new KeyValuePair('password', password),
      new KeyValuePair('productName', 'Power Pulse Report'),
    ]);
    return this.http
      .get(this.getAuthorizationUrl, options as any)
      .toPromise()
      .then((res) => {
        return res;
      })
      .catch(this.handleError);
  }

  getAuth(user: string, password: string): Observable<any> {
    return new Observable<any>((observer) => {
      this.getAuthorization(true, user, password)
        .then((authRep) => {
          let sessionId = authRep['sessionId'];
          window.localStorage.setItem('sessionId', sessionId);
          observer.next(authRep);
          observer.complete();
        })
        .catch((e) => {
          observer.next(false);
          observer.complete();
        });
    });
  }

  getAuthorizationForUtility(
    acquireSessionForcibly: boolean,
    email: string,
    password: string
  ) {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([
      new KeyValuePair(
        'acquireSessionForcibly',
        acquireSessionForcibly ? 'Yes' : 'No'
      ),
      new KeyValuePair('email', email),
      new KeyValuePair('password', password),
    ]);
    return this.http
      .get(this.getAuthorizationUrl, options as any)
      .toPromise()
      .then((res) => {
        return res;
      })
      .catch(this.handleError);
  }

  getAuthForUtility(user: string, password: string): Observable<any> {
    return new Observable<any>((observer) => {
      this.getAuthorizationForUtility(true, user, password)
        .then((authRep) => {
          let sessionId = authRep['sessionId'];
          window.localStorage.setItem('sessionId', sessionId);
          observer.next(authRep);
          observer.complete();
        })
        .catch((e) => {
          observer.next(false);
          observer.complete();
        });
    });
  }

  killsessions(uuid: string) {
    const options = this.prepareHttpOptions(true, true);
    options.params = this.prepareParams([new KeyValuePair('uuid', uuid)]);
    return this.http.get(this.getKillSessionURL, options as any);
  }

  setUserDetail(userDetail: LoginDetailModel): void {
    LocalStorage.setItem('userDetail', userDetail);
    LocalStorage.setItem('listId', userDetail.ListID);
    if (!LocalStorage.getItem('mainSymbol')) {
      LocalStorage.setItem('mainSymbol', 'AMZN');
    }

    this.userDetail = userDetail;
  }

  getUserDetail(): LoginDetailModel {
    const userDetail: LoginDetailModel = LocalStorage.getItem('userDetail');
    return userDetail;
  }

  protected handleError(error: Response | Error) {
    let errMsg = ''; //this.getErrorMessage(error);
    return Promise.reject(errMsg);
  }

  // RoleNameProductTierMapping = [
  //   { userType: 'CPGR-CPP-CLST', productTier: 'Elite' },
  //   { userType: 'CPGR-CLST-CPP', productTier: 'Elite' },
  //   { userType: 'CPP-CPGR-CLST', productTier: 'Elite' },
  //   { userType: 'CPP-CLST-CPGR', productTier: 'Elite' },
  //   { userType: 'CLST-CPGR-CPP', productTier: 'Elite' },
  //   { userType: 'CLST-CPP-CPGR', productTier: 'Elite' },
  //   { userType: 'CPGR-CPP', productTier: 'Standard' },
  //   { userType: 'CPP-CPGR', productTier: 'Standard' },
  //   { userType: 'CLST', productTier: 'Checklist' }
  // ];
  // RoleNameProductTierMapping = [
  //   { userType: 'CPGR0-CPP0-CLST0', productTier: 'Login' },
  //   { userType: 'CPGR1-CPP1-CLST1', productTier: 'Dashboard_Report_Checklist' },
  //   { userType: 'CPGR1-CPP1-CLST0', productTier: 'Dashboard_Report' },
  //   { userType: 'CPGR1-CPP0-CLST1', productTier: 'Report_Checklist' },
  //   { userType: 'CPGR0-CPP1-CLST1', productTier: 'Dashboard_Checklist' },
  //   { userType: 'CPGR0-CPP1-CLST0', productTier: 'Dashboard' },
  //   { userType: 'CPGR0-CPP0-CLST1', productTier: 'Checklist' },
  //   { userType: 'CPGR1-CPP0-CLST0', productTier: 'Report' },

  // ];

  // RoleNameProductTierMapping = [
  //   { userType: 'CPGR0-CPP0-CLST0', productTier: 'Login' },
  //   { userType: 'CPGR1-CPP1-CLST1', productTier: 'Dashboard_Report_Checklist' },
  //   { userType: 'CPGR1-CPP1-CLST0', productTier: 'Dashboard_Report' },
  //   { userType: 'CPGR1-CPP0-CLST1', productTier: 'Report_Checklist' },
  //   { userType: 'CPGR0-CPP1-CLST1', productTier: 'Dashboard_Checklist' },
  //   { userType: 'CPGR0-CPP1-CLST0', productTier: 'Dashboard' },
  //   { userType: 'CPGR0-CPP0-CLST1', productTier: 'Checklist' },
  //   { userType: 'CPGR1-CPP0-CLST0', productTier: 'Report' },

  // ];

  RoleNameProductTierMapping = [
    { userType: 'CPGR0-CPP0-CLST0', productTier: 'Login' },
    { userType: 'CPGR1-CPP1-CLST1', productTier: 'Dashboard_Report_Checklist' },
    { userType: 'CPGR1-CPP1-CLST0', productTier: 'Dashboard_Report' },
    { userType: 'CPGR1-CPP0-CLST1', productTier: 'Report_Checklist' },
    { userType: 'CPGR0-CPP1-CLST1', productTier: 'Dashboard_Checklist' },
    { userType: 'CPGR0-CPP1-CLST0', productTier: 'Dashboard' },
    { userType: 'CPGR0-CPP0-CLST1', productTier: 'Checklist' },
    { userType: 'CPGR1-CPP0-CLST0', productTier: 'Report' },
  ];

  //  { "userType": "CAUser", "productTier": "Elite" }
  productTiersMappingData = {
    Dashboard_Report_Checklist: {
      report: true,
      dashboard: true,
      checklist: true,
    },
    Dashboard_Report: {
      report: true,
      dashboard: true,
      checklist: false,
    },
    Report_Checklist: {
      report: true,
      dashboard: false,
      checklist: true,
    },
    Dashboard_Checklist: {
      report: false,
      dashboard: true,
      checklist: true,
    },
    Dashboard: {
      report: false,
      dashboard: true,
      checklist: false,
    },
    Checklist: {
      report: false,
      dashboard: false,
      checklist: true,
    },
    Report: {
      report: true,
      dashboard: false,
      checklist: false,
    },
    Login: {
      report: false,
      dashboard: false,
      checklist: false,
    },
  };
}
