import { Injectable } from '@angular/core';
import {
  AdvanceSearch,
  GlobalListWithPowbarModel,
  ListSymbolsPriceForSelectedTimeFrameModel,
  ProductAccessModel,
  ProductMapping,
  TabbedStructureData,
  UserRoleMappingModel,
  PriceMovementSorting,
} from '../data-models/app-model';
import {
  CookiesDataObject,
  DashboardData,
  ProductCodesAndPreferenceObject,
} from '../data-models/service-model';
import { AppConst } from '../app-constants/app-const';

@Injectable({
  providedIn: 'root',
})
export class AppDataService {
  cookiesData: CookiesDataObject;
  topHeaderData: any;
  productAccessModel: ProductAccessModel;
  productPreference: ProductCodesAndPreferenceObject;
  productMapping: ProductMapping;
  productsListModel: any;
  dashboardData: DashboardData;
  callOnceGetWatchListIdDataApiInBottomHeader: number = 0;
  oldSelectedListId: any;
  userList: GlobalListWithPowbarModel;
  advanceSearch: AdvanceSearch;
  userRoleMappingModel: UserRoleMappingModel;
  tabbedStructureData: TabbedStructureData;
  previousRoutingUrl: string;
  priceMovementSortingModel = new PriceMovementSorting();
  priceMovementRenderCount: number = 0;
  sortingParamsModel = [];

  listSymbolsPriceForSelectedTimeFrameArray : ListSymbolsPriceForSelectedTimeFrameModel[];
  constructor() {
    this.productPreference = new ProductCodesAndPreferenceObject();
    this.productMapping = new ProductMapping();
    this.advanceSearch = new AdvanceSearch();
    this.tabbedStructureData = new TabbedStructureData();
    this.priceMovementSortingModel.sortingOrder = AppConst.CONST.sortOrderAsc;
    this.priceMovementSortingModel.sortingOrderPgr =
      AppConst.CONST.sortOrderAsc;
    this.priceMovementSortingModel.sortingOrderSymbols =
      AppConst.CONST.sortOrderDesc;
    this.priceMovementSortingModel.sortingOrderPrice =
      AppConst.CONST.sortOrderAsc;
    // this.topHeaderData = new RecentlyViewedInfo();
  }
  removeAllData() {
    this.productPreference = new ProductCodesAndPreferenceObject();
    this.userList = new GlobalListWithPowbarModel();
    this.advanceSearch = new AdvanceSearch();
    //  this.topHeaderData = new RecentlyViewedInfo();
  }
}
