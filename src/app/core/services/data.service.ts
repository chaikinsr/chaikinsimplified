import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { RecentlyViewedInfo } from "../data-models/app-model";

@Injectable({ providedIn: 'root' })
export class DataService {
    recentlyViewedData: RecentlyViewedInfo;
    public matSideNavContentScrollChange = new Subject<any>();
    public showAllPopup = new Subject<boolean>();
    public showProject = new Subject<boolean>();
    
    updateMatSideNavContentScroll(data: any) {
        this.matSideNavContentScrollChange.next(data);
    }

    updatePopup(openQuickDescriptionPopup: boolean): void {
        this.showAllPopup.next(openQuickDescriptionPopup);
    }

    setProjectAccess(value):void{
        this.showProject.next(value);
    }

    constructor() {
        this.recentlyViewedData = new RecentlyViewedInfo();
    }

}