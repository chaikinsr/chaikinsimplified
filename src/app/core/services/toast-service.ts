import { Injectable, TemplateRef } from '@angular/core';
import {AppConfig } from '../app-config/app-config';
import {ToastrService} from 'ngx-toastr'

@Injectable({ providedIn: 'root' })
export class ToastService {
  constructor(public toastr: ToastrService){}
  toasts: any[] = [];

  private show(textOrTpl: string | TemplateRef<any>, options: any = {}) {
    this.toasts.push({ textOrTpl, ...options });
  }

  remove(toast) {
    this.toasts = this.toasts.filter(t => t !== toast);
  }

  showStandard(message:string) {
    this.show(message);
  }

  showSuccess(message:string) {
    this.toastr.success(message, "Message" , {timeOut: 3000});
  }

  showDanger(message:string) {
    this.toastr.warning(message,'Message', {timeOut: 3000});
  }
}
