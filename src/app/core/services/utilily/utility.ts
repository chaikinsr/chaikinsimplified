
import { NavigationExtras } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { AppConfig } from '../../app-config/app-config';
import { AppConst } from '../../app-constants/app-const';
import { SortingModel } from '../../data-models/app-model';


export class LocalStorage {
  static getItem(key: string): any {
    const userData = localStorage.getItem(key);
    if (userData && userData !== 'undefined') {
      return JSON.parse(userData);
    } else {
      return null;
    }
  }

  static setItem(key: string, value: any): void {
    localStorage.setItem(key, JSON.stringify(value));
  }

  static removeItem(key: string): void {
    localStorage.removeItem(key);
  }
}

export class SharedUtil {
  constructor(
    public cookieService: CookieService,
  ) {}
  static getPgrRating(pgr, rawPgr): number {
    if (!pgr || pgr <= 0) {
      return -1;
    }
    if (pgr !== 3) {
      return [1, 2, 3, 8, 9][pgr - 1];
    } else {
      return [3, 4, 5, 6, 7][rawPgr - 1];
    }
  }

  static formatChangeDashWat = function (change) {
    if (typeof change != 'undefined' && change == 0) {
        // new Number(change) * 100).toFixed(2) + "%");
        return new Number(parseFloat(change) * 100).toFixed(2) + '%';
    }
    var num = parseFloat(change);
    return ((num >= 0) ? '+' : '') + new Number(change).toFixed(2) + '%';

};
static formatPriceDashWat = function (change) {
  if (typeof change != 'undefined' && change == 0) {
      // new Number(change) * 100).toFixed(2) + "%");
      return new Number(parseFloat(change) * 100).toFixed(2);
  }
  var num = parseFloat(change);
  return ((num >= 0) ? '' : '') + new Number(change).toFixed(2);

};


static getToken(cookieService): any {
  let hostname = window.location.hostname.split(".")[0];
  if (hostname+'.chaikinanalytics.com' === window.location.hostname) {
    return cookieService.get(hostname+'-token');
  }
  // if ('dev-login.chaikinanalytics.com' === window.location.hostname || 'dev.chaikinanalytics.com' === window.location.hostname) {
  //  // this.uid = this.cookieService.get('dev-uid');
  //   return cookieService.get('dev-token');
  // } else if ('localhost' === window.location.hostname) {
  // //  this.uid = this.cookieService.get('local-uid');
  //   return cookieService.get('local-token');

  // } else if ('qa.chaikinanalytics.com' === window.location.hostname) {
  // //  this.uid = this.cookieService.get('qa-uid');
  //   return cookieService.get('qa-token');

  // }
  // else if ('staging.chaikinanalytics.com' === window.location.hostname) {
  // //  this.uid = this.cookieService.get('staging-uid');
  //   return cookieService.get('staging-token');

  // }
  // else if ('app.chaikinanalytics.com' === window.location.hostname) {
  //  // this.uid = this.cookieService.get('app-uid');
  //   return cookieService.get('app-token');

  // }

}

static getProductCode (projectName): any{
  switch(projectName){
    case'Power Gauge Report':
     return  "CPGR";
    case'Power Gauge Analytics':
    return "CPGI";
    case'Chaikin Analytics':
    return "CHAP" ;
     case'Power Pulse':
     return "CPP" ;
     case 'Chaikin Power Tactics' :
      return "CTAC";
      case 'Chaikin Power Trader' :
        return "CPTR";
  }

}




static getBeaconStreetJwtToken(cookieService): any {
  let hostname = window.location.hostname.split(".")[0];
        if (hostname+'.chaikinanalytics.com' === window.location.hostname) {
          return cookieService.get(hostname+'-beaconStreetJwtToken');
        }
  // if ('dev-login.chaikinanalytics.com' === window.location.hostname || 'dev.chaikinanalytics.com' === window.location.hostname) {
  //  // this.uid = this.cookieService.get('dev-uid');
  //   return cookieService.get('dev-beaconStreetJwtToken');
  // } else if ('localhost' === window.location.hostname) {
  // //  this.uid = this.cookieService.get('local-uid');
  //   return cookieService.get('local-beaconStreetJwtToken');

  // } else if ('qa.chaikinanalytics.com' === window.location.hostname) {
  // //  this.uid = this.cookieService.get('qa-uid');
  //   return cookieService.get('qa-beaconStreetJwtToken');

  // }
  // else if ('staging.chaikinanalytics.com' === window.location.hostname) {
  // //  this.uid = this.cookieService.get('staging-uid');
  //   return cookieService.get('staging-beaconStreetJwtToken');

  // }
  // else if ('app.chaikinanalytics.com' === window.location.hostname) {
  //  // this.uid = this.cookieService.get('app-uid');
  //   return cookieService.get('app-beaconStreetJwtToken');

  // }

}


static getProductListObject(): any{
   let productsListModel  = {
    powerGaugeInvestor: {
      name: 'Power Gauge Investor',
      subscriptionCode: ['CPGI'],
      productAccess: false,
      productPreference: false,
    },
    chaikinAnalytics: {
      name: 'Chaikin Analytics',
      subscriptionCode: ['CHAP', 'CHA', 'CAMS', 'CAPW', 'BCHAP', 'BCHAPF'],
      productAccess: false,
      productPreference: false,
    },
    checklist: {
      name: 'Checklist',
      subscriptionCode: ['CLST'],
      productAccess: false,
    },
    powerGaugeReport: {
      name: 'Power Gauge Report',
      subscriptionCode: ['CPGR'],
      productAccess: false,
      productPreference: false,
    },
    // powerPulse: {
    //   name: 'Power Pulse Premium',
    //   subscriptionCode: ['CPP', 'CPOW'],
    //   productAccess: false,
    //   productPreference: false,
    // },
    powerPulse: {
      name: 'Power Pulse Premium',
      subscriptionCode: ['CPP', 'CPOW'],
      productAccess: false,
      productPreference: false,
    },
    powerTactics: {
      name: 'Chaikin Power Tactics',
      subscriptionCode: ['CTAC'],
      productAccess: false,
    },
    powerTrader: {
      name: ' Chaikin Power Trader',
      subscriptionCode: ['CPTR'],
      productAccess: false,
    }
    // powerGaugeRating: {
    //   name: 'PowerGauge Rating',
    //   subscriptionCode: [],
    //   productAccess: false,
    // },
  };
  return productsListModel;
} 

  static calculateWidth(val , dashboardTempArray): number{
    const sliderBarArray = [];
    // array.forEach(element => {
    // });
    for (let i = 0; i < dashboardTempArray.length; i++) {
        sliderBarArray.push(Math.abs(dashboardTempArray[i].percent_change));
    }
    const maxVal = Math.max.apply(null, sliderBarArray);
    if (maxVal > 0) {
        const sliderBarPercent = (Math.abs(val) / maxVal) * 100;
        return sliderBarPercent;
    } else {
        return 0;
    }
  }

  static getPgrNameBasedOnRating(rating: number): string {
    let pgrNameBasedOnRating = '';
    switch (rating) {
      case AppConst.PGRS.veryBullish:
        pgrNameBasedOnRating = 'Very Bullish';
        break;
      case AppConst.PGRS.bullish:
        pgrNameBasedOnRating = 'Bullish';
        break;
      case AppConst.PGRS.neutralNegativeFirst:
      case AppConst.PGRS.neutralNegativeSecond:
        pgrNameBasedOnRating = 'Neutral (via technical rise)';
        break;
      case AppConst.PGRS.neutralPositiveFirst:
      case AppConst.PGRS.neutralPositiveSecond:
        pgrNameBasedOnRating = 'Neutral (via technical decline)';
        break;
      case  AppConst.PGRS.neutral:
        pgrNameBasedOnRating = 'Neutral';
        break;
      case AppConst.PGRS.bearish:
        pgrNameBasedOnRating = 'Bearish';
        break;
      case AppConst.PGRS.veryBearish:
        pgrNameBasedOnRating = 'Very Bearish';
        break;
      default:
        pgrNameBasedOnRating = 'None';
    }
    return pgrNameBasedOnRating;
  }

  static getPgrArcWithoutNeutralValue(
    pgr: number,
    rawPgr: number,
    isEtfSymbol: boolean
  ): string {
    let imgUrl = '';
    let imgName: string;
    if (isEtfSymbol) {
      imgUrl = './assets/';
    } else {
      imgUrl = './assets/';
    }
    if (pgr <= 0) {
      imgName = 'Arc-Large_None.svg';
    } else if (pgr === 3) {
      imgName = [
        'Arc-Large_Neutral_Negative.svg',
        'Arc-Large_Neutral_Negative.svg',
        'Arc-Large_Neutral.svg',
        'Arc-Large_Neutral_Positive.svg',
        'Arc-Large_Neutral_Positive.svg',
      ][rawPgr - 1];
    } else {
      imgName = [
        'Arc-Large_verybearish.svg',
        'Arc-Large_Bearish.svg',
        'Arc-Large_Neutral.svg',
        'Arc-Large_Bullish.svg',
        'Arc-Large_verybullish.svg',
      ][pgr - 1];
    }

    return imgUrl.concat(imgName);
  }

  static getPgrGradientArcWithoutNeutralValue(
    pgr: number,
    rawPgr: number,
    isEtfSymbol: boolean
  ): string {
    let imgUrl = '';
    let imgName: string;
    if (isEtfSymbol) {
      imgUrl = './assets/';
    } else {
      imgUrl = './assets/';
    }
    if (pgr <= 0) {
      imgName = 'Arc-Large_None--Gradient.svg';
    } else if (pgr === 3) {
      imgName = [
        'Arc-Large_Neutral_Negative--Gradient.svg',
        'Arc-Large_Neutral_Negative--Gradient.svg',
        'Arc-Large_Neutral--Gradient.svg',
        'Arc-Large_Neutral_Positive--Gradient.svg',
        'Arc-Large_Neutral_Positive--Gradient.svg',
      ][rawPgr - 1];
    } else {
      imgName = [
        'Arc-Large_VeryBearish--Gradient.svg',
        'Arc-Large_Bearish--Gradient.svg',
        'Arc-Large_Neutral--Gradient.svg',
        'Arc-Large_Bullish--Gradient.svg',
        'Arc-Large_VeryBullish--Gradient.svg',
      ][pgr - 1];
    }

    return imgUrl.concat(imgName);
  }

  static getPgrSmallArcWithoutNeutralValue(
    pgr: number,
    rawPgr: number,
    isEtfSymbol: boolean
  ): string {
    let imgUrl = '';
    let imgName: string;
    if (isEtfSymbol) {
      imgUrl = './assets/';
    } else {
      imgUrl = './assets/';
    }
    if (pgr <= 0) {
      imgName = 'Arc-Small_None.svg';
    } else if (pgr === 3) {
      imgName = [
        'Arc-Small_Neutral_Negative.svg',
        'Arc-Small_Neutral_Negative.svg',
        'Arc-Small_Neutral.svg',
        'Arc-Small_Neutral_Positive.svg',
        'Arc-Small_Neutral_Positive.svg',
      ][rawPgr - 1];
    } else {
      imgName = [
        'Arc-Small_verybearish.svg',
        'Arc-Small_Bearish.svg',
        'Arc-Small_Neutral.svg',
        'Arc-Small_Bullish.svg',
        'Arc-Small_verybullish.svg',
      ][pgr - 1];
    }

    return imgUrl.concat(imgName);
  }

  static getPgrMicroArcWithoutNeutralValue(
    pgr: number,
    rawPgr: number,
    isEtfSymbol: boolean
  ): string {
    let imgUrl = '';
    let imgName: string;
    if (isEtfSymbol) {
      imgUrl = './assets/';
    } else {
      imgUrl = './assets/';
    }
    if (pgr <= 0) {
      imgName = 'Arc-Micro_None.svg';
    } else if (pgr === 3) {
      imgName = [
        'Arc-Micro_Neutral_Negative.svg',
        'Arc-Micro_Neutral_Negative.svg',
        'Arc-Micro_Neutral.svg',
        'Arc-Micro_Neutral_Positive.svg',
        'Arc-Micro_Neutral_Positive.svg',
      ][rawPgr - 1];
    } else {
      imgName = [
        'Arc-Micro_verybearish.svg',
        'Arc-Micro_Bearish.svg',
        'Arc-Micro_Neutral.svg',
        'Arc-Micro_Bullish.svg',
        'Arc-Micro_verybullish.svg',
      ][pgr - 1];
    }
    return imgUrl.concat(imgName);
  }

  static getPgrNameWithoutNeutralValues(rating: number): string {
    let pgrNameBasedOnRating = '';
    switch (rating) {
      case AppConst.PGRS.veryBullish:
        pgrNameBasedOnRating = 'Very Bullish';
        break;
      case AppConst.PGRS.bullish:
        pgrNameBasedOnRating = 'Bullish';
        break;
      case AppConst.PGRS.neutralNegativeFirst:
      case AppConst.PGRS.neutralNegativeSecond:
        pgrNameBasedOnRating = 'Neutral-';
        break;
      case AppConst.PGRS.neutralPositiveFirst:
      case AppConst.PGRS.neutralPositiveSecond:
        pgrNameBasedOnRating = 'Neutral+';
        break;
      case  AppConst.PGRS.neutral:
        pgrNameBasedOnRating = 'Neutral';
        break;
      case AppConst.PGRS.bearish:
        pgrNameBasedOnRating = 'Bearish';
        break;
      case AppConst.PGRS.veryBearish:
        pgrNameBasedOnRating = 'Very Bearish';
        break;
      default:
        pgrNameBasedOnRating = 'None';
    }
    return pgrNameBasedOnRating;
  }

  static getPgrArcNoTxtImageUrl(
    pgr: number,
    rawPgr: number,
    isEtfSymbol: boolean
  ): string {

   
    let imgUrl = '';
    let imgName: string;
    if (isEtfSymbol) {
      imgUrl = '/assets/ico--PGR-E__NoTxt-';
    } else {
      imgUrl = '/assets/ico--PGR-S__NoTxt-';
    }
    if (pgr <= 0) {
      imgName = 'none.svg';
    } else if (pgr === 3) {
      imgName = [
        'neutral-minus.svg',
        'neutral-minus.svg',
        'neutral.svg',
        'neutral-plus.svg',
        'neutral-plus.svg',
      ][rawPgr - 1];
    } else {
      imgName = [
        'very-bearish.svg',
        'bearish.svg',
        'neutral.svg',
        'bullish.svg',
        'very-bullish.svg',
      ][pgr - 1];
    }

    return imgUrl.concat(imgName);
  }



  static getPgrArcImageUrl(
    pgr: number,
    rawPgr: number,
    isEtfSymbol: boolean
  ): string {

  //  Arc-Small_Bullish
    let imgUrl = '';
    let imgName: string;
    if (isEtfSymbol) {
      imgUrl = './assets/';
    } else {
      imgUrl = './assets/';
    }
    if (pgr <= 0) {
      imgName = 'Arc-Small_None.svg';
    } else if (pgr === 3) {
      imgName = [
        'Arc-Small_Neutral_Negative.svg',
        'Arc-Small_Neutral_Negative.svg',
        'Arc-Small_Neutral.svg',
        'Arc-Small_Neutral_Positive.svg',
        'Arc-Small_Neutral_Positive.svg',
      ][rawPgr - 1];
    } else {
      imgName = [
        'Arc-Small_verybearish.svg',
        'Arc-Small_Bearish.svg',
        'Arc-Small_Neutral.svg',
        'Arc-Small_Bullish.svg',
        'Arc-Small_verybullish.svg',
      ][pgr - 1];
    }

    return imgUrl.concat(imgName);
  }

  static getColorForStrengthAndWeek(type: string): string {
    let color = 'grey';
    switch (type) {
      case 'up':
      case 'positive':
      case 'green':
      case 'Strong':
        color = 'green';
        break;
      case 'down':
      case 'negative':
      case 'red':
      case 'Weak':
        color = 'red';
        break;
      case 'Neutral':
        color = 'neutral-label';
        break;
      default:
        color  = 'grey';
        break;
      }
      return color;

  }
  
  static getColorForRating(rating: string): string {
    let color = 'grey';
    switch (rating) {
      case 'bullish':
      case 'Bullish':
      case 'Buy':
        color = 'bullish';
        break;
      case 'Very Bullish':
      case 'Strong Buy':
        color = 'veryBullish';
        break;
      case 'bearish':
      case 'Bearish':
      case 'Sell':
        color = 'bearish';
        break;
      case 'Very Bearish':
      case 'Strong Sell':
        color = 'veryBearish';
        break;
      case 'neutral':
      case 'Neutral':
      case 'Neutral+':
      case 'Neutral-':
      case 'Hold':
        color = 'neutral';
        break;
      default:
        color  = 'grey';
        break;
    }
    return color;
  }

  static formatedString(value: string, rounding = 2): string {
    let numberValue: number;
    if (isNaN(Number(value))) {
      return 'NA';
    } else if (
      value === 'N/A' ||
      value === 'NA' ||
      value === '' ||
      value === 'Null'
    ) {
      return value;
    } else {
      numberValue = this.convertStringToNumber(value);
      return numberValue.toFixed(rounding);
    }
  }

  static formatNumber(value: number, rounding = 2): string {
    if (isNaN(value)) {
      return 'NA';
    } else {
      return value.toFixed(rounding);
    }
  }

  static appendSymbol(value: string, formatedSymbol: string): string {
    let finalValue = value;

    if (value === '' || value === 'NA' || value === 'N/A') {
      return finalValue;
    }

    switch (formatedSymbol) {
      case '$':
        finalValue = `$${finalValue}`;
        break;
      case '%':
        finalValue = `${finalValue}%`;
        break;
      case '()':
        finalValue = `(${finalValue})`;
        break;
      default:
        finalValue = `${finalValue}`;
        break;
    }

    return finalValue;
  }

  static convertStringToNumber(stringToConvert: string): number {
    // Handel null
    if (!stringToConvert) {
      return NaN;
    }

    // Is Empty string
    if (stringToConvert.trim().length === 0) {
      return NaN;
    }

    if (!isNaN(Number(stringToConvert))) {
      return Number(stringToConvert);
    } else {
      // 'Not a Number'
      return NaN;
    }
  }

  static formatDate(value): string {
    if (value === 'N/A' || value === 'NA') {
      return value;
    } else {
      const res = value.substring(0, 4);
      const res1 = value.substring(4, 6);
      const res2 = value.substring(6, 8);
      const value1 = res + '-' + res1 + '-' + res2;

      const getDate1 = new Date(value1.replace(/-/g, '/'));

      const month = getDate1.getMonth();
      const date = getDate1.getDate();
      const year = getDate1.getFullYear();
      const fulldate = month + 1 + '/' + date + '/' + year;

      return fulldate;
    }
  }

  static convertAum(value: number): string {
    if (value > 1000) {
      return this.formatNumber(value / 1000) + 'b';
    } else {
      return this.formatNumber(value) + 'm';
    }
  }

  static convertMillonIntoBillion(value: number): string {
    if (value > 1000) {
      return this.formatNumber(value / 1000) + 'b';
    } else {
      return this.formatNumber(value) + 'm';
    }
  }

  static convertIntoMB(value: number): string {
   if (value > 0 && value < 1000000000 ) {
      return this.formatNumber(value / 1000000) + 'm';
    } else if (value >=  1000000000) {
      return this.formatNumber(value / 1000000000) + 'b';
    } else {
      return this.formatNumber(value) + 'm';
    }
    // if(value < 1000000) {
    //   return this.formatNumber(value);
    // } else if (value >= 1000000 && value < 1000000000 ) {
    //   return this.formatNumber(value / 1000000) + 'm';
    // } else if (value >=  1000000000) {
    //   return this.formatNumber(value / 1000000000) + 'b';
    // } 
  }

  static convertAvg(value: number): string {
    return this.formatNumber(value / 20 / 1000000) + 'm';
    // if (((num / 20) / 1000000) < 1.0) {
    //   return this.rounding(num / 20);
    // } else {
    //   return this.rounding((num / 20) / 1000000) + 'm';
    // }
  }




}

export class Signal {
  static parseSignal(signal: string): any {
    let signalInt;
    let buySignalNames = [];
    let sellSignalNames = [];
    if (typeof signal == 'string') {
      signalInt = this.toInt(signal);
    }

    if (signalInt !== 0) {
      for (let i = 0; i < 6; i += 1) {
        if ((signalInt & Math.pow(2, 2 * i)) != 0) {
          buySignalNames.push(AppConfig.SignalMap[2 * i]);
        }

        if ((signalInt & Math.pow(2, 2 * i + 1)) != 0) {
          sellSignalNames.push(AppConfig.SignalMap[2 * i + 1]);
        }
      }
    }

    return {
      sell: sellSignalNames.length > 0,
      buy: buySignalNames.length > 0,
      sellSignalNames: sellSignalNames,
      buySignalNames: buySignalNames,
    };
  }

  static toInt(signal: string): number {
    if (typeof signal === 'string') {
      signal = this.reverse(signal.replace(/-1/g, '0'));
      return parseInt(signal, 2);
    }
    return undefined;
  }

  static reverse(s): any {
    return s.split('').reverse().join('');
  }
}

export class Sort {
  static sortUnique(sortingParams: SortingModel): Array<any> {
    let sortedData = this.createTreeDataStructure(sortingParams);
    let extendedSortingList = Object.assign({}, sortingParams);
    while (extendedSortingList.isExtended) {
      extendedSortingList.extendedData.sortData = sortedData;
      sortedData = this.extendedSorting(extendedSortingList);
      extendedSortingList = extendedSortingList.extendedData;
    }
    return sortedData;
  }

  static createTreeDataStructure(sortingParams: SortingModel): Array<any> {
    // Create root node.
    const BST = new BinarySearchTreeForObject();

    sortingParams.sortData.forEach((data) => {
      // Check data is matches with ignoreDataKeys array keys.
      const elementIndex = sortingParams.ignoreDataKeys.findIndex(
        (value) => value === data[sortingParams.sortKey]
      );
      if (elementIndex !== -1) {
        sortingParams.ignoreData.push(data);
      } else {
        BST.insertData(data, sortingParams.sortKey);
      }
    });

    let sortedData = [];
    if (sortingParams.sortOrder === AppConst.CONST.sortOrderAsc) {
      BST.ascendingTraversal(BST.root, sortedData);
    } else {
      BST.descendingTraversal(BST.root, sortedData);
    }

    sortedData = sortedData.concat(sortingParams.ignoreData);
    return sortedData;
  }

  static extendedSorting(sortingParams: SortingModel): Array<any> {
    const sortedData = this.sortingNextLevel(
      sortingParams,
      sortingParams.extendedData
    );
    return sortedData;
  }

  static sortingNextLevel(
    sortingParams: SortingModel,
    extendedData: SortingModel
  ): Array<any> {
    const concatArray = [];
    let value: any;
    let startIndex: number;
    let endIndex: number;
    const length = extendedData.sortData.length;
    for (let i = 0; i < length; i++) {
      if (!value) {
        startIndex = i;
        endIndex = i;
        value = extendedData.sortData[i];
      } else if (this.ischeckSortLevelValueEqual(extendedData, value, i)) {
        // value[sortingParams.sortKey] === extendedData.sortData[i][sortingParams.sortKey]
        endIndex = i;
      } else {
        const slicedData = extendedData.sortData.slice(
          startIndex,
          endIndex + 1
        );
        const sortedData = this.createNewSortModel(slicedData, extendedData);
        concatArray.push(sortedData);
        startIndex = i;
        endIndex = i;
        value = extendedData.sortData[i];
      }
      if (i === length - 1) {
        const slicedData = extendedData.sortData.slice(
          startIndex,
          endIndex + 1
        );
        const sortedData = this.createNewSortModel(slicedData, extendedData);
        concatArray.push(sortedData);
      }
    }
    const finalSortedData = this.concatArrayOfObject(concatArray);
    return finalSortedData;
  }

  static ischeckSortLevelValueEqual(
    extendedData: SortingModel,
    value: any,
    index: number
  ): boolean {
    const data = extendedData.sortData[index];
    const len = extendedData.sortLevelKey.length;
    let flag = true;
    for (let i = 0; i < len; i++) {
      const key = extendedData.sortLevelKey[i];
      if (value[key] !== data[key]) {
        flag = false;
        break;
      }
    }
    return flag;
  }

  static createNewSortModel(
    data: Array<any>,
    extendedData: SortingModel
  ): Array<any> {
    const sortingParams = new SortingModel();
    sortingParams.sortData = data;
    sortingParams.sortKey = extendedData.sortKey;
    sortingParams.sortOrder = extendedData.sortOrder;
    sortingParams.ignoreDataKeys = extendedData.ignoreDataKeys;
    sortingParams.ignoreData = [];
    sortingParams.isExtended = extendedData.isExtended;
    sortingParams.sortLevelKey = extendedData.sortLevelKey;
    const sortedData = this.createTreeDataStructure(sortingParams);
    return sortedData;
  }

  static concatArrayOfObject(concatArray: Array<any>): Array<any> {
    let finalArray = [];
    concatArray.forEach((element) => {
      finalArray = finalArray.concat(element);
    });
    return finalArray;
  }
}

export class BinarySearchTreeForObject {
  root: any;
  constructor() {
    this.root = null;
  }

  getRootNode(): any {
    return this.root;
  }

  insertData(data, key): void {
    const newNode = new SortNode(data, key);

    if (this.root === null) {
      this.root = newNode;
    } else {
      this.insertNode(this.root, newNode);
    }
  }

  insertNode(node, newNode): void {
    if (newNode.data[newNode.key] < node.data[node.key]) {
      if (node.left === null) {
        node.left = newNode;
      } else {
        this.insertNode(node.left, newNode);
      }
    } else {
      if (node.right === null) {
        node.right = newNode;
      } else {
        this.insertNode(node.right, newNode);
      }
    }
  }

  ascendingTraversal(node, sortedData): void {
    if (node !== null) {
      this.ascendingTraversal(node.left, sortedData);
      sortedData.push(node.data);
      this.ascendingTraversal(node.right, sortedData);
    }
  }

  descendingTraversal(node, sortedData): void {
    if (node !== null) {
      this.descendingTraversal(node.right, sortedData);
      sortedData.push(node.data);
      this.descendingTraversal(node.left, sortedData);
    }
  }

  findMinNode(node): any {
    if (node.left === null) {
      return node;
    } else {
      return this.findMinNode(node.left);
    }
  }

  search(node, data): any {
    // if trees is empty return null
    if (node === null) {
      return null;
    }
    // if data is less than node's data
    // move left
    else if (data < node.data) {
      return this.search(node.left, data);
    }
    // if data is less than node's data
    // move left
    else if (data > node.data) {
      return this.search(node.right, data);
    }
    // if data is equal to the node data
    // return node
    else {
      return node;
    }
  }

  remove(data): void {
    // root is re-initialized with
    // root of a modified tree.
    this.root = this.removeNode(this.root, data);
  }

  removeNode(node, key): any {
    // if the root is null then tree is
    // empty
    if (node === null) {
      return null;
    }
    // if data to be delete is less than
    // roots data then move to left subtree
    else if (key < node.data) {
      node.left = this.removeNode(node.left, key);
      return node;
    }

    // if data to be delete is greater than
    // roots data then move to right subtree
    else if (key > node.data) {
      node.right = this.removeNode(node.right, key);
      return node;
    }

    // if data is similar to the root's data
    // then delete this node
    else {
      // deleting node with no children
      if (node.left === null && node.right === null) {
        node = null;
        return node;
      }

      // deleting node with one children
      if (node.left === null) {
        node = node.right;
        return node;
      } else if (node.right === null) {
        node = node.left;
        return node;
      }

      // Deleting node with two children
      // minumum node of the rigt subtree
      // is stored in aux
      const aux = this.findMinNode(node.right);
      node.data = aux.data;

      node.right = this.removeNode(node.right, aux.data);
      return node;
    }
  }
}

export class SortNode {
  data: any;
  left: any;
  right: any;
  key: string;
  constructor(data, key) {
    this.data = data;
    this.left = null;
    this.right = null;
    this.key = key;
  }
}

export class BinarySearchTree {
  root: any;
  constructor() {
    this.root = null;
  }

  getRootNode(): any {
    return this.root;
  }

  insertData(data): void {
    const newNode = new Node(data);

    if (this.root === null) {
      this.root = newNode;
    } else {
      this.insertNode(this.root, newNode);
    }
  }

  insertNode(node, newNode): void {
    if (newNode.data < node.data) {
      if (node.left === null) {
        node.left = newNode;
      } else {
        this.insertNode(node.left, newNode);
      }
    } else {
      if (node.right === null) {
        node.right = newNode;
      } else {
        this.insertNode(node.right, newNode);
      }
    }
  }

  inorderTraverse(node): void {
    if (node !== null) {
      this.inorderTraverse(node.left);
      this.inorderTraverse(node.right);
    }
  }

  preorderTraverse(node): void {
    if (node !== null) {
      this.preorderTraverse(node.left);
      this.preorderTraverse(node.right);
    }
  }

  postorderTraverse(node): void {
    if (node !== null) {
      this.postorderTraverse(node.left);
      this.postorderTraverse(node.right);
    }
  }

  descOrderTraversal(node): void {
    if (node !== null) {
      this.descOrderTraversal(node.right);
      this.descOrderTraversal(node.left);
    }
  }

  findMinNode(node): any {
    if (node.left === null) {
      return node;
    } else {
      return this.findMinNode(node.left);
    }
  }

  search(node, data): any {
    // if trees is empty return null
    if (node === null) {
      return null;
    }
    // if data is less than node's data
    // move left
    else if (data < node.data) {
      return this.search(node.left, data);
    }
    // if data is less than node's data
    // move left
    else if (data > node.data) {
      return this.search(node.right, data);
    }
    // if data is equal to the node data
    // return node
    else {
      return node;
    }
  }

  remove(data): void {
    // root is re-initialized with
    // root of a modified tree.
    this.root = this.removeNode(this.root, data);
  }

  removeNode(node, key): any {
    // if the root is null then tree is
    // empty
    if (node === null) {
      return null;
    }
    // if data to be delete is less than
    // roots data then move to left subtree
    else if (key < node.data) {
      node.left = this.removeNode(node.left, key);
      return node;
    }

    // if data to be delete is greater than
    // roots data then move to right subtree
    else if (key > node.data) {
      node.right = this.removeNode(node.right, key);
      return node;
    }

    // if data is similar to the root's data
    // then delete this node
    else {
      // deleting node with no children
      if (node.left === null && node.right === null) {
        node = null;
        return node;
      }

      // deleting node with one children
      if (node.left === null) {
        node = node.right;
        return node;
      } else if (node.right === null) {
        node = node.left;
        return node;
      }

      // Deleting node with two children
      // minumum node of the rigt subtree
      // is stored in aux
      const aux = this.findMinNode(node.right);
      node.data = aux.data;

      node.right = this.removeNode(node.right, aux.data);
      return node;
    }
  }
}

export class Node {
  data: any;
  left: any;
  right: any;
  constructor(data) {
    this.data = data;
    this.left = null;
    this.right = null;
  }
}

export class SortListNavData {
  static listNavSorting(listData, sortingKey: string): any {
    let sortingParams: any;
    if (sortingKey === 'listName') {
      sortingParams = this.setListNavSorting(listData, [
        {
          sortingKey: 'listNameLowerCase',
          sortingOrder: AppConst.CONST.sortOrderAsc,
          ignoreKey: [-1],
        },
        {
          sortingKey: 'bullish',
          sortingOrder: AppConst.CONST.sortOrderDesc,
          ignoreKey: [],
        },
      ]);
    } else {
      sortingParams = this.setListNavSorting(listData, [
        {
          sortingKey: 'bullish',
          sortingOrder: AppConst.CONST.sortOrderDesc,
          ignoreKey: [-1],
        },
      ]);
    }
    return Sort.sortUnique(sortingParams);
  }

  static setListNavSorting(data: Array<any>, sortObj): SortingModel {
    const len = sortObj.length - 1;
    const sortingModelData = new SortingModel();
    const sortLevelKey = [];

    sortObj.forEach((element, index) => {
      let sorting: SortingModel;
      if (index === 0) {
        sorting = sortingModelData;
        sorting.sortData = data;
      } else {
        sortingModelData.extendedData = new SortingModel();
        sorting = sortingModelData.extendedData;
        sorting.sortData = [];
      }

      sorting.sortKey = element.sortingKey;
      sorting.sortOrder = element.sortingOrder;
      sorting.ignoreDataKeys = element.ignoreKey;
      sorting.sortLevelKey = sortLevelKey;
      sorting.ignoreData = [];
      sorting.isExtended = index === len ? false : true;
      sortLevelKey.push(element.sortingKey);
    });

    return sortingModelData;
  }
}

export class ListNavigationSort {
  static listNavSorting(listData, sortingKey: string): any {
    let sortingParams: any;
    if (sortingKey === 'listName') {
      sortingParams = this.setListNavSortingParams(listData, [
        {
          sortingKey: 'listNameFormatted',
          sortingOrder: AppConst.CONST.sortOrderAsc,
          ignoreKey: [-1],
        },
        {
          sortingKey: 'powerBarBullish',
          sortingOrder: AppConst.CONST.sortOrderDesc,
          ignoreKey: [],
        },
      ]);
    } else {
      sortingParams = this.setListNavSortingParams(listData, [
        {
          sortingKey: 'powerBarBullish',
          sortingOrder: AppConst.CONST.sortOrderDesc,
          ignoreKey: [-1],
        },
      ]);
    }
    return Sort.sortUnique(sortingParams);
  }

  static quickBarSorting(listData, sortingKey: string): any {
    let sortingParams: any;
    if (sortingKey === 'symbol') {
      sortingParams = this.setListNavSortingParams(listData, [
        {
          sortingKey: 'symbol',
          sortingOrder: AppConst.CONST.sortOrderAsc,
          ignoreKey: [-1],
        },
        {
          sortingKey: 'pgr',
          sortingOrder: AppConst.CONST.sortOrderDesc,
          ignoreKey: [],
        },
      ]);
    } 
    return Sort.sortUnique(sortingParams);
  }


  static setListNavSortingParams(data: Array<any>, sortObj): SortingModel {
    const len = sortObj.length - 1;
    const sortingModelData = new SortingModel();
    const sortLevelKey = [];

    sortObj.forEach((element, index) => {
      let sorting: SortingModel;
      if (index === 0) {
        sorting = sortingModelData;
        sorting.sortData = data;
      } else {
        sortingModelData.extendedData = new SortingModel();
        sorting = sortingModelData.extendedData;
        sorting.sortData = [];
      }

      sorting.sortKey = element.sortingKey;
      sorting.sortOrder = element.sortingOrder;
      sorting.ignoreDataKeys = element.ignoreKey;
      sorting.sortLevelKey = sortLevelKey;
      sorting.ignoreData = [];
      sorting.isExtended = index === len ? false : true;
      sortLevelKey.push(element.sortingKey);
    });

    return sortingModelData;
  }

  
}

export class IndustryExposureSort {
  static IndustryExposureSorting(listData, sortingKey: string): any {
    let sortingParams: any;
    if (sortingKey === 'listName') {
      sortingParams = this.setIndustryExposureSortingParams(listData, [
        {
          sortingKey: 'listNameFormatted',
          sortingOrder: AppConst.CONST.sortOrderAsc,
          ignoreKey: [-1],
        },
        {
          sortingKey: 'powerBarBullish',
          sortingOrder: AppConst.CONST.sortOrderDesc,
          ignoreKey: [],
        },
      ]);
    } else {
      sortingParams = this.setIndustryExposureSortingParams(listData, [
        {
          sortingKey: 'powerBarBullish',
          sortingOrder: AppConst.CONST.sortOrderDesc,
          ignoreKey: [-1],
        },
      ]);
    }
    return Sort.sortUnique(sortingParams);
  }

  static setIndustryExposureSortingParams(data: Array<any>, sortObj): SortingModel {
    const len = sortObj.length - 1;
    const sortingModelData = new SortingModel();
    const sortLevelKey = [];

    sortObj.forEach((element, index) => {
      let sorting: SortingModel;
      if (index === 0) {
        sorting = sortingModelData;
        sorting.sortData = data;
      } else {
        sortingModelData.extendedData = new SortingModel();
        sorting = sortingModelData.extendedData;
        sorting.sortData = [];
      }

      sorting.sortKey = element.sortingKey;
      sorting.sortOrder = element.sortingOrder;
      sorting.ignoreDataKeys = element.ignoreKey;
      sorting.sortLevelKey = sortLevelKey;
      sorting.ignoreData = [];
      sorting.isExtended = index === len ? false : true;
      sortLevelKey.push(element.sortingKey);
    });

    return sortingModelData;
  }
}

export class RedirectParameter {
  static getNavigationExtrasDataForResearch(symbol: string, duration: string, watch_List_Id?: number, portfolio_List_Id?: number , page?:string): NavigationExtras {
    let navigationExtras: NavigationExtras = {};
    // if (watch_List_Id && duration) {
    //   navigationExtras = {
    //     queryParams: {
    //       "data": JSON.stringify({ symbol: symbol, duration: duration, watch_List_Id: watch_List_Id })
    //     }
    //   };
    // } else if (watch_List_Id) {
    //   navigationExtras = {
    //     queryParams: {
    //       "data": JSON.stringify({ symbol: symbol, watch_List_Id: watch_List_Id })
    //     }
    //   };
    // } else if (portfolio_List_Id) {
    //   navigationExtras = {
    //     queryParams: {
    //       "data": JSON.stringify({ symbol: symbol, portfolio_List_Id: portfolio_List_Id })
    //     }
    //   };
    // } else 
    if (duration) {
      navigationExtras = {
        queryParams: {
          'data': JSON.stringify({ symbol: symbol, page: duration })
        }
      };
    } else {
      navigationExtras = {
        queryParams: {
          'data': JSON.stringify({ symbol: symbol })
        }
      };
    }
    return navigationExtras;
  }

  
  

}





