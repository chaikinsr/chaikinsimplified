import { ConeColumn } from "@amcharts/amcharts4/charts";
import { ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from "@angular/core";
import { CookieService } from "ngx-cookie-service";
import { map } from "rxjs/operators";
import { ProductMapping } from "src/app/core/data-models/app-model";
import { ProductCodesAndPreferenceObject } from "src/app/core/data-models/service-model";
import { APIService } from "src/app/core/http-services/api.service";
import { AuthService } from "src/app/core/http-services/auth.service";
import { AppDataService } from "src/app/core/services/app-data.service";
import { environment } from "src/environments/environment";

export class TopHeaderData {
    status: boolean;
    productMapping: ProductMapping;
    productPreference: ProductCodesAndPreferenceObject;
    page: string;
}

@Component({
    selector: 'app-applet-header',
    templateUrl: './applet-header.component.html',
    styleUrls: ['./applet-header.component.scss'],
})
export class AppletHeaderComponent implements OnInit, OnChanges {
    uid: any;
    token: string;
    env: string = environment.env;
    productMapping: ProductMapping;
    productPreference: ProductCodesAndPreferenceObject;
    @Input() page: string;
    topHeaderData: TopHeaderData;
    constructor(
        private cookieService: CookieService,
        private apiService: APIService,
        private authService: AuthService,
        private appDataService: AppDataService,
        private cd: ChangeDetectorRef
    ){}
    ngOnChanges(changes: SimpleChanges): void {
      
    }
    ngOnInit(): void {
        this.initialData()
    }

    initialData() {
        this.getProductCodesAndPreference();
    }

    getProductCodesAndPreference(): void {
        this.token = this.getToken();
        // if(!this.token){
        //   this.token = this.cookieService.get('app-token');
        // } 
        if (this.env === 'local') {
          this.token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImhhcmRlZXAuZGVvbEBwYXhjZWwubmV0IiwiY3VzdG9tZXJOdW1iZXIiOiJTQUMwMDIzMTAzNjQ0Iiwic3Vic2NyaXB0aW9ucyI6IkNIQVAsQ1BHSSxDUFAsQ1BXLENQR1IsQ0xTVCIsImlhdCI6MTY0MDYwNzQ0NiwiZXhwIjoxNjQwNjkzODQ2fQ.HKjXTKCvGKWbYBNrSycNADWU3PsoQYoFAZgq3Mi6cwA"
        }

        const httpProductCodesAndPreference$ = this.apiService.getProductCodesAndPreference(this.token)

        httpProductCodesAndPreference$
        .pipe(
            map((resp) => {
            if (resp["status"]  === "true") {
                const productCodesAndPreferenceData = resp as unknown as ProductCodesAndPreferenceObject;
                return productCodesAndPreferenceData;
            }
            return new ProductCodesAndPreferenceObject();
            })
        )
        .subscribe((data: ProductCodesAndPreferenceObject) => {
            this.appDataService.productPreference = data;
            this.productPreference = data;
            this.productMapping = this.getMappingForProduct(data);
            this.appDataService.productMapping = this.productMapping;
            this.setTopHeaderData();
        });


        // this.apiService.getProductCodesAndPreference(this.token).subscribe((response) => {
        //     if (response["status"] === "true") {
        //         this.productsSubscriptionList = response['productCodes'];
        //         let productPreference = response['productPreference']
        //         this.unlockUserSubscriptions(productPreference);
        //         this.uid = response['uid'];
        //         // this.apiService.updateProductPreferance('NA',this.uid).subscribe((response)=>{
        //         //   console.log(response)
        //         // })
        //       } else {
        //         if (this.env != 'local') {
        //           this.cookieService.deleteAll();
        //           window.location.replace(window.location.protocol + "//" + window.location.host + "/login/");
        //         }
        //       }
        //      // this.emitProductListData(this.productsList)
        // });
    }

    setTopHeaderData() {
        let headerData = new TopHeaderData();
        headerData.status = true;
        headerData.page = this.page;
        headerData.productMapping = this.productMapping;
        headerData.productPreference = this.productPreference;
        this.topHeaderData = Object.assign({}, headerData);
       
    }

    getMappingForProduct(data: ProductCodesAndPreferenceObject): ProductMapping {
        let productCodes = data.productCodes;
        let userType = "CPGR"
        let cpgrCode = productCodes.includes("CPGR");
        let cppCode = productCodes.includes("CPP");
        let clstCode = productCodes.includes("CLST");
        let userPermissionData = new ProductMapping();
        userType = userType + this.getCode(cpgrCode, true) + "CPP" + this.getCode(cppCode, true) + "CLST" + this.getCode(clstCode, false);
        let userTypeDetail = this.authService.RoleNameProductTierMapping.find((item) => item.userType.toLocaleLowerCase() === userType.toLocaleLowerCase());
        if (userTypeDetail) {
            if (userTypeDetail.productTier === "Login") {
                // redirected to login page
                window.location.replace(window.location.protocol + "//" + window.location.host + "/login/");
            } else {
                userPermissionData = this.authService.productTiersMappingData[userTypeDetail.productTier];
            }
        }
        return userPermissionData;
    }

    getCode(code: boolean, withExt: boolean) {
        if(code) {
            return withExt ? "1-" : "1";
        } else {
            return withExt ? "0-" : "0";
        }
    }

    getToken(): any {
        let hostname = window.location.hostname.split(".")[0];
        if (hostname+'.chaikinanalytics.com' === window.location.hostname) {
            this.uid = this.cookieService.get(hostname+'-uid');
          return this.cookieService.get(hostname+'-token');
        }
        // if ('dev-login.chaikinanalytics.com' === window.location.hostname || 'dev.chaikinanalytics.com' === window.location.hostname) {
        //   this.uid = this.cookieService.get('dev-uid');
        //   return this.cookieService.get('dev-token');
        // } else if ('localhost' === window.location.hostname) {
        //   this.uid = this.cookieService.get('local-uid');
        //   return this.cookieService.get('local-token');
        // } else if ('qa.chaikinanalytics.com' === window.location.hostname) {
        //   this.uid = this.cookieService.get('qa-uid');
        //   return this.cookieService.get('qa-token');
        // }
        // else if ('staging.chaikinanalytics.com' === window.location.hostname) {
        //   this.uid = this.cookieService.get('staging-uid');
        //   return this.cookieService.get('staging-token');
        // }
        // else if ('app.chaikinanalytics.com' === window.location.hostname) {
        //   this.uid = this.cookieService.get('app-uid');
        //   return this.cookieService.get('app-token');
        // }
    
      }
}
