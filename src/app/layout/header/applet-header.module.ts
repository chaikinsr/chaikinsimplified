import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatNativeDateModule } from "@angular/material/core";
import { MatDialogModule } from "@angular/material/dialog";
import { MatDividerModule } from "@angular/material/divider";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatListModule } from "@angular/material/list";
import { MatTooltipModule } from '@angular/material/tooltip';

import { AppletHeaderComponent } from "./applet-header.component";
import { SharedModule } from "src/app/page/shared/shared.module";

import { TopHeaderComponent } from "./top-header/top-header.component";
import { BottomHeaderComponent } from "./bottom-header/bottom-header.component";
import { CrawlHeaderComponent } from "./crawl-header/crawl-header.component";
import { SubnavHeaderComponent } from "./subnav-header/subnav-header.component";



@NgModule({
    imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      HttpClientModule,
      MatButtonModule,
      MatCheckboxModule,
      MatDialogModule,
      MatDividerModule,
      MatFormFieldModule,
      MatIconModule,
      MatInputModule,
      MatListModule,
      MatNativeDateModule,
      MatTooltipModule,
      SharedModule
    ],
    declarations: [
      TopHeaderComponent,
      AppletHeaderComponent,
      BottomHeaderComponent,
      CrawlHeaderComponent,
      SubnavHeaderComponent,
    ],
    exports: [
      AppletHeaderComponent,
      TopHeaderComponent,
      BottomHeaderComponent,
      CrawlHeaderComponent,
      SubnavHeaderComponent,
    ],
    entryComponents: [
    ]
  })
  export class AppletHeaderModule { }