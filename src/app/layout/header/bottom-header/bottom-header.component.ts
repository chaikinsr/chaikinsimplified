import { stretch } from '@amcharts/amcharts4/.internal/core/utils/Math';
import { array, number, string } from '@amcharts/amcharts4/core';
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import {
  GlobalListWithPowbarModel,
  HeaderDataModel,
  NavigationListWithPowbarModel,
  ProductAccessModel,
  specialList,
  UserRoleMappingModel,
} from 'src/app/core/data-models/app-model';
import {
  GetListSymbolsData,
  GetSymbolsDataAccToList,
  LoginDetailModel,
  NavigationListWithPowbar,
  WordpressIdMapping,
} from 'src/app/core/data-models/service-model';
import { APIService } from 'src/app/core/http-services/api.service';
import { AppDataService } from 'src/app/core/services/app-data.service';
import { DataService } from 'src/app/core/services/data.service';
import { ToastService } from 'src/app/core/services/toast-service';
import {
  ListNavigationSort,
  LocalStorage,
  SharedUtil,
} from 'src/app/core/services/utilily/utility';

// Components
import { AddTickersToListComponent } from 'src/app/page/shared/components/add-tickers-to-list/add-tickers-to-list.component';
import { CreateListDialogComponent } from 'src/app/page/shared/components/create-list-dialog/create-list-dialog.component';
import { DuplicatewpDialogComponent } from 'src/app/page/shared/components/duplicatewp-dialog/duplicatewp-dialog.component';
import { EditListDialogComponent } from 'src/app/page/shared/components/edit-list-dialog/edit-list-dialog.component';
import { InfoDialogComponent } from 'src/app/page/shared/components/info-dialog/info-dialog.component';

@Component({
  selector: 'app-bottom-header',
  templateUrl: './bottom-header.component.html',
  styleUrls: ['./bottom-header.component.scss'],
})
export class BottomHeaderComponent implements OnInit, OnChanges {
  @Input() headerDataModel: HeaderDataModel = new HeaderDataModel();
  @Input() projectAccess: boolean;
  @Input() selectedListId: boolean;
  @Input() projectAccessDashboard: boolean;
  @Input() watchlistDataModel1: GlobalListWithPowbarModel;
  @Input() emailAlertModeValue: boolean;

  @Output() selectedWatchListId = new EventEmitter<any>();
  @Output() watchlistDataUpdate = new EventEmitter<any>();
  @Output() setEmailAlertModeValue = new EventEmitter<any>();
  showPortfolioList: boolean = false;
  createPortfolioListContainerXPosition: any = 0;
  createPortfolioListContainerYPosition: any = 0;

  selectedListIdData = {
    listName: 'My Stocks',
    listId: 1420416,
    symbolCount: 11,
  };

  arrayOfNonModifiedListName = [
    'My Stocks',
    'Power Gauge Investor - Special Opportunities',
    'Power Gauge Investor - Industry Monitor Portfolio',
    'Power Gauge Report',
  ];
  saveUsername = false;

  // specialList
  //   = {
  //     listName: "",
  //     listId: 1420416,
  //     symbolCount: 11
  //   };

  epsInfoDialogueValue: any;
  earningDetailwatchPanel: any;
  splittedText: any;
  arrayOfPowerBar = [];
  neutralPerCountStyle: number;
  bearishPerCountStyle: number;
  bullishPerCountStyle: number;
  documentClickEventRegister: boolean = false;
  @ViewChild('qickDescriptionPopup', { static: false })
  qickDescriptionPopup: any;
  showQuickScanDescription: boolean = false;
  createQickDescriptionXPosition: any = 0;
  createQickDescriptionYPosition: any = 0;
  fixedCreateQickDescriptionYPosition: any = 0;
  quickDescriptionId: string;
  quickDescriptionIdCheck: string;
  quickDescriptionDefaultText: string;
  userDetails: LoginDetailModel;
  showDashboard: boolean = false;
  // specialListArray: [];
  //this.cookieService.get('role') === 'Power Pulse'
  wordpressIdMapping: WordpressIdMapping = new WordpressIdMapping();
  watchlistDataModel: GlobalListWithPowbarModel =
    new GlobalListWithPowbarModel();
  pulseChaikinDataModel: GlobalListWithPowbarModel =
    new GlobalListWithPowbarModel();
  getListSymbolData = new GetListSymbolsData();
  productAccessModel: ProductAccessModel;
  mobileTooltipFlag = false;
  oldSelectedListId: any;
  userAccess: UserRoleMappingModel;
  // specialData:specialList[] = [];
  fixedScrollTop: any = 0;
  showDashboardHeader: boolean = false;
  private destroyed$ = new Subject();
  constructor(
    private api: APIService,
    private dataService: DataService,
    public router: Router,
    public appDataService: AppDataService,
    private route: ActivatedRoute,
    private cookieService: CookieService,
    public dialog: MatDialog,
    public toastService: ToastService
  ) {
    // const userDetail: LoginDetailModel = LocalStorage.getItem('userDetail');
    if (!this.userDetails) {
      this.userDetails = LocalStorage.getItem('userDetail');
    }
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.userAccess = this.appDataService.userRoleMappingModel;
    this.pulseChaikinDataModel = new GlobalListWithPowbarModel();
    this.getEarningData();
    if (this.watchlistDataModel1) {
      this.watchlistDataModel = this.watchlistDataModel1;
    } else {
      // if(this.productAccessModel && this.productAccessModel.productsSubscriptionList.length === 2){
      if (this.userAccess.userPermission.chaikinList) {
        this.getPulseChaikinListData();
      }
      this.getUserWatchlistData('onchange');
    }

    // this.this.selectedListId'
    //  if(!this.selectedListId){

    //   this.selectedListId =LocalStorage.getItem("listId")
    //  }

    // if(this.selectedListId !==  this.appDataService.oldSelectedListId){
    //   this.appDataService.oldSelectedListId = this.selectedListId
    //   this.oldSelectedListId = this.selectedListId;

    // }
    this.dataService.showProject.subscribe((event: boolean) => {
      if (event) {
        // this.getListSymbolsData('addSymbol');
        this.getUserWatchlistData('onchange');
      }
    });
    this.getEnabledAlertListId();
  }

  ngOnInit(): void {
    this.userAccess = this.appDataService.userRoleMappingModel;
    this.pulseChaikinDataModel = new GlobalListWithPowbarModel();
    if (this.appDataService.cookiesData.isMobile) {
      this.mobileTooltipFlag = true;
      setTimeout(() => {
        this.mobileTooltipFlag = false;
      }, 5000);
    }

    // this.productAccessModel = this.appDataService.productAccessModel;
    // if (this.productAccessModel && this.productAccessModel.productsSubscriptionList.length === 2){

    if (this.userAccess.userPermission.chaikinList) {
      this.getPulseChaikinListData();
    }
    if (this.projectAccessDashboard) {
      this.showDashboardHeader = true;
      this.projectAccess = true;
    } else if (!this.projectAccess) {
      this.showDashboardHeader = false;
      this.projectAccessDashboard = false;
    }

    if (this.appDataService.callOnceGetWatchListIdDataApiInBottomHeader === 0) {
      this.getUserWatchlistData('onchange');
      this.appDataService.callOnceGetWatchListIdDataApiInBottomHeader = 1;
    }

    //   this.getUserWatchlistData();
    // this.getListSymbolsData('addSymbol');
    this.dataService.showAllPopup
      .pipe(takeUntil(this.destroyed$))
      .subscribe((status) => {
        setTimeout(() => {
          if (status) {
            let powerBarDescription: HTMLElement = document.getElementById(
              'powerBarDescription'
            ) as HTMLElement;
            if (powerBarDescription) {
              powerBarDescription.click();
            }
            let industryrDescription: HTMLElement = document.getElementById(
              'industryDescription'
            ) as HTMLElement;
            if (industryrDescription) {
              industryrDescription.click();
            }
          } else {
            this.showQuickScanDescription = false;
            this.quickDescriptionIdCheck = '0';
          }
        }, 20);
      });
  }

  onClickedOutside(e: Event) {
    this.mobileTooltipFlag = false;
  }
  offClickHandler(event: any) {
    if (
      this.qickDescriptionPopup &&
      this.qickDescriptionPopup.nativeElement &&
      !this.qickDescriptionPopup.nativeElement.contains(event.target)
    ) {
      // check click origin
      this.showQuickScanDescription = false;
      if (this.documentClickEventRegister) {
        document.removeEventListener('click', this.offClickHandler.bind(this));
        this.documentClickEventRegister = false;
      }
    }
  }

  openQuickDescriptionPopup(id: string, defaultText: string, event) {
    event.stopPropagation();
    // if (!this.documentClickEventRegister) {
    //   document.addEventListener('click', this.offClickHandler.bind(this));
    //   this.documentClickEventRegister = true;
    // }
    if (!this.showQuickScanDescription) {
      this.onClickPortfolioOpenPopup(event);
      this.quickDescriptionId = id;
      this.quickDescriptionDefaultText = defaultText;
      this.openQuickDescription();
    } else {
      this.showQuickScanDescription = false;
      this.quickDescriptionIdCheck = '0';
    }
  }

  onClickPortfolioOpenPopup(event: any) {
    if (event.clientX > 0) {
      this.createQickDescriptionXPosition = event.clientX - 150;
      this.createQickDescriptionYPosition = event.clientY + 7;
      this.fixedCreateQickDescriptionYPosition =
        this.createQickDescriptionYPosition;
    } else {
      let element: HTMLElement;

      let powerBarDescription: HTMLElement = document.getElementById(
        'powerBarDescription'
      ) as HTMLElement;
      if (powerBarDescription) {
        element = powerBarDescription;
      }
      let industryrDescription: HTMLElement = document.getElementById(
        'industryDescription'
      ) as HTMLElement;
      if (industryrDescription) {
        element = industryrDescription;
      }

      //  console.log(industryrDescription);
      let topPos = element.getBoundingClientRect().top + window.scrollY;
      let leftPos = element.getBoundingClientRect().left + window.scrollX;
      this.createQickDescriptionXPosition = leftPos - 150;
      this.createQickDescriptionYPosition = 150 + 7;
      this.fixedCreateQickDescriptionYPosition =
        this.createQickDescriptionYPosition;
    }

    // this.fixedScrollTop = this.currentScrollTop;
  }

  openQuickDescription() {
    if (this.quickDescriptionId !== this.quickDescriptionIdCheck) {
      this.showQuickScanDescription = true;
      this.quickDescriptionIdCheck = this.quickDescriptionId;
    } else {
      this.showQuickScanDescription = false;
      this.quickDescriptionIdCheck = '0';
    }
  }

  onCloseQuickDescriptionPopupEvent() {
    this.showQuickScanDescription = false;
    this.quickDescriptionIdCheck = '';
  }
  getEarningData(): void {
    if (this.headerDataModel.epsDiffDescription) {
      this.splittedText = this.headerDataModel.epsDiffDescription.split(' ', 1);
      if (this.headerDataModel.earningsReportInfo === 1) {
        this.epsInfoDialogueValue = 1;
        //this.earningDetailwatchPanel = 4;
      } else if (this.headerDataModel.preReportInfo === 1) {
        this.epsInfoDialogueValue = 2;
      } else if (this.headerDataModel.estimateRevisionInfo === 1) {
        this.epsInfoDialogueValue = 3;
      } else {
        this.epsInfoDialogueValue = 4;
      }
    }
  }

  getPulseChaikinListData(): void {
    const httpPulseChaikinList$ = this.api.getPulseChaikinList();
    httpPulseChaikinList$
      .pipe(
        map((resp) => {
          if (resp.status) {
            const listData = resp.data as NavigationListWithPowbar[];
            // this.appDataService.userList = resp.data;
            return this.perparePulseChaikinListData(listData);
          }
          return [];
        })
      )
      .subscribe((pulseList: GlobalListWithPowbarModel) => {
        this.pulseChaikinDataModel.data.push(pulseList.data[0]);
        // this.pulseChaikinDataModel.data.length === 2 ? '' :  this.pulseChaikinDataModel.data.push(pulseList.data[0]);
        this.deleteDuplicateValueFRomArray();
        this.pulseChaikinDataModel.title = pulseList.title;
      });
  }

  getUserWatchlistData(value): void {
    const httpUserWatchlist$ = this.api.getUserWatchlistData(
      this.userDetails.UID
    );
    httpUserWatchlist$
      .pipe(
        map((resp) => {
          if (resp.status) {
            const userWatchlistData = resp.data as NavigationListWithPowbar[];
            this.appDataService.userList = resp.data;
            return this.perpareUserWatchlistData(userWatchlistData, value);
          }
          return [];
        })
      )
      .subscribe((userList: GlobalListWithPowbarModel) => {
        // this.appDataService.userList = userList;
      });
  }

  perparePulseChaikinListData(
    listData: NavigationListWithPowbar[]
  ): GlobalListWithPowbarModel {
    let pulseChaikinDataModel = new GlobalListWithPowbarModel();
    pulseChaikinDataModel.data = [];
    if (listData) {
      listData.forEach((data) => {
        const navigationListWithPowbarModel =
          new NavigationListWithPowbarModel();
        if (data) {
          if (LocalStorage.getItem('listId') === data.listId) {
            this.selectedListIdData.listId = data.listId;
            this.selectedListIdData.listName = data.listName;
            this.selectedListIdData.symbolCount = data.symbolCount;
            this.setPowerBarWidth(data.powerBar);
          }
        }
        navigationListWithPowbarModel.listId = data.listId;
        navigationListWithPowbarModel.hasChart = data.hasChart;
        navigationListWithPowbarModel.isExtenable = data.isExtenable;
        navigationListWithPowbarModel.description = data.description;
        navigationListWithPowbarModel.listName = data.listName;
        navigationListWithPowbarModel.listType = data.listType;
        navigationListWithPowbarModel.symbolCount = data.symbolCount;
        navigationListWithPowbarModel.powerBar = data.powerBar;
        navigationListWithPowbarModel.extendedData = undefined;
        navigationListWithPowbarModel.powerBarBullish = data.powerBar.green;
        navigationListWithPowbarModel.listNameFormatted =
          data.listName.toLowerCase();
        pulseChaikinDataModel.data.push(navigationListWithPowbarModel);
      });
    }
    return pulseChaikinDataModel;
  }

  perpareUserWatchlistData(
    listData: NavigationListWithPowbar[],
    value
  ): GlobalListWithPowbarModel {
    this.watchlistDataModel = new GlobalListWithPowbarModel();
    this.watchlistDataModel.data = [];

    this.setListIdOnListChange(listData);
    if (listData) {
      listData.forEach((data) => {
        const navigationListWithPowbarModel =
          new NavigationListWithPowbarModel();
        // const selectedData = new specialList();
        const userDetails = LocalStorage.getItem('userDetail');
        //   if(data.listName === 'Power Gauge Investor Portfolio'  || data.listName === 'Power Gauge Report'){
        //       selectedData.listName =  data.listName;
        //       selectedData.listId = data.listId;
        //       selectedData.symbolCount = data.symbolCount;
        //  }

        // this.loadWatchlistData(this.selectedListIdData);
        // console.log( this.selectedListIdData);
        navigationListWithPowbarModel.listId = data.listId;
        navigationListWithPowbarModel.hasChart = data.hasChart;
        navigationListWithPowbarModel.isExtenable = data.isExtenable;
        navigationListWithPowbarModel.description = data.description;
        navigationListWithPowbarModel.listName = data.listName;
        navigationListWithPowbarModel.listType = data.listType;
        navigationListWithPowbarModel.symbolCount = data.symbolCount;
        navigationListWithPowbarModel.powerBar = data.powerBar;
        navigationListWithPowbarModel.extendedData = undefined;
        navigationListWithPowbarModel.powerBarBullish = data.powerBar.green;
        navigationListWithPowbarModel.listNameFormatted =
          data.listName.toLowerCase();
        navigationListWithPowbarModel.powerBar = data.powerBar;
        navigationListWithPowbarModel.priorityList = data.priorityList;
        if (data.priorityList) {
          this.pulseChaikinDataModel.data.push(navigationListWithPowbarModel);
          // this.pulseChaikinDataModel.data.length === 2 ? '' :  this.pulseChaikinDataModel.data.push(navigationListWithPowbarModel);
        } else {
          this.watchlistDataModel.data.push(navigationListWithPowbarModel);
        }
        // this.watchlistDataModel.data.push(navigationListWithPowbarModel);
      });

      if (this.oldSelectedListId !== this.selectedListIdData.listId) {
        this.oldSelectedListId = this.selectedListIdData.listId;
        this.selectedWatchListId.emit(this.selectedListIdData);
      }

      this.deleteDuplicateValueFRomArray();
      if (value !== 'onchange') this.loadWatchlistData(this.selectedListIdData);
    }
    const sortedData = ListNavigationSort.listNavSorting(
      this.watchlistDataModel.data,
      'listName'
    );
    this.watchlistDataModel.data = sortedData;
    this.watchlistDataModel.title = 'My Lists';
    // this.setListIdOnListChange(listData);
    // console.log(this.watchlistDataModel);'
    return this.watchlistDataModel;
  }

  deleteDuplicateValueFRomArray(): any {
    let originalArray = this.pulseChaikinDataModel.data;
    let prop = 'listName';
    var newArray = [];
    var lookupObject = {};

    for (var i in originalArray) {
      lookupObject[originalArray[i][prop]] = originalArray[i];
    }

    for (i in lookupObject) {
      newArray.push(lookupObject[i]);
    }
    this.pulseChaikinDataModel.data = newArray;
  }

  setListIdOnListChange(data): void {
    let userDetails = LocalStorage.getItem('userDetail');
    // if(this.pulseChaikinDataModel.data.length >0  || this.productAccessModel.productsSubscriptionList.length !== 2){
    if (
      this.pulseChaikinDataModel.data.length > 0 ||
      this.userAccess.userPermission.tickerBanner
    ) {
      if (
        this.pulseChaikinDataModel.data.length > 0 &&
        +LocalStorage.getItem('listId') ===
          this.pulseChaikinDataModel.data[0].listId
      ) {
        this.selectedListIdData.listId =
          this.pulseChaikinDataModel.data[0].listId;
        this.selectedListIdData.listName =
          this.pulseChaikinDataModel.data[0].listName;
        this.selectedListIdData.symbolCount =
          this.pulseChaikinDataModel.data[0].symbolCount;
        this.setPowerBarWidth(this.pulseChaikinDataModel.data[0].powerBar);
        return;
      }

      if (
        LocalStorage.getItem('listId') === -1 ||
        LocalStorage.getItem('listId') === undefined
      ) {
        const listIdData = data.filter(
          (element) => element.listId === +userDetails.ListID
        );
        if (listIdData.length > 0) {
          LocalStorage.setItem('listId', listIdData[0].listId);
          this.selectedListIdData.listId = listIdData[0].listId;
          this.selectedListIdData.listName = listIdData[0].listName;
          this.selectedListIdData.symbolCount = listIdData[0].symbolCount;
          this.setPowerBarWidth(listIdData[0].powerBar);
          return;
        }
      } else {
        const listIdData1 = data.filter(
          (element) => element.listId === LocalStorage.getItem('listId')
        );
        if (listIdData1.length > 0) {
          LocalStorage.setItem('listId', listIdData1[0].listId);
          this.selectedListIdData.listId = listIdData1[0].listId;
          this.selectedListIdData.listName = listIdData1[0].listName;
          this.selectedListIdData.symbolCount = listIdData1[0].symbolCount;
          this.setPowerBarWidth(listIdData1[0].powerBar);
          return;
        } else {
          const listIdData = data.filter(
            (element) => element.listId === +userDetails.ListID
          );
          if (listIdData[0]) {
            LocalStorage.setItem('listId', listIdData[0].listId);
            this.selectedListIdData.listId = listIdData[0].listId;
            this.selectedListIdData.listName = listIdData[0].listName;
            this.selectedListIdData.symbolCount = listIdData[0].symbolCount;
            this.setPowerBarWidth(listIdData[0].powerBar);
          }
          return;
        }
      }
    }
  }

  loadWatchlistData(loadWatchlistData): void {
    LocalStorage.setItem('listId', loadWatchlistData.listId);
    this.appDataService.priceMovementRenderCount = 0;
    //  this.selectedListIdData = loadWatchlistData;
    this.selectedListIdData.listId = loadWatchlistData.listId;
    this.setPowerBarWidth(loadWatchlistData.powerBar);
    this.selectedListIdData.listName = loadWatchlistData.listName;
    this.selectedListIdData.symbolCount = loadWatchlistData.symbolCount;
    // LocalStorage.setItem("listId", loadWatchlistData.listId);
    this.selectedWatchListId.emit(loadWatchlistData);
  }

  setPowerBarWidth(powerbar: any): void {
    //  console.log("powerbar",powerbar);
    if (powerbar) {
      this.arrayOfPowerBar = [];

      for (var i in powerbar) {
        this.arrayOfPowerBar.push({ word: i, count: powerbar[i] });
      }
      // console.log(this.arrayOfPowerBar[0].count, this.arrayOfPowerBar[1].count)
      if (this.arrayOfPowerBar[0].count !== undefined) {
        const totalCount =
          this.arrayOfPowerBar[0].count +
          this.arrayOfPowerBar[1].count +
          this.arrayOfPowerBar[2].count;
        // tslint:disable-next-line: radix
        const bearishPer = Math.round(
          totalCount === 0
            ? 0
            : (this.arrayOfPowerBar[0].count / totalCount) * 100
        );
        // tslint:disable-next-line: radix
        const neutralPer = Math.round(
          totalCount === 0
            ? 0
            : (this.arrayOfPowerBar[2].count / totalCount) * 100
        );
        // tslint:disable-next-line: radix
        const bullishPer = Math.round(
          totalCount === 0
            ? 0
            : (this.arrayOfPowerBar[1].count / totalCount) * 100
        );

        this.neutralPerCountStyle = 30;
        const bearishCountPer =
          bearishPer === 0 ? 0 : (bearishPer / (bearishPer + bullishPer)) * 70;
        const bullishCountPer =
          bullishPer === 0 ? 0 : (bullishPer / (bearishPer + bullishPer)) * 70;
        if (bearishCountPer === 0 && bullishCountPer === 0) {
          this.bearishPerCountStyle = 35;
          this.bullishPerCountStyle = 35;
        } else if (20 - bearishCountPer > 0) {
          this.bearishPerCountStyle = 20;
          this.bullishPerCountStyle = bullishCountPer - (20 - bearishCountPer);
        } else if (20 - bullishCountPer > 0) {
          this.bearishPerCountStyle = bearishCountPer - (20 - bullishCountPer);
          this.bullishPerCountStyle = 20;
        } else {
          this.bearishPerCountStyle = bearishCountPer;
          this.bullishPerCountStyle = bullishCountPer;
        }
        if (this.arrayOfPowerBar[0].count === 0) {
          this.bearishPerCountStyle = 0;
        } else if (this.arrayOfPowerBar[1].count === 0) {
          this.bullishPerCountStyle = 0;
        }
      }
    }
  }

  // Export List
  exportListReport(): void {
    const exportResultData = this.getExportResultData();
    const fileName: string = this.getExportResultFileName();
    if (exportResultData.length > 0) {
      this.downloadFile(exportResultData, fileName);
    }
  }

  getListSymbolsData(value): void {
    this.api
      .getListSymbols(this.selectedListIdData.listId, this.userDetails.UID)
      .subscribe((resp) => {
        // let getListSymbolsData = new GetListSymbolsData
        this.getListSymbolData.PowerBar = resp.PowerBar;
        this.getListSymbolData.listRating = resp.listRating;
        this.getListSymbolData.list_id = resp.list_id;
        //const symbol  = this.prepareListSymbolData(resp.symbols);
        this.getListSymbolData.symbols = this.prepareListSymbolData(
          resp.symbols
        );
        // console.log(resp);
        if (value === 'export') {
          this.exportListReport();
        }
      });
  }

  prepareListSymbolData(data): GetSymbolsDataAccToList[] {
    let getSymbolsDataAccToListArray: GetSymbolsDataAccToList[] = [];
    const b = [];
    data.forEach((data) => {
      const a = data.name.split(',');

      const getSymbolsDataAccToListObject = new GetSymbolsDataAccToList();
      getSymbolsDataAccToListObject.symbol = data.symbol;
      getSymbolsDataAccToListObject.Change = data.Change;
      getSymbolsDataAccToListObject.Last = data.Last;
      getSymbolsDataAccToListObject.PGR = data.PGR;
      getSymbolsDataAccToListObject.Percentage = data.Percentage;
      getSymbolsDataAccToListObject.SummaryRating = data.SummaryRating;
      getSymbolsDataAccToListObject.TechnicalRating = data.TechnicalRating;
      getSymbolsDataAccToListObject.div_yield = data.div_yield;
      getSymbolsDataAccToListObject.filter = data.filter;
      getSymbolsDataAccToListObject.industry_ListID = data.industry_ListID;
      getSymbolsDataAccToListObject.industry_name = data.industry_name;
      getSymbolsDataAccToListObject.isNoteExist = data.isNoteExist;
      getSymbolsDataAccToListObject.is_etf_symbol = data.is_etf_symbol;
      getSymbolsDataAccToListObject.list_rating = data.list_rating;
      getSymbolsDataAccToListObject.market_cap = data.market_cap;
      getSymbolsDataAccToListObject.name = a[1] ? a[0] + a[1] : a[0];
      getSymbolsDataAccToListObject.raw_PGR = data.raw_PGR;
      getSymbolsDataAccToListObject.signals = data.signals;
      getSymbolsDataAccToListObject.pgr = SharedUtil.getPgrRating(
        data.PGR,
        data.raw_PGR
      );
      getSymbolsDataAccToListObject.ratingType =
        SharedUtil.getPgrNameWithoutNeutralValues(
          getSymbolsDataAccToListObject.pgr
        );

      getSymbolsDataAccToListArray.push(getSymbolsDataAccToListObject);
    });
    // this.getListSymbolData.symbols = getSymbolsDataAccToListArray;
    return getSymbolsDataAccToListArray;
  }

  private getExportResultData(): string[] {
    // let analysisChecklistData: AnalysisChecklistDataModel[] = [];
    const exportResultData: string[] = [];
    // const selectedData = this.checklistSelection.selected;
    this.getListSymbolData.symbols =
      this.getListSymbolData.symbols.length > 0
        ? this.getListSymbolData.symbols
        : [];

    if (this.getListSymbolData.symbols) {
      exportResultData.push('Ticker,Name,Rating,');
      this.getListSymbolData.symbols.forEach((item) => {
        //  const pgrNameBasedOnRating = item.pgrNameBasedOnRating;
        const csvString =
          item.symbol + ',' + item.name + ',' + item.ratingType + ',';
        exportResultData.push(csvString);
      });
    }
    return exportResultData;
  }

  getExportResultFileName(): string {
    const today = new Date();
    const dd = today.getDate();
    const mm = today.getMonth() + 1;
    const yyyy = today.getFullYear();
    const date: string = dd + '.' + mm + '.' + yyyy;
    const fileName = this.selectedListIdData.listName + ' ' + date + '.csv';
    return fileName;
  }

  private downloadFile(data: string[], fileName: string): void {
    const csvArray = data.join('\r\n');

    const blob = new Blob([csvArray], { type: 'text/csv' });
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = fileName;
    a.click();
    window.URL.revokeObjectURL(url);
    a.remove();
  }

  /// Create List

  createNewList(event: any, symbol): void {
    event.stopPropagation();
    const dialogRef = this.dialog.open(CreateListDialogComponent, {
      data: symbol,
    });
    dialogRef.afterClosed().subscribe((listId) => {
      if (listId) {
        LocalStorage.setItem('listId', listId);
        //  if (!symbol){
        this.getUserWatchlistData('');
        // }else{

        //   this.perpareUserWatchlistData(listId,"onchange" );
        this.watchlistDataUpdate.emit();
        this.selectedWatchListId.emit(symbol);
        // }

        // this.selectedWatchListId.emit(LocalStorage.getItem('listId'));

        // this.getUserWatchlistData('');
        this.toastService.showSuccess('successfully added');
      }
    });
  }

  deleteList(event: any, listData) {
    let listname: string;
    event.stopPropagation();
    this.watchlistDataModel.data.forEach((element) => {
      if (element.listId === listData.listId) {
        this.getConfirmationFromUserToDelete(element);
      }
    });
    if (
      this.selectedListIdData.listName ===
        'Power Gauge Investor - Special Opportunities' ||
      this.selectedListIdData.listName ===
        'Power Gauge Investor - Industry Monitor Portfolio' ||
      this.selectedListIdData.listName === 'PowerTactics Portfolio'
    ) {
      this.getConfirmationFromUserToDelete(listData);
    }

    if (this.pulseChaikinDataModel.data[0].listId === listData.listId) {
      this.getConfirmationFromUserToDelete(this.pulseChaikinDataModel.data[0]);
    }
  }

  getConfirmationFromUserToDelete(list: NavigationListWithPowbarModel): void {
    if (
      list.listName === 'My Stocks' ||
      list.listName === 'Power Gauge Investor - Special Opportunities' ||
      list.listName === 'Power Gauge Investor - Industry Monitor Portfolio' ||
      list.listName === 'Power Gauge Report Portfolio' ||
      list.listName === 'PowerTactics Portfolio'
    ) {
      this.toastService.showDanger(
        "You can't delete " + '"' + list.listName + '"'
      );
      return;
    }

    const msg = 'Are you sure you want to delete this list?';
    const dialogRef = this.dialog.open(InfoDialogComponent, {
      data: {
        message: msg,
        noButtonText: 'No, I changed my mind',
        yesButtonText: 'Yes, delete',
      },
    });

    dialogRef.afterClosed().subscribe((dialogResult) => {
      const result = dialogResult;
      if (result) {
        this.selectedWatchListId.emit(list.listName);
        // if (this.selectedListNavModel['listId'] === list.listId) {
        //   const selectedListNavModel = new SelectedListNavModel();
        //   const selectedListObj = this.listData.data.find(data => data.listId === this.userDetailListId);
        //   selectedListNavModel.listId = selectedListObj.listId;
        //   selectedListNavModel.listType = selectedListObj.listType;
        //   selectedListNavModel.listName = selectedListObj.listName;
        //   this.selectedList.emit(selectedListNavModel);
        // }
        this.deleteUserList(list);
        // this.toastService.showSuccess('successfully added');
      }
    });
  }

  deleteUserList(list: NavigationListWithPowbarModel): void {
    //  console.log(list);
    const userDetails: LoginDetailModel = LocalStorage.getItem('userDetail');
    const listId = list.listId;
    const httpDeleteUserList$ = this.api.deleteStockList(
      listId,
      userDetails.UID
    );

    httpDeleteUserList$
      .pipe(
        map((resp: any) => {
          return resp.Status;
        })
      )
      .subscribe((status: boolean) => {
        if (status) {
          this.getUserWatchlistData('');
          this.toastService.showSuccess('Successfully removed');
          // this.store.dispatch(
          //   ListNavigationAction.updateUserLists({
          //     isUserListUpdate: false,
          //   })
          //  );
        } else {
          //  this.toastService.showDanger('Please try again');
        }
      });
  }

  editList(event: any, listData) {
    let list: any;
    this.watchlistDataModel.data.forEach((element) => {
      if (element.listId === listData.listId) {
        list = element;
      }
    });
    if (!list) {
      if (this.pulseChaikinDataModel.data[0].listId === listData.listId) {
        list = this.pulseChaikinDataModel.data[0];
      }
    }
    if (
      listData.listName === 'Power Gauge Investor - Special Opportunities' ||
      listData.listName ===
        'Power Gauge Investor - Industry Monitor Portfolio' ||
      list.listName === 'My Stocks' ||
      list.listName === 'Power Gauge Report Portfolio' ||
      list.listName === 'PowerTactics Portfolio'
    ) {
      list
        ? this.toastService.showDanger(
            "You can't edit " + '"' + list.listName + '"'
          )
        : this.toastService.showDanger(
            "You can't edit " + '"' + listData.listName + '"'
          );
      return;
    }
    event.stopPropagation();
    const headerText = 'Update Watchlist';
    const componentTaskText = `Enter a new name for your "${list.listName}"`;
    const matInputLabelText = 'Enter new name';
    const btnCancelText = 'Cancel';
    const btnSaveText = 'Update';
    const screenName = list.listName;

    const dialogRef = this.dialog.open(EditListDialogComponent, {
      autoFocus: true,
      data: {
        headerText: headerText,
        componentTaskText: componentTaskText,
        matInputLabelText: matInputLabelText,
        btnCancelText: btnCancelText,
        btnSaveText: btnSaveText,
        screenName: screenName,
      },
    });

    dialogRef.afterClosed().subscribe((dialogResult) => {
      const result = dialogResult;
      if (result) {
        this.api
          .editSymbolList(list.listId, result, this.userDetails.UID)
          .subscribe((resp) => {
            if (resp['msgForUpdateListName'] === 'successfully rename list') {
              const msg = `"${screenName}" successfully renamed to "${result}".`;
              this.getUserWatchlistData('');
              this.toastService.showSuccess(msg);
              // this.store.dispatch(
              //   ListNavigationAction.updateUserLists({
              //     isUserListUpdate: false,
              //   })
              //  );
            }
          });
        // setTimeout(() => {
        //   // if (this.selectedListNavModel['listId'] === list.listId) {
        //   //   const selectedListNavModel = new SelectedListNavModel();
        //   //   const selectedListObj = this.listData.data.find(data => data.listId === this.selectedListNavModel['listId']);
        //   //   selectedListNavModel.listId = selectedListObj.listId;
        //   //   selectedListNavModel.listType = selectedListObj.listType;
        //   //   selectedListNavModel.listName = result;
        //   //   this.selectedList.emit(selectedListNavModel);
        //   // }
        // }, 1000);
      }
    });
  }

  openPopupToAddSymbolsInList(event: MouseEvent): void {
    if (
      this.selectedListIdData.listName === 'Power Gauge Report Portfolio' ||
      this.selectedListIdData.listName ===
        'Power Gauge Investor - Special Opportunities' ||
      this.selectedListIdData.listName ===
        'Power Gauge Investor - Industry Monitor Portfolio' ||
      this.selectedListIdData.listName === 'PowerTactics Portfolio'
    ) {
      this.toastService.showDanger(
        "You can't add symbols to  " +
          '"' +
          this.selectedListIdData.listName +
          '"'
      );
      return;
    }
    this.getListSymbolsData('addSymbol');
    this.openAddTickersToListDialog();
  }

  openAddTickersToListDialog(): void {
    const dialogRef = this.dialog.open(AddTickersToListComponent, {
      data: {
        listId: this.selectedListIdData.listId,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.getUserWatchlistData('');
        this.toastService.showSuccess('Successfully added');
      }
    });
  }

  createDuplicateFromPortfolioList(event, portfolio) {
    event.stopPropagation();
    this.getListSymbolsData('addSymbol');
    if (portfolio) {
      const symbols = this.getListSymbolData.symbols;
      // portfolio.etfSymbols = symbols as ETFSymbolsDetail[];
      this.openCreateDuplicateWP(portfolio);
    }
  }

  openCreateDuplicateWP(portfolioData) {
    this.getListSymbolsData('');
    //  console.log(this.getListSymbolData, portfolioData);
    const dialogRef = this.dialog.open(DuplicatewpDialogComponent, {
      data: {
        isWatchListSelected: false,
        watchlist: this.selectedListIdData,
        watchlistSymbolsData: this.selectedListIdData,
        stepZeroHeaderText: 'Copy and save this list as a new:',
        portfolioData: this.getListSymbolData,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.selectedListIdData.listId = result.listId;
        this.selectedListIdData.listId = result.listName;
        //this.selectedListIdData.listId = result.listId;
        LocalStorage.setItem('listId', result.listId);
        this.getUserWatchlistData('');
        this.selectedWatchListId.emit(result.listName);
        this.toastService.showSuccess('Successfully added');
      }
    });
  }

  afterDeletedSelectedList(): void {}

  onClickPortfolioOpenPopup1(event: any) {
    this.createPortfolioListContainerXPosition = event.clientX - 250;
    this.createPortfolioListContainerYPosition = event.clientY + 7;
    //TBD
    this.showPortfolioList = true;
    // this.updateUI();
  }

  onClosePortfolioListPopupEvent(list: any) {
    //TBD
    //
    if (list) {
      // this.closePortfolioData.emit(list);
      //   if (list.updateList) {
      //     if (list.listName) {
      //       this.getEtfsThroughPortfolio(list.listName, null);
      //     } else {
      //       this.getEtfsThroughPortfolio(null, list.listId);
      //     }
      //   } else {
      //     this.selectPortfolio(list.porfolioSelected).then(resp => {
      //       list.etfSymbols = resp;
      //       this.showPortfolioList = false;
      //     });
      //   }
    }
    this.showPortfolioList = false;
    // this.updateUI();
  }

  onClose() {
    this.showPortfolioList = false;
  }
  // onCreateNewPortfoliolist() {

  //   const dialogRef = this.dialog.open(CreateListDialogComponent, {
  //     data: { isNewPortfolio: true, symbol: this.symbol, portfolio: null }
  //   });
  //   dialogRef.afterClosed().subscribe(result => {

  //     if (result) {
  //       const findList = this.portfolioList.find((wl) => wl.listId === this.selectedPortfolioId);
  //       this.messageToClose.emit({
  //         listName : result,
  //         listId: null,
  //         updateList: true,
  //         porfolioSelected: findList
  //       });
  //      // this.toastService.showSuccess('Portfolio successfully created');
  //     }

  //   });
  // }

  addTickerIntoList(symbol, list): void {
    const userDetail: LoginDetailModel = LocalStorage.getItem('userDetail');
    this.api
      .addStockIntoList(symbol, list.listId, userDetail.UID)
      .subscribe((resp) => {
        if (resp) {
          LocalStorage.setItem('listId', list.listId);
          // this.headerDataModel.symbol= symbol;
          this.watchlistDataUpdate.emit(true);
          this.toastService.showSuccess('Successfully added');
        }

        //this.getUserWatchlistData('');
      });
  }

  redirectOnPgr(list): void {
    LocalStorage.setItem('listId', -1);
    // LocalStorage.setItem('mainSymbol', symbol);
    this.router.navigate(
      ['/']
      // window.location.replace(
      //   window.location.protocol +
      //   '//' +
      //   window.location.host +
      //   '/pgr/'
    );
  }

  clearRecentSearch(event: Event): void {
    event.stopPropagation();
    this.dataService.recentlyViewedData.clearSymbolsMetoInfo();
  }

  get recentSearchs(): string[] | null {
    const recentSearchs = localStorage.getItem('PGR_RecentlyViewedSymbols');
    if (recentSearchs) {
      return JSON.parse(recentSearchs);
    }
    return null;
  }

  deleteForEnabledAlertList(): void {
    this.api
      .deleteForEnabledPowerPulseAlertList(
        this.selectedListId,
        this.userDetails.UID
      )
      .subscribe((resp) => {
        this.setEmailAlertModeValue.emit(this.saveUsername);
      });
  }

  addListForEnableAlert(value): void {
    this.saveUsername = value;
    if (this.saveUsername) {
      this.api
        .addListForEnablePowerPulseAlert(this.selectedListId)
        .subscribe((resp) => {
          this.setEmailAlertModeValue.emit(this.saveUsername);
        });
    } else {
      this.deleteForEnabledAlertList();
    }
  }

  getEnabledAlertListId(): void {
    this.api
      .getPowerPulseEnabledAlertList(this.userDetails.UID)
      .subscribe((resp) => {
        if (resp.status) {
          for (let element of resp.listId) {
            if (element === +this.selectedListId) {
              this.saveUsername = true;
              break;
            } else {
              this.saveUsername = false;
            }
          }
        }
      });
  }
}
