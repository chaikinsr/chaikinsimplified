import {
  AfterViewInit,
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { element } from 'protractor';
import { Observable } from 'rxjs/internal/Observable';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { of } from 'rxjs/internal/observable/of';
import { mergeMap } from 'rxjs/internal/operators/mergeMap';
import { map } from 'rxjs/operators';
import { AppConfig } from 'src/app/core/app-config/app-config';
import { MarketIndicesDataModel } from 'src/app/core/data-models/app-model';
import {
  LoginDetailModel,
  MarketQuickViewBarData,
  SymbolsCsvPriceData,
} from 'src/app/core/data-models/service-model';
import { APIService } from 'src/app/core/http-services/api.service';
import { LocalStorage, SharedUtil } from 'src/app/core/services/utilily/utility';

@Component({
  selector: 'app-crawl-header',
  templateUrl: './crawl-header.component.html',
  styleUrls: ['./crawl-header.component.scss'],
})
export class CrawlHeaderComponent
  implements OnInit, OnChanges, OnDestroy, AfterViewInit {
  symbolCsv = 'SPY,DIA,QQQ,IWM,TLT,IEF,GLD';
 //symbolCsv= 'DIA,GLD,IEF,IWM,QQQ,SPY,TLT';
  marketIndicesData: MarketIndicesDataModel[] = [];
  refereshPriceDataOnIntervel: NodeJS.Timer;
  startIndex = 0;
  constructor(private api: APIService, public router: Router,
    private route: ActivatedRoute,) {}

  ngOnInit(): void {
    this.getMarketQuickBarData(this.symbolCsv);
  }
  ngOnChanges(): void {}
  ngOnDestroy(): void {
    clearInterval(this.refereshPriceDataOnIntervel);
  }
  ngAfterViewInit(){}

  getMarketQuickBarData(symbolCsv: string): void {
    forkJoin([
      this.getMarketIndicesData(symbolCsv),
      this.getSymbolsCsvPriceData(symbolCsv),
    ]).subscribe(([marketQuickViewBarData, symbolsCsvPriceData]) => {
      const symbolArray = symbolCsv.split(',');
      this.marketIndicesData = this.prepareMarketQuickViewBarData(
        symbolArray,
        marketQuickViewBarData,
        symbolsCsvPriceData
      );
    
      this.refereshPriceData();
    });

    // const requestApis = of('getMarketQuickViewBarData', 'getSymbolsCSVPrice');
    // requestApis
    //   .pipe(
    //     mergeMap((apiName: string) => {
    //       if (apiName === 'getMarketQuickViewBarData') {
    //         return this.getMarketIndicesData(this.symbolCsv);
    //       } else if (apiName === 'getSymbolsCSVPrice') {
    //         return this.getSymbolsCsvPriceData(this.symbolCsv);
    //       }
    //     })
    //   )
    //   .subscribe((data: any) => {
    //     console.log(data);
    //   });

  }

  redirectOnPgr(symbol): void {
    LocalStorage.setItem('mainSymbol', symbol);
    this.router.navigate(["/"]
    // window.location.replace(
    //   window.location.protocol +
    //   '//' +
    //   window.location.host +
    //   '/pgr/'
    );

  }

  getMarketIndicesData(symbolCsv: string): Observable<any> {
    return new Observable<any>((observer) => {
      const httpMarketQuickViewBar$ = this.api.getMarketQuickViewBarData(
        symbolCsv
      );
      httpMarketQuickViewBar$
        .pipe(
          map(
            (resp: {
              data: MarketQuickViewBarData[];
              msg: string;
              status: boolean;
            }) => {
              if (resp.status) {
                const marketQuickViewBarData = resp.data as MarketQuickViewBarData[];
                return marketQuickViewBarData;
              }
              return null;
            }
          )
        )
        .subscribe((data: MarketQuickViewBarData[]) => {
          if (data == null) {
            observer.next(null);
            observer.complete();
          } else {
            const marketQuickViewBarData: MarketQuickViewBarData[] = data;
            observer.next(marketQuickViewBarData);
            observer.complete();
          }
        });
    });
  }

  getSymbolsCsvPriceData(symbolCsv: string): Observable<any> {
    const userDetails:LoginDetailModel = LocalStorage.getItem('userDetail');
    return new Observable<any>((observer) => {
      const httpSymbolsCsvPrice$ = this.api.getSymbolsCSVPrice(symbolCsv,userDetails.UID);
      httpSymbolsCsvPrice$
        .pipe(
          map((resp: SymbolsCsvPriceData[]) => {
            if (resp.length > 0) {
              const symbolsCsvPriceData = resp as SymbolsCsvPriceData[];
              return this.prepareSymbolCsvData(symbolsCsvPriceData);
            }
            return null;
          })
        )
        .subscribe((data: SymbolsCsvPriceData[]) => {
          if (data == null) {
            observer.next(null);
            observer.complete();
          } else {
            const symbolsCsvPriceData: SymbolsCsvPriceData[] = data;
            observer.next(symbolsCsvPriceData);
            observer.complete();
          }
        });
    });
  }

  prepareSymbolCsvData(
    symbolsCsvPriceData: SymbolsCsvPriceData[]
  ): SymbolsCsvPriceData[] {
    symbolsCsvPriceData.forEach((data) => {
      data.percentage = data['Percentage '];
    });
    return symbolsCsvPriceData;
  }

  prepareMarketQuickViewBarData(
    symbolArray: string[],
    marketQuickViewBarData: MarketQuickViewBarData[],
    symbolsCsvPriceData: SymbolsCsvPriceData[]
  ): MarketIndicesDataModel[] {
    const marketIndicesDataArray: MarketIndicesDataModel[] = [];
    symbolArray.forEach((symbol, i) => {
      const marketIndicesData: MarketIndicesDataModel = new MarketIndicesDataModel();
      marketQuickViewBarData.forEach((marketQuickViewBarDataValue) => {
        if(marketQuickViewBarDataValue){
          if (
            symbol === marketQuickViewBarDataValue.symbol &&
            symbol === symbolsCsvPriceData[i].Symbol
          ) {
            marketIndicesData.listId = marketQuickViewBarDataValue.listId;
            marketIndicesData.hasChart = marketQuickViewBarDataValue.hasChart;
            marketIndicesData.symbol = marketQuickViewBarDataValue.symbol;
            marketIndicesData.isExtenable = marketQuickViewBarDataValue.isExtenable;
            marketIndicesData.description = marketQuickViewBarDataValue.description;
            marketIndicesData.listName = marketQuickViewBarDataValue.listName;
            marketIndicesData.listType = marketQuickViewBarDataValue.listType;
    
            marketIndicesData.change = symbolsCsvPriceData[i].Change;
            marketIndicesData.last = symbolsCsvPriceData[i].Last;
            const lastInString = SharedUtil.formatNumber(marketIndicesData.last);
            marketIndicesData.lastInString = lastInString;
            marketIndicesData.percentage = symbolsCsvPriceData[i].percentage;
            if (marketIndicesData.percentage) {
              let percentageChangeInString = SharedUtil.formatNumber(
                marketIndicesData.percentage
              );
              percentageChangeInString = SharedUtil.appendSymbol(
                percentageChangeInString,
                '%'
              );
              marketIndicesData.percentageChangeInString = SharedUtil.appendSymbol(
                percentageChangeInString,
                '()'
              );
            } else {
              marketIndicesData.percentageChangeInString = 'N/A';
            }
    
            marketIndicesData.pgr = symbolsCsvPriceData[i].corrected_pgr;
            marketIndicesData.isEtf = symbolsCsvPriceData[i].is_etf;
            marketIndicesData.rawPgr = symbolsCsvPriceData[i].raw_pgr;
    
            if (marketIndicesData.percentage > 0) {
              marketIndicesData.colorClass = 'positive';
              marketIndicesData.arrowDirection = 'arrow_upward';
            } else if (marketIndicesData.percentage < 0) {
              marketIndicesData.colorClass = 'negative';
              marketIndicesData.arrowDirection = 'arrow_downward';
            } else {
              marketIndicesData.colorClass = 'no-movement';
              marketIndicesData.arrowDirection = 'minimize';
            }
    
            
          }
        }
      });
      marketIndicesDataArray.push(marketIndicesData);
    });
   // console.log(marketIndicesDataArray);
    return marketIndicesDataArray;
  }

  moveBarToRightDirection(): void {
    const visibleBarArea = document.getElementById('viewed-area-of-bar');
    const leftPos = visibleBarArea.scrollLeft;
    const symbolArr = (this.symbolCsv).split(',');
    const elementRefId = 'chyron-item-' + symbolArr[this.startIndex];
    const elementRefBoundaryValue = document.getElementById(elementRefId).getBoundingClientRect();
    visibleBarArea.scrollLeft = leftPos + elementRefBoundaryValue.width;
    this.startIndex = (this.startIndex >= (symbolArr.length - 1)) ? symbolArr.length - 1 : this.startIndex + 1;
  }

  moveBarToLeftDirection(): void {
    const visibleBarArea = document.getElementById('viewed-area-of-bar');
    const leftPos = visibleBarArea.scrollLeft;
    const symbolArr = (this.symbolCsv).split(',');
    this.startIndex = this.startIndex === 0 ? 0 : this.startIndex - 1;
    const elementRefId = 'chyron-item-' + symbolArr[this.startIndex];
    const elementRefBoundaryValue = document.getElementById(elementRefId).getBoundingClientRect();
    visibleBarArea.scrollLeft = leftPos - elementRefBoundaryValue.width;
  }

  private refereshPriceData(): void {
    clearInterval(this.refereshPriceDataOnIntervel);
    this.refereshPriceDataOnIntervel = setInterval(async () => {
      this.getSymbolsCsvPriceData(this.symbolCsv).subscribe(
        (symbolsCsvPriceData) => {
          this.preparePriceUpdateData(this.symbolCsv, symbolsCsvPriceData);
        }
      );
    }, AppConfig.PriceDataInterval.INTERVEL);
  }

  preparePriceUpdateData(
    symbolCsv: string,
    symbolsCsvPriceData: SymbolsCsvPriceData[]
  ): MarketIndicesDataModel[] {
    const symbolArray = symbolCsv.split(',');
    const marketIndicesDataArray: MarketIndicesDataModel[] = [];
    this.marketIndicesData.forEach((previousData, i) => {
      symbolsCsvPriceData.forEach((updatesData) => {
        if (previousData.symbol === updatesData.Symbol) {
          previousData.change = updatesData.Change;
          previousData.last = updatesData.Last;
          const lastInString = SharedUtil.formatNumber(previousData.last);
          previousData.lastInString = lastInString;

          previousData.percentage = updatesData.percentage;
          if (previousData.percentage) {
            let percentageChangeInString = SharedUtil.formatNumber(
              previousData.percentage
            );
            percentageChangeInString = SharedUtil.appendSymbol(
              percentageChangeInString,
              '%'
            );
            previousData.percentageChangeInString = SharedUtil.appendSymbol(
              percentageChangeInString,
              '()'
            );
          } else {
            previousData.percentageChangeInString = 'N/A';
          }

          previousData.pgr = updatesData.corrected_pgr;
          previousData.isEtf = updatesData.is_etf;
          previousData.rawPgr = updatesData.raw_pgr;

          if (previousData.percentage > 0) {
            previousData.colorClass = 'positive';
            previousData.arrowDirection = 'arrow_upward';
          } else if (previousData.percentage < 0) {
            previousData.colorClass = 'negative';
            previousData.arrowDirection = 'arrow_downward';
          } else {
            previousData.colorClass = 'none';
            previousData.arrowDirection = 'arrow_upward';
          }
        }
      });
    });
    return marketIndicesDataArray;
  }
}
