import { ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges } from "@angular/core";
import { AlertComponentData, ProductAccessModel, ProductMapping } from "src/app/core/data-models/app-model";
import { ProductCodesAndPreferenceObject } from "src/app/core/data-models/service-model";
import { AppDataService } from "src/app/core/services/app-data.service";
import { TopHeaderData } from "../applet-header.component";

@Component({
    selector: 'app-subnav-header',
    templateUrl: './subnav-header.component.html',
    styleUrls: ['./subnav-header.component.scss'],
})

export class SubnavHeaderComponent implements OnInit, OnChanges {
    page: string;
    productMapping: ProductMapping;
    productPreference: ProductCodesAndPreferenceObject;
    @Input() headerData: TopHeaderData;
    @Input() earningCalenderDataLength1 : any;
    earningCalenderTotalDatalength : number;
    //  @Input() alertComponentData1:AlertComponentData;
    productAccessModel: ProductAccessModel;
    @Input() alertComponentData: AlertComponentData;
    alertComponentData1: AlertComponentData;
    setAccessValue: any;
    isMobile: boolean = false;
    constructor(
        public appDataService: AppDataService,private detectorRef: ChangeDetectorRef
    ) {
        // this.earningCalenderTotalDatalength = this.earningCalenderDataLength1?.period.events.length;
        // this.detectorRef.detectChanges();
    }
    ngOnChanges(): void {
        this.alertComponentData1 = this.alertComponentData;
        this.earningCalenderTotalDatalength = this.earningCalenderDataLength1;
        if (this.headerData) {
            //  if (this.appDataService.productAccessModel){
            this.setAccessValue = this.appDataService.userRoleMappingModel.userPermission;
            // }else{
            //     this.setAccessValueForPga = false;
            // }
            this.page = this.headerData.page;
            this.productMapping = this.headerData.productMapping;
            this.productPreference = this.headerData.productPreference;
        }
    }
    ngOnInit(): void {
     //   this.alertComponentData1 = this.alertComponentData;
        this.earningCalenderTotalDatalength = this.earningCalenderDataLength1;
        this.isMobile = this.appDataService.cookiesData.isMobile;
        this.setAccessValue = this.appDataService.userRoleMappingModel.userPermission;
        // if (this.appDataService.productAccessModel){

        //     this.setAccessValueForPga =  this.appDataService.productAccessModel.powerGaugeAnalytics;
        //     }else{
        //         this.setAccessValueForPga = false;
        //     }
        // if (this.appDataService.productAccessModel){
        //     this.productAccessModel =  this.appDataService.productAccessModel;
        //     }else{
        //         this.productAccessModel = new ProductAccessModel();
        //     }
    }
}