import { ViewportScroller } from "@angular/common";
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ActivatedRoute, Router } from "@angular/router";
import { CookieService } from "ngx-cookie-service";
import { ProductAccessModel, ProductMapping, TabbedStructureData, UserRoleMappingModel } from "src/app/core/data-models/app-model";
import { LoginDetailModel, ProductCodesAndPreferenceObject, SymbolLookUp } from "src/app/core/data-models/service-model";
import { APIService } from "src/app/core/http-services/api.service";
import { AuthService } from "src/app/core/http-services/auth.service";
import { AppDataService } from "src/app/core/services/app-data.service";
import { DataService } from "src/app/core/services/data.service";
import { ToastService } from "src/app/core/services/toast-service";
import { LocalStorage } from "src/app/core/services/utilily/utility";
import { TopHeaderData } from "../applet-header.component";

@Component({
    selector: 'app-top-header',
    templateUrl: './top-header.component.html',
    styleUrls: ['./top-header.component.scss'],
})
export class TopHeaderComponent implements OnInit, OnChanges {
  toggleIcon  = false;
    page: string;
    productMapping: ProductMapping;
    productAccessModel: ProductAccessModel;
    isMobile: boolean = false;
    productsList: any;
    totalProductAccessArray = [];
    productPreference: ProductCodesAndPreferenceObject;
    @Input() headerData: TopHeaderData;
    userDetails: LoginDetailModel;
    createLogoutXPosition: any = 0;
    createLogoutYPosition: any = 0;
    fixedLogoutYPosition: any = 0;
    documentClickEventRegister: boolean = false;
    productPopUpShow: boolean = false;
    showLogoutPopup: boolean = false;
    manageMembershipUrl:string;
    token:string; 
    @ViewChild('logoutPopup', { static: false }) logoutPopup: any;
    @Output() openSubscriptionPopup = new EventEmitter<any>();
    @Output() selectedSymbolFromLookUp = new EventEmitter<any>();
    @Output() selectedListId = new EventEmitter<any>();
    @Output() showWelcomePopUp = new EventEmitter<any>();
    @Input() tabbedStructureData :TabbedStructureData;
    userAccess: UserRoleMappingModel;
    tabbedStructureActive : TabbedStructureData;
    productPreferenceApiData : ProductCodesAndPreferenceObject;
    window: any;


    constructor(
        private api: APIService,
        public dialog: MatDialog,
        private cookieService: CookieService,
        private dataService: DataService, public router: Router,
        private route: ActivatedRoute,
        public toastService: ToastService,
        public appDataService: AppDataService,
        public auth: AuthService,
        public viewportScroller: ViewportScroller,
      ) {
        this.window = window;
        if (!this.userDetails) {
          this.userDetails = this.getUserDetail();
        }
      }
    ngOnChanges(): void {
      if(this.tabbedStructureData){
        this.tabbedStructureActive = this.tabbedStructureData;
       }
       if(this.headerData.status) {
           this.page = this.headerData.page;
           this.productMapping = this.headerData.productMapping;
           this.productPreference = this.headerData.productPreference;
       }
    }
    ngOnInit(): void {
this.productPreferenceApiData = this.appDataService.productPreference;
      this.tabbedStructureActive = this.appDataService.tabbedStructureData;
      this.userAccess = this.appDataService.userRoleMappingModel;
      if(this.tabbedStructureData){
        this.tabbedStructureActive = this.tabbedStructureData;
       }
  // this.productAccessModel = this.appDataService.productAccessModel;
   this.token = this.appDataService.cookiesData.token;
    this.isMobile = this.appDataService.cookiesData.isMobile;
    // this.productsList = this.appDataService.productsListModel;
    // this.totalProductAccessArray = this.appDataService.productAccessModel.totalProductAccessArray[0];
    this.manageMembershipUrl = "https://account.chaikinanalytics.com/ca/s/login/?brandId=11001&jwtToken="+this.appDataService.cookiesData.beaconStreetJwtToken;

    }

    getUserDetail(): LoginDetailModel {
        const userDetail: LoginDetailModel = LocalStorage.getItem('userDetail');
        return userDetail;
      }

      logOut(): void {
        this.auth.killsessions(this.appDataService.cookiesData.email).subscribe((resp) => {
          if (resp["status"] === "success") {
            this.appDataService.removeAllData();
            this.cookieService.deleteAll('/', '.chaikinanalytics.com');
            window.location.replace(window.location.protocol + "//" + window.location.host + "/login/#/logout");
            // window.location.replace(window.location.protocol + "//" + window.location.host + "/login/");
          }
        });
      }


      openLogoutPopup(e: any) {
        this.openSubscriptionPopup.emit(e);
      }

      onClickLogoutOpenPopup(event: any) {
        this.createLogoutXPosition = event.clientX - 150;
        this.createLogoutYPosition = event.clientY + 20;
      }

      showWelcomePopUpForPGA(){
       this.showWelcomePopUp.emit(true);
      }

      openLogoutPopupForAccount() {
        this.showLogoutPopup = true;
      }

      onCloseLogOutPopupEvent() {
        this.showLogoutPopup = false;
      }

      offClickHandler(event: any) {
        if (
          this.logoutPopup &&
          this.logoutPopup.nativeElement &&
          !this.logoutPopup.nativeElement.contains(event.target)
        ) {
          // check click origin
          this.showLogoutPopup = false;
          if (this.documentClickEventRegister) {
            document.removeEventListener('click', this.offClickHandler.bind(this));
            this.documentClickEventRegister = false;
          }
        }
      }

      onSymbolSelect(data: SymbolLookUp): void {
        this.selectedSymbolFromLookUp.emit(data);
      }

      getWatchListUpdate(e:any):void {
        this.selectedListId.emit(e);
      }

      goToDashboard() {

       this.tabbedStructureActive.activeTabForHome = true;
       this.tabbedStructureActive.activeTabForMarket = false;
       this.tabbedStructureActive.activeTabForAdvancedSearch = false;
       this.appDataService.tabbedStructureData =  this.tabbedStructureActive;
       setTimeout(() => {
        this.router.navigate(['/dashboard']);
       }, 300);
        
      }
      goToMarkets() {
       
        this.tabbedStructureActive.activeTabForHome = false;
        this.tabbedStructureActive.activeTabForMarket = true;
        this.tabbedStructureActive.activeTabForAdvancedSearch = false;
        this.appDataService.tabbedStructureData =  this.tabbedStructureActive
        setTimeout(() => {
          this.router.navigate(['/market-scanner']);
         }, 300);
       
      }
      goToAdvancedSearch():void {
        this.tabbedStructureActive.activeTabForHome = false;
        this.tabbedStructureActive.activeTabForMarket = false;
        this.tabbedStructureActive.activeTabForAdvancedSearch = true;
        this.appDataService.tabbedStructureData =  this.tabbedStructureActive;
        setTimeout(() => {
          this.router.navigate(['/advanced-search']);
         }, 300);
      }

      switchToAdvancedMode(): void{
        this.toggleIcon = !this.toggleIcon
        if(this.cookieService.get('role') === 'Power Gauge Analytics'){
          this.cookieService.set('role', 'Power Gauge Analytics', { path: '/', domain: '.chaikinanalytics.com' });
          window.location.replace(window.location.protocol + "//" + window.location.host + "/"); }
          else if(this.cookieService.get('role') == 'Chaikin Power Tactics'){
            this.cookieService.set('role','Chaikin Power Tactics',{path: '/', domain: '.chaikinanalytics.com'})
            window.location.replace(window.location.protocol + "//" + window.location.host + "/");
          }
          else if(this.cookieService.get('role') == 'Chaikin Power Trader'){
            this.cookieService.set('role','Chaikin Power Trader',{path: '/', domain: '.chaikinanalytics.com'})
            window.location.replace(window.location.protocol + "//" + window.location.host + "/");
          }
      }

    cookiePreference(): void {
      //console.log(this.window.osano);
     // event.addEventListener("click", window.Osano.cm.showDrawer('osano-cm-dom-info-dialog-open'));
    this.window.Osano.cm.showDrawer('osano-cm-dom-info-dialog-open');
    //  window.Osano.cm
    //  window.Osano.cm.showDrawer('osano-cm-dom-info-dialog-open');
       }
}
