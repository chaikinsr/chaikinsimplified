import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { LoginGuard } from '../core/guard/login.guard';
import { MarketResolver } from '../resolver/market.resolver';
import { AdvancedSearchResolver } from '../resolver/advancedSearch.resolver';

const layoutRoutes: Routes = [
    {
        path: '',
        component: NavMenuComponent,
        canActivate: [LoginGuard],
        children: [
            {
                path: '',
                loadChildren: () =>  import('../page/report/pgr.module').then(m => m.PowerGaugeReportModule), 
            },
            {
                path: 'dashboard',
                loadChildren: () =>  import('../page/dashboard/dashboard.module').then(m => m.DashboardModule), 
            },
            {
                path: 'market-scanner',
                resolve: {
                    data: MarketResolver
                  },
                loadChildren: () =>  import('../page/market-scanner/market-scanner.module').then(m => m.MarketScannerModule), 
            },
            {
                path: 'advanced-search',
                resolve: {
                    data: AdvancedSearchResolver
                  },
                loadChildren: () =>  import('../page/advanced-search/advanced-search.module').then(m => m.AdvancedSearchModule), 
            },
            // {
            //     path: 'widget',
            //     loadChildren: () =>  import('../pgr-widget/widget.module').then(m => m.WidgetModule)
            // }
        ]
    }
];

// {
//     path: 'widget',
//     loadChildren: () =>  import('../page/widget/widget.module').then(m => m.WidgetModule)
// }
            // {
            //     path: 'dashboard',
            //     loadChildren: () =>  import('../module/dashboard/dashboard.module').then(m => m.DashboardModule), 
            // }
// canActivate: [LoginGuard],

// {
//     path: 'widget',
//     loadChildren: () =>  import('../page/widget/widget.module').then(m => m.WidgetModule)
// }
@NgModule({
    imports: [
        RouterModule.forChild(layoutRoutes)
    ],
    exports: [
        RouterModule
    ],
    declarations: [
    ]
})
export class LayoutRoutingModule { }