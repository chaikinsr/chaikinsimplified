import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ClickOutsideModule } from 'ng-click-outside';
import { LoginGuard } from '../core/guard/login.guard';
//import { AddToPortfolioComponent } from './add-to-portfolio/add-to-portfolio.component';
import { LayoutRoutingModule } from './layout-routing.module';
import { NavMenuComponent } from './nav-menu/nav-menu.component';


@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        LayoutRoutingModule,
        ClickOutsideModule
    ],
    declarations: [
        NavMenuComponent,
      
    ],
    entryComponents: [],
    providers: [LoginGuard]
})
export class LayoutModule { }
//  AddToPortfolioComponent