import { Component, OnInit } from "@angular/core";
import { map, tap } from "rxjs/operators";
import { AuthorizationModel, LoginDetailModel } from "src/app/core/data-models/service-model";
import { APIService } from "src/app/core/http-services/api.service";
import { LocalStorage } from "src/app/core/services/utilily/utility";

@Component({
    selector: 'app-nav-menu',
    templateUrl: './nav-menu.component.html',
    styles: []
})
export class NavMenuComponent implements OnInit {
    userDetail: LoginDetailModel;
    constructor(
        private api: APIService
    ){
      //userName: 'akash.kumar@paxcel.net',
    //     let formValues = {
    //         userName: 'akash.kumar@paxcel.net',
    //         password: '1234567',
    //       }
    // this.getAuthorization(formValues);
    // let email = localStorage.getItem('email');
    //    this.api.getLogin(email).subscribe((resp) => {
    //     if (resp) {
    //       this.setUserDetail(resp);
    //     }
    //   });
    }

    ngOnInit(): void {
    }

    getAuthorization(formValues: any): void {
        const httpAuthorization$ = this.api.getAuthorization(
          true,
          formValues.userName,
          formValues.password
        );
    
        const login$ = httpAuthorization$.pipe(
          tap(() => console.log('HTTP Request executed')),
          map((resp: AuthorizationModel) => { 
            if (resp.loggedInStatus) {
              window.localStorage.setItem("sessionId", resp.sessionId);
            }
            return resp.loggedInStatus
          })
        );
    
        login$.subscribe((loginStatus) => {
          if (loginStatus === 'true') {
            this.api.getLogin(formValues.userName).subscribe((resp) => {
              if (resp) {
                this.setUserDetail(resp);
              }
            });
          }
        });
      }
    
      setUserDetail(userDetail: LoginDetailModel): void {
        LocalStorage.setItem('userDetail', userDetail);
        LocalStorage.setItem('mainSymbol', 'AMZN');
        this.userDetail = userDetail;
        this.api.userDetail = userDetail;
      }
    
      getUserDetail(): LoginDetailModel {
        const userDetail: LoginDetailModel = LocalStorage.getItem('userDetail');
        return userDetail;
      }
}