import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { LoginGuard } from 'src/app/core/guard/login.guard';
import { LoginComponent } from '../login/login.component';
import { UtilityLayoutComponent } from './utility-layout.component';
const layoutRoutes: Routes = [
    {
        path: '',
        component: UtilityLayoutComponent,
        // canActivate: [LoginGuard],
        children: [
           
            {
                path: 'list-view',
                loadChildren: () =>  import('../list/list.module').then(m => m.ListModule), 
            },
            {
                path: 'screener-view',
                loadChildren: () =>  import('../screener/screener.module').then(m => m.ScreenerModule), 
            }
        ]
    }
];
// canActivate: [LoginGuard],

@NgModule({
    imports: [
        RouterModule.forChild(layoutRoutes)
    ],
    exports: [
        RouterModule
    ],
    declarations: [
    ]
})
export class UtilityLayoutRoutingModule { }