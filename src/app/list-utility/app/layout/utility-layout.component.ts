import { Component } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

import { AppDataService } from 'src/app/core/services/app-data.service';
import { AuthService } from 'src/app/core/http-services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-utility-layout',
  templateUrl: './utility-layout.component.html',
  styleUrls: ['./utility-layout.component.scss'],
})
export class UtilityLayoutComponent {
  constructor(
    private authService: AuthService,
    public appDataService: AppDataService,
    private cookieService: CookieService,
    private router: Router
  ) {}

  logOut(): void {
    const sessionedUserEmail = localStorage.getItem('email');

    this.authService.killsessions(sessionedUserEmail).subscribe((resp) => {
      if (resp['status'] === 'success') {
        localStorage.removeItem('UID');
        localStorage.removeItem('email');
       // this.appDataService.removeAllData();
      //  this.cookieService.deleteAll('/', '.chaikinanalytics.com');
      this.router.navigate(['/']);
      }
    });
  }
}
