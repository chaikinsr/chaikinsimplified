import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { APIService } from 'src/app/core/http-services/api.service';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

import {
  InvestorIdeaListDataObject,
  InvestorListData,
} from 'src/app/core/data-models/service-model';
import { environment } from 'src/environments/environment';
import { loginCredentialMapping } from 'src/app/list-utility/list-utility.constant';
import { ListEditDialogComponent } from '../shared/list-edit-dialog/list-edit-dialog.component';
import { ConfirmDialogComponent } from '../shared/confirm-dialog/confirm-dialog.component';

export interface ScreenLists {
  name: string;
  description: string;
  guru: string;
  limit: number;
  showHide: string;
  products: string;
}

let env: string = environment.env;
@Component({
  selector: 'app-list-view',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  investorListData: InvestorListData[];

  constructor(
    private api: APIService,
    public dialog: MatDialog,
    private route: Router,
    private router: Router,
    private cookieService: CookieService
  ) {}

  ngOnInit(): void {
    let email = '';
    let loginCredentialValid = false;
    email = localStorage.getItem('email');
    if (!email) {
      email = this.cookieService.get('app-email');
    }
    loginCredentialMapping.forEach((resp) => {
      resp.email === email ? (loginCredentialValid = true) : '';
    });
    loginCredentialValid
      ? this.getInvestorIdeaList()
      : this.router.navigateByUrl('/');
  }

  // Screener List Table
  displayedColumns: string[] = [
    'listName',
    'description',
    'guru',
    'limit',
    'showHide',
    'associatedPubCodes',
    'action',
  ];
  screenerLibrary;

  getInvestorIdeaList() {
    const httpGetInvestorListsDetails$ = this.api.getInvestorListsDetails();
    httpGetInvestorListsDetails$
      .pipe(
        map((resp) => {
          const ideaListData = resp as unknown as InvestorIdeaListDataObject;
          return this.prepareInvestorIdeaListData(ideaListData);
        })
      )
      .subscribe((investorListData: InvestorListData[]) => {
        this.investorListData = investorListData;
        this.screenerLibrary = this.investorListData;
      });
  }

  prepareInvestorIdeaListData(listData: InvestorIdeaListDataObject) {
    let screenerList = listData.data;
    return screenerList;
  }

  editList(screenData: InvestorListData) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      headerText: 'Edit List',
      AddListScreentitleText: 'Select the type of list you want to create:',
      EditScreentitleText: 'Please fill out the following information:',
      editScreenButtonText: 'Update List',
      editListEnable: true,
      screenData,
    };

    const dialogRef = this.dialog.open(ListEditDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((result) => {
      if (result.message == 'Update List') {
        this.getInvestorIdeaList();
        // this.toastService.showSuccess('successfully added');
      }
    });
  }

  importList() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      headerText: 'Add List',
      AddListScreentitleText: 'Select the type of list you want to create:',
      EditScreentitleText: 'Please fill out the following information:',
      editScreenButtonText: 'Continue',
      editListEnable: false,
      screenData: null,
    };

    const dialogRef = this.dialog.open(ListEditDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.getInvestorIdeaList();
        // this.toastService.showSuccess('successfully added');
      }
    });
  }

  deleteList(screen) {
    let msg = `Are you sure you want to delete this screen?`;
    let listName = screen.listName;
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '500px',
      data: {
        title: 'Delete List',
        message: msg,
        noButtonText: 'No, I changed my mind',
        yesButtonText: 'Yes,delete',
      },
    });

    dialogRef.afterClosed().subscribe((dialogResult) => {
      let result = dialogResult;
      if (result) {
        this.api.deleteListDetails(listName).subscribe((res) => {
          if (localStorage.getItem('screenerUtilityListName') == listName) {
            localStorage.setItem('screenerUtilityListName', '');
          }
          if (res['status'] && res['message'] == 'SUCCESS')
            this.getInvestorIdeaList();
        });
      }
    });
  }

  AddSymbols(screen) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      headerText: 'Add Symbols',
      AddListScreentitleText: '',
      EditScreentitleText: '',
      editScreenButtonText: 'Update List',
      editListEnable: true,
      screenData: screen,
    };

    const dialogRef = this.dialog.open(ListEditDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((result) => {
      if (result && result.message == 'Update List') {
        this.getInvestorIdeaList();
        // this.toastService.showSuccess('successfully added');
      }
    });
  }

  screenerCriteria(screen) {
    localStorage.setItem('screenerUtilityListName', screen.listName);
    this.route.navigate(['/screener-view']);
  }
}
