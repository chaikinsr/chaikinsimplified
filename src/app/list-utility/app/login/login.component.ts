import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import * as CryptoJS from 'crypto-js';
import { AuthService } from 'src/app/core/http-services/auth.service';
import { APIService } from 'src/app/core/http-services/api.service';

export class UserFormDetail {
  email: string;
  password: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup = this.fb.group({
    email: ['', Validators.compose([Validators.required])],
    password: ['', Validators.compose([Validators.required])],
  });
  subscriptions: string;
  showInvalidMsg: boolean = false;
  errorMsg: string;
  showtext: boolean = false;
  isLoginButtonDisabled = false;
  totalCodeInProduct = [];
  productsSubscriptionList = [];
  productsArray: string;
  rememberMe: any = false;
  encryptedPassword: any;
  decryptedPassword: any;
  showPopOnCAMobile: boolean = false;
  tokenValue: any;

  loginCredentialMapping = [
    {
      uid: 1060585,
      customer_number: 'SAC0015808022',
      email: 'jsingh@stansberryresearch.com',
    },
    {
      uid: 1021370,
      customer_number: 'SAC0023195997',
      email: 'mfedick@chaikinanalytics.com',
    },
    {
      uid: 1050114,
      customer_number: 'SAC0019370744',
      email: 'shayman@chaikinanalytics.com',
    },
    {
      uid: 1067163,
      customer_number: '',
      email: 'akash.kumar@paxcel.net',
    },
    {
      uid: 1060666,
      customer_number: '',
      email: 'hardeep.deol@paxcel.net',
    },
    {
      uid: 1058333,
      customer_number: '',
      email: 'sarabjit.singh@paxcel.net',
    },
  ];

  constructor(
    private fb: FormBuilder,
    private apiService: APIService,
    private cookieService: CookieService,
    private router: Router,
    private auth: AuthService
  ) {
    //this.callAwsLoginAPi().subscribe();
    // this.apiService.getSession({}).subscribe();
  }

  ngOnInit(): void {
    this.checkRememberMeOnLoginScreen();
  }

  // get appAxiosBaseUrl(): string {
  //   const url = environment.appAxiosBaseUrl;
  //   return url;
  // }

  private getUserDetails(): UserFormDetail {
    const loginFormValues = this.loginForm.value;
    return {
      email: loginFormValues.email,
      password: loginFormValues.password,
    };
  }

  showPassword() {
    this.showtext = !this.showtext;
  }

  loginSubmit(): any {
    if (this.loginForm.valid) {
      this.errorMsg = '';
      this.isLoginButtonDisabled = true;

      let formValues = this.getUserDetails();
      localStorage.setItem('email', formValues.email);
      let loginCredentialValid = false;
      this.loginCredentialMapping.forEach((resp) => {
        resp.email === formValues.email ? (loginCredentialValid = true) : '';
      });
      if (loginCredentialValid) {
        this.auth
          .getAuthForUtility(formValues.email, formValues.password)
          .subscribe((authResp: any) => {
            let loggedInStatus = authResp.loggedInStatus;
            if (loggedInStatus === 'true') {
              this.auth.getLoggedInstate(formValues.email).subscribe((resp) => {
                const { Status, UID } = resp;
                if (Status === 'UserExist') {
                  localStorage.setItem('UID', UID);
                  this.router.navigateByUrl('/list-view');
                } else {
                  this.errorMsg = 'Please try again.';
                  this.isLoginButtonDisabled = false;
                }
              });
            } else {
              this.showInvalidMsg = true;
              this.errorMsg = 'Invalid email or password.';
              this.isLoginButtonDisabled = false;
            }
          });
      } else {
        this.showInvalidMsg = true;
        this.errorMsg = 'Invalid email or password.';
        this.isLoginButtonDisabled = false;
      }
    } else {
      this.isLoginButtonDisabled = false;
    }
  }

  onMobileLogin(): void {
    let _isNotMobile = (function () {
      let check = false;
      (function (a) {
        if (
          /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(
            a
          ) ||
          /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
            a.substr(0, 4)
          )
        )
          check = true;
      })(navigator.userAgent || navigator.vendor);
      return !check;
    })();

    if (!_isNotMobile) {
      this.showPopOnCAMobile = true;
    } else {
      this.cookieService.set('role', 'Chaikin Analytics', {
        path: '/',
        domain: '.chaikinanalytics.com',
      });
      window.location.replace(
        window.location.protocol + '//' + window.location.host + '/'
      );
      this.showPopOnCAMobile = false;
    }
  }

  onContinueWithMobile(): void {
    this.showPopOnCAMobile = false;
    this.cookieService.set('role', 'Chaikin Analytics', {
      path: '/',
      domain: '.chaikinanalytics.com',
    });
    this.cookieService.set('app-email', window.localStorage.getItem('email'), {
      path: '/',
      domain: '.chaikinanalytics.com',
    });
    // this.callGetAuthorization('Power Gauge Analytics',this.tokenValue);
    window.location.replace(
      window.location.protocol +
        '//' +
        window.location.host +
        '/pgr/#/dashboard'
    );
    //  window.location.replace(window.location.protocol + "//" + window.location.host + "/pulse/");
    // this.cookieService.set('role', 'Power Pulse', { path: '/', domain: '.chaikinanalytics.com' });
    // this.callGetAuthorization('Power Pulse', this.tokenValue);
  }

  checkRememberMeOnLoginScreen(): void {
    if (
      typeof window.localStorage.getItem('password') == 'undefined' ||
      window.localStorage.getItem('password') == '' ||
      window.localStorage.getItem('password') == null
    ) {
    } else {
      this.decryptedPassword = CryptoJS.AES.decrypt(
        window.localStorage.getItem('password'),
        'pwd'
      );
    }
    this.rememberMe =
      window.localStorage.getItem('rememberMe') == 'true' ? true : false;
    var email =
      window.localStorage.getItem('email') != 'undefined'
        ? window.localStorage.getItem('email')
        : undefined;
    var password = window.localStorage.getItem('password')
      ? this.decryptedPassword.toString(CryptoJS.enc.Utf8)
      : undefined;
    var userRoleForLogin = window.localStorage.getItem('UserRole');
    if (this.rememberMe) {
      this.loginForm.setValue({ email: email, password: password });
    }
  }

  setRememberMe(formValues): void {
    //console.log(formValues.email);
    window.localStorage.setItem('email', formValues.email);

    window.localStorage.setItem('rememberMe', this.rememberMe);
    if (window.localStorage.getItem('rememberMe') == 'true') {
      if (this.encryptedPassword == undefined) {
        this.encryptedPassword = CryptoJS.AES.encrypt(
          formValues.password,
          'pwd'
        );
      }
      window.localStorage.setItem(
        'password',
        this.encryptedPassword.toString()
      );
    } else {
      window.localStorage.setItem('password', '');
    }
  }

  callGetAuthorization(productName: string, token): any {}
}
