import { Component, OnInit, DoCheck } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { cloneDeep } from 'lodash';

import {
  ScreenerDataModel,
  screenerInitialDataModel,
  ScreenerResultSymbolModel,
} from 'src/app/core/data-models/app-model';
import {
  InvestorIdeaListDataObject,
  InvestorListData,
  ScreenerResultDataObject,
} from 'src/app/core/data-models/service-model';
import { APIService } from 'src/app/core/http-services/api.service';
import { SharedUtil } from 'src/app/core/services/utilily/utility';
import { ListEditDialogComponent } from 'src/app/list-utility/app/shared/list-edit-dialog/list-edit-dialog.component';

import {
  ETF_CHARACTERISTICS_LIST,
  FACTORS_LIST,
  PERFORMANCE_LIST,
  SELECT_A_SCREEN,
  TECHNICALS_LIST,
} from '../../screener.constant';

@Component({
  selector: 'app-etf',
  templateUrl: './etf.component.html',
  styleUrls: ['./etf.component.scss'],
})
export class EtfComponent implements OnInit, DoCheck {
  readonly FACTORS_LIST = FACTORS_LIST;
  readonly PERFORMANCE_LIST = PERFORMANCE_LIST;
  readonly TECHNICALS_LIST = TECHNICALS_LIST;
  readonly ETF_CHARACTERISTICS_LIST = ETF_CHARACTERISTICS_LIST;
  screenerOriginalData: screenerInitialDataModel;
  screenerInitialData: screenerInitialDataModel;
  investorListData: InvestorListData[];
  screenerListControl = new FormControl('');
  selectedListName: string = SELECT_A_SCREEN;
  selectedUniverse: ScreenerDataModel;
  selectedSubUniverse: any;
  selectedThirdUniverse: any;
  subStartingUniverse: any;
  subSubStartingUniverse: any;
  currentRating: any[] = [];
  screenerResultData: ScreenerResultSymbolModel[] = [];
  stockRatingLabel: string;
  clearTimer: any;

  constructor(
    private api: APIService,
    private router: Router,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    if (localStorage.getItem('UID')) {
      this.getInvestorIdeaList();
      this.parseInitialData();
    } else {
      this.router.navigateByUrl('/');
    }
  }

  ngDoCheck(): void {
    if (this.selectedUniverse) {
      this.subStartingUniverse = this.screenerInitialData[
        'ETF Starting Universe'
      ]?.factor.find((element) => {
        return element.name === this.selectedUniverse.name;
      });
    }

    if (this.subStartingUniverse?.value) {
      const subSubStartingUniverse = this.subStartingUniverse?.value.find(
        (element) => {
          return element.name === this.selectedSubUniverse?.name;
        }
      );

      if (subSubStartingUniverse) {
        this.subSubStartingUniverse = subSubStartingUniverse;
      }
    }
  }

  private convertBooleanToNumber(respData) {
    for (const key in respData) {
      respData[key].factor.forEach((element) => {
        element.defaultStatus = element.defaultStatus ? 1 : 0;

        if (element.hasOwnProperty('value')) {
          element.value.forEach((subElement) => {
            subElement.defaultStatus = subElement.defaultStatus ? 1 : 0;

            if (subElement.hasOwnProperty('value2')) {
              subElement.value2.forEach((subSubElement) => {
                subSubElement.defaultStatus = subSubElement.defaultStatus
                  ? 1
                  : 0;
              });
            }
          });
        }
      });

      if (respData[key].hasOwnProperty('value')) {
        respData[key].value.forEach((element) => {
          element.defaultStatus = element.defaultStatus ? 1 : 0;
        });
      }
    }

    return respData;
  }

  /**
   * @description prepareScreenerInitialData function is used to prepare the initial data for the screener filter form
   * @param data Response data from the API call
   * @returns parsed data for the screener to be used in the filter
   */
  private prepareScreenerInitialData(data: any): screenerInitialDataModel {
    return data.reduce((parsedData, currentData) => {
      if (currentData.hasOwnProperty('value')) {
        return {
          ...parsedData,
          [currentData.header_name]: {
            factor: currentData.factor,
            value: currentData.value,
          },
        };
      } else {
        return {
          ...parsedData,
          [currentData.header_name]: {
            factor: currentData.factor,
          },
        };
      }
    }, {}) as screenerInitialDataModel;
  }

  /**
   * @description parseInitialData function is used to make the API call to get the initial data for the screener filter form and then parse the data in prepareScreenerInitialData function
   */
  private parseInitialData() {
    let uid = localStorage.getItem('UID');
    const httpGetSymbolData$ = this.api.getEtfScreenerInitialData(uid);

    httpGetSymbolData$
      .pipe(
        map((response) =>
          this.convertBooleanToNumber(this.prepareScreenerInitialData(response))
        )
      )
      .subscribe((screenerInitialData: screenerInitialDataModel) => {
        this.screenerInitialData = cloneDeep(screenerInitialData);
        this.screenerOriginalData = cloneDeep(screenerInitialData);

        if (this.selectedListName !== 'Select a screen') {
          let selectedListNameData = this.investorListData?.find(
            (element) => element.listName == this.selectedListName
          );

          if (selectedListNameData) {
            this.loadScreenerList(
              cloneDeep(selectedListNameData.screenCriteria)
            );
          }
        }
      });
  }

  /**
   * @description prepareInvestorIdeaListData function is used to prepare the data for the investor idea list
   * @param listData contains the data of the already saved list
   * @returns filtered data for the investor idea list or screener list
   */
  private prepareInvestorIdeaListData(
    listData: InvestorIdeaListDataObject
  ): InvestorListData[] {
    return listData.data.filter((ele) => ele.updationMode === 'etf-screener');
  }

  /**
   * @description getInvestorIdeaList function is used to make the API call to get the data for the investor idea list or screener list
   */
  private getInvestorIdeaList() {
    this.api
      .getInvestorListsDetails()
      .pipe(
        map((response) => {
          const ideaListData =
            response as unknown as InvestorIdeaListDataObject;
          return this.prepareInvestorIdeaListData(ideaListData);
        })
      )
      .subscribe((investorListData: InvestorListData[]) => {
        this.investorListData = investorListData;
        const screenerListName = localStorage.getItem(
          'screenerEtfUtilityListName'
        );

        if (screenerListName) {
          const selectedListNameData = investorListData.find(
            (element) => element.listName === screenerListName
          );

          if (selectedListNameData) {
            this.selectedListName = selectedListNameData.listName;
            this.screenerListControl.patchValue(this.selectedListName);
          }
        } else {
          this.clearListName();
        }
      });
  }

  /**
   * @description getResultSet is a factory function used to create the oject for the request body for the API call to get the screener result
   * @param headerName {string} contains the name of the header
   * @param data cotains the data of the header
   * @returns {{header: string, factorId: number, valueId: number, value2Id: number}} Object for the request body
   */
  private getResultSet(
    headerName: string,
    data: any
  ): { header: string; factorId: number; valueId: number; value2Id: number } {
    return {
      header: headerName,
      factorId: data.factor.find((element) => element.defaultStatus)?.id,
      valueId: data?.value?.find((element) => element.defaultStatus)?.id || -1,
      value2Id:
        data?.value2?.find((element) => element.defaultStatus)?.id || -1,
    };
  }

  /**
   * @description createResultSetForStartingUniverse function specifically creates the object for the request body for the API call to get the screener result for the starting universe
   * @returns {{header: string, factorId: number, valueId: number, value2Id: number}} Object for the request body
   */
  private createResultSetForStartingUniverse() {
    const startingUniverse =
      this.screenerInitialData['ETF Starting Universe'].factor;
    const universeObject = startingUniverse.find(
      (element) => element.defaultStatus
    ) as any;

    let subUniverse;
    let subSubUniverse;

    if (universeObject.hasOwnProperty('value')) {
      subUniverse = universeObject.value.find(
        (element) => element.defaultStatus
      );

      if (subUniverse.hasOwnProperty('value2')) {
        subSubUniverse = subUniverse.value2.find(
          (element) => element.defaultStatus
        );
      }
    }

    return {
      header: 'ETF Starting Universe',
      factorId: universeObject.id,
      valueId: subUniverse ? subUniverse.id : -1,
      value2Id: subSubUniverse ? subSubUniverse.id : -1,
    };
  }

  /**
   * @description createResultSetForEtfRating function specifically creates the object for the request body for the API call to get the screener result for the ETF rating
   * @returns {{header: string, factorId: number, valueId: number[], value2Id: number[]}} Object for the request body
   */
  private createResultSetForEtfRating() {
    const factorId: number[] = this.screenerInitialData['ETF Rating'].factor
      .filter((element) => element.defaultStatus)
      .map((element) => element.id);

    return {
      header: 'ETF Rating',
      factorId,
      valueId: [...Array(factorId.length).fill(-1)],
      value2Id: [...Array(factorId.length).fill(-1)],
    };
  }

  /**
   * @descriptoin createResultSetForOtherEtfCriteria function specifically creates the object for the request body for the API call to get the screener result for the other ETF criteria
   * @returns {Array<{header: string, factorId: number, valueId: number, value2Id: number}>} array of the ojects for the request body
   */
  private createResultSetForOtherEtfCriteria() {
    const resultSet = [];

    for (const key in this.screenerInitialData) {
      if (key !== 'ETF Starting Universe' && key !== 'ETF Rating') {
        resultSet.push(this.getResultSet(key, this.screenerInitialData[key]));
      }
    }

    return resultSet;
  }

  /**
   * @description calculateResultCriteria function is used to create the request body for the API call to get the screener result
   * @returns {Array<{header: string, factorId: number, valueId: number, value2Id: number}>} Object for the request body
   */
  private calculateResultCriteria() {
    const universeResult = this.createResultSetForStartingUniverse();
    const etfRatingResult = this.createResultSetForEtfRating();
    const otherEtfResult = this.createResultSetForOtherEtfCriteria();

    return [universeResult, etfRatingResult, ...otherEtfResult];
  }

  /**
   * Function prepare data which we bind on UI to see result Card.
   * @param data is data for result api
   * @returns data for result card
   */
  private prepareScreenerResultData(
    data: ScreenerResultDataObject[]
  ): ScreenerResultSymbolModel[] {
    let resultSymbols: ScreenerResultSymbolModel[] = [];

    let symbols = data[0]['symbols'];
    symbols.forEach((ele) => {
      let symbolData = new ScreenerResultSymbolModel();
      symbolData.change = ele.Change;
      symbolData.last = ele.Last;
      symbolData.pgr = ele.PGR;
      symbolData.percentage = ele.Percentage;
      symbolData.summaryRating = ele['SummaryRating '];
      symbolData.technicalRating = ele['TechnicalRating '];
      symbolData.industryListId = ele['industry_ListID '];
      symbolData.isEtf = ele.is_etf;
      symbolData.name = ele.name;
      symbolData.rawPgr = ele.raw_PGR;
      symbolData.signals = ele.signals;
      symbolData.symbol = ele.symbol;

      symbolData.pgrImage = SharedUtil.getPgrMicroArcWithoutNeutralValue(
        ele.PGR,
        ele.raw_PGR,
        ele.is_etf
      );
      symbolData.pgrName = SharedUtil.getPgrNameWithoutNeutralValues(
        SharedUtil.getPgrRating(ele.PGR, ele.raw_PGR)
      );
      symbolData.ratingTypeColorLabel =
        SharedUtil.getColorForRating(symbolData.pgrName) + '-label';
      symbolData.changeFormattedClass = ele.Change > 0 ? 'up' : 'down';
      symbolData.percentageFormattedClass = ele.Percentage > 0 ? 'up' : 'down';
      symbolData.lastFormatted =
        '$' + SharedUtil.formatPriceDashWat(ele.Last).toString();
      symbolData.percentageFormatted =
        '(' + SharedUtil.formatChangeDashWat(ele.Percentage).toString() + ')';
      symbolData.rating = SharedUtil.getPgrRating(ele.PGR, ele.raw_PGR);
      resultSymbols.push(symbolData);
    });

    return resultSymbols;
  }

  /**
   * @description getEtfScreenerResult function is used to get the screener result for the ETF screener from the API based on the request body
   * @param resultPayload is parsed data for the result API
   */
  private getEtfScreenerResult(resultPayload) {
    let uid = localStorage.getItem('uid');

    this.api
      .getEtfScreenerResults(uid, JSON.stringify(resultPayload))
      .pipe(
        map((response) => {
          const screenerResultData =
            response as unknown as ScreenerResultDataObject[];
          return this.prepareScreenerResultData(screenerResultData);
        })
      )
      .subscribe((screenerResultData: ScreenerResultSymbolModel[]) => {
        this.screenerResultData = screenerResultData;
      });
  }

  /**
   * @description clearListName function is used to clear the list name when the user clicks on the reset button
   */
  private clearListName(): void {
    localStorage.setItem('screenerEtfUtilityListName', '');
    this.selectedListName = 'Select a screen';
    this.screenerListControl = new FormControl('');
  }

  /**
   * @description resetDefaultStatus function is used to reset the defaultStatus value of all the factors to false or zero when the user views the selected list data
   */
  private resetDefaultStatus(initialValueRef) {
    for (const key in initialValueRef) {
      initialValueRef[key].factor.forEach((element) => {
        element.defaultStatus = 0;

        if (element.hasOwnProperty('value')) {
          element.value.forEach((subElement) => {
            subElement.defaultStatus = 0;

            if (subElement.hasOwnProperty('value2')) {
              subElement.value2.forEach((subSubElement) => {
                subSubElement.defaultStatus = 0;
              });
            }
          });
        }
      });

      if (initialValueRef[key].hasOwnProperty('value')) {
        initialValueRef[key].value.forEach((element) => {
          element.defaultStatus = 0;
        });
      }
    }
  }

  /**
   * getHeaderName function is used to get the header name for the selected list data based on the header
   * @param headerName {string} is the header name of the factor
   * @returns {string} original header name used in the initial data
   */
  private getHeaderOriginalName(headerName: string): string {
    if (headerName === 'ETF/ETN') return 'ETF/ETN';
    if (headerName === 'Stockhastriggered') return 'Stock has triggered';
    if (headerName === 'ETFReturnvs.SPY') return 'ETF Return vs. SPY';
    if (headerName === 'ETFLong/Short') return 'ETF Long/Short';

    let headerOriginalName = 'ETF';

    for (let i = 3; i < headerName.length; i++) {
      if (
        headerName[i] === headerName[i].toUpperCase() &&
        Number.isNaN(Number(headerName[i]))
      ) {
        headerOriginalName += ' ' + headerName[i];
      } else {
        headerOriginalName += headerName[i];
      }
    }

    return headerOriginalName;
  }

  /**
   * @description startingUniverseScreenerListData is used to set the defaultStatus of the screener universe factors to true or one when the user views the selected list data
   * @param initialValueRef Initial value reference of the factors which is used to set the defaultStatus of the factors to true or one
   * @param startingUniverseCriteria Starting universe criteria of the selected list data incoming from the API
   */
  private startingUniverseScreenerListData(
    initialValueRef,
    startingUniverseCriteria
  ) {
    initialValueRef[
      this.getHeaderOriginalName(startingUniverseCriteria.header)
    ].factor.forEach((element: any) => {
      if (element.id === startingUniverseCriteria.factorId) {
        element.defaultStatus = 1;
        this.subStartingUniverse = element;

        if (startingUniverseCriteria.valueId !== -1) {
          element.value.forEach((subElement) => {
            if (subElement.id === startingUniverseCriteria.valueId) {
              console.log('subElement', subElement);
              subElement.defaultStatus = 1;
              this.subSubStartingUniverse = subElement;

              if (startingUniverseCriteria.value2Id !== -1) {
                subElement.value2.forEach((subSubElement) => {
                  if (subSubElement.id === startingUniverseCriteria.value2Id) {
                    subSubElement.defaultStatus = 1;
                  }
                });
              }
            }
          });
        }
      }
    });
  }

  /**
   * @description ratingScreenerListData is used to set the defaultStatus of the rating factors to true or one when the user views the selected list data
   * @param initialValueRef Initial value reference of the factors which is used to set the defaultStatus of the factors to true or one
   * @param ratingCriteria Rating criteria of the selected list data incoming from the API
   */
  private ratingScreenerListData(initialValueRef, ratingCriteria) {
    for (const eachFactorId of ratingCriteria.factorId) {
      initialValueRef[
        this.getHeaderOriginalName(ratingCriteria.header)
      ].factor.forEach((element: any) => {
        if (element.id === eachFactorId) {
          element.defaultStatus = 1;
        }
      });
    }
  }

  /**
   *
   * @param initialValueRef Initial value reference of the factors which is used to set the defaultStatus of the factors to true or one
   * @param otherCriteria All the other criteria of the selected list data incoming from the API
   */
  private otherScreenerListData(initialValueRef, otherCriteria) {
    const headerName = this.getHeaderOriginalName(otherCriteria.header);

    initialValueRef[headerName].factor.forEach((element: any) => {
      if (element.id === otherCriteria.factorId) {
        element.defaultStatus = 1;
      }
    });

    if (otherCriteria.valueId !== -1) {
      initialValueRef[
        this.getHeaderOriginalName(otherCriteria.header)
      ]?.value.forEach((element: any) => {
        if (element.id === otherCriteria.valueId) {
          element.defaultStatus = 1;
        }
      });
    }
  }

  /**
   * @description loadScreenerList function is used to parse the selected list data to display in the fillter form
   * @param criteria Criteria of the selected list data incoming from the API
   */
  loadScreenerList(criteria) {
    const initialValueRef = cloneDeep(this.screenerInitialData);
    this.resetDefaultStatus(initialValueRef);

    for (const criteriaItem of criteria) {
      if (criteriaItem.header === 'ETFStartingUniverse') {
        this.startingUniverseScreenerListData(initialValueRef, criteriaItem);
      } else if (criteriaItem.header === 'ETFRating') {
        this.ratingScreenerListData(initialValueRef, criteriaItem);
      } else {
        this.otherScreenerListData(initialValueRef, criteriaItem);
      }
    }

    this.screenerInitialData = cloneDeep(initialValueRef);
  }

  /**
   * @description This method is used to get the selected data from the dropdown value change
   * @param selectedData selected data from the dropdown
   */
  updateValueOnChange(selectedData: {
    data: ScreenerDataModel;
    headerName: string;
  }) {
    if (selectedData.headerName === 'ETF Starting Universe') {
      this.selectedUniverse = selectedData.data;
    }
  }

  /**
   * @description This method is used to get the data investor list data if available
   * @param selectedListName {string} name of the best selected list
   */
  selectScreenerList(selectedListName: string): void {
    this.selectedListName = selectedListName;
    localStorage.setItem('screenerEtfUtilityListName', selectedListName);
    let selectedListNameData = this.investorListData.find(
      (element) => element.listName === selectedListName
    );
    if (selectedListNameData) {
      this.loadScreenerList(cloneDeep(selectedListNameData.screenCriteria));
    }
  }

  /**
   * @description saveScreener() method is used to save the created list with the given name and infomation
   */
  saveScreen() {
    const resultPayload = this.calculateResultCriteria();
    const criteria = resultPayload.map((element) => {
      element.header = element.header.replace(/ /g, '');
      return element;
    });

    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      headerText: 'Add List',
      AddListScreentitleText: 'Select the type of list you want to create:',
      EditScreentitleText: 'Please fill out the following information:',
      editScreenButtonText: 'Continue',
      editListEnable: false,
      screenData: null,
      updationMode: 'etf-screener',
      criteria,
      filterType: 'etf-screener',
    };

    const dialogRef = this.dialog.open(ListEditDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.getInvestorIdeaList();
      }
    });
  }

  /**
   * @description This method updates the already created list with the given name and information
   */
  updateScreen() {
    let resultPayload = this.calculateResultCriteria();

    let criteria = resultPayload.map((ele) => {
      ele.header = ele.header.replace(/ /g, '');
      return ele;
    });
    let list;
    let selectedListNameData = this.investorListData.filter(
      (ele) => ele.listName === this.selectedListName
    );
    if (selectedListNameData.length > 0) {
      list = JSON.parse(JSON.stringify(selectedListNameData[0]));
      list.screenCriteria = criteria;
    }

    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      headerText: 'Edit List',
      AddListScreentitleText: 'Select the type of list you want to create:',
      EditScreentitleText: 'Please fill out the following information:',
      editScreenButtonText: 'Update List',
      editListEnable: true,
      screenData: list,
      updationMode: 'etf-screener',
    };

    const dialogRef = this.dialog.open(ListEditDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((result) => {
      if (result.message == 'Update List') {
        this.getInvestorIdeaList();
      }
    });
  }

  /**
   * @description getResult() method get the result of the screener based on the selected universe and other factors
   */
  getResult() {
    const resultCriteria = this.calculateResultCriteria();
    this.getEtfScreenerResult(resultCriteria);
  }

  /**
   * @description resetFilters mehtod is used to reset all the filters used in the screener
   */
  resetFilters() {
    this.screenerResultData = [];
    this.clearListName();
    this.parseInitialData();
  }

  /**
   * @description handleChange method is used to handle the change in the sub universe dropdown
   * @param selectedData selected data from the dropdown
   */
  handleChange(selectedData: { data: ScreenerDataModel; headerName: string }) {
    this.selectedSubUniverse = selectedData.data;
  }

  /**
   * @description handleThirdUniverseChange method is used to handle the change in the third universe dropdown
   * @param selectedData selected data from the dropdown
   */
  handleThirdUniverseChange(selectedData: {
    data: ScreenerDataModel;
    headerName: string;
  }) {
    this.selectedThirdUniverse = selectedData.data;
  }
}
