import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-form-actions',
  templateUrl: './form-actions.component.html',
  styleUrls: ['./form-actions.component.scss'],
})
export class FormActionsComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}

  @Input() selectedListName!: string;

  @Output() getResult = new EventEmitter();
  @Output() saveScreen = new EventEmitter();
  @Output() updateScreen = new EventEmitter();
  @Output() resetFilters = new EventEmitter();
}
