import { Component, OnInit, Input } from '@angular/core';

import { ScreenerResultSymbolModel } from 'src/app/core/data-models/app-model';

@Component({
  selector: 'app-screener-results',
  templateUrl: './screener-results.component.html',
  styleUrls: ['./screener-results.component.scss'],
})
export class ScreenerResultsComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}

  @Input() screenerResultData: ScreenerResultSymbolModel[] = [];
}
