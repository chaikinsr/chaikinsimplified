import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators,
} from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { element } from 'protractor';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, startWith } from 'rxjs/operators';
import {
  ScreenerDataModel,
  ScreenerFactorModel,
  ScreenerFirstModel,
  screenerInitialDataModel,
  ScreenerValueModel,
  ScreenerResultSymbolModel,
} from 'src/app/core/data-models/app-model';
import {
  InvestorIdeaListDataObject,
  InvestorListData,
  ScreenerResultDataObject,
} from 'src/app/core/data-models/service-model';
import { APIService } from 'src/app/core/http-services/api.service';
import { DataService } from 'src/app/core/services/data.service';
import { SharedUtil } from 'src/app/core/services/utilily/utility';
import { CookieService } from 'ngx-cookie-service';
import { environment } from 'src/environments/environment';
import { ListEditDialogComponent } from 'src/app/list-utility/app/shared/list-edit-dialog/list-edit-dialog.component';

import { loginCredentialMapping } from '../../../../list-utility.constant';

@Component({
  selector: 'app-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.scss'],
})
export class StocksComponent implements OnInit {
  screenerOriginalData: screenerInitialDataModel;
  screenerInitialData: screenerInitialDataModel;
  selectedUniverse: ScreenerDataModel;
  screenerResultData: ScreenerResultSymbolModel[] = [];
  selectedListName: string = 'Select a screen';
  screenerListControl = new FormControl('');
  investorListData: InvestorListData[];

  stockRatingForm: FormGroup;
  ratingChange = [];
  ratingPersistency = [];
  currentRating = [];
  email: any;
  stockRatingLabel: string;
  env: string = environment.env;

  ScreenerHeaderObject = [
    'Rating Components & Factors1',
    'Rating Components & Factors2',
    'Rating Components & Factors3',
    'Price Return',
    'Price Return2',
    'Rel Return',
    'Price Levels',
    'Price Levels 2',
    'Chaikin Money Flow',
    'Chaikin Rel Strength',
    'Relative Volume',
    'Min Price',
    'Max Price',
    'Market Cap',
    'Div Yield',
    'Earnings Report Date',
    'Beta',
    'Stock has triggered',
  ];

  viewValue: string[] = [];

  valueSelected(value: string): boolean {
    return this.viewValue.indexOf(value) !== -1;
  }

  constructor(
    private api: APIService,
    private fb: FormBuilder,
    private dataService: DataService,
    private actRoute: ActivatedRoute,
    private router: Router,
    private cookieService: CookieService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    let loginCredentialValid = false;
    this.email = localStorage.getItem('email');
    if (!this.email) {
      this.email = this.cookieService.get('app-email');
    }
    loginCredentialMapping.forEach((resp) => {
      resp.email === this.email ? (loginCredentialValid = true) : '';
    });
    if (loginCredentialValid) {
      this.getInvestorIdeaList();
      this.getScreenerInitialData();
    } else {
      this.router.navigateByUrl('/');
    }
  }

  /**
   * Function use to load screen when user select.
   * @param criteria is a Resut set payload which we recieve from API.
   */
  loadScreenerList(criteria) {
    // criteria = "[{\"header\":\"StartingUniverse\",\"factorId\":[9, 10, 11],\"valueId\":[-1,-1]},{\"header\":\"PowerGaugeRating\",\"factorId\":29,\"valueId\":-1},{\"header\":\"RatingComponents&Factors1\",\"factorId\":51,\"valueId\":2},{\"header\":\"RatingComponents&Factors2\",\"factorId\":61,\"valueId\":13},{\"header\":\"RatingComponents&Factors3\",\"factorId\":83,\"valueId\":25},{\"header\":\"PriceReturn\",\"factorId\":169,\"valueId\":37},{\"header\":\"PriceReturn2\",\"factorId\":176,\"valueId\":50},{\"header\":\"RelReturn\",\"factorId\":183,\"valueId\":63},{\"header\":\"PriceLevels\",\"factorId\":192,\"valueId\":-1},{\"header\":\"PriceLevels2\",\"factorId\":34537,\"valueId\":-1},{\"header\":\"ChaikinMoneyFlow\",\"factorId\":215,\"valueId\":-1},{\"header\":\"ChaikinRelStrength\",\"factorId\":226,\"valueId\":-1},{\"header\":\"RelativeVolume\",\"factorId\":227,\"valueId\":-1},{\"header\":\"MinPrice\",\"factorId\":302,\"valueId\":-1},{\"header\":\"MaxPrice\",\"factorId\":307,\"valueId\":-1},{\"header\":\"MarketCap\",\"factorId\":235,\"valueId\":-1},{\"header\":\"DivYield\",\"factorId\":241,\"valueId\":-1},{\"header\":\"EarningsReportDate\",\"factorId\":245,\"valueId\":-1},{\"header\":\"Beta\",\"factorId\":291,\"valueId\":-1},{\"header\":\"Stockhastriggered\",\"factorId\":264,\"valueId\":75}]"
    let screenerCriteria = JSON.parse(criteria);

    let screenerObjRef = JSON.parse(JSON.stringify(this.screenerOriginalData));
    screenerCriteria.forEach((ele) => {
      if (ele.header === 'StartingUniverse') {
        screenerObjRef['Starting Universe'].factor.forEach((val) => {
          if (val.id == ele.factorId) {
            val.defaultStatus = 1;
            this.updateValueOnChange({
              data: val,
              headerName: 'Starting Universe',
            });
          } else {
            val.defaultStatus = 0;
          }
        });
        if (typeof ele.factorId != 'object') {
          let defaultUserListObj,
            checkUser = false;
          screenerObjRef['User Lists'].factor.forEach((val) => {
            if (val.defaultStatus == 1) {
              defaultUserListObj = val;
            }

            if (val.id == ele.factorId) {
              val.defaultStatus = 1;
              let fil = screenerObjRef['Starting Universe'].factor.filter(
                (e) => e.name == 'Select a User List...'
              );
              fil[0].defaultStatus = 1;
              checkUser = true;
              this.updateValueOnChange({
                data: fil[0],
                headerName: 'Starting Universe',
              });
            } else {
              val.defaultStatus = 0;
            }
          });

          if (!checkUser && defaultUserListObj) {
            defaultUserListObj.defaultStatus = 1;
          }

          let defaultIndustryListObj,
            checkUserIndustry = false;
          screenerObjRef['Industry'].factor.forEach((val) => {
            if (val.defaultStatus == 1) {
              defaultIndustryListObj = val;
            }

            if (val.id == ele.factorId) {
              val.defaultStatus = 1;
              let fil = screenerObjRef['Starting Universe'].factor.filter(
                (e) => e.name == 'Select an Industry...'
              );
              fil[0].defaultStatus = 1;
              checkUserIndustry = true;
              this.updateValueOnChange({
                data: fil[0],
                headerName: 'Starting Universe',
              });
            } else {
              val.defaultStatus = 0;
            }
          });
          if (!checkUserIndustry && defaultIndustryListObj) {
            defaultIndustryListObj.defaultStatus = 1;
          }
        } else {
          let checkSector = false,
            checkSubSector = false;

          screenerObjRef['Sector'].factor.forEach((val) => {
            if (ele.factorId.indexOf(val.id) !== -1) {
              val.defaultStatus = 1;

              checkSector = true;
            } else {
              val.defaultStatus = 0;
            }
          });
          if (checkSector) {
            let fil = screenerObjRef['Starting Universe'].factor.filter(
              (e) => e.name == 'Select Sectors...'
            );
            fil[0].defaultStatus = 1;
            this.updateValueOnChange({
              data: fil[0],
              headerName: 'Starting Universe',
            });
          }

          screenerObjRef['SubSector'].factor.forEach((val) => {
            if (ele.factorId.indexOf(val.id) !== -1) {
              val.defaultStatus = 1;
              checkSubSector = true;
            } else {
              val.defaultStatus = 0;
            }
          });
          if (checkSubSector) {
            let fil = screenerObjRef['Starting Universe'].factor.filter(
              (e) => e.name == 'Select Subsectors...'
            );
            fil[0].defaultStatus = 1;
            this.updateValueOnChange({
              data: fil[0],
              headerName: 'Starting Universe',
            });
          }
        }
      }

      if (ele.header === 'PowerGaugeRating') {
        if (typeof ele.factorId != 'object') {
          screenerObjRef['Power Gauge Rating'].factor.forEach((val) => {
            if (val.id == ele.factorId) {
              val.defaultStatus = 1;
            } else {
              val.defaultStatus = 0;
            }
          });
        } else {
          screenerObjRef['Power Gauge Rating'].factor.forEach((val) => {
            if (ele.factorId.indexOf(val.id) !== -1) {
              val.defaultStatus = 1;
            } else {
              val.defaultStatus = 0;
            }
          });
        }
      }

      this.ScreenerHeaderObject.forEach((value) => {
        let key = value.replace(/ /g, '');
        if (ele.header === key) {
          this.loadFactorData(screenerObjRef[value], ele);
        }
      });
    });

    this.screenerInitialData = JSON.parse(JSON.stringify(screenerObjRef));
    this.setStockRatingFormValues(this.screenerInitialData);
  }

  loadFactorData(mainData, criteriaObj) {
    let factor = mainData.factor;
    let value = mainData.value ? mainData.value : null;
    if (factor) {
      if (typeof criteriaObj.factorId != 'object') {
        factor.forEach((val) => {
          if (val.id == criteriaObj.factorId) {
            val.defaultStatus = 1;
          } else {
            val.defaultStatus = 0;
          }
        });
      } else {
        factor.forEach((val) => {
          if (criteriaObj.factorId.indexOf(val.id) !== -1) {
            val.defaultStatus = 1;
          } else {
            val.defaultStatus = 0;
          }
        });
      }
    }

    if (value) {
      if (typeof criteriaObj.valueId != 'object') {
        value.forEach((val) => {
          if (val.id == criteriaObj.valueId) {
            val.defaultStatus = 1;
          } else {
            val.defaultStatus = 0;
          }
        });
      } else {
        value.forEach((val) => {
          if (criteriaObj.valueId.indexOf(val.id) !== -1) {
            val.defaultStatus = 1;
          } else {
            val.defaultStatus = 0;
          }
        });
      }
    }
  }

  /**
   * Function used to set the form values for rating
   * @param data is Power Gauge Rating data
   */
  setStockRatingFormValues(data) {
    let ratings = data['Power Gauge Rating']['factor'];
    let ratingChange = ratings.filter((ele) => ele.name.includes('turned'));
    let ratingPersistency = ratings.filter((ele) =>
      ele.name.includes('persistency')
    );
    let combinedRating = ratingChange.concat(ratingPersistency);
    let idOfCombinedRating = combinedRating.map((ele) => ele.id);
    let currentRating = ratings.filter(
      (ele) => !idOfCombinedRating.includes(ele.id)
    );

    let currentRatingForm = {};
    currentRating.forEach((element) => {
      let value = element.defaultStatus === 1 ? true : false;
      currentRatingForm[element.name.replace(/ /g, '')] = new FormControl(
        value
      );
    });

    let currentRatingFormGp = new FormGroup(currentRatingForm);

    let radioGroupValue = 'currentRating';
    let selectValue = '';
    let ratingChangeObj = ratingChange.filter((e) => e['defaultStatus'] == 1);
    if (ratingChangeObj.length > 0) {
      radioGroupValue = 'ratingChange';
      selectValue = ratingChangeObj[0]['name'].replace(/ /g, '');
      this.stockRatingLabel = ratingChangeObj[0]['name'];
    }

    let perObj = ratingPersistency.filter((e) => e['defaultStatus'] == 1);
    if (perObj.length > 0) {
      radioGroupValue = 'ratingPersistency';
      selectValue = ratingChangeObj[0]['name'].replace(/ /g, '');
      this.stockRatingLabel = ratingChangeObj[0]['name'];
    }

    let ratingChangeValue =
      radioGroupValue == 'ratingChange' ? selectValue : '';
    let ratingPerValue =
      radioGroupValue == 'ratingPersistency' ? selectValue : '';
    // this.stockRatingForm = new FormGroup({
    //     radioGroup: new FormControl('currentRating'),
    //     currentRatingGp: currentRatingFormGp,
    //     ratingChange: new FormControl(''),
    //     ratingPersistency: new FormControl(''),

    // })
    this.stockRatingForm = new FormGroup({
      radioGroup: new FormControl(radioGroupValue),
      currentRatingGp: currentRatingFormGp,
      ratingChange: new FormControl(ratingChangeValue),
      ratingPersistency: new FormControl(ratingPerValue),
    });

    this.currentRating = currentRating.map((ele) => {
      return { data: ele, value: ele.name.replace(/ /g, '') };
    });
    this.ratingChange = ratingChange.map((ele) => {
      return { data: ele, value: ele.name.replace(/ /g, '') };
    });
    this.ratingPersistency = ratingPersistency.map((ele) => {
      return { data: ele, value: ele.name.replace(/ /g, '') };
    });
    this.setFormLabel(this.stockRatingForm.value);
    this.stockRatingForm.valueChanges.subscribe((ele) => {
      this.setFormLabel(ele);
    });
  }

  /**
   * @param formValue is form value of
   */
  setFormLabel(formValue) {
    this.stockRatingLabel = '';
    if (formValue.radioGroup === 'currentRating') {
      let keys = Object.keys(formValue.currentRatingGp);
      let counter = 0;
      keys.forEach((ele) => {
        if (formValue.currentRatingGp[ele]) {
          if (counter == 0) {
            this.stockRatingLabel = ele;
          } else {
            this.stockRatingLabel = this.stockRatingLabel + ', ' + ele;
          }
          counter++;
        }
      });
    } else if (formValue.radioGroup === 'ratingPersistency') {
      this.ratingPersistency.forEach((ele) => {
        if (ele.value == formValue.ratingPersistency) {
          this.stockRatingLabel = ele.data.name;
        }
      });
    } else if (formValue.radioGroup === 'ratingChange') {
      this.ratingChange.forEach((ele) => {
        if (ele.value == formValue.ratingChange) {
          this.stockRatingLabel = ele.data.name;
        }
      });
    }
  }

  ngOnChanges(changes: SimpleChanges): void {}

  /**
   * Function get initial data for stock screener
   */
  getScreenerInitialData() {
    let uid = localStorage.getItem('UID');
    
    const httpGetSymbolData$ = this.api.getScreenerInitialData(uid);

    httpGetSymbolData$
      .pipe(
        map((resp) => {
          const screenerInitialData = resp;
          return this.prepareScreenerInitialData(screenerInitialData);
        }),
      )
      .subscribe((screenerInitialData: screenerInitialDataModel) => {
        this.screenerInitialData = screenerInitialData;

        this.screenerOriginalData = JSON.parse(
          JSON.stringify(screenerInitialData)
        );

        if (this.selectedListName != 'Select a screen') {
          let selectedListNameData = this.investorListData.filter(
            (ele) => ele.listName === this.selectedListName
          );
          if (selectedListNameData.length > 0) {
            this.loadScreenerList(
              JSON.stringify(selectedListNameData[0].screenCriteria)
            );
          }
        } else if (this.selectedListName == 'Select a screen') {
          this.setStockRatingFormValues(screenerInitialData);
        }
      });
  }

  /**
   * Function prepare formatted data for stock screener and return screenerInitialDataModel.
   * @param data is data from api
   * @returns
   */
  prepareScreenerInitialData(data: any): screenerInitialDataModel {
    let screenerInitialData: screenerInitialDataModel =
      new screenerInitialDataModel();

    data.forEach((element) => {
      let screenerFirstModel = new ScreenerFirstModel();
      // Get Factor name like "Starting Universe","Power Gauge Rating"
      let objectKey = Object.keys(element)[0];
      // Get factor Object if exist.
      let factor = element[objectKey].filter((val) => {
        if (val.factor) {
          return val.factor;
        }
      });

      // If we have factor then convert into ScreenerDataModel data
      if (factor.length) {
        let factorObject: ScreenerDataModel[] = factor[0]['factor'].map(
          (val) => {
            let screenerFactorModel = new ScreenerDataModel();
            screenerFactorModel.defaultStatus = val.defaultStatus;
            screenerFactorModel.id = val.id;
            screenerFactorModel.name = val.name;
            return screenerFactorModel;
          }
        );

        screenerFirstModel.factor = factorObject;
      }

      // Get value Object if exist.
      let value = element[objectKey].filter((val) => {
        if (val.value) {
          return val.value;
        }
      });

      // If we have value then convert into ScreenerValueModel data
      if (value.length) {
        let valueObject: ScreenerDataModel[] = value[0].value.map((val) => {
          let screenerValueModel = new ScreenerDataModel();
          screenerValueModel.defaultStatus = val.defaultStatus;
          screenerValueModel.id = val.id;
          screenerValueModel.name = val.name;
          return screenerValueModel;
        });
        screenerFirstModel.value = valueObject;
      }

      screenerInitialData[objectKey] = screenerFirstModel;
    });

    return screenerInitialData;
  }

  updateValueOnChange(selectedData: {
    data: ScreenerDataModel;
    headerName: string;
  }): void {
    //  this.clearListName();

    if (selectedData.headerName === 'Starting Universe') {
      this.selectedUniverse = selectedData.data;
    }
  }

  /**
   * Function return payload for get result api.
   * @returns payload for [{header:String , factorId: [] | number, valueId: [] | number}]
   */
  calculateResultCriteria() {
    let resultPayload = [];
    let resultSetForObj;

    // Build Starting Universe Object
    resultPayload.push(this.createResultSetForStartingUniverse());

    // Build Power Gauge Rating Object
    this.buildStockRatingDataForResult();
    resultPayload.push(
      this.getResultSet(
        'Power Gauge Rating',
        this.screenerInitialData['Power Gauge Rating']
      )
    );

    this.ScreenerHeaderObject.forEach((value) => {
      resultPayload.push(
        this.getResultSet(value, this.screenerInitialData[value])
      );
    });

    return resultPayload;
  }

  /**
   * Function hit result api to see data for applied filter.
   */
  getResult() {
    let resultPayload = this.calculateResultCriteria();

    this.getScreenerResults(resultPayload);
  }

  buildStockRatingDataForResult() {
    let ratingFormValue = this.stockRatingForm.value;
    let ratingFactor = this.screenerInitialData['Power Gauge Rating']['factor'];
    if (
      ratingFormValue.radioGroup === 'ratingPersistency' &&
      ratingFormValue.ratingPersistency
    ) {
      let filterValue = this.ratingPersistency.filter(
        (ele) => ele.value === ratingFormValue.ratingPersistency
      );

      if (filterValue.length > 0) {
        let id = filterValue[0].data.id;
        ratingFactor.forEach((ele) => {
          if (ele.id === id) {
            ele.defaultStatus = 1;
          } else {
            ele.defaultStatus = 0;
          }
        });
      }
    } else if (
      ratingFormValue.radioGroup === 'ratingChange' &&
      ratingFormValue.ratingChange
    ) {
      let filterValue = this.ratingChange.filter(
        (ele) => ele.value === ratingFormValue.ratingChange
      );
      if (filterValue.length > 0) {
        let id = filterValue[0].data.id;
        ratingFactor.forEach((ele) => {
          if (ele.id === id) {
            ele.defaultStatus = 1;
          } else {
            ele.defaultStatus = 0;
          }
        });
      }
    } else {
      let objJson = {};
      this.currentRating.forEach((ele) => {
        if (ratingFormValue.currentRatingGp[ele.value]) {
          ele.data.defaultStatus = 1;
          objJson[ele.data.id] = 1;
        } else {
          ele.data.defaultStatus = 0;
          objJson[ele.data.id] = 0;
        }
      });
      let flag = 0;
      ratingFactor.forEach((ele) => {
        if (objJson[ele.id]) {
          ele.defaultStatus = 1;
          flag++;
        } else {
          ele.defaultStatus = 0;
        }
      });
      if (flag == 0) {
        ratingFactor[0].defaultStatus = 1;
      }
    }
  }

  /**
   * Function Hits api to get result and set global variable with that data
   * @param resultPayload is payload for result api
   */
  getScreenerResults(resultPayload) {
    let uid = localStorage.getItem('UID');
    const httpGetScreenerResultData$ = this.api.getScreenerResults(
      uid,
      JSON.stringify(resultPayload)
    );

    httpGetScreenerResultData$
      .pipe(
        map((resp) => {
          const screenerResultData =
            resp as unknown as ScreenerResultDataObject[];
          return this.prepareScreenerResultData(screenerResultData);
        })
      )
      .subscribe((screenerResultData: ScreenerResultSymbolModel[]) => {
        this.screenerResultData = screenerResultData;
      });
  }

  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || 'server error.');
  }

  /**
   * Function prepare data which we bind on UI to see result Card.
   * @param data is data for result api
   * @returns
   */
  prepareScreenerResultData(
    data: ScreenerResultDataObject[]
  ): ScreenerResultSymbolModel[] {
    let resultSymbols: ScreenerResultSymbolModel[] = [];

    let symbols = data[0]['symbols'];
    symbols.forEach((ele) => {
      let symbolData = new ScreenerResultSymbolModel();
      symbolData.change = ele.Change;
      symbolData.last = ele.Last;
      symbolData.pgr = ele.PGR;
      symbolData.percentage = ele.Percentage;
      symbolData.summaryRating = ele['SummaryRating '];
      symbolData.technicalRating = ele['TechnicalRating '];
      symbolData.industryListId = ele['industry_ListID '];
      symbolData.isEtf = ele.is_etf;
      symbolData.name = ele.name;
      symbolData.rawPgr = ele.raw_PGR;
      symbolData.signals = ele.signals;
      symbolData.symbol = ele.symbol;

      symbolData.pgrImage = SharedUtil.getPgrMicroArcWithoutNeutralValue(
        ele.PGR,
        ele.raw_PGR,
        ele.is_etf
      );
      symbolData.pgrName = SharedUtil.getPgrNameWithoutNeutralValues(
        SharedUtil.getPgrRating(ele.PGR, ele.raw_PGR)
      );
      symbolData.ratingTypeColorLabel =
        SharedUtil.getColorForRating(symbolData.pgrName) + '-label';
      symbolData.changeFormattedClass = ele.Change > 0 ? 'up' : 'down';
      symbolData.percentageFormattedClass = ele.Percentage > 0 ? 'up' : 'down';
      symbolData.lastFormatted =
        '$' + SharedUtil.formatPriceDashWat(ele.Last).toString();
      symbolData.percentageFormatted =
        '(' + SharedUtil.formatChangeDashWat(ele.Percentage).toString() + ')';
      symbolData.rating = SharedUtil.getPgrRating(ele.PGR, ele.raw_PGR);
      resultSymbols.push(symbolData);
    });

    return resultSymbols;
  }

  /**
   * Function uses to reset filters.
   */
  resetFilters() {
    // clear result pannel.
    this.screenerResultData = [];
    this.clearListName();
    this.getScreenerInitialData();
  }

  /**
   * Function calls when user select stock idea list and load filers with that data.
   * @param value list name in string.
   */
  selectScreenerList(value) {
    this.selectedListName = value;
    localStorage.setItem('screenerUtilityListName', value);
    let selectedListNameData = this.investorListData.filter(
      (ele) => ele.listName === value
    );
    if (selectedListNameData.length > 0) {
      let data = selectedListNameData[0];

      this.loadScreenerList(JSON.stringify(data.screenCriteria));
    }
  }

  /**
   * function uses to save screen with filter data.
   */
  saveScreen() {
    let resultPayload = this.calculateResultCriteria();
    let criteria = resultPayload.map((ele) => {
      ele.header = ele.header.replace(/ /g, '');
      return ele;
    });

    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      headerText: 'Add List',
      AddListScreentitleText: 'Select the type of list you want to create:',
      EditScreentitleText: 'Please fill out the following information:',
      editScreenButtonText: 'Continue',
      editListEnable: false,
      screenData: null,
      updationMode: 'screener',
      criteria,
    };

    const dialogRef = this.dialog.open(ListEditDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.getInvestorIdeaList();
        // this.toastService.showSuccess('successfully added');
      }
    });
  }

  /**
   * Function uses to update screen with new changes.
   */
  updateScreen() {
    let resultPayload = this.calculateResultCriteria();

    let criteria = resultPayload.map((ele) => {
      ele.header = ele.header.replace(/ /g, '');
      return ele;
    });
    let list;
    let selectedListNameData = this.investorListData.filter(
      (ele) => ele.listName === this.selectedListName
    );
    if (selectedListNameData.length > 0) {
      list = JSON.parse(JSON.stringify(selectedListNameData[0]));
      list.screenCriteria = criteria;
    }

    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      headerText: 'Edit List',
      AddListScreentitleText: 'Select the type of list you want to create:',
      EditScreentitleText: 'Please fill out the following information:',
      editScreenButtonText: 'Update List',
      editListEnable: true,
      screenData: list,
      updationMode: 'screener',
    };

    const dialogRef = this.dialog.open(ListEditDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((result) => {
      if (result.message == 'Update List') {
        this.getInvestorIdeaList();
        // this.toastService.showSuccess('successfully added');
      }
    });
  }

  /**
   * Function call api to get all Idea screens.
   */
  getInvestorIdeaList() {
    const httpGetInvestorListsDetails$ = this.api.getInvestorListsDetails();
    httpGetInvestorListsDetails$
      .pipe(
        map((resp) => {
          const ideaListData = resp as unknown as InvestorIdeaListDataObject;
          return this.prepareInvestorIdeaListData(ideaListData);
        })
      )
      .subscribe((investorListData: InvestorListData[]) => {
        this.investorListData = investorListData;
        let sceenerListName = localStorage.getItem('screenerUtilityListName');

        if (sceenerListName) {
          let selectedListNameData = investorListData.filter(
            (ele) => ele.listName === sceenerListName
          );
          if (selectedListNameData.length > 0) {
            this.selectedListName = selectedListNameData[0].listName;
            this.screenerListControl.patchValue(this.selectedListName);
          }
        } else {
          // localStorage.setItem("screenerUtilityListName", "");
          // this.selectedListName = "Select a screen";
          // this.screenerListControl = new FormControl('');
          this.clearListName();
        }
      });
  }

  /**
   * Function uses to prepare data for investor idea list.
   * @param listData is idealist data.
   * @returns
   */
  prepareInvestorIdeaListData(listData: InvestorIdeaListDataObject) {
    let screenerList = listData.data.filter(
      (ele) => ele.updationMode === 'screener'
    );
    return screenerList;
  }

  /**
   * Function prepare result payload for starting Universe.
   * @returns
   */
  createResultSetForStartingUniverse() {
    let universe = this.screenerInitialData['Starting Universe'].factor;
    let universeObject = universe.filter(
      (element) => element.defaultStatus === 1
    );
    let result;
    let result1;

    // Build Starting Universe Object
    if (universeObject.length > 0) {
      if (universeObject[0].name === 'Select an Industry...') {
        result = this.getResultSet(
          'Starting Universe',
          this.screenerInitialData['Industry']
        );
      } else if (universeObject[0].name === 'Select a User List...') {
        result = this.getResultSet(
          'Starting Universe',
          this.screenerInitialData['User Lists']
        );
      } else if (universeObject[0].name === 'Select Sectors...') {
        result = this.getResultSet(
          'Starting Universe',
          this.screenerInitialData['Sector']
        );
      } else if (universeObject[0].name === 'Select Subsectors...') {
        result = this.getResultSet(
          'Starting Universe',
          this.screenerInitialData['SubSector']
        );
      } else {
        result = this.getResultSet(
          'Starting Universe',
          this.screenerInitialData['Starting Universe']
        );
      }
    }

    return result;
  }

  /**
   * Function users to prepare result payload for result set.
   * @param headerName is name of filter.
   * @param data is type of ScreenerFirstModel
   * @returns
   */
  getResultSet(headerName: string, data: ScreenerFirstModel) {
    let factorId = this.getResultPayloadIds(data.factor);
    return {
      header: headerName,
      factorId: factorId,
      valueId: data.value
        ? this.getResultPayloadIds(data.value)
        : this.addDummyValues(factorId),
    };
  }

  /**
   * Function uses to to prepate factor and value id for result payload.
   * @param data
   * @returns
   */
  getResultPayloadIds(data: ScreenerDataModel[]) {
    let selectedData = data.filter((element) => element.defaultStatus === 1);
    let values = [];
    if (selectedData.length === 1) {
      // TODO: review for bug.
      return selectedData[0].id;
    } else if (selectedData.length > 1) {
      selectedData.forEach((ele) => {
        values.push(ele.id);
      });
      return values;
    }
  }

  /**
   * Function set dummy values if value is not defined.
   * @param data
   * @returns
   */
  addDummyValues(data) {
    if (Array.isArray(data)) {
      let value = [];
      return data.map((ele) => -1);
    } else {
      return -1;
    }
  }

  /**
   * Function uses to clear the list name.
   */
  clearListName() {
    localStorage.setItem('screenerUtilityListName', '');
    this.selectedListName = 'Select a screen';
    this.screenerListControl = new FormControl('');
  }
}
