import { EventEmitter, Component, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';

import { InvestorListData } from 'src/app/core/data-models/service-model';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss'],
})
export class TopBarComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}

  @Input() selectedListName!: string;
  @Input() screenerListControl!: FormControl;
  @Input() investorListData: InvestorListData[];

  @Output() selectScreenerList = new EventEmitter<string>();
  @Output() saveScreen = new EventEmitter();
  @Output() updateScreen = new EventEmitter();
}
