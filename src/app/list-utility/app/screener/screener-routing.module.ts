import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ScreenerComponent } from "./screener.component";


const routes: Routes = [
    { 
        path: '',
        component: ScreenerComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: []
})
export class ScreenerRoutingModule { }