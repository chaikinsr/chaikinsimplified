export const FACTORS_LIST = [
  {
    label: 'Technical Rank',
    headerName: 'ETF Technical Rank',
  },
  {
    label: 'US Equity PGRs',
    headerName: 'ETF Avg Stock Rating',
  },
  {
    label: 'US Equity (Weighted)',
    headerName: 'ETF Wavg Stock Rating',
  },
  {
    label: 'ETF Power Bar',
    headerName: 'ETF Power Bar',
  },
];

export const PERFORMANCE_LIST = [
  {
    label: 'Price Return',
    headerName: 'ETF Price Return',
  },
  {
    label: 'Price Return2',
    headerName: 'ETF Price Return2',
  },
  {
    label: 'Return vs. SPY',
    headerName: 'ETF Return vs. SPY',
  },
];

export const TECHNICALS_LIST = [
  {
    label: 'Price Level',
    headerName: 'ETF Price Level',
  },
  {
    label: 'Price Level 2',
    HeaderName: 'ETF Price Level2',
  },
  {
    label: 'Money Flow',
    headerName: 'ETF Money Flow',
  },
  {
    label: 'Rel Strength',
    headerName: 'ETF Rel Strength',
  },
  {
    label: 'Relative Volume',
    headerName: 'ETF Relative Volume',
  },
  {
    label: 'Min Price',
    headerName: 'ETF Min Price',
  },
  {
    label: 'Max Price',
    headerName: 'ETF Max Price',
  },
];

export const ETF_CHARACTERISTICS_LIST = [
  {
    label: 'Asset Class',
    headerName: 'ETF Asset Class',
  },
  {
    label: 'Long/Short',
    headerName: 'ETF Long/Short',
  },
  {
    label: 'Management',
    headerName: 'ETF Management',
  },
  {
    label: 'Expense Ratio',
    headerName: 'ETF Expense Ratio',
  },
  {
    label: 'Dividend Yield',
    headerName: 'ETF Dividend Yield',
  },
  {
    label: 'Assets',
    headerName: 'ETF Assets',
  },
  {
    label: 'Avg Daily Volume',
    headerName: 'ETF Avg Daily Volume',
  },
  {
    label: 'Beta',
    headerName: 'ETF Beta',
  },
  {
    label: 'ETF/ETN',
    headerName: 'ETF/ETN',
  },
  {
    label: 'ETF Issuer',
    headerName: 'ETF Issuer',
  },
];

export const SELECT_A_SCREEN = 'Select a Screen';