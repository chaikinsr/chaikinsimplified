import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatNativeDateModule } from '@angular/material/core';
import { MatRadioModule } from '@angular/material/radio';
import { MatSortModule } from '@angular/material/sort';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTableModule } from '@angular/material/table';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatTabsModule } from '@angular/material/tabs';

// import { SharedModule } from '../shared/shared.module';
import { AppletHeaderModule } from 'src/app/layout/header/applet-header.module';

import { ScreenerComponent } from './screener.component';
import { ScreenerRoutingModule } from './screener-routing.module';
import { SharedUtilModule } from '../shared/shared.module';
import { EtfComponent } from './components/etf/etf.component';
import { ScreenerResultsComponent } from './components/screener-results/screener-results.component';
import { TopBarComponent } from './components/top-bar/top-bar.component';
import { FormActionsComponent } from './components/form-actions/form-actions.component';
import { StocksComponent } from './components/stocks/stocks.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ScreenerRoutingModule,
    HttpClientModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDialogModule,
    MatDividerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSortModule,
    MatSelectModule,
    MatTooltipModule,
    MatAutocompleteModule,
    MatTableModule,
    AppletHeaderModule,
    MatGridListModule,
    SharedUtilModule,
    MatSidenavModule,
    MatTabsModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
  ],
  declarations: [
    ScreenerComponent,
    EtfComponent,
    ScreenerResultsComponent,
    TopBarComponent,
    FormActionsComponent,
    StocksComponent,
  ],
  bootstrap: [ScreenerComponent],
})
export class ScreenerModule {}
