import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styles: [],
})
export class ConfirmDialogComponent implements OnInit {
  title: string;
  message: string;
  noButtonText: string;
  yesButtonText: string;

  constructor(
    public dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    if (data.title) {
        this.title = data.title;
    }
    if (data.message) {
        this.message = data.message;
    }
    if (data.noButtonText) {
      this.noButtonText = data.noButtonText;
    }
    if (data.yesButtonText) {
        this.yesButtonText = data.yesButtonText;
    }
  }

  ngOnInit(): void {}

  onConfirm(): void {
    this.dialogRef.close(true);
  }

  onDismiss(): void {
    this.dialogRef.close(false);
  }
}
