import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from "@angular/core";
import { fromEvent } from "rxjs";
import { debounceTime, distinctUntilChanged, map } from "rxjs/operators";



@Component({
    selector: 'app-mat-input',
    templateUrl: './input.component.html',
    styleUrls: ['./input.component.scss']
})
export class MatInputComponent implements OnInit, AfterViewInit {
    @Input() inputFormControlName: string;
    @Input() inputType: string;
    @Input() id: string;
    @Input() inputPlaceholder: string;
    @Input() inputValue: string;
    @ViewChild('inputElement') inputElement: ElementRef;
    constructor() {

    }

    ngOnInit(): void {
        
    }

    ngAfterViewInit(): void {
        fromEvent(this.inputElement.nativeElement, 'input')
          .pipe(map((event: Event) => (event.target as HTMLInputElement).value))
          .pipe(debounceTime(3000))
          .pipe(distinctUntilChanged())
          .subscribe(data => notTypingStuff(data));
      }

}

function notTypingStuff(data): void {
    console.log(data);
}
