import { CdkStep, StepperSelectionEvent } from '@angular/cdk/stepper';
import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { MatStepper } from '@angular/material/stepper';
import { map } from 'rxjs/operators';
import { InvestorListData } from 'src/app/core/data-models/service-model';
import { APIService } from 'src/app/core/http-services/api.service';

@Component({
  selector: 'app-list-edit-dialog',
  templateUrl: './list-edit-dialog.component.html',
  styleUrls: ['./list-edit-dialog.component.scss'],
})
export class ListEditDialogComponent implements OnInit, AfterViewInit {
  isLoading = true;
  isUploadStepBtnDisabled = false;
  invalidFileMsg: string;
  fileToUpload: File = null;
  fileData: any;
  columnForTickers = 0;
  myStepper: MatStepper;
  headerText: string;
  updationMsg: string;
  AcknowledgeNoticeHeader: string;
  // Variable Related to edit screen view
  editScreenButtonText: string = 'Continue';
  editListEnable: boolean = false;
  screenData: InvestorListData;
  listDetailScreenErrorMsg = '';
  EditScreentitleText: string;
  updationMode: string;
  criteria: any;
 
  primarySort1 = [
    { id: 'ASC', view: 'Alphabetical A-Z'  , name : "Alphabetical" , sortingOrderId : 0},
    { id: 'DESC', view: 'Rating (Very Bullish to Very Bearish)' , name: 'Rating' , sortingOrderId : 1},
    { id: 'ASC',  view: 'Rating (Very Bearish to Very Bullish)' ,   name : 'Rating' , sortingOrderId : 2},
    { id: 'DESC', view: 'Performace YTD (Highest to Lowest)',   name : 'Performace YTD' , sortingOrderId : 3},
    { id: 'DESC', view: 'Rating Raw Rank(Highest to Lowest)', name : 'Rating Raw Rank' , sortingOrderId : 4}
];
secondarySort1 = [
  { id: 'ASC', view: 'Alphabetical A-Z', name :"Alphabetical" , sortingOrderId : 0},
  { id: 'DESC', view: 'Rating (Very Bullish to Very Bearish)' , name : 'Rating' , sortingOrderId : 1},
  { id: 'ASC',  view: 'Rating (Very Bearish to Very Bullish)' ,   name : 'Rating' , sortingOrderId : 2},
  { id: 'DESC', view: 'Performace YTD (Highest to Lowest)',   name: 'Performace YTD', sortingOrderId : 3 },
  { id: 'DESC', view: 'Rating Raw Rank(Highest to Lowest)',  name : 'Rating Raw Rank', sortingOrderId : 4},
];
selectedValueForPrimary =  { id: 'ASC',  view: 'Rating (Very Bearish to Very Bullish)' ,   name : 'Rating' , sortingOrderId : 2}
;
selectedValueForSecondary = { id: 'ASC', view: 'Alphabetical A-Z', name :"Alphabetical" , sortingOrderId : 0};
  filterType: string = 'screener';

  // Variable Realated to Create New List.
  listType = 'import';
  manualSaveMsg: string = '';
  manualcsvTickers: string = '';
  AddListScreentitleText: string;
  importInvalidFileMsg: string;
  
  @ViewChild('fileInput') fileInputVar: ElementRef;
  @ViewChild('stepper') private stepper: MatStepper;
  currentStep = 0;
  listDetailForm = new FormGroup({
    listName: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    description: new FormControl(''),
    guru: new FormControl(''),
    limit: new FormControl(0),
    isActive: new FormControl(true),
    pubCodes: new FormControl(''),
    primarySort: new FormControl('Rating (Very Bullish to Very Bearish)'),
    secondarySort: new FormControl('Alphabetical A-Z'),
    primarySortOrder: new FormControl('DESC'),
    secondarySortOrder: new FormControl('ASC')
  });

constructor(
    private api: APIService,
    public dialogRef: MatDialogRef<ListEditDialogComponent>,
@Inject(MAT_DIALOG_DATA) data
  ) {

    this.headerText = data.headerText ? data.headerText : '';
    this.EditScreentitleText = data.EditScreentitleText
      ? data.EditScreentitleText
      : '';
    this.editScreenButtonText = data.editScreenButtonText
      ? data.editScreenButtonText
      : 'Continue';
    this.editListEnable = data.editListEnable ? data.editListEnable : false;
    this.screenData = data.screenData ? data.screenData : null;
    this.AddListScreentitleText = data.AddListScreentitleText
      ? data.AddListScreentitleText
      : '';
    this.updationMode = data.updationMode ? data.updationMode : 'mannual';
    this.criteria = data.criteria ? data.criteria : null;
    this.filterType = data.filterType ? data.filterType : 'screener';
    if (this.editListEnable) {
      this.listDetailForm.controls['listName'].disable();
    }

    if (this.screenData) {
      this.selectedValueForPrimary = this.filterSortingData(this.screenData.primarySort , this.primarySort1 ,'primarySortOrder', this.screenData.primarySortOrder);
      this.selectedValueForSecondary = this.filterSortingData(this.screenData.secondarySort , this.secondarySort1 ,'secondarySortOrder', this.screenData.secondarySortOrder);

      this.listDetailForm.patchValue({
        listName: this.screenData.listName,
        description: this.screenData.description,
        guru: this.screenData.guru,
        limit: this.screenData.limit,
        isActive: this.screenData.isActive,
        pubCodes: this.screenData.associatedPubCodes,
        primarySort: this.selectedValueForPrimary.view,
        secondarySort: this.selectedValueForSecondary.view,
        primarySortOrder: this.selectedValueForPrimary.id,
        secondarySortOrder: this.selectedValueForSecondary.id
      });
      this.manualcsvTickers = this.screenData.symbols.join(',');
    }
  }
ngAfterViewInit(): void {}

updateAcknowledgeMgs(headerMgs: string, updateMsg: string) {
    this.AcknowledgeNoticeHeader = headerMgs;
    this.updationMsg = updateMsg;
    this.stepper.selectedIndex = 4;
  }

onCompleteFileImport() {
    this.dialogRef.close({
      message: 'Update List',
    });
  }



stepperChange(event: StepperSelectionEvent) {
    if (event.selectedIndex === 1) {
      this.clearListDetailData();
    }
  }

ngOnInit(): void {
    this.clearListDetailData();
    if (this.editListEnable && this.headerText == 'Edit List') {
      this.currentStep = 1;
    }
if (this.headerText == 'Add List' && this.criteria) {
      this.currentStep = 1;
    }

if (this.headerText == 'Add Symbols') {
      this.currentStep = 3;
    }
   
  }

onClose() {
    this.dialogRef.close();
  }

onUploadStepContinue() {
    this.importInvalidFileMsg = '';
    if (!this.fileToUpload) {
      this.invalidFileMsg = 'Select a valid csv file to continue.';
      return;
    }
    this.isUploadStepBtnDisabled = true;

    let file = this.fileToUpload;

    let reader = new FileReader();
    reader.readAsArrayBuffer(file);

    reader.onload = (e) => {
      let data: any = e.target['result']; //e.target.result;// reader.result;
      //console.log(data);
      let isMatch = true;
      var data1 = new Uint8Array(data);
      // console.log(data1);
      var dataFile = '';
      var arr = new Array();
      for (let i = 0; i != data1.length; ++i) {
        arr[i] = String.fromCharCode(data1[i]);
        // console.log(arr[i]);
        let match = /\r/.exec(arr[i]); //CPT-2302
        let match1 = /\n/.exec(String.fromCharCode(data1[i + 1]));
        //	console.log(match, match1);
        if (match1) {
          if (i + 2 == data1.length) {
            isMatch = false;
          }
        }
        if (match && !isMatch) {
          arr[i] = '\r\n';
        }
      }
      // console.log(arr);
      dataFile = arr.join("");
      this.fileData = dataFile;
      let ticker = this.filterFileData(true, 0, dataFile);
      let formValues = this.listDetailForm.value;
      let investorListData = new InvestorListData();
      investorListData.listName = formValues.listName
        ? formValues.listName
        : this.screenData.listName;
      investorListData.description = formValues.description;
      investorListData.guru = formValues.guru;
      investorListData.limit = formValues.limit;
      investorListData.associatedPubCodes =
        typeof formValues.pubCodes === 'string'
          ? formValues.pubCodes.split(',')
          : formValues.pubCodes;
      investorListData.isActive = formValues.isActive;
      investorListData.primarySort = this.selectedValueForPrimary.name;
      investorListData.primarySortOrder = formValues.primarySortOrder;
      investorListData.secondarySort = this.selectedValueForSecondary.name;
      investorListData.secondarySortOrder = formValues.secondarySortOrder;

      investorListData.screenCriteria = null;
      investorListData.symbols = ticker;
      investorListData.updationMode = 'mannual';

      this.updateInvestorDetailApi(investorListData);
    
    };
  }

filterFileData(isNextStep: boolean, columnIndex: number, dataFile) {
    let fileData = dataFile;
    let fileDataArr = [];

    let arrCsvTickers = this.fileData
      .split('\n')
      .join(',')
      .split(',')
      .filter((value) => value.trim() != '')
      .filter((value, index, self) => self.indexOf(value) == index) // distinct
      .map((item) => item.toUpperCase());

    // let manualcsvTickers = arrCsvTickers.join(',');
    // console.log(manualcsvTickers);

    //   let fileDataArrTemp = fileData
    //     .split(/\r?\n/)
    //     .filter(value => value.trim() != "")
    //     .filter((value, index, self) => self.indexOf(value) == index) // distinct
    //     .map(item => item.toUpperCase());

    //     console.log(fileDataArrTemp);

    //   let fileDataArr1 = fileDataArrTemp.map(data => {
    //     return data.split(',')[columnIndex];
    //   });

    //   let fileDataArr2 = fileDataArrTemp.map(data => {
    //     return data.split(',');
    //   });

    //   console.log(fileDataArr1);
    //   console.log(fileDataArr2);

    //   fileDataArr1 = fileDataArr1
    //     .filter(value => value.trim() != "")
    //     .filter((value, index, self) => self.indexOf(value) === index) // distinct
    //     .map(item => item.toUpperCase());

    //     console.log(fileDataArr1);
    return arrCsvTickers;
  }

onChangeSortPrimary(event): any {
   this.selectedValueForPrimary =   this.filterSortingData(event.target.options.selectedIndex , this.primarySort1 , 'primarySortOrder' , '');
  }

onChangeSortSecondary(event): any {
   this.selectedValueForSecondary = this.filterSortingData(event.target.options.selectedIndex , this.secondarySort1 , 'secondarySortOrder', '');
      }

filterSortingData(value , sortData , key , order): any {
        let selectedValue: any;
        sortData.filter(resp => {
          if (resp.sortingOrderId === value){
            selectedValue = resp;
            this.listDetailForm.controls[key].setValue(selectedValue.id);
          }else if(resp.name === value && (resp.id === order || 'ASE' === order ) ){
            selectedValue = resp;
          }
         });
        if(!selectedValue){
          if('primarySortOrder' === key ){
            selectedValue =     { id: 'ASC',  view: 'Rating (Very Bearish to Very Bullish)' ,   name : 'Rating' , sortingOrderId : 2}
        
          }else{
            selectedValue =   { id: 'ASC', view: 'Alphabetical A-Z', name :"Alphabetical" , sortingOrderId : 0}
          }
          
         }
        return selectedValue;
      }

onManualSave(csvTickers: string): void {

        this.manualcsvTickers = csvTickers.replace(/ +/g, "").trim();
        let arrCsvTickers = [];
        if (this.manualcsvTickers) {

          let lastChar = this.manualcsvTickers.slice(-1);
          if (lastChar == ",") {
            this.manualcsvTickers = this.manualcsvTickers.slice(0, -1);
          }


          arrCsvTickers = this.manualcsvTickers
            .split("\n")
            .join(",")
            .split(",")
            .filter(value => value.trim() != "")
            .filter((value, index, self) => self.indexOf(value) == index) // distinct
            .map(item => item.toUpperCase());

          this.manualcsvTickers = arrCsvTickers.join(',');
          this.manualSaveMsg = "";
          if (this.manualcsvTickers.trim() == "") {
            this.manualSaveMsg = "Add comma separated tickers to continue."
            return;
          }
        }
        else {
          this.manualSaveMsg = "Add comma separated tickers to continue."
          return;
        }

let formValues = this.listDetailForm.value;
let investorListData = new InvestorListData();
investorListData.listName = formValues.listName ? formValues.listName : this.screenData.listName;
investorListData.description = formValues.description;
investorListData.guru = formValues.guru;
investorListData.limit = formValues.limit;
investorListData.associatedPubCodes = typeof formValues.pubCodes === 'string' ? formValues.pubCodes.split(',') : formValues.pubCodes;
investorListData.isActive = formValues.isActive;
investorListData.secondarySort = this.selectedValueForSecondary.name;
investorListData.secondarySortOrder = formValues.secondarySortOrder;
investorListData.primarySort = this.selectedValueForPrimary.name;
investorListData.primarySortOrder = formValues.primarySortOrder;


investorListData.screenCriteria = null;
investorListData.symbols = arrCsvTickers ? arrCsvTickers : [];
investorListData.updationMode = 'mannual';
this.updateInvestorDetailApi(investorListData);

        // const maxTickerLimit = 1000;
        // if (arrCsvTickers.length > maxTickerLimit) {
        //   let msg = `The number of tickers separated by comma exceeds the maximum number of ${maxTickerLimit}. 
        //   To accept the first ${maxTickerLimit} tickers for this list click Yes. To terminate the request click No.`;

        //   const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        //     maxWidth: "500px",
        //     data: { title: "Confirm action", message: msg }
        //   });

        //   dialogRef.afterClosed().subscribe(dialogResult => {
        //     let result = dialogResult;
        //     if (result) {

        //       this.manualcsvTickers = arrCsvTickers.slice(0, maxTickerLimit).join(",");
        //       // alert(this.manualcsvTickers)
        //       this.addListToPortfolio();
        //     }
        //   });


        // }
        // else {
        //   // alert(this.manualcsvTickers);
        //   this.addListToPortfolio();
        // }

      }

onSubmitListDetail(form: FormGroup) {
    if (form.status === 'VALID') {
      this.updateListDetails(form.value);
    } else {
      this.listDetailScreenErrorMsg =
        'You have entered invalid values please try again';
    }
  }

updateListDetails(formValues) {
    let investorListData = new InvestorListData();
    investorListData.listName = formValues.listName
      ? formValues.listName
      : this.screenData.listName;
    investorListData.description = formValues.description;
    investorListData.guru = formValues.guru;
    investorListData.limit = formValues.limit;
    investorListData.associatedPubCodes =
      typeof formValues.pubCodes === 'string'
        ? formValues.pubCodes.split(',')
        : formValues.pubCodes;
    investorListData.isActive = formValues.isActive;
    investorListData.primarySort = this.selectedValueForPrimary.name;
    investorListData.primarySortOrder = formValues.primarySortOrder;
    investorListData.secondarySort = this.selectedValueForSecondary.name;
    investorListData.secondarySortOrder = formValues.secondarySortOrder;

    if (this.editListEnable) {
      investorListData.listType = this.screenData.listType;
      investorListData.screenCriteria = this.screenData.screenCriteria;
      investorListData.symbols = this.screenData.symbols;
      investorListData.updationMode = this.screenData.updationMode;
      this.updateInvestorDetailApi(investorListData);

      console.log('Inside if condition', investorListData.updationMode);
    } else {
      investorListData.screenCriteria = this.criteria;
      investorListData.symbols = [];
      investorListData.updationMode = this.criteria
        ? this.filterType === 'screener'
          ? 'screener'
          : 'etf-screener'
        : 'mannual';
      this.addInvestorListData(investorListData);

      console.log('Inside else condition', this.criteria);
    }
  }

updateInvestorDetailApi(investorListData) {
    const httpUpdateInvestorListsDetails$ = this.api.investorUpdateListDetails(investorListData);
    httpUpdateInvestorListsDetails$
      .pipe(
        map((resp) => {

          const ideaListData = resp;
          return ideaListData;
        })
      )
      .subscribe((investorListData) => {
        if (
          investorListData['status'] &&
          investorListData['message'] == 'SUCCESS'
        ) {
          this.updateAcknowledgeMgs('', 'Successfully Updated.');
          // this.dialogRef.close({
          //   message: "Update List"
          // });
        }
      });
  }

addInvestorListData(investorListData) {
    this.clearListDetailData();
    const httpAddInvestorListsDetails$ =
      this.api.addNewListDetails(investorListData);
    httpAddInvestorListsDetails$
      .pipe(
        map((resp) => {
          const ideaListData = resp;
          return ideaListData;
        })
      )
      .subscribe((investorListData) => {
        if (
          investorListData['status'] &&
          investorListData['message'] == 'SUCCESS'
        ) {
          if (this.criteria) {
            this.updateAcknowledgeMgs('', 'Successfully Updated.');
          } else {
            this.listType === 'import'
              ? this.stepper.next()
              : (this.stepper.selectedIndex = 3);
          }
        } else {
          this.listDetailScreenErrorMsg = investorListData['message'];
        }
      });
  }

clearListDetailData() {
    this.listDetailScreenErrorMsg = '';
  }

onuploadFileChange(files: File[]) {
    this.columnForTickers = 0;
    let file = files[0];

    let isCSVFile = file.name.endsWith('.csv');

    if (isCSVFile) {
      this.fileToUpload = file;
      this.invalidFileMsg = '';
    } else {
      this.fileInputVar.nativeElement.value = '';
      this.invalidFileMsg = 'Select a valid  file to upload';
      this.fileToUpload = null;
    }

    this.isUploadStepBtnDisabled = false;
  }

onlistTypeChange(val: string) {
    this.listType = val;
  }
}
