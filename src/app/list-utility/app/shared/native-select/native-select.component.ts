import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  ScreenerDataModel,
  ScreenerFirstModel,
} from 'src/app/core/data-models/app-model';

@Component({
  selector: 'app-native-select',
  templateUrl: './native-select.component.html',
  styleUrls: ['./native-select.component.scss'],
})
export class NativeSelectComponent implements OnInit {
  @Input() data: ScreenerDataModel[] | any;
  @Input() isFactor: boolean;
  @Input() headerName: string;
  @Output() onChangeSelection = new EventEmitter<{
    data: ScreenerDataModel;
    headerName: string;
  }>();
  ngOnInit(): void {}
  onChange(value: string) {
    let id = parseInt(value);
    let selectedObject: ScreenerDataModel;
    this.data.forEach((element) => {
      if (element.id === id) {
        element.defaultStatus = 1;
        selectedObject = element;
      } else {
        element.defaultStatus = 0;
      }
    });
    this.onChangeSelection.emit({
      data: selectedObject,
      headerName: this.headerName,
    });
  }
}
