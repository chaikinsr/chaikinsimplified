import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { FormControl } from "@angular/forms";
import { ScreenerDataModel, ScreenerFirstModel } from "src/app/core/data-models/app-model";



@Component({
    selector: 'app-checkbox-select',
    templateUrl: './select-checkbox.component.html',
    styleUrls: ['./select-checkbox.component.scss']
})
export class CheckboxSelectComponent implements OnInit {
    @Input() data: ScreenerDataModel[];
    @Input() isFactor: boolean;
    @Input() headerName: string;
    // @Output() onChangeSelection = new EventEmitter<ScreenerDataModel>();
    selectedObject: ScreenerDataModel[] = [];
    selectedList = new FormControl('');
    ngOnInit(): void {
        let selectedObject: ScreenerDataModel[] = [];
        let filterEnableData = this.data.filter(ele => ele.defaultStatus ==1);
        this.selectedObject = filterEnableData;
        let selectedData = filterEnableData.map(ele => ele.id);
        this.selectedList.patchValue(selectedData)
    }
    onChange(value) {
        let selectedObject: ScreenerDataModel[] = [];
        this.data.forEach(element => {
            let objectExist = value.filter(ele => ele === element.id)
            if (objectExist.length > 0) {
                element.defaultStatus = 1;
                selectedObject.push(element);
            } else {
                element.defaultStatus = 0;
            }
        })
        this.selectedObject = selectedObject;
        //      this.onChangeSelection.emit(selectedObject)
    }
}