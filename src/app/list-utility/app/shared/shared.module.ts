import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { NativeSelectComponent } from './native-select/native-select.component';
import { CheckboxSelectComponent } from './select-checkbox/select-checkbox.component';
import { MatStepperModule } from '@angular/material/stepper';
import { ListEditDialogComponent } from './list-edit-dialog/list-edit-dialog.component';
import { MatInputComponent } from './input/input.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDialogModule,
    MatDividerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatStepperModule,
    MatListModule,
    MatButtonToggleModule,
    MatSelectModule,
    MatTooltipModule,
    MatSlideToggleModule
  ],
  declarations: [
    NativeSelectComponent,
    CheckboxSelectComponent,
    ListEditDialogComponent,
    MatInputComponent,
    ConfirmDialogComponent
  ],
  exports: [
    NativeSelectComponent,
    CheckboxSelectComponent,
    ListEditDialogComponent,
    MatInputComponent,
    ConfirmDialogComponent
  ],
  entryComponents: [
  ]
})
export class SharedUtilModule { }