import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SessionResolver } from 'src/app/list-utility/session.resolver';
import { ListComponent } from './app/list/list.component';
import { ListUtilityComponent } from './list-utility.component';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./app/login/login.module').then((m) => m.LoginModule),
    canActivate: [SessionResolver],
    data: {
      isPublic: true,
    },
  },
  {
    path: '',
    loadChildren: () =>
      import('./app/layout/utility-layout.module').then(
        (m) => m.UtilityLayoutModule
      ),
    canActivate: [SessionResolver],
    data: {
      isPublic: true,
    },
  },
];
// {path: '', redirectTo: '/utility', pathMatch: 'full'}

// loadChildren: () => import('./layout/layout.module').then(m => m.LayoutModule),
// data: {
// isPublic: true
// }

@NgModule({
  imports: [
    // [RouterModule.forRoot(routes,{useHash: true})]
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled',
      anchorScrolling: 'enabled',
      onSameUrlNavigation: 'reload',
      useHash: true,
      relativeLinkResolution: 'legacy',
    }),
  ],
  exports: [RouterModule],
  providers: [],
})

export class ListUtilityRoutingModule {}
