import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { AuthService } from 'src/app/core/http-services/auth.service';

import { loginCredentialMapping } from './list-utility.constant';

@Component({
  selector: 'app-list-utility',
  templateUrl: './list-utility.component.html',
  styleUrls: ['./list-utility.component.scss'],
})
export class ListUtilityComponent {
  constructor(private authService: AuthService, private router: Router) {}

  // ngOnInit(): void {
  //   const sessionedUserEmail = localStorage.getItem('email');
  //   this.authService
  //     .getLoggedInstate(sessionedUserEmail)
  //     .pipe(
  //       catchError((error) => {
  //         if (error.status === 403) {
  //           this.router.navigateByUrl('/');
  //         }

  //         return throwError(error);
  //       })
  //     )
  //     .subscribe((response) => {
  //       loginCredentialMapping.forEach((user) => {
  //         if (user.uid == response.UID) {
  //           localStorage.setItem('UID', response.UID);
  //           this.router.navigateByUrl('/list-view');
  //         }
  //       });
  //     });
  // }
}
