export const loginCredentialMapping = [
  {
    uid: 1060585,
    customer_number: 'SAC0015808022',
    email: 'jsingh@stansberryresearch.com',
  },
  {
    uid: 1021370,
    customer_number: 'SAC0023195997',
    email: 'mfedick@chaikinanalytics.com',
  },
  {
    uid: 1050114,
    customer_number: 'SAC0019370744',
    email: 'shayman@chaikinanalytics.com',
  },
  {
    uid: 1067163,
    customer_number: '',
    email: 'akash.kumar@paxcel.net',
  },
  {
    uid: 1060666,
    customer_number: '',
    email: 'hardeep.deol@paxcel.net',
  },
  {
    uid: 1058333,
    customer_number: '',
    email: 'sarabjit.singh@paxcel.net',
  },
];
