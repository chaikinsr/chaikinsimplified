import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatTableModule } from '@angular/material/table';
import { ToastrModule } from 'ngx-toastr';

import { ListUtilityRoutingModule } from './list-utility-routing.module';
import { ListUtilityComponent } from './list-utility.component';
import { AppletHeaderModule } from 'src/app/layout/header/applet-header.module';
import { SharedModule } from '../page/shared/shared.module';
import { httpInterceptorProviders } from '../core/http-interceptor/interceptor-providers';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../app-routing.module';
import { LayoutModule } from '@angular/cdk/layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SessionErrorInterceptor } from './session-error.interceptor';
import { SessionResolver } from './session.resolver';

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    ListUtilityRoutingModule,
    LayoutModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatButtonToggleModule,
    MatTableModule,
    ToastrModule.forRoot(),
  ],
  declarations: [ListUtilityComponent],
  exports: [ListUtilityComponent],
  providers: [
    // httpInterceptorProviders,
    SessionResolver,
    SessionErrorInterceptor,
  ],
  bootstrap: [ListUtilityComponent],
})
export class UtilityModule {}
