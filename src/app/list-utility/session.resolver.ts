import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';

import { AuthService } from 'src/app/core/http-services/auth.service';

import { loginCredentialMapping } from 'src/app/core/app-constants/app-const';

@Injectable({
  providedIn: 'root',
})
export class SessionResolver implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> {
    let isUserLoggedIn = false;
    const response = await this.checkSession();

    loginCredentialMapping.forEach((user) => {
      if (user.uid == response.UID) {
        localStorage.setItem('UID', response.UID);
        isUserLoggedIn = true;
      }
    });

    // not logged in so redirect to login page with the return url and return false
    // this.router.navigate(['login'], { queryParams: { returnUrl: state.url } });
    console.log(isUserLoggedIn, state.url, ' url');
    if (isUserLoggedIn && state.url === '/') {
      this.router.navigate(['/list-view']);
      return false;
    }
    // if (!isUserLoggedIn && state.url === '/') {
    //   this.router.navigate(['/login-page']);
    //   return false;
    // }

    return true;
  }

  async checkSession(): Promise<any> {
    const sessionedUserEmail = localStorage.getItem('email');
    return this.authService.getLoggedInstate(sessionedUserEmail).toPromise();
  }
}
