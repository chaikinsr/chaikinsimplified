import { Component, OnChanges, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { CookieService } from 'ngx-cookie-service';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment-timezone';
import { DataService } from 'src/app/core/services/data.service';
import { AppDataService } from 'src/app/core/services/app-data.service';
import { AuthService } from 'src/app/core/http-services/auth.service';
import { AdvancedSearchSorting, AlertDataModel, ChaikinAdvanceIdeaLists, ChaikinAdvanceListData, GlobalListWithPowbarModel, NavigationListWithPowbarModel, ProductAccessModel, SortingModel, SortObjectModel } from 'src/app/core/data-models/app-model';
import { LocalStorage, SharedUtil, Sort } from 'src/app/core/services/utilily/utility';
import { AlertsData, ChaikinAdvanceListDataObject, ChaikinAdvanceListDataSymbols, ChaikinAdvanceListObject, ChaikinAdvanceLists, LoginDetailModel, NavigationListWithPowbar } from 'src/app/core/data-models/service-model';
import { MatTableDataSource } from '@angular/material/table';
import { APIService } from 'src/app/core/http-services/api.service';
import { map, switchMap } from 'rxjs/operators';
import { AppConst } from 'src/app/core/app-constants/app-const';
import * as _ from 'lodash';
import { forkJoin } from 'rxjs';
import { CreateListDialogComponent } from '../shared/components/create-list-dialog/create-list-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { ToastService } from 'src/app/core/services/toast-service';


@Component({
  selector: 'app-advanced-search',
  templateUrl: './advanced-search.component.html',
  styleUrls: ['./advanced-search.component.scss']
})

export class AdvancedSearchComponent implements OnInit, OnChanges {
  
  // Top Nav
  @ViewChild('logoutPopup', { static: false }) logoutPopup: any;
  setPreferenceForCPP: boolean = false;
  showLogoutPopup: boolean = false;
  productsList: any;
  productPopUpShow: boolean = false;
  productsAccessCount: any;
  productAccessModel: ProductAccessModel;
  totalProductAccessArray = [];
  userDetails: LoginDetailModel;
  documentClickEventRegister: boolean = false;
  createLogoutXPosition: any = 0;
  createLogoutYPosition: any = 0;
  fixedLogoutYPosition: any = 0;
  watchlistDataModel: GlobalListWithPowbarModel = new GlobalListWithPowbarModel();
  advancedSearchSorting = new AdvancedSearchSorting();
  

 
  // Hardeep
  chaikinAdvanceLists: ChaikinAdvanceIdeaLists[] = [];
  chaikinAdvanceListsData: ChaikinAdvanceListData[] = [];
  activeListId: number;
  activeListName: string;
  listDescription: string;

  // Table
  displayedColumns: string[] = ['rating', 'symbol', 'name', 'last', 'change', 'percentage', 'alert','add'];
  advanceListMatTableData = new MatTableDataSource<ChaikinAdvanceListData>([]);
  formattedDate: string = '';
  constructor(
    private api: APIService,
    private appDataService: AppDataService,
    public dialog: MatDialog,
    public toastService: ToastService,
    public router: Router
  ){}
  ngOnChanges(): void {}
  ngOnInit(): void {
    this.getUserWatchlistData()
    let activeListId = this.appDataService.advanceSearch.activeListId;
    if(activeListId) {
      this.listId = activeListId;
    }
    this.getChaikinAdvanceList();
  }


  //  TOP NAV  // 
  openLogoutPopup(e: any) {
    this.productPopUpShow = !this.productPopUpShow
    e.stopPropagation();
    //this.userId = localStorage.getItem('email');
    if (!this.documentClickEventRegister) {
      document.addEventListener('click', this.offClickHandler.bind(this));
      this.documentClickEventRegister = true;
      this.onClickLogoutOpenPopup(e);
      this.openLogoutPopupForAccount();
    }
  }
  onClickLogoutOpenPopup(event: any) {
    this.createLogoutXPosition = event.clientX - 150;
    this.createLogoutYPosition = event.clientY + 20;
  }
  openLogoutPopupForAccount() {
    this.showLogoutPopup = true;
  }
  onCloseLogOutPopupEvent() {
    this.showLogoutPopup = false;
  }
  offClickHandler(event: any) {
    if (
      this.logoutPopup &&
      this.logoutPopup.nativeElement &&
      !this.logoutPopup.nativeElement.contains(event.target)
    ) {
      // check click origin
      this.showLogoutPopup = false;
      if (this.documentClickEventRegister) {
        document.removeEventListener('click', this.offClickHandler.bind(this));
        this.documentClickEventRegister = false;
      }
    }
  }

// Hardeep Code start
getChaikinAdvanceList(): void {
  this.setFormattedData();
  const userDetails: LoginDetailModel = LocalStorage.getItem('userDetail');
 
  const httpGetChaikinAdvanceLists$ = this.api.getChaikinAdvanceList(userDetails.UID);
  // const httpAllAlerts$ = this.api.getAllAlerts(listId, this.userDetails.UID);
  //
  httpGetChaikinAdvanceLists$
    .pipe(
      map((resp) => {
        const chaikinAdvanceListData = resp as ChaikinAdvanceListObject;
        let listData: ChaikinAdvanceIdeaLists[] = []
        if(chaikinAdvanceListData.status) {
          listData = this.prepareChaikinAdvanceListData(chaikinAdvanceListData.list_data);
          this.setActiveListData(listData);
          this.chaikinAdvanceLists = listData;
          return listData;
        }
        return listData;
      }),
      switchMap((editedResponse1: ChaikinAdvanceIdeaLists[]) => forkJoin(this.api.getChaikinAdvanceListData(this.activeListId), this.api.getAllAlerts(this.activeListId as unknown as string, userDetails.UID))),
      map(([resp2, resp3]) => {
        const listSymbolData = resp2 as ChaikinAdvanceListDataObject;
        const alertData = resp3 as AlertsData;
        let symbolData: ChaikinAdvanceListData[] = [];
        let modifiedAlertData: AlertDataModel[] = [];
        if (resp2.symbols.length > 0){
          modifiedAlertData = this.prepareAlertData(alertData);
          symbolData = this.prepareAdvanceListSymbolData(resp2.symbols, modifiedAlertData);
          symbolData = this.sortingOnList(symbolData, 'rating', 'symbol');
          return symbolData
        }
        return symbolData
      })
    )
    .subscribe((chaikinAdvanceListsData: ChaikinAdvanceListData[]) => {
      this.emptyDataInMatTable();
      this.fillFinalDataInMatTable(chaikinAdvanceListsData);
    });
}

prepareAlertData(data: AlertsData): AlertDataModel[] {
  const modifiedAlertData: AlertDataModel[] = [];
  for (const key in data) {
    if (Object.prototype.hasOwnProperty.call(data, key)) {
      const element = data[key];
      element.forEach((alertData) => {
        const alertDate = alertData.AlertDate;
        const formattedDate = this.formatDate(alertDate);
        let alertInfo = '';
        let alertName = '';
        let changeInPer: number;
        let changeInPerFormatted = '';
        let actualEps = '';
        let estimateEps = '';
        let oldValue = '';
        let newValue = '';
        let pgr: number;
        if (key === 'CTI') {
          alertInfo = alertData.Category;
        } else {
          alertInfo = alertData.Text;
        }
        alertInfo = alertInfo.trim();
        if (alertInfo === 'Earnings Surprise') {
          alertName = alertInfo;
          changeInPer = +alertData.PercentageSurprise;
          if (alertData.Value > 3) {
            changeInPerFormatted =
              alertData.PercentageSurprise.indexOf('+') === -1
                ? '+' + alertData.PercentageSurprise
                : alertData.PercentageSurprise;
          } else if (alertData.Value < 3) {
            changeInPerFormatted =
              alertData.PercentageSurprise.indexOf('-') === -1
                ? '-' + alertData.PercentageSurprise
                : alertData.PercentageSurprise;
          }
          changeInPerFormatted = SharedUtil.appendSymbol(
            changeInPerFormatted,
            '%'
          );
          changeInPerFormatted = SharedUtil.appendSymbol(
            changeInPerFormatted,
            '()'
          );
          actualEps = alertData.ActualEPS;
          estimateEps = alertData.ConsensusEstimate;
          pgr = alertData.pgrRating;
        } else if (alertInfo === 'Estimate Revision') {
          alertName = alertInfo;
          changeInPer = +alertData.ESTPercentageChange;
          if (alertData.Value > 3) {
            changeInPerFormatted =
              alertData.ESTPercentageChange.indexOf('+') === -1
                ? '+' + alertData.ESTPercentageChange
                : alertData.ESTPercentageChange;
          } else if (alertData.Value < 3) {
            changeInPerFormatted =
              alertData.ESTPercentageChange.indexOf('-') === -1
                ? '-' + alertData.ESTPercentageChange
                : alertData.ESTPercentageChange;
          }
          changeInPerFormatted = SharedUtil.appendSymbol(
            changeInPerFormatted,
            '%'
          );
          changeInPerFormatted = SharedUtil.appendSymbol(
            changeInPerFormatted,
            '()'
          );
          oldValue = alertData.MeanESTPreviousDay;
          newValue = alertData.MeanESTCurrentDay;
          pgr = alertData.pgrRating;
        } else if (key === 'PGR') {
          alertInfo = alertData.Text;
          alertName = 'Rating Change';
          pgr = alertData.Value;
        }

        pgr = !pgr ? 0 : alertData.pgrRating;

        const alertDataModel = new AlertDataModel();
        alertDataModel.date = alertDate;
        alertDataModel.formattedDate = formattedDate;
        alertDataModel.alertName = alertName;
        alertDataModel.alertInfo = alertInfo;
        alertDataModel.isEtf = alertData.is_etf;
        alertDataModel.pgr = pgr;
        alertDataModel.rawPGR = alertData.rawPgrRating;
        alertDataModel.symbol = alertData.Symbol;
        alertDataModel.value = alertData.Value;
        alertDataModel.oldValue = oldValue;
        alertDataModel.newValue = newValue;
        alertDataModel.actualEps = actualEps;
        alertDataModel.estimateEps = estimateEps;
        alertDataModel.changeInPer = changeInPer;
        alertDataModel.classForPerChange =
          changeInPer < 0 ? 'negative' : 'positive';
        alertDataModel.changeInPerFormatted = changeInPerFormatted;
        alertDataModel.close = undefined;
        alertDataModel.rank = undefined;
        modifiedAlertData.push(alertDataModel);
      });
    }
  }
  return modifiedAlertData;
}

formatDate(stringDate: string): string {
  const months = [
    'JAN',
    'FEB',
    'MAR',
    'APR',
    'MAY',
    'JUN',
    'JUL',
    'AUG',
    'SEP',
    'OCT',
    'NOV',
    'DEC',
  ];
  const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  const currentDateTime = new Date(stringDate);
  const formattedDate =
    days[moment(stringDate).format('d')] +
    '. ' +
    months[((+moment(stringDate).format('M')) - 1)] +
    ' ' +
    moment(stringDate).format('D') +
    ', ' +
    moment(stringDate).format('Y').toString();
  return formattedDate;
}

redirectOnPgr(symbol): void {
  LocalStorage.setItem('mainSymbol', symbol);
  this.router.navigate(["/"]
  // window.location.replace(
  //   window.location.protocol +
  //   '//' +
  //   window.location.host +
  //   '/pgr/'
  );

}

getChaikinAdvanceListData() {
  this.advancedSearchSorting = new AdvancedSearchSorting();
  const userDetails: LoginDetailModel = LocalStorage.getItem('userDetail');
  const httpGetChaikinAdvanceListsData$ = this.api.getChaikinAdvanceListData(this.activeListId);
  const httpAllAlerts$ = this.api.getAllAlerts(this.activeListId as unknown as string, userDetails.UID);
  forkJoin(httpGetChaikinAdvanceListsData$,  httpAllAlerts$).subscribe(([resp2, resp3]) => { 
      const listSymbolData = resp2 as ChaikinAdvanceListDataObject;
      const alertData = resp3 as AlertsData;
      let symbolData: ChaikinAdvanceListData[] = [];
      let modifiedAlertData: AlertDataModel[] = [];
      if (resp2.symbols.length > 0){
        modifiedAlertData = this.prepareAlertData(alertData);
        symbolData = this.prepareAdvanceListSymbolData(resp2.symbols, modifiedAlertData);
        symbolData = this.sortingOnList(symbolData, 'rating', 'symbol');
      }
      this.emptyDataInMatTable();
      this.fillFinalDataInMatTable(symbolData);
  });
}

prepareChaikinAdvanceListData(lists: ChaikinAdvanceLists[]): ChaikinAdvanceIdeaLists[] {
  let chaikinAdvanceIdeaLists: ChaikinAdvanceIdeaLists[] = [];
  lists.forEach(element => {
     let listData = new ChaikinAdvanceIdeaLists();
     listData.listId = element.list_id;
     listData.listName = element.list_name;
     listData.description = element.description;
     chaikinAdvanceIdeaLists.push(listData);
  });
  return chaikinAdvanceIdeaLists
}

prepareAdvanceListSymbolData( symbols: ChaikinAdvanceListDataSymbols[], modifiedAlertData: AlertDataModel[]): ChaikinAdvanceListData[] {
  let symbolData: ChaikinAdvanceListData[] = [];
  const alertData = this.convertAlertDataIntoJson(modifiedAlertData);
  symbols.forEach(element => {
    let data = new ChaikinAdvanceListData();
    data.symbol = element.symbol;
    data.rawPgr = element.raw_PGR;
    data.industryName = element.industry_name;
    data.change = element.Change;
    data.last = element.Last;
    data.isEtf = element.is_etf_symbol;
    data.name = element.name;
    data.pgr = element.PGR;
    data.percentage = element['Percentage '];
    data.rating = SharedUtil.getPgrRating(
      data.pgr,
      data.rawPgr
    );
    data.imgSrc = SharedUtil.getPgrArcWithoutNeutralValue(data.pgr, data.rawPgr, data.isEtf);
    let lastFormattedString = SharedUtil.formatNumber(data.last);
    data.lastFormattedString = SharedUtil.appendSymbol(
      lastFormattedString,
      '$'
    );

    let changeFormattedString = SharedUtil.formatNumber(
      data.change
    );
    data.changeFormattedString = changeFormattedString;

    let percentageChangeFormattedString = SharedUtil.formatNumber(
      data.percentage
    );
    data.percentageChangeFormattedString = SharedUtil.appendSymbol(
      percentageChangeFormattedString,
      '%'
    );
    if (percentageChangeFormattedString !== 'NA') {
      if (data.percentage > 0) {
        data.percentageChangeFormattedString =
          '+' + data.percentageChangeFormattedString + '';
        data.percentageChangeColor = 'up';
      } else if (data.percentage < 0) {
        data.percentageChangeFormattedString =
          '' + data.percentageChangeFormattedString + '';
        data.percentageChangeColor = 'down';
      } else {
        data.percentageChangeFormattedString =
          '' + data.percentageChangeFormattedString + '';
        data.percentageChangeColor = 'grey';
      }
    }
    
    data.add = '';
    if (alertData.hasOwnProperty(data.symbol)) {
      data.alert = alertData[data.symbol];
    } else {
      data.alert = null;
    }

    symbolData.push(data);
  });

  return symbolData;
}

convertAlertDataIntoJson(data: AlertDataModel[]): {[key: string]: AlertDataModel} {
  const alertDataObject = {};
  data.forEach(element => {
    alertDataObject[element.symbol] = element;
  });
  return alertDataObject;
}

emptyDataInMatTable(): void {
  this.advanceListMatTableData = new MatTableDataSource<ChaikinAdvanceListData>([]);
}

fillDataInMatTable(data: ChaikinAdvanceListData[]): void {
  this.advanceListMatTableData = new MatTableDataSource<ChaikinAdvanceListData>(data);
}

fillFinalDataInMatTable(data: ChaikinAdvanceListData[]): void {
  this.chaikinAdvanceListsData = data;
  this.fillDataInMatTable(data);
}

setActiveListData(listData: ChaikinAdvanceIdeaLists[]) {
  let activeListId = this.appDataService.advanceSearch.activeListId;
  if (listData.length > 0 && !activeListId) {
    this.setListData(listData[0]);
  } else {
    let list = listData.filter(list => list.listId == this.activeListId);
    if (list.length > 0) {
      this.setListData(list[0]);
    }
  }
}

onSelectAdvanceList(list: ChaikinAdvanceIdeaLists) {
  this.setListData(list);
  this.getChaikinAdvanceListData();
}

setListData(list: ChaikinAdvanceIdeaLists) {
  this.listId = list.listId;
  this.listName = list.listName
  this.listDescription =  list.description;
  this.appDataService.advanceSearch.activeListId = this.listId;
}

sortingOnList(data: ChaikinAdvanceListData[], sortingKey: string, extendKey: string): ChaikinAdvanceListData[] {
  let sortObject =   [
    {
      sortingKey: sortingKey,
      sortingOrder: AppConst.CONST.sortOrderDesc,
      ignoreKey: [-1],
    },
    {
      sortingKey: extendKey,
      sortingOrder: AppConst.CONST.sortOrderAsc,
      ignoreKey: [],
    },
  ]

  const sortingParams = this.setSortingModel(data, sortObject);
  return Sort.sortUnique(sortingParams);
}


sortingOnListFromHeader(data: ChaikinAdvanceListData[], sortingKey: string, extendKey: string):void {
  this.advancedSearchSorting.selectedKey = sortingKey;
 let sortingOrderOfKey = this.setSortingOrderForDifferentKey(sortingKey);

  let sortObject =   [
    {
      sortingKey: sortingKey,
      sortingOrder: sortingOrderOfKey,
      ignoreKey: [-1],
    },
    {
      sortingKey: extendKey,
      sortingOrder: AppConst.CONST.sortOrderAsc,
      ignoreKey: [],
    },
  ]

  const sortingParams = this.setSortingModel(data, sortObject);
  let sortedData = Sort.sortUnique(sortingParams);
   this.fillFinalDataInMatTable(sortedData);
}

setSortingOrderForDifferentKey(sortingKey): string {
  let sortingOrder: any;
  switch (sortingKey) {
    case 'rating':
      sortingOrder =
        this.advancedSearchSorting.ratingSortingOrder === AppConst.CONST.sortOrderAsc
          ? AppConst.CONST.sortOrderDesc
          : AppConst.CONST.sortOrderAsc;
      return this.advancedSearchSorting.ratingSortingOrder = sortingOrder;

    case 'symbol':
      sortingOrder =
        this.advancedSearchSorting.symbolSortingOrder === AppConst.CONST.sortOrderAsc
          ? AppConst.CONST.sortOrderDesc
          : AppConst.CONST.sortOrderAsc;
         
      return this.advancedSearchSorting.symbolSortingOrder = sortingOrder;

    case 'name':
      sortingOrder =
        this.advancedSearchSorting.nameSortingOrder === AppConst.CONST.sortOrderAsc
          ? AppConst.CONST.sortOrderDesc
          : AppConst.CONST.sortOrderAsc;
         
      return this.advancedSearchSorting.nameSortingOrder = sortingOrder;

      case 'change':
        sortingOrder =
          this.advancedSearchSorting.changeSortingOrder === AppConst.CONST.sortOrderAsc
            ? AppConst.CONST.sortOrderDesc
            : AppConst.CONST.sortOrderAsc;
           
        return this.advancedSearchSorting.changeSortingOrder = sortingOrder;

        case 'last':
          sortingOrder =
            this.advancedSearchSorting.lastSortingOrder === AppConst.CONST.sortOrderAsc
              ? AppConst.CONST.sortOrderDesc
              : AppConst.CONST.sortOrderAsc;
            
          return this.advancedSearchSorting.lastSortingOrder = sortingOrder;
          case 'percentage':
            sortingOrder =
              this.advancedSearchSorting.percentageSortingOrder === AppConst.CONST.sortOrderAsc
                ? AppConst.CONST.sortOrderDesc
                : AppConst.CONST.sortOrderAsc;
               
            return this.advancedSearchSorting.percentageSortingOrder = sortingOrder;

    default:
      break;
  }

}



setSortingModel(data: Array<any>, sortObj: SortObjectModel[]): SortingModel {
  const len = sortObj.length - 1;
  const sortingModelData = new SortingModel();
  const sortLevelKey = [];

  sortObj.forEach((element, index) => {
    let sorting: SortingModel;
    if (index === 0) {
      sorting = sortingModelData;
      sorting.sortData = data;
    } else {
      sortingModelData.extendedData = new SortingModel();
      sorting = sortingModelData.extendedData;
      sorting.sortData = [];
    }

    sorting.sortKey = element.sortingKey;
    sorting.sortOrder = element.sortingOrder;
    sorting.ignoreDataKeys = element.ignoreKey;
    sorting.sortLevelKey = _.cloneDeep(sortLevelKey);
    sorting.ignoreData = [];
    sorting.isExtended = index === len ? false : true;

    sortLevelKey.push(element.sortingKey);
  });

  return sortingModelData;
}

setFormattedData() {
  this.formattedDate = moment()
  .tz('America/New_York')
  .format('MM/DD/YY HH:mmA z');
}

public set listId(id: number) {
  this.activeListId = id;
}


public get listId(): number  {
  return this.activeListId;
}

public set listName(name : string) {
  this.activeListName = name;
}


public get listName() : string {
  return this.activeListName;
}


getUserWatchlistData(): void {
  const userDetail: LoginDetailModel = LocalStorage.getItem('userDetail');
  const httpUserWatchlist$ = this.api.getUserWatchlistData(userDetail.UID);
  httpUserWatchlist$
    .pipe(
      map((resp) => {

        if (resp.status) {
          const userWatchlistData = resp.data as NavigationListWithPowbar[];
          this.appDataService.userList = resp.data;
          return this.perpareUserWatchlistData(userWatchlistData);
        }
        return [];
      })
    )
    .subscribe((userList: GlobalListWithPowbarModel) => {
     // this.appDataService.userList = userList;

    });
}

perpareUserWatchlistData(
  listData: NavigationListWithPowbar[]
): any {
  this.watchlistDataModel = new GlobalListWithPowbarModel();
  this.watchlistDataModel.data = [];

  if (listData) {
    listData.forEach(data => {

      const navigationListWithPowbarModel = new NavigationListWithPowbarModel();
      // const selectedData = new specialList();
      const userDetails = LocalStorage.getItem('userDetail');
      //   if(data.listName === 'Power Gauge Investor Portfolio'  || data.listName === 'Power Gauge Report'){
      //       selectedData.listName =  data.listName;
      //       selectedData.listId = data.listId;
      //       selectedData.symbolCount = data.symbolCount;
      //  }

      // this.loadWatchlistData(this.selectedListIdData);
      // console.log( this.selectedListIdData);
      navigationListWithPowbarModel.listId = data.listId;
      navigationListWithPowbarModel.hasChart = data.hasChart;
      navigationListWithPowbarModel.isExtenable = data.isExtenable;
      navigationListWithPowbarModel.description = data.description;
      navigationListWithPowbarModel.listName = data.listName;
      navigationListWithPowbarModel.listType = data.listType;
      navigationListWithPowbarModel.symbolCount = data.symbolCount;
      navigationListWithPowbarModel.powerBar = data.powerBar;
      navigationListWithPowbarModel.extendedData = undefined;
      navigationListWithPowbarModel.powerBarBullish = data.powerBar.green;
      navigationListWithPowbarModel.listNameFormatted = data.listName.toLowerCase();
      navigationListWithPowbarModel.powerBar = data.powerBar;
      this.watchlistDataModel.data.push(navigationListWithPowbarModel);
     // this.watchlistDataModel.data.push(navigationListWithPowbarModel);
    });

}
}


addTickerIntoList(symbol, list): void {
  const userDetail: LoginDetailModel = LocalStorage.getItem('userDetail');
  this.api.addStockIntoList(symbol, list.listId
    , userDetail.UID).subscribe(resp => {
      if (resp) {
        LocalStorage.setItem('listId', list.listId);
       this.toastService.showSuccess('Successfully added');
      }
    })
}

createNewList(event: any, symbol): void {
  event.stopPropagation();
  const dialogRef = this.dialog.open(CreateListDialogComponent, {
    data: symbol
  });
  dialogRef.afterClosed().subscribe(listId => {

    if (listId) {
      LocalStorage.setItem('listId', listId);
      //  if (!symbol){
      this.getUserWatchlistData();
      this.toastService.showSuccess('successfully added');
    }
  });
}

}
