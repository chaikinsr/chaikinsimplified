import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {
  AlertComponentData,
  AlertDataModel,
  AlertFilterType,
  AlertModel,
  AlertSortingDataModel,
  SignalDataModel,
  SortingModel,
  SortObjectModel,
} from 'src/app/core/data-models/app-model';
import {
  LocalStorage,
  RedirectParameter,
  SharedUtil,
  Signal,
  Sort,
} from 'src/app/core/services/utilily/utility';
import {
  AlertsData,
  DashboardData,
  LoginDetailModel
} from 'src/app/core/data-models/service-model';

import { forkJoin, Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { AppConst } from 'src/app/core/app-constants/app-const';
import * as _ from 'lodash';
import { MatDialog } from '@angular/material/dialog';
import { APIService } from 'src/app/core/http-services/api.service';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from 'src/app/core/services/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { ToastService } from 'src/app/core/services/toast-service';
import { AppDataService } from 'src/app/core/services/app-data.service';
import { AuthService } from 'src/app/core/http-services/auth.service';
import { MatTableDataSource } from '@angular/material/table';







@Component({
  selector: 'app-alerts-table',
  templateUrl: './alerts-table.component.html',
  styleUrls: ['./alerts-table.component.scss']
})

export class AlertsTableComponent implements OnInit {
  selectedSymbol: string;
  @Input() selectedListId: boolean;
  //  selectedListId: any;
  isBullishSelected = false;
  saveUsername: boolean = false;
  isBearishSelected = false;
  isAllAlertSelected = true;
  hideExpandCollapseOption = false;
  userDetails: LoginDetailModel;
  dashboardData = new DashboardData();
  dateForDashboard:any;
  ratingClassArray = [];
  alertSortingDataModel = new AlertSortingDataModel();
  @Input() updateSentToAlerts: string;
  @Input() emailAlertModeValueFromHeader:boolean;
  @Output() dashBoardDataUpdate = new EventEmitter<any>();
  @Output() emailAlertModeValue = new EventEmitter<any>();

   alertComponentData1: AlertComponentData;
   dataSource = new MatTableDataSource<AlertDataModel>([]);

  //Alert Table Dummy Data
  displayedColumns: string[] = ['formattedDate', 'symbol', 'alertInfo' , 'alertName' ];
 // dataSource = ELEMENT_DATA;



  //Alert 

  alertsData: AlertDataModel[] = [];
  alertModel: AlertModel = new AlertModel();
  selectedAlertFilter = 'All';
  showExpandText:boolean= true;
  filterType: AlertFilterType[] = [
    {
      value: 'All',
      viewValue: 'All Alerts',
      comment: 'default to show all alerts without applying a filter',
    },
    {
      value: 'Signals',
      viewValue: 'Signals',
      comment: 'Signals only',
    },
    {
      value: 'Earnings',
      viewValue: 'Earnings Alerts',
      comment: 'including surprises and revisions',
    },
    {
      value: 'Rating',
      viewValue: 'Rating Changes',
      comment: 'Rating Changes only',
    },
  ];
  healthCheckRefresh$: Observable<boolean>;
  ngOnInit(): void {
    this.dateForDashboard = moment()
    .tz('America/New_York')
    .format('MM/DD/YY HH:mmA z');
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.selectedListId = LocalStorage.getItem('listId');
    // this.emailAlertModeValueFromHeader ?  this.saveUsername = this.emailAlertModeValueFromHeader : this.saveUsername;
   // this.dashboardData = this.appDataService.dashboardData;
    this.init();
  }

  init(): void {
    this.isBullishSelected = false;
    this.getAllAlerts(this.selectedListId);
    this.getEnabledAlertListId();
  }

  constructor(
    private api: APIService,
    public dialog: MatDialog,
    private cookieService: CookieService,
    private dataService: DataService, public router: Router,
    private route: ActivatedRoute,
    public toastService: ToastService,
    public appDataService: AppDataService,
    public auth: AuthService

  ) {
    if (!this.userDetails) {
      this.userDetails = this.getUserDetail();
    }
  }
  getUserDetail(): LoginDetailModel {
    const userDetail: LoginDetailModel = LocalStorage.getItem('userDetail');
    return userDetail;
  }

  // ALert API

  getAllAlerts(listId): void {
    if (!listId) {
      listId = this.userDetails.ListID;
    }
    let alertComponentData = new AlertComponentData();
    const httpAllAlerts$ = this.api.getAllAlerts(listId, this.userDetails.UID);
    const httpSignalDataForList = this.api.getSignalDataForList(listId, this.userDetails.UID);
    forkJoin([httpAllAlerts$, httpSignalDataForList]).subscribe((resp) => {
      const apiAlertData = resp[0] as AlertsData;
      const apiSignalAlertData = resp[1];
      const signalAlert = this.prepareSignalAlertData(apiSignalAlertData);
      const otherAlert = this.prepareAlertData(apiAlertData);
      this.alertsData = signalAlert.concat(otherAlert);
      this.dataSource.data = this.alertsData;
      this.alertModel.data = this.alertsData;
      
    //  this.alertsData = this.alertsData.splice(0,3)
     // val = this.alertsData.splice(0,5);
      let bullishCount = 0;
      let bearishCount = 0;

      bullishCount = this.alertModel.data
        // tslint:disable-next-line: max-line-length
        // .filter((item) => (item.value > 3 && (item.alertName === 'Rating Change' || item.alertInfo === 'Estimate Revision' || item.alertInfo === 'Earnings Surprise')))
        // .map((item) => item.symbol)
        // .filter((value, index, self) => self.indexOf(value) === index).length;
        .filter((item) => (item.value > 3 && (item.alertName === 'Rating Change' || item.alertInfo === 'Estimate Revision' || item.alertInfo === 'Earnings Surprise')))
        .map((item) => item.symbol).length;

      bearishCount = this.alertModel.data
        // tslint:disable-next-line: max-line-length
        .filter((item) => (item.value < 3 && (item.alertName === 'Rating Change' || item.alertInfo === 'Estimate Revision' || item.alertInfo === 'Earnings Surprise')))
        .map((item) => item.symbol).length;
      this.alertModel.count.bullish = bullishCount;
      this.alertModel.count.bearish = bearishCount;
      this.onAlertFilterType(this.selectedAlertFilter);
      alertComponentData.alertModel = this.alertModel;
      alertComponentData.alertDataModel = this.alertsData;
      alertComponentData.totalAlertCount = bullishCount + bearishCount;

      // this.appDataService.dashboardData.alertComponentData.alertDataModel = this.alertsData;
      // this.appDataService.dashboardData.alertComponentData.alertModel =  this.alertModel;
      // this.appDataService.dashboardData.alertComponentData.totalAlertCount = bullishCount + bearishCount
      this.alertComponentData1 = alertComponentData;
      this.dashboardData.alertComponentData = this.alertComponentData1;
      this.appDataService.dashboardData = this.dashboardData;
      this.dashBoardDataUpdate.emit(this.alertComponentData1);
      this.showExpandText = true;
      this.dataSource.data = this.alertsData;
     // this.alertsData = this.alertsData.splice(0,3)
      //  this.dashboardPriceMoment('1day', listId);
    });

  }

  getEnabledAlertListId(): void {
    this.api.getPowerPulseEnabledAlertList(this.userDetails.UID).subscribe(resp => {
      if (resp.status) {
        for (let element of resp.listId) {

          if (element === (+this.selectedListId)) {
            this.saveUsername = true;
            break;
          } else {
            this.saveUsername = false;
          }

        }
      }
    })
  }


  prepareSignalAlertData(signalData): AlertDataModel[] {
    const modifiedAlertData: AlertDataModel[] = [];
    let formattedDate = '';
    const signalDataModel = new SignalDataModel();
    signalData.forEach((element) => {
      signalDataModel.signal = this.convertToArray(element.Signals);
      signalDataModel.close = this.convertToArray(element.Close);
      signalDataModel.signalDate = this.convertToArray(element.SignalDate);
      signalDataModel.symbol = element.Symbol;

      signalDataModel.signal.forEach((signal, i) => {
        const parsedSignals = Signal.parseSignal(signal);

        parsedSignals.sellSignalNames.forEach((data) => {
          formattedDate = this.formatDate(signalDataModel.signalDate[i]);

          const alertDataModel = new AlertDataModel();
          alertDataModel.date = signalDataModel.signalDate[i];
          alertDataModel.formattedDate = formattedDate;
          alertDataModel.alertName = 'Sell Signal';
          alertDataModel.alertInfo = AppConst.sellSignalInfo[data].name;
          alertDataModel.isEtf = undefined;
          alertDataModel.pgr = undefined;
          alertDataModel.rawPGR = undefined;
          alertDataModel.symbol = signalDataModel.symbol;
          alertDataModel.value = 2;
          alertDataModel.oldValue = undefined;
          alertDataModel.newValue = undefined;
          alertDataModel.actualEps = undefined;
          alertDataModel.estimateEps = undefined;
          alertDataModel.changeInPer = undefined;
          alertDataModel.classForPerChange = undefined;
          alertDataModel.changeInPerFormatted = undefined;
          alertDataModel.close = signalDataModel.close[i];
          alertDataModel.rank = AppConst.sellSignalInfo[data].rank;
          modifiedAlertData.push(alertDataModel);
        });

        parsedSignals.buySignalNames.forEach((data) => {
          formattedDate = this.formatDate(signalDataModel.signalDate[i]);

          const alertDataModel = new AlertDataModel();
          alertDataModel.date = signalDataModel.signalDate[i];
          alertDataModel.formattedDate = formattedDate;
          alertDataModel.alertName = 'Buy Signal';
          alertDataModel.alertInfo = AppConst.buySignalInfo[data].name;
          alertDataModel.isEtf = undefined;
          alertDataModel.pgr = undefined;
          alertDataModel.rawPGR = undefined;
          alertDataModel.symbol = signalDataModel.symbol;
          alertDataModel.value = 4;
          alertDataModel.oldValue = undefined;
          alertDataModel.newValue = undefined;
          alertDataModel.actualEps = undefined;
          alertDataModel.estimateEps = undefined;
          alertDataModel.changeInPer = undefined;
          alertDataModel.classForPerChange = undefined;
          alertDataModel.changeInPerFormatted = undefined;
          alertDataModel.close = signalDataModel.close[i];
          alertDataModel.rank = AppConst.buySignalInfo[data].rank;
          modifiedAlertData.push(alertDataModel);
        });
      });
    });
    return modifiedAlertData;
  }

  prepareAlertData(data): AlertDataModel[] {
    const modifiedAlertData: AlertDataModel[] = [];
    for (const key in data) {
      if (Object.prototype.hasOwnProperty.call(data, key)) {
        const element = data[key];
        element.forEach((alertData) => {
          const alertDate = alertData.AlertDate;
          const formattedDate = this.formatDate(alertDate);
          let alertInfo = '';
          let alertName = '';
          let changeInPer: number;
          let changeInPerFormatted = '';
          let actualEps = '';
          let estimateEps = '';
          let oldValue = '';
          let newValue = '';
          let pgr: number;
          if (key === 'CTI') {
            alertInfo = alertData.Category;
          } else {
            // let str = alertData.Text;
            // let splitted = str.split(" -> ", 3);
            // alertInfo = splitted;
            // this.setRatingClass(splitted);

            alertInfo = alertData.Text;
          }
          alertInfo = alertInfo.trim();
          if (alertInfo === 'Earnings Surprise') {
            alertName = alertInfo;
            changeInPer = +alertData.PercentageSurprise;
            if (alertData.Value > 3) {
              changeInPerFormatted =
                alertData.PercentageSurprise.indexOf('+') === -1
                  ? '+' + alertData.PercentageSurprise
                  : alertData.PercentageSurprise;
            } else if (alertData.Value < 3) {
              changeInPerFormatted =
                alertData.PercentageSurprise.indexOf('-') === -1
                  ? '-' + alertData.PercentageSurprise
                  : alertData.PercentageSurprise;
            }
            changeInPerFormatted = SharedUtil.appendSymbol(
              changeInPerFormatted,
              '%'
            );
            changeInPerFormatted = SharedUtil.appendSymbol(
              changeInPerFormatted,
              '()'
            );
            actualEps = alertData.ActualEPS;
            estimateEps = alertData.ConsensusEstimate;
            pgr = alertData.pgrRating;
          } else if (alertInfo === 'Estimate Revision') {
            alertName = alertInfo;
            changeInPer = +alertData.ESTPercentageChange;
            if (alertData.Value > 3) {
              changeInPerFormatted =
                alertData.ESTPercentageChange.indexOf('+') === -1
                  ? '+' + alertData.ESTPercentageChange
                  : alertData.ESTPercentageChange;
            } else if (alertData.Value < 3) {
              changeInPerFormatted =
                alertData.ESTPercentageChange.indexOf('-') === -1
                  ? '-' + alertData.ESTPercentageChange
                  : alertData.ESTPercentageChange;
            }
            changeInPerFormatted = SharedUtil.appendSymbol(
              changeInPerFormatted,
              '%'
            );
            changeInPerFormatted = SharedUtil.appendSymbol(
              changeInPerFormatted,
              '()'
            );
            oldValue = alertData.MeanESTPreviousDay;
            newValue = alertData.MeanESTCurrentDay;
            pgr = alertData.pgrRating;
          } else if (key === 'PGR') {
            let str = alertData.Text;
            let splitted = str.split(" -> ", 3);
           // alertInfo = alertData.Text;
            alertInfo = splitted;
            this.setRatingClass(splitted)
           // console.log(splitted, this.ratingClassArray);
            alertName = 'Rating Change';
            pgr = alertData.Value;
          }

          pgr = !pgr ? 0 : alertData.pgrRating;

          const alertDataModel = new AlertDataModel();
          alertDataModel.date = alertDate;
          alertDataModel.formattedDate = formattedDate;
          alertDataModel.alertName = alertName;
          alertDataModel.alertInfo = alertInfo;
          alertDataModel.isEtf = alertData.is_etf;
          alertDataModel.pgr = pgr;
          alertDataModel.rawPGR = alertData.rawPgrRating;
          alertDataModel.symbol = alertData.Symbol;
          alertDataModel.value = alertData.Value;
          alertDataModel.oldValue = oldValue;
          alertDataModel.newValue = newValue;
          alertDataModel.actualEps = actualEps;
          alertDataModel.estimateEps = estimateEps;
          alertDataModel.changeInPer = changeInPer;
         // this.ratingClassArray.length >0 ?  alertDataModel.ratingClass = (this.ratingClassArray) :  alertDataModel.ratingClass = (this.ratingClassArray[0])
          alertDataModel.ratingClass = (this.ratingClassArray);

         
          alertDataModel.classForPerChange =
            changeInPer < 0 ? 'negative' : 'positive';
          alertDataModel.changeInPerFormatted = changeInPerFormatted;
          alertDataModel.close = undefined;
          alertDataModel.rank = undefined;
          modifiedAlertData.push(alertDataModel);
        });
      }
    }
    return modifiedAlertData;
  }


  setRatingClass(value): any{
    this.ratingClassArray = [];
    value.forEach(element => {
      switch(element) {
        case 'Neutral':
        case 'Neutral+':
        case 'Neutral-':
         this.ratingClassArray.push('yellow')
          return
        case 'Bullish':
        case 'Very Bullish':
          this.ratingClassArray.push('green')
          return 
        case 'Bearish':
        case 'Very Bearish':
        this.ratingClassArray.push('red')
         return 
      }
    });
    
    
  }

  deleteForEnabledAlertList(): void {
    this.api.deleteForEnabledPowerPulseAlertList(this.selectedListId, this.userDetails.UID).subscribe(resp => {
      this.emailAlertModeValue.emit(this.saveUsername)
    })
  }


  addListForEnableAlert(value): void {
    this.saveUsername = value;

    if (this.saveUsername) {
      this.api.addListForEnablePowerPulseAlert(this.selectedListId).subscribe(resp => {
        this.emailAlertModeValue.emit(this.saveUsername)
      })
    } else {
      this.deleteForEnabledAlertList();
    }
  }

  redirectOnPgr(symbol): void {
    LocalStorage.setItem('mainSymbol', symbol);
    this.router.navigate(["/"]
    );
  }

  onAlertFilterType(filterType: string): void {
    this.selectedAlertFilter = filterType;
    switch (filterType) {
      case 'All':
        this.alertsData = this.formatAlertData(filterType);

        this.alertsData = this.alertSorting(this.alertsData, 'symbol', 'date');
        // if (this.isBullishSelected && this.alertModel.count.bullish > 3){
        //   this.hideExpandCollapseOption = true;
        // }else if (!this.isBullishSelected && this.alertModel.count.bearish > 3){
        //   this.hideExpandCollapseOption = true;
        // }else{
        //   this.hideExpandCollapseOption = false;
        // }
     //   if (this.showExpandText){
          // this.alertsData =  ( this.alertsData.filter((item) => ((item.alertName === 'Rating Change'
          // || item.alertInfo === 'Estimate Revision' || item.alertInfo === 'Earnings Surprise'))));
     //   }


        break;
      case 'Signals':
        this.alertsData = this.formatAlertData(filterType);
        break;
      case 'Earnings':
        this.alertsData = this.formatAlertData(filterType);
        break;
      case 'Rating':
        this.alertsData = this.formatAlertData(filterType);
        break;
      default:
        this.alertsData = this.formatAlertData('');
    }
    this.dataSource.data = this.alertsData;
  }

  formatDate(stringDate: string): string {
    const months = [
      'JAN',
      'FEB',
      'MAR',
      'APR',
      'MAY',
      'JUN',
      'JUL',
      'AUG',
      'SEP',
      'OCT',
      'NOV',
      'DEC',
    ];
    const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    const currentDateTime = new Date(stringDate);
    const formattedDate =
      days[moment(stringDate).format('d')] +
      '. ' +
      months[((+moment(stringDate).format('M')) - 1)] +
      ' ' +
      moment(stringDate).format('D') +
      ', ' +
      moment(stringDate).format('Y').toString();
   // return formattedDate;
    return ((+moment(stringDate).format('M')) ) +'/' + moment(stringDate).format('D')
  }

  convertToArray(str): any {
    const a = str.split(',');
    for (let i = 0; i < a.length; i++) {
      a[i] = a[i].replace(/\[/g, '').replace(/\]/g, '').replace(/ /g, '');
    }
    return a;
  }

  alertSorting(sectorData, sortingKey: string, extendKey: string) {
    let sortingOrder: string;
    let sortingOrderExtend: string;
    if (sortingKey === 'date') {
      sortingOrder = AppConst.CONST.sortOrderAsc;
      sortingOrderExtend = AppConst.CONST.sortOrderDesc;
    } else if (extendKey === 'date') {
      sortingOrder = AppConst.CONST.sortOrderDesc;
      sortingOrderExtend = AppConst.CONST.sortOrderAsc;
    }

    const sortingParams = this.setAlertSorting(sectorData, [
      {
        sortingKey: sortingKey,
        sortingOrder: AppConst.CONST.sortOrderAsc,
        ignoreKey: [-1],
      },
      {
        sortingKey: extendKey,
        sortingOrder: AppConst.CONST.sortOrderAsc,
        ignoreKey: [],
      },
    ]);
    return Sort.sortUnique(sortingParams);
  }

  setAlertSorting(data: Array<any>, sortObj: SortObjectModel[]): SortingModel {
    const len = sortObj.length - 1;
    const sortingModelData = new SortingModel();
    const sortLevelKey = [];

    sortObj.forEach((element, index) => {
      let sorting: SortingModel;
      if (index === 0) {
        sorting = sortingModelData;
        sorting.sortData = data;
      } else {
        sortingModelData.extendedData = new SortingModel();
        sorting = sortingModelData.extendedData;
        sorting.sortData = [];
      }

      sorting.sortKey = element.sortingKey;
      sorting.sortOrder = element.sortingOrder;
      sorting.ignoreDataKeys = element.ignoreKey;
      sorting.sortLevelKey = _.cloneDeep(sortLevelKey);
      sorting.ignoreData = [];
      sorting.isExtended = index === len ? false : true;

      sortLevelKey.push(element.sortingKey);
    });
    return sortingModelData;
  }

alertTabSorting(sortingKey):void {
  // if (this.alertSortingDataModel.defaultValueIsTrue) {
  //   this.alertSortingDataModel.defaultValueIsTrue = false;
  //   this.alertSortingDataModel.sortingOrder = 'desc';
  // } else {
    this.alertSortingDataModel.sortingOrder = this.setSortingOrderForDifferentKey(sortingKey);
 // }
  this.alertSortingDataModel.sortingKey = sortingKey;
  const sortingParams = this.setAlertSorting(this.alertsData, [
    {
      sortingKey: this.alertSortingDataModel.sortingKey,
      sortingOrder: this.alertSortingDataModel.sortingOrder,
      ignoreKey: [-1],
    },
    {
      sortingKey: 'industry',
      sortingOrder: AppConst.CONST.sortOrderAsc,
      ignoreKey: [],
    },
  ]);

  this.alertsData =  Sort.sortUnique(sortingParams);
  this.dataSource.data = this.alertsData;
}


setSortingOrderForDifferentKey(sortingKey): string {
  let sortingOrder: any;
  switch (sortingKey) {
    case 'date':
      sortingOrder =
        this.alertSortingDataModel.date === AppConst.CONST.sortOrderAsc
          ? AppConst.CONST.sortOrderDesc
          : AppConst.CONST.sortOrderAsc;
      return this.alertSortingDataModel.date = sortingOrder;

    case 'symbol':
      sortingOrder =
        this.alertSortingDataModel.symbol === AppConst.CONST.sortOrderAsc
          ? AppConst.CONST.sortOrderDesc
          : AppConst.CONST.sortOrderAsc;
      return this.alertSortingDataModel.symbol = sortingOrder;


    default:
      break;
  }

}


  formatAlertData(type: string): any[] {
    let totalAlerts: AlertDataModel[] = [];
    const data = this.alertModel.data;
    switch (type) {
      case 'All':
        // totalAlerts = data.filter((alert) => {
        //   if (alert.value < 3 || alert.value > 3) {
        //       return alert;
        //     } 
          // if (this.isBullishSelected && alert.value > 3) {
          //   return alert;
          // } else if (!this.isBullishSelected && alert.value < 3) {
          //   return alert;
          // }
      //  });
        totalAlerts = this.concatData();
        totalAlerts = this.alertSorting(totalAlerts, 'symbol', 'date');
        break;
      case 'Signals':
        totalAlerts = data.filter((alert) => {
          if (
            alert.alertName === 'Buy Signal' ||
            alert.alertName === 'Sell Signal'
          ) {
            if (this.isBullishSelected && alert.value > 3) {
              return alert;
            } else if (!this.isBullishSelected && alert.value < 3) {
              return alert;
            }
          }
        });
        break;
      case 'Earnings':
        totalAlerts = data.filter((alert) => {
          if (
            alert.alertName === 'Earnings Surprise' ||
            alert.alertName === 'Estimate Revision'
          ) {
            if (this.isBullishSelected && !this.isAllAlertSelected && alert.value > 3) {
              return alert;
            } else if (!this.isBullishSelected && !this.isAllAlertSelected && alert.value < 3) {
              return alert;
            } else if(this.isAllAlertSelected && !this.isBullishSelected && (alert.value > 3 || alert.value < 3)){
              return alert;
            }
          }
        });
        break;
      case 'Rating':
        totalAlerts = data.filter((alert) => {
          if (alert.alertName === 'Rating Change') {
            if (this.isBullishSelected && !this.isAllAlertSelected && alert.value > 3) {
              return alert;
            } else if (!this.isBullishSelected && !this.isAllAlertSelected && alert.value < 3) {
              return alert;
            } else if(this.isAllAlertSelected && !this.isBullishSelected && (alert.value > 3 || alert.value < 3)){
              return alert;
            }
          }
        });
        break;
      default:
    }
    return totalAlerts;
  }

  concatData() {
    return this.formatAlertData('Earnings')
      .concat(this.formatAlertData('Rating'));
  }

  selectAlertType(selectType: any): void {
    const type = selectType;
    switch (type) {
      case 'bullishAlerts':
        this.isBullishSelected = true;
        this.isAllAlertSelected = false;
        this.onAlertFilterType(this.selectedAlertFilter);
        break;
      case 'bearishAlerts':
        this.isBullishSelected = false;
        this.isAllAlertSelected = false;
        this.onAlertFilterType(this.selectedAlertFilter);
        break;
        case 'All':
        this.isAllAlertSelected = true;
        this.isBullishSelected = false;
        this.selectedAlertFilter = 'All';
        this.onAlertFilterType(this.selectedAlertFilter);
        break;
      default:
        break;
    }
  }



}