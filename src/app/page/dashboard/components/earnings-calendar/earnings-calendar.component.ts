import {
  Component,
  OnInit,
  EventEmitter,
  ChangeDetectionStrategy,
  ViewChild,
  AfterViewInit,
  ChangeDetectorRef,
  OnDestroy,
  Input,
  ViewEncapsulation,
  SimpleChanges,
  Output
} from "@angular/core";
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';

import { APIService } from "src/app/core/http-services/api.service";
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView,
  CalendarMonthViewDay,
  DAYS_OF_WEEK,
  CalendarWeekViewBeforeRenderEvent,
  CalendarMonthViewBeforeRenderEvent
} from 'angular-calendar';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  isSameMinute,
  addHours,
  subMonths,
  addMonths,
} from 'date-fns';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { LocalStorage, SharedUtil } from "src/app/core/services/utilily/utility";
import { CalendarEarningsDataModel, EarningEstimateData, EarningSurpriseData, EstimateModifiedDataModel, SurpriseModifiedDataModel } from "src/app/core/data-models/app-model";
import { CalendarEarningData, EarningEstimate, EarningSurprise } from "src/app/core/data-models/service-model";
import * as moment from "moment";
import { MatPaginator } from "@angular/material/paginator";
import { WeekDay, WeekViewHourColumn } from "calendar-utils";
import { any } from "@amcharts/amcharts4/.internal/core/utils/Array";
import { array } from "@amcharts/amcharts4/core";
import { Router } from "@angular/router";


interface EventGroupMeta {
  type: string;
}

export interface EarningsCalendarTable {
  rating: string;
  symbol: string;
  company: string;
  last: number;
  change: number;
  time: string;
  estimate: number;
  lastyear: number;
  reported: number;
  surprise: number;
}

const EARNINGS_DATA: EarningsCalendarTable[] = [
  {
    rating: '',
    symbol: 'ABC',
    company: 'ABC Corp, Inc',
    last: 11.11,
    change: 1.12,
    time: '',
    estimate: 0.45,
    lastyear: 0,
    reported: 1.23,
    surprise: 1.23,
  },
  {
    rating: '',
    symbol: 'IBM',
    company: 'International Business Machines',
    last: 121.21,
    change: 15.12,
    time: '',
    estimate: 0.45,
    lastyear: 0.99,
    reported: 0.83,
    surprise: 1.23,
  },
  {
    rating: '',
    symbol: 'DIS',
    company: 'Disney Corporation',
    last: 11.11,
    change: 1.12,
    time: '',
    estimate: 0.45,
    lastyear: 0,
    reported: 1.23,
    surprise: 1.23,
  },
  {
    rating: '',
    symbol: 'ETSY',
    company: 'Etsy, LLC',
    last: 11.11,
    change: 1.12,
    time: '',
    estimate: 0.45,
    lastyear: 0,
    reported: 1.23,
    surprise: 1.23,
  },
  {
    rating: '',
    symbol: 'GOOGL',
    company: 'Alphabet, Inc.',
    last: 121.21,
    change: 15.12,
    time: '',
    estimate: 0.45,
    lastyear: 0.99,
    reported: 0.83,
    surprise: 1.23,
  },
  {
    rating: '',
    symbol: 'CMCSA',
    company: 'Comcast, Corp',
    last: 11.11,
    change: 1.12,
    time: '',
    estimate: 0.45,
    lastyear: 0,
    reported: 1.23,
    surprise: 1.23,
  },
];


@Component({
  selector: 'app-earnings-calendar',
  templateUrl: './earnings-calendar.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./earnings-calendar.component.scss']
})

export class EarningsCalendarComponent implements OnInit, AfterViewInit, OnDestroy {

  constructor(
    private api: APIService,
    private breakpointObserver: BreakpointObserver,
    private cd: ChangeDetectorRef,
    public router: Router,
  ) { }
  @Input() sentToEarningCalendar: any;
  dataSourceSurpriseCalendar = new MatTableDataSource<EarningSurpriseData>([]);
  dataSourceEstimateCalendar = new MatTableDataSource<EarningEstimateData>([]);
  displaySurpriseColumn = [
    'rating',
    'symbol',
    'company',
    'lastPrice',
    'change',
    'reportingTime',
    'consensusEstimate',
    'epsYearAgo',
    'actualEPS',
    'percentageSurprise',

  ];
  displayEstimateColumn = [
    'rating',
    'symbol',
    'company',
    'lastPrice',
    'change',
    'reportingTime',
    'epsYearAgo',
    'epsEstimateCurrent',
    'actualEPS',
    'percentageSurprise',

  ];

  @ViewChild(MatPaginator, { static: false }) surprisePaginator: MatPaginator;
  @ViewChild(MatPaginator, { static: false }) estimatePaginator: MatPaginator;
  /** Earnings Calendar Table **/
  displayedColumns: string[] = ['rating', 'symbol', 'company', 'last', 'change', 'time', 'estimate', 'lastyear', 'reported', 'surprise'];
  dataSource = EARNINGS_DATA;
  selectedDateOnCalender: any;
  selectedDateOnCalenderFormatted: any;
  @Input() selectedListId: boolean;
  @Input() updateSentToEarnings: string;
  @Output() earningCalenderDataLength = new EventEmitter<any>();

  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
  }
  header: WeekDay[];
  selectedMonthViewDay: CalendarMonthViewDay;
  selectedDayViewDate: Date;
  //  hourColumns: WeekViewHourColumn[];
  hourColumns: CalendarWeekViewBeforeRenderEvent;
  monthColumns: CalendarMonthViewBeforeRenderEvent;
  earningsDataFromApi = new CalendarEarningsDataModel();
  /** Earnings Calendar Component **/
  view: CalendarView = CalendarView.Week;
  viewDate: Date = new Date();
  events: CalendarEvent[];
  setTrueForWeekDefaultValue = false;

  groupedSimilarEvents: CalendarEvent[] = [];
  CalendarView = CalendarView;
  weekStartsOn = DAYS_OF_WEEK.SUNDAY;
  viewChange = new EventEmitter<CalendarView>();
  viewDateChange = new EventEmitter<Date>();
  minDate: Date = subMonths(new Date(), 3);
  maxDate: Date = addMonths(new Date(), 3);
  activeDayIsOpen: boolean;
  arrayOfDateData = [];
  calDaySelect: boolean = false;

  /** Responsive cal **/
  daysInWeek = 7;
  private destroy$ = new Subject<void>();

  ngOnInit(): void {
    this.selectedListId = LocalStorage.getItem('listId');
    this.selectedDateOnCalenderFormatted = moment(this.selectedDateOnCalender).format('dddd, MMMM D, YYYY');
  }

  ngOnChanges(changes: SimpleChanges) {
    this.view = this.sentToEarningCalendar === "nextweek" ? CalendarView.Week : CalendarView.Week;
    this.getEarningCalenderData();

  }

  ngOnDestroy() {
    this.destroy$.next();
  }


  calenderWork(): any {
    this.groupedSimilarEvents = [];
    const processedEvents = new Set();

    this.events.forEach((event) => {
      if (processedEvents.has(event)) {
        return;
      }
      const similarEvents = this.events.filter((otherEvent) => {
        return (
          otherEvent !== event &&
          !processedEvents.has(otherEvent) &&
          isSameMinute(otherEvent.start, event.start) &&
          (isSameMinute(otherEvent.end, event.end) ||
            (!otherEvent.end && !event.end))
        );
      });
      processedEvents.add(event);
      similarEvents.forEach((otherEvent) => {
        processedEvents.add(otherEvent);
      });
      if (similarEvents.length > 0) {
        this.groupedSimilarEvents.push({
          title: `${similarEvents.length + 1}`,
          start: event.start,
          end: event.end,
          meta: {
            groupedEvents: [event, ...similarEvents],
          },
        });
      } else {
        this.groupedSimilarEvents.push(event);
      }
    });


    /** Responsive cal **/
    const CALENDAR_RESPONSIVE = {
      small: {
        breakpoint: '(max-width: 576px)',
        daysInWeek: 5,
      },
      medium: {
        breakpoint: '(max-width: 768px)',
        daysInWeek: 7,
      },
      large: {
        breakpoint: '(max-width: 960px)',
        daysInWeek: 7,
      },
    };

    /** Responsive cal **/
    this.breakpointObserver
      .observe(
        Object.values(CALENDAR_RESPONSIVE).map(({ breakpoint }) => breakpoint)
      )
      .pipe(takeUntil(this.destroy$))
      .subscribe((state: BreakpointState) => {
        const foundBreakpoint = Object.values(CALENDAR_RESPONSIVE).find(
          ({ breakpoint }) => !!state.breakpoints[breakpoint]
        );
        if (foundBreakpoint) {
          this.daysInWeek = foundBreakpoint.daysInWeek;
        } else {
          this.daysInWeek = 7;
        }
        this.cd.markForCheck();
      });
  }


  beforeWeekViewRender(renderEvent: CalendarWeekViewBeforeRenderEvent) {
    let tomorrow  = moment(renderEvent.period.start).add(1,'days');
    this.selectedDateOnCalenderFormatted = moment(tomorrow).format('dddd, MMMM D, YYYY');
    // 
    let  totalEarningCountInMonth : number = 0;
    renderEvent.period.events.forEach((value) => {
      totalEarningCountInMonth =  totalEarningCountInMonth + (parseInt(value.title))
    })
    this.earningCalenderDataLength.emit(totalEarningCountInMonth);
    this.hourColumns = renderEvent;
    this.dataSourceEstimateCalendar = new MatTableDataSource<EarningEstimateData>(
      []
    );
    this.dataSourceSurpriseCalendar = new MatTableDataSource<EarningSurpriseData>(
      []
    );
    renderEvent.header.forEach((hourColumn) => {
      if (moment(new Date(hourColumn.date).setHours(0, 0, 0, 0)).isSame(moment(new Date().setHours(0, 0, 0, 0)))) {
        this.setTrueForWeekDefaultValue = true;
        let hasDataOfDate = this.arrayOfDateData.find(element => element === moment(hourColumn.date).format('YYYY-MM-DD'));

        //  setTimeout(() => {
        if (hasDataOfDate) {
          let elements = document.getElementsByClassName("cal-event");
          this.setEarningTableDataByDefault(hourColumn.date, '', 'week');

        }
        hourColumn.cssClass = '';
      }
    });

    if (!this.setTrueForWeekDefaultValue) {
      // this.setTrueForWeekDefaultValue = false;
      renderEvent.header.forEach((hourColumn) => {
        if (hourColumn.day === 1) {
          let hasDataOfDate = this.arrayOfDateData.find(element => element === moment(hourColumn.date).format('YYYY-MM-DD'));

          //  setTimeout(() => {
          if (hasDataOfDate) {
            setTimeout(() => {
              let elements = document.getElementsByClassName("cal-event");
              //  elements[0].className += " weekday-selected";
              this.setEarningTableDataByDefault(hourColumn.date, '', 'week');
            }, 80);
          }
          this.setTrueForWeekDefaultValue = false;
        }
      });
    }

    this.setTrueForWeekDefaultValue = false;
    this.cd.detectChanges();
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    return;
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        //  this.events.cssClass = 'bg-pink';
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
  }


  getEarningCalenderData(): void {
    this.selectedDateOnCalender = moment().format('YYYY-MM-DD');
    this.selectedDateOnCalenderFormatted = moment(this.selectedDateOnCalender).format('dddd, MMMM D, YYYY');
    this.events = [];
    const listId = null;
    //  console.log(this.minDate,this.maxDate)
    const httpEarningCalenderData$ = this.api.getEarningCalenderData(
      LocalStorage.getItem('listId'),
      moment(this.minDate).format('YYYY-MM-DD'),
      moment(this.maxDate).format('YYYY-MM-DD')
    );

    httpEarningCalenderData$
      .pipe(
        map((resp) => {
          const earningData = resp as CalendarEarningData;
          return this.prepareEarningCalenderData(earningData);
        })
      )
      .subscribe((earningsDataModel: CalendarEarningsDataModel) => {
        this.earningsDataFromApi = earningsDataModel;
        this.events = [];
        if (earningsDataModel.data.earningSurprise) {
          earningsDataModel.data.earningSurprise.forEach(element => {
            this.events.push(
              {
                title: '1',
                start: startOfDay(moment(element.reportDate).toDate()),
                cssClass: '',
                meta: {
                  type: 'earningsDataModel',
                },
              },
            );
          });
        }

        if (earningsDataModel.data.earningEstimate) {
          earningsDataModel.data.earningEstimate.forEach(element => {
            this.events.push(
              {
                title: '1',
                start: startOfDay(moment(element.nextReportDate).toDate()),
                cssClass: '',
                meta: {
                  type: 'earningsDataModel',
                },
              },
            );
          });
          this.setEstimateDataInMatTable(
            earningsDataModel.estimateModifiedData,
            '2022-03-02'
          );
        }
        this.calenderWork();
        this.setArrayOfPresentDateData();
        // this.setSurpriseDataInMatTable(
        //   earningsDataModel.surpriseModifiedData,
        //   '2022-02-18'
        // );
        // moment().format('YYYY-MM-DD')
      });
  }

  setArrayOfPresentDateData(): void {
    this.arrayOfDateData = [];
    // tslint:disable-next-line: forin
    for (const key in this.earningsDataFromApi.estimateModifiedData) {

      this.arrayOfDateData.push(key);
    }
    for (const key in this.earningsDataFromApi.surpriseModifiedData) {

      this.arrayOfDateData.push(key);
    }
  }

  setEarningTableDataOnSelatedDate(date, val, calendarView): any {
    //  dt.valueOf() == new Date().setHours(0, 0, 0, 0).valueOf()
    this.selectedDateOnCalender = date;
    this.selectedDateOnCalenderFormatted = moment(date).format('dddd, MMMM D, YYYY');
    if (moment().isBefore(moment(date)) || new Date().setHours(0, 0, 0, 0).valueOf() === new Date(date).setHours(0, 0, 0, 0).valueOf()) {
      if (calendarView === 'week') {
        this.addSelectedDayViewClass(date);
      } else if (calendarView === 'month') {
        this.addSelectedDayOnMonthViewClass(date);
      }
      this.setEstimateDataInMatTable(
        this.earningsDataFromApi.estimateModifiedData,
        moment(date).format('YYYY-MM-DD')
      );

    } else if ((moment()).isAfter(moment(date))) {
      if (calendarView === 'week') {
        this.addSelectedDayViewClass(date);
      } else if (calendarView === 'month') {
        this.addSelectedDayOnMonthViewClass(date);
      }
      this.setSurpriseDataInMatTable(
        this.earningsDataFromApi.surpriseModifiedData,
        moment(date).format('YYYY-MM-DD')
      );
    }
    //      const combined1 = [].concat(this.dataSourceEstimateCalendar.filteredData, this.dataSourceSurpriseCalendar.filteredData);
    // console.log(combined1);
  }
  prepareEarningCalenderData(earningData: CalendarEarningData): CalendarEarningsDataModel {
    const earningsDataModel = new CalendarEarningsDataModel();
    earningsDataModel.data.earningSurprise = earningData.earningSurprise;
    earningsDataModel.data.earningEstimate = earningData.earningEstimate;

    const surpriseDataObject: EarningSurpriseData[] = this.setSurpriseDataInModel(
      earningData.earningSurprise
    );
    const estimateDataObject: EarningEstimateData[] = this.setEstimateDataInModel(
      earningData.earningEstimate
    );
    surpriseDataObject.forEach((surpriseData) => {
      if (!earningsDataModel.surpriseModifiedData[surpriseData.reportDate]) {
        earningsDataModel.surpriseModifiedData[surpriseData.reportDate] = [];
      }
      earningsDataModel.surpriseModifiedData[surpriseData.reportDate].push(
        surpriseData
      );
    });

    estimateDataObject.forEach((estimateData) => {
      if (!earningsDataModel.estimateModifiedData[estimateData.nextReportDate]) {
        earningsDataModel.estimateModifiedData[
          estimateData.nextReportDate
        ] = [];
      }
      earningsDataModel.estimateModifiedData[estimateData.nextReportDate].push(
        estimateData
      );
    });

    return earningsDataModel;
  }

  setSurpriseDataInMatTable(
    surpriseData: SurpriseModifiedDataModel,
    date: string
  ): void {

    this.dataSourceSurpriseCalendar = new MatTableDataSource<EarningSurpriseData>(
      []
    );
    this.dataSourceEstimateCalendar = new MatTableDataSource<EarningEstimateData>(
      []
    );
    const data = surpriseData[date] as EarningSurpriseData[];
    if (data) {
      this.dataSourceSurpriseCalendar = new MatTableDataSource<EarningSurpriseData>(
        data
      );
    } else {
      this.dataSourceSurpriseCalendar = new MatTableDataSource<EarningSurpriseData>(
        []
      );
    }
    this.dataSourceSurpriseCalendar.paginator = this.surprisePaginator;
    this.cd.detectChanges();
  }

  setEstimateDataInMatTable(
    estimateData: EstimateModifiedDataModel,
    date: any
  ): void {

    this.dataSourceSurpriseCalendar = new MatTableDataSource<EarningSurpriseData>(
      []
    );
    this.dataSourceEstimateCalendar = new MatTableDataSource<EarningEstimateData>(
      []
    );
    const data = estimateData[date] as EarningEstimateData[];
    //  console.log(estimateData, data, date)
    if (data) {
      this.dataSourceEstimateCalendar = new MatTableDataSource<EarningEstimateData>(
        data
      );
    } else {
      this.dataSourceEstimateCalendar = new MatTableDataSource<EarningEstimateData>(
        []
      );
    }
    this.cd.detectChanges();
  }

  setEstimateDataInModel(
    earningData: EarningEstimate[]
  ): EarningEstimateData[] {
    const estimateDataObject: EarningEstimateData[] = [];
    earningData.forEach((element) => {
      const estimateData = new EarningEstimateData();
      estimateData.estimateTrend = (element.estimateTrend === 'positve') ? 'Bullish'
        : (element.estimateTrend === 'negative') ? 'Bearish' : 'N/A';
      estimateData.estimateTrendClass = (element.estimateTrend === 'positve') ? 'bullish'
        : (element.estimateTrend === 'negative') ? 'bearish' : '';
      estimateData.symbol = element.symbol;
      estimateData.epsEstimateCurrent = element.epsEstimateCurrent;
      estimateData.epsEstimateCurrentFormatted = SharedUtil.formatNumber(
        element.epsEstimateCurrent
      );
      estimateData.percentChange = element.percentChange;
      estimateData.percentChangeFormatted = SharedUtil.formatNumber(
        element.percentChange
      );
      if (!element.epsYearAgo) {
        estimateData.epsYearAgo = "--";
      } else {
        estimateData.epsYearAgo = '$' + SharedUtil.formatedString(element.epsYearAgo);
      }
      estimateData.change = element.change;
      const changeFormatted = SharedUtil.formatNumber(element.change);
      estimateData.changeFormatted = SharedUtil.appendSymbol(
        changeFormatted,
        '%'
      );

      estimateData.changeFormattedClass = this.setCssClassUpDown((changeFormatted).split("")[0]);
      estimateData.reportingTime = element.reportingTime;
      estimateData.isEtf = element.isEtf;
      estimateData.pgrRating = element.pgrRating;
      estimateData.rawPgrRating = element.rawPgrRating;
      estimateData.rating = SharedUtil.getPgrRating(
        element.pgrRating,
        element.rawPgrRating
      );
      estimateData.imageSrc = SharedUtil.getPgrArcImageUrl(
        element.pgrRating,
        element.rawPgrRating,
        element.isEtf
      );
      estimateData.nextReportDate = element.nextReportDate;
      estimateData.nextReportDateFormatted = SharedUtil.formatDate(
        element.nextReportDate
      );
      estimateData.company = element.company;
      estimateData.quarter = element.quarter;
      estimateData.quaterFormatted =
        element.quarter < 1 ? 'N/A' : 'Q' + element.quarter.toString();
      estimateData.lastPrice = element.lastPrice;
      estimateData.lastPriceFormatted = SharedUtil.formatNumber(
        element.lastPrice
      );

      estimateDataObject.push(estimateData);
    });
    return estimateDataObject;
  }

  setSurpriseDataInModel(
    earningData: EarningSurprise[]
  ): EarningSurpriseData[] {
    const surpriseDataObject: EarningSurpriseData[] = [];
    earningData.forEach((element) => {
      const surpriseData = new EarningSurpriseData();
      surpriseData.symbol = element.symbol;
      surpriseData.percentChange = element.percentChange;
      surpriseData.percentChangeFormatted = SharedUtil.formatNumber(
        element.percentChange
      );

      if (!element.epsYearAgo) {
        surpriseData.epsYearAgo = "--";
      } else {
        surpriseData.epsYearAgo = '$' + SharedUtil.formatedString(element.epsYearAgo);
      }
      surpriseData.change = element.change;
      const changeFormatted = SharedUtil.formatNumber(element.change);
      surpriseData.changeFormattedClass = this.setCssClassUpDown((changeFormatted).split("")[0]);
      surpriseData.changeFormatted = SharedUtil.appendSymbol(
        changeFormatted,
        '%'
      );
      surpriseData.reportingTime = element.reportingTime;
      surpriseData.consensusEstimate = SharedUtil.formatedString(
        element.consensusEstimate
      );
      surpriseData.percentageSurprise = SharedUtil.formatedString(
        element.percentageSurprise
      );
      surpriseData.percentChangeFormattedClass = this.setCssClassUpDown((surpriseData.percentageSurprise).split("")[0]);
      surpriseData.isEtf = element.isEtf;
      surpriseData.pgrRating = element.pgrRating;
      surpriseData.rawPgrRating = element.rawPgrRating;
      surpriseData.rating = SharedUtil.getPgrRating(
        element.pgrRating,
        element.rawPgrRating
      );
      surpriseData.imageSrc = SharedUtil.getPgrArcImageUrl(
        element.pgrRating,
        element.rawPgrRating,
        element.isEtf
      );
      surpriseData.reportDate = element.reportDate;
      surpriseData.reportDateFormatted = SharedUtil.formatDate(element.reportDate);
      surpriseData.company = element.company;
      surpriseData.actualEPS = element.actualEPS;
      surpriseData.actualEPSFormatted = SharedUtil.formatNumber(
        element.actualEPS
      );
      surpriseData.quarter = element.quarter;
      surpriseData.quaterFormatted =
        element.quarter < 1 ? 'N/A' : 'Q' + element.quarter.toString();
      surpriseData.lastPrice = element.lastPrice;
      surpriseData.lastPriceFormatted = SharedUtil.formatNumber(
        element.lastPrice
      );

      surpriseDataObject.push(surpriseData);
    });
    return surpriseDataObject;
  }


  setCssClassUpDown(value): boolean {
    return value == '-' ? false : true;
  }


  beforeMonthViewRender(renderEvent: CalendarMonthViewBeforeRenderEvent): void {
   let  totalEarningCountInMonth : number = 0;
    renderEvent.period.events.forEach((value) => {
      totalEarningCountInMonth =  totalEarningCountInMonth + (parseInt(value.title))
    })
    this.monthColumns = renderEvent;
    let selectedDay: any = 1;
    renderEvent.body.forEach((day) => {
      const dayOfMonth = day.date.getDate();
      if ((dayOfMonth === selectedDay && day.inMonth) && ((day.day !== 0) && (day.day !== 6))) {
        let hasDataOfDate = this.arrayOfDateData.find(element => element === moment(day.date).format('YYYY-MM-DD'));
        if (hasDataOfDate! = -1) {
          this.setEarningTableDataByDefault(day.date, '', 'week');
        }
        // day.cssClass = 'bg-pink';
      } else if ((dayOfMonth === 1) || (dayOfMonth === 2) && ((day.day === 0) || (day.day === 6))) {
        selectedDay = ((day.day === 0) || (day.day === 6)) ? selectedDay + 1 : selectedDay;
      }
    });
    this.earningCalenderDataLength.emit(totalEarningCountInMonth);
  }

  hourSegmentClicked(event: any) {
    return;
    this.selectedDayViewDate = event.day.date;
    this.addSelectedDayViewClass(event.day.date);
  }

  private addSelectedDayOnMonthViewClass(date) {
    // this.monthColumns = renderEvent;
    this.monthColumns.body.forEach((day) => {
      if (moment(new Date(day.date).setHours(0, 0, 0, 0)).isSame(moment(new Date(date).setHours(0, 0, 0, 0))) && day.inMonth) {
        day.cssClass = 'bg-pink';
      } else {
        day.cssClass = '';
      }
    });
  }



  private addSelectedDayViewClass(date) {
    this.hourColumns.header.forEach((hourColumn) => {
      if (moment(new Date(hourColumn.date).setHours(0, 0, 0, 0)).isSame(moment(new Date(date).setHours(0, 0, 0, 0)))) {
        hourColumn.cssClass = 'bg-pink';
      } else {
        hourColumn.cssClass = '';
      }
    });
  }

  setEarningTableDataByDefault(date, val, calendarView): any {
    this.selectedDateOnCalender = date;
    this.selectedDateOnCalenderFormatted = moment(date).format('dddd, MMMM D, YYYY');
    if ((moment()).isBefore(moment(date)) || new Date().setHours(0, 0, 0, 0).valueOf() === new Date(date).setHours(0, 0, 0, 0).valueOf()) {
      this.setEstimateDataInMatTable(
        this.earningsDataFromApi.estimateModifiedData,
        moment(date).format('YYYY-MM-DD')
      );
    } else if ((moment()).isAfter(moment(date))) {
        this.setSurpriseDataInMatTable(
        this.earningsDataFromApi.surpriseModifiedData,
        moment(date).format('YYYY-MM-DD')
      );
    }
  }


  redirectOnPgr(symbol): void {
    LocalStorage.setItem('mainSymbol', symbol);
    this.router.navigate(["/"]
    );
  }
}