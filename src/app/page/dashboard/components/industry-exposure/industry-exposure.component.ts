import {
  AfterViewInit,
  Component,
  Input,
  OnInit,
  OnDestroy,
  ViewChild,
  Output,
  EventEmitter,
  SimpleChanges
} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { APIService } from "src/app/core/http-services/api.service";
import { Subject } from 'rxjs';
import * as _ from 'lodash';
import { map, takeUntil } from 'rxjs/operators';
import { LocalStorage, SharedUtil, Sort } from "src/app/core/services/utilily/utility";
import { IndustryExposureDataObject, IndustryExposureSymbolListData } from "src/app/core/data-models/service-model";
import { IndustryExposureDataModel, IndustryExposureSortingDataModel, IndustryExposureSymbolListDataModel, SortingModel, SortObjectModel, StrengthCountObject } from "src/app/core/data-models/app-model";
import { AppConst } from "src/app/core/app-constants/app-const";
import { Router } from "@angular/router";




@Component({
  selector: 'app-industry-exposure',
  templateUrl: './industry-exposure.component.html',
  styleUrls: ['./industry-exposure.component.scss']
})

export class IndustryExposureComponent implements OnInit, AfterViewInit {
  industryExposureData: IndustryExposureDataModel[] = [];
  industryExposureMatTableData = new MatTableDataSource<IndustryExposureDataModel>([]);
  strengthCountObjectData = new StrengthCountObject();
  @Output() strengthCountObject = new EventEmitter<StrengthCountObject>();
  @Input() selectedListId: any;
  @Input() sentUpdateToIndustryExposure: any;
  industryExposureSortingDataModel = new IndustryExposureSortingDataModel();
  displayedColumns: string[] = [ 'industryIcon', 'industry', 'strength', 'symbolCount', 'symbolList'];
  constructor(
    private api: APIService,
    public router: Router,
  ) {}
  ngAfterViewInit(): void {
  }

  ngOnInit(): void {
    //  this.getIndustryExposure();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.doSomethingOnChange(changes);
  }

  doSomethingOnChange(changes): void {
   // if (changes.selectedListId && changes.sentUpdateToIndustryExposure) {}
    if ((changes.selectedListId && Number(changes.selectedListId.previousValue) !== (Number(LocalStorage.getItem('listId')))) ||
    (!changes.selectedListId && changes.sentUpdateToIndustryExposure)) {
      this.getIndustryExposure();
    }
    // if (!changes.selectedListId && changes.sentUpdateToIndustryExposure) {
    //   this.getIndustryExposure();
    // }
  }

  getIndustryExposure(): void {
    const listId = LocalStorage.getItem('listId');
    this.emptyDataInMatTable();
    const httpGetIndustryExposureData$ = this.api.getIndustryExposure(listId);
    httpGetIndustryExposureData$
      .pipe(
        map((resp) => {
          const industryExposuretData = resp as IndustryExposureDataObject;
          let industryData: IndustryExposureDataModel[] = [];
          if (industryExposuretData.status === 'true') {
            industryData = this.prepareIndustryExposureData(industryExposuretData);
            // sorting
            // let dataAfterSorting = this.sortIndustryData(industryData);
            return industryData;
          }
          return industryData;
        })
      )
      .subscribe((industryExposuretData: IndustryExposureDataModel[]) => {
        this.industryExposureSortingDataModel.defaultValueIsTrue = true;
        this.sortIndustryDataFromUi(industryExposuretData, this.industryExposureSortingDataModel.defaultSorting);
        // this.fillFinalData(industryExposuretData);
      });

  }

  fillFinalData(industryExposuretData: IndustryExposureDataModel[]): void {
    this.industryExposureData = industryExposuretData;
    this.fillDataInMatTable(industryExposuretData);
  }

  sortIndustryDataFromUi(data: IndustryExposureDataModel[], sortingKey: string): void {
    if (this.industryExposureSortingDataModel.defaultValueIsTrue) {
      this.industryExposureSortingDataModel.defaultValueIsTrue = false;
      this.industryExposureSortingDataModel.sortingOrder = 'desc';
    } else {
      this.industryExposureSortingDataModel.sortingOrder = this.setSortingOrderForDifferentKey(sortingKey);
    }
    this.industryExposureSortingDataModel.sortingKey = sortingKey;
    const sortingParams = this.setIndustryExposureSorting(data, [
      {
        sortingKey: this.industryExposureSortingDataModel.sortingKey,
        sortingOrder: this.industryExposureSortingDataModel.sortingOrder,
        ignoreKey: [-1],
      },
      {
        sortingKey: 'industry',
        sortingOrder: AppConst.CONST.sortOrderAsc,
        ignoreKey: [],
      },
    ]);
    let dataAfterSorting: IndustryExposureDataModel[] = Sort.sortUnique(sortingParams);
    this.fillFinalData(dataAfterSorting);
  }

  setSortingOrderForDifferentKey(sortingKey): string {
    let sortingOrder: any;
    switch (sortingKey) {
      case 'powerBarRating':
        sortingOrder =
          this.industryExposureSortingDataModel.powerBarRating === AppConst.CONST.sortOrderAsc
            ? AppConst.CONST.sortOrderDesc
            : AppConst.CONST.sortOrderAsc;
        return this.industryExposureSortingDataModel.powerBarRating = sortingOrder;

      case 'industry':
        sortingOrder =
          this.industryExposureSortingDataModel.industry === AppConst.CONST.sortOrderAsc
            ? AppConst.CONST.sortOrderDesc
            : AppConst.CONST.sortOrderAsc;
        return this.industryExposureSortingDataModel.industry = sortingOrder;

      case 'symbolCount':
        sortingOrder =
          this.industryExposureSortingDataModel.exposure === AppConst.CONST.sortOrderAsc
            ? AppConst.CONST.sortOrderDesc
            : AppConst.CONST.sortOrderAsc;
        return this.industryExposureSortingDataModel.exposure = sortingOrder;

      default:
        break;
    }

  }

  setIndustryExposureSorting(
    data: Array<any>,
    sortObj: SortObjectModel[]
  ): SortingModel {
    const len = sortObj.length - 1;
    const sortingModelData = new SortingModel();
    const sortLevelKey = [];

    sortObj.forEach((element, index) => {
      let sorting: SortingModel;
      if (index === 0) {
        sorting = sortingModelData;
        sorting.sortData = data;
      } else {
        sortingModelData.extendedData = new SortingModel();
        sorting = sortingModelData.extendedData;
        sorting.sortData = [];
      }

      sorting.sortKey = element.sortingKey;
      sorting.sortOrder = element.sortingOrder;
      sorting.ignoreDataKeys = element.ignoreKey;
      sorting.sortLevelKey = _.cloneDeep(sortLevelKey);
      sorting.ignoreData = [];
      sorting.isExtended = index === len ? false : true;

      sortLevelKey.push(element.sortingKey);
    });

    return sortingModelData;
  }

  sortIndustryData(data: IndustryExposureDataModel[]): IndustryExposureDataModel[] {
    // sorting and return final data
    return data;
  }

  emptyDataInMatTable(): void {
    this.industryExposureMatTableData = new MatTableDataSource<IndustryExposureDataModel>([]);
  }

  fillDataInMatTable(data: IndustryExposureDataModel[]): void {
    this.industryExposureMatTableData = new MatTableDataSource<IndustryExposureDataModel>(data);
  }

  prepareIndustryExposureData(industryExposure: IndustryExposureDataObject): IndustryExposureDataModel[] {
    let industryExposureDataModel: IndustryExposureDataModel[] = [];
    let weekCount = 0, strongCount = 0, neutralCount = 0;
    let industryExposureData = industryExposure.data;
    industryExposureData.forEach(data => {
      let industryExposureFormattedData = new IndustryExposureDataModel();
      industryExposureFormattedData.powerBarRating = data.rating;
      industryExposureFormattedData.industry = data.listName;
      
      industryExposureFormattedData.strength = data.Strength;
      industryExposureFormattedData.symbolCount = data.symbolCount;
      industryExposureFormattedData.imgSrcBind = './assets/industry-icons/'+data.listName+'.svg'
      industryExposureFormattedData.strengthClass = SharedUtil.getColorForStrengthAndWeek(data.Strength);
      industryExposureFormattedData.svgColorSet = industryExposureFormattedData.strengthClass === 'red' ? 'svg-redcolor-set' : 'svg-greencolor-set' ;
      weekCount = industryExposureFormattedData.strengthClass === 'red' ? weekCount + 1 : weekCount;
      strongCount = industryExposureFormattedData.strengthClass === 'green' ? strongCount + 1 : strongCount;
      neutralCount = industryExposureFormattedData.strengthClass === 'neutral-label' ? neutralCount + 1 : neutralCount;
      industryExposureFormattedData.sliderwidth = this.calculateWidth(data.symbolCount, industryExposureData);
      let symbolList: IndustryExposureSymbolListDataModel[] = this.prepareSymbolListData(data.symbolList);
      industryExposureFormattedData.symbolList = symbolList;
      industryExposureDataModel.push(industryExposureFormattedData);
    });
    this.strengthCountObjectData.strong = strongCount;
    this.strengthCountObjectData.weak = weekCount;
    this.strengthCountObject.emit(this.strengthCountObjectData);
    return industryExposureDataModel;
  }

  prepareSymbolListData(data: IndustryExposureSymbolListData[]): IndustryExposureSymbolListDataModel[] {
    let symbolList: IndustryExposureSymbolListDataModel[] = [];
    data.forEach(symbolData => {
      let symbolListData = new IndustryExposureSymbolListDataModel();
      symbolListData.pgr = symbolData.pgr;
      symbolListData.rawPgr = symbolData.raw_PGR;
      symbolListData.symbol = symbolData.symbol;
      symbolListData.rating = SharedUtil.getPgrRating(
        symbolData.pgr,
        symbolData.raw_PGR
      );
      symbolListData.pgrImg = SharedUtil.getPgrMicroArcWithoutNeutralValue(symbolData.pgr,
        symbolData.raw_PGR, false);
      symbolList.push(symbolListData);
    });

    return symbolList;
  }

  calculateWidth(val, industryData): number {
    const sliderBarArray = [];
    industryData.forEach(data => {
      sliderBarArray.push(Math.abs(data.symbolCount));
    });
    const maxVal = Math.max.apply(null, sliderBarArray);
    if (maxVal > 0) {
      const sliderBarPercent = (Math.abs(val) / maxVal) * 100;
      return sliderBarPercent;
    } else {
      return 0;
    }
  }

  redirectOnPgr(symbol): void {
    LocalStorage.setItem('mainSymbol', symbol);
    this.router.navigate(["/"]
    );
  }

}