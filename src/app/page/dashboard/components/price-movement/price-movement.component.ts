// import { Component, OnInit } from "@angular/core";
// import { MatDialog } from "@angular/material/dialog";
// import { ActivatedRoute, Router } from "@angular/router";
// import { CookieService } from "ngx-cookie-service";
// import { APIService } from "src/app/core/http-services/api.service";
// import { DataService } from "src/app/core/services/data.service";

import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
// import { APIService } from 'src/app/core/http-services/api.service';
import {
  AlertComponentData,
  AlertDataModel,
  AlertFilterType,
  AlertModel,
  ContextSummaryDataModel,
  HeaderDataModel,
  ListSymbolsPriceForSelectedTimeFrameModel,
  PriceMovementSorting,
  ProductAccessModel,
  SignalDataModel,
  SortingModel,
  SortObjectModel,
  UserRoleMappingModel,
} from 'src/app/core/data-models/app-model';
import {
  LocalStorage,
  RedirectParameter,
  SharedUtil,
  Signal,
  Sort,
} from 'src/app/core/services/utilily/utility';
import {
  AlertsData,
  DashboardData,
  EarningsGlance,
  LoginDetailModel,
  SymbolDataRootObject,
  SymbolLookUp,
} from 'src/app/core/data-models/service-model';

import { forkJoin, Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { AppConst } from 'src/app/core/app-constants/app-const';
import * as _ from 'lodash';
// import { InfoDialogComponent } from '../shared/info-dialog/info-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { APIService } from 'src/app/core/http-services/api.service';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from 'src/app/core/services/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { ToastService } from 'src/app/core/services/toast-service';
import { AppDataService } from 'src/app/core/services/app-data.service';
import { AuthService } from 'src/app/core/http-services/auth.service';
import { ViewportScroller } from '@angular/common';

@Component({
  selector: 'app-price-movement',
  templateUrl: './price-movement.component.html',
  styleUrls: ['./price-movement.component.scss'],
})
export class PriceMovementComponent implements OnInit {
  @Output() dashBoardDataUpdate = new EventEmitter<any>();
  @Input() updateSentToEarnings: string;
  @Input() updateSentToPriceMovement: string;
  @Input() selectedListId: any;
  @Input() sendSelectedListData: any;
  sentToEarningCalendar: string;
  alertComponentData1: AlertComponentData;
  dashboardData = new DashboardData();
  documentClickEventRegister: boolean = false;
  userDetails: LoginDetailModel;
  isDefaultSorting: boolean = true;
  saveUsername: boolean = false;
  dateForDashboard: any;
  checkCurrentListDataInterval: NodeJS.Timer;
  totalProductAccessArray = [];
  refereshLastPriceDataOnIntervel: NodeJS.Timer;
  alertsData: AlertDataModel[] = [];
  alertModel: AlertModel = new AlertModel();
  selectedAlertFilter = 'All';
  userAccess: UserRoleMappingModel;

  filterType: AlertFilterType[] = [
    {
      value: 'All',
      viewValue: 'All Alerts',
      comment: 'default to show all alerts without applying a filter',
    },
    {
      value: 'Signals',
      viewValue: 'Signals',
      comment: 'Signals only',
    },
    {
      value: 'Earnings',
      viewValue: 'Earnings Alerts',
      comment: 'including surprises and revisions',
    },
    {
      value: 'Rating',
      viewValue: 'Rating Changes',
      comment: 'Rating Changes only',
    },
  ];
  healthCheckRefresh$: Observable<boolean>;
  private destroyed$ = new Subject();
  // priceMovementSortingModel = new PriceMovementSorting();
  productAccessModel: ProductAccessModel;
  selectedTimeFrame: any = '1day';
  updateSentToAlerts: string;
  isMobile: boolean = false;

  constructor(
    private api: APIService,
    public dialog: MatDialog,
    private cookieService: CookieService,
    private dataService: DataService,
    public router: Router,
    private route: ActivatedRoute,
    public toastService: ToastService,
    public appDataService: AppDataService,
    public auth: AuthService,
    public viewportScroller: ViewportScroller
  ) {
    if (!this.userDetails) {
      this.userDetails = this.getUserDetail();
    }
  }

  ngOnInit(): void {
    this.userAccess = this.appDataService.userRoleMappingModel;
    // this.productAccessModel = this.appDataService.productAccessModel;
    this.isMobile = this.appDataService.cookiesData.isMobile;
    this.dateForDashboard = moment()
      .tz('America/New_York')
      .format('MM/DD/YY HH:mmA z');

    if (
      LocalStorage.getItem('listId') === -1 ||
      LocalStorage.getItem('listId') === undefined ||
      !LocalStorage.getItem('listId')
    ) {
      this.userDetails = LocalStorage.getItem('userDetail');
      this.selectedListId = this.userDetails.ListID;
    } else {
      this.selectedListId = LocalStorage.getItem('listId');
    }
    //   this.loadInitialState(this.selectedListId);
  }

  ngOnChanges(): void {
    setTimeout(() => {
      if (this.appDataService.dashboardData?.alertComponentData !== undefined) {
        this.dashboardData.alertComponentData =
          this.appDataService.dashboardData.alertComponentData;
        this.loadInitialState(this.selectedListId);
      }
    }, 3000);
    if (
      LocalStorage.getItem('listId') === -1 ||
      LocalStorage.getItem('listId') === undefined
    ) {
      this.userDetails = LocalStorage.getItem('userDetail');
      this.selectedListId = this.userDetails.ListID;
    } else {
      this.selectedListId = LocalStorage.getItem('listId');
    }
    this.refereshLastPrice();
    //  this.loadInitialState(this.selectedListId);
    // this.init();
  }

  private refereshLastPrice() {
    clearInterval(this.refereshLastPriceDataOnIntervel);
    this.refereshLastPriceDataOnIntervel = setInterval(async () => {
      this.dashboardPriceMoment(
        this.selectedTimeFrame,
        this.selectedListId,
        'interval'
      );
    }, 72000);
  }

  ngOnDestroy() {
    clearInterval(this.refereshLastPriceDataOnIntervel);
  }

  getUserDetail(): LoginDetailModel {
    const userDetail: LoginDetailModel = LocalStorage.getItem('userDetail');
    return userDetail;
  }

  getSymbolData(): void {
    let symbol = LocalStorage.getItem('mainSymbol');
    this.recentlyReviewSymbol(symbol);
  }

  recentlyReviewSymbol(symbol): void {
    this.route.queryParams.subscribe((params) => {
      if (params['data']) {
        const data = JSON.parse(params['data']);
        const navigationExtras =
          RedirectParameter.getNavigationExtrasDataForResearch(
            symbol,
            null,
            null,
            null
          );
        this.dataService.recentlyViewedData.setSymbol(data.symbol);
        this.router.navigate(['pgr/#/'], navigationExtras);
      }
    });
  }

  getWatchListUpdate(event): void {
    this.selectedListId = event;
    this.loadInitialState(this.selectedListId);
  }

  dashboardPriceMoment(timeFrame, listId, value): void {
    this.selectedTimeFrame = timeFrame;
    this.dateForDashboard = moment()
      .tz('America/New_York')
      .format('MM/DD/YY HH:mmA z');
    this.api
      .getListSymbolsPriceForSelectedTimeFrame(
        timeFrame,
        listId,
        this.userDetails.UID
      )
      .subscribe((resp) => {
        this.appDataService.listSymbolsPriceForSelectedTimeFrameArray = [];

        resp.data.forEach((element) => {
          const listSymbolsPriceForSelectedTimeFrame =
            new ListSymbolsPriceForSelectedTimeFrameModel();
          listSymbolsPriceForSelectedTimeFrame.change =
            element.change.toFixed(2);
          listSymbolsPriceForSelectedTimeFrame.corrected_pgr =
            element.corrected_pgr;
          listSymbolsPriceForSelectedTimeFrame.is_etf = element.is_etf;
          listSymbolsPriceForSelectedTimeFrame.last = +element.last.toFixed(2);
          listSymbolsPriceForSelectedTimeFrame.percent_change =
            element.percent_change;
          listSymbolsPriceForSelectedTimeFrame.raw_pgr = element.raw_pgr;
          listSymbolsPriceForSelectedTimeFrame.symbol = element.symbol;
          listSymbolsPriceForSelectedTimeFrame.companyName =
            element.company_name;
          for (let alert of this.dashboardData.alertComponentData?.alertModel
            ?.data) {
            // tslint:disable-next-line: max-line-length
            if (
              alert.symbol === element.symbol &&
              (alert.alertName === 'Rating Change' ||
                alert.alertInfo === 'Estimate Revision' ||
                alert.alertInfo === 'Earnings Surprise') &&
              (alert.value > 3 || alert.value < 3)
            ) {
              listSymbolsPriceForSelectedTimeFrame.alertTrue = true;
              listSymbolsPriceForSelectedTimeFrame.alertType = alert.alertName;
              break;
            } else {
              listSymbolsPriceForSelectedTimeFrame.alertTrue = false;
              listSymbolsPriceForSelectedTimeFrame.alertType = alert.alertName;
            }
          }

          if (element.percent_change === 0) {
            listSymbolsPriceForSelectedTimeFrame.listPriceClass = 'grey';
          } else {
            listSymbolsPriceForSelectedTimeFrame.sliderValue =
              element.percent_change.toString().split('')[0] === '-'
                ? false
                : true;
            listSymbolsPriceForSelectedTimeFrame.listPriceClass =
              listSymbolsPriceForSelectedTimeFrame.sliderValue ? 'up' : 'down';
          }
          // if (element.percent_change.toString().split('')[0] === '-') {
          //   listSymbolsPriceForSelectedTimeFrame.sliderValue = false;
          // } else {
          //   listSymbolsPriceForSelectedTimeFrame.sliderValue = true;
          // }

          listSymbolsPriceForSelectedTimeFrame.finalRating =
            SharedUtil.getPgrRating(element.corrected_pgr, element.raw_pgr);
          listSymbolsPriceForSelectedTimeFrame.selectedTimeFrame = timeFrame;
          listSymbolsPriceForSelectedTimeFrame.modifiedChangeValue =
            SharedUtil.formatChangeDashWat(element.percent_change);
          listSymbolsPriceForSelectedTimeFrame.calculateWidth =
            SharedUtil.calculateWidth(element.percent_change, resp.data);
          listSymbolsPriceForSelectedTimeFrame.imgSrc =
            SharedUtil.getPgrArcWithoutNeutralValue(
              element.corrected_pgr,
              element.raw_pgr,
              element.is_etf
            );
          this.appDataService.listSymbolsPriceForSelectedTimeFrameArray.push(
            listSymbolsPriceForSelectedTimeFrame
          );
        });

        if (
          value === 'interval' ||
          this.appDataService.priceMovementRenderCount === 1
        ) {
          const sortingParams = this.setPriceMovementSorting(
            this.appDataService.listSymbolsPriceForSelectedTimeFrameArray,
            this.appDataService.sortingParamsModel
          );

          this.appDataService.listSymbolsPriceForSelectedTimeFrameArray =
            Sort.sortUnique(sortingParams);
        } else if (this.appDataService.priceMovementRenderCount === 0) {
          this.appDataService.priceMovementRenderCount = 1;
          // tslint:disable-next-line: no-unused-expression
          this.appDataService.priceMovementSortingModel.sortingOrder =
          AppConst.CONST.sortOrderAsc;
          this.appDataService.listSymbolsPriceForSelectedTimeFrameArray =
            this.priceMovementSorting('percent_change', 'symbol');
        }

        this.dashboardData.priceMovementComponentData =
          this.appDataService.listSymbolsPriceForSelectedTimeFrameArray;
        this.appDataService.dashboardData = this.dashboardData;
      });
  }

  selectTimeFrame(timeFrame): void {
    this.dashboardPriceMoment(timeFrame, this.selectedListId, '');
  }

  priceMovementSorting(sortingKey: string, extendKey: string): any {
    if (sortingKey == 'percent_change') {
      if (
        this.appDataService.priceMovementSortingModel.sortingOrder ===
        AppConst.CONST.sortOrderAsc
      ) {
        this.appDataService.priceMovementSortingModel.sortingOrder =
          AppConst.CONST.sortOrderDesc;
      } else {
        this.appDataService.priceMovementSortingModel.sortingOrder =
          AppConst.CONST.sortOrderAsc;
      }
      this.appDataService.priceMovementSortingModel.finalSorting =
        this.appDataService.priceMovementSortingModel.sortingOrder;
    } else if (sortingKey == 'finalRating') {
      if (
        this.appDataService.priceMovementSortingModel.sortingOrderPgr ===
        AppConst.CONST.sortOrderAsc
      ) {
        this.appDataService.priceMovementSortingModel.sortingOrderPgr =
          AppConst.CONST.sortOrderDesc;
      } else {
        this.appDataService.priceMovementSortingModel.sortingOrderPgr =
          AppConst.CONST.sortOrderAsc;
      }
      this.appDataService.priceMovementSortingModel.finalSorting =
        this.appDataService.priceMovementSortingModel.sortingOrderPgr;
    } else if (sortingKey == 'symbol') {
      if (
        this.appDataService.priceMovementSortingModel.sortingOrderSymbols ===
        AppConst.CONST.sortOrderAsc
      ) {
        this.appDataService.priceMovementSortingModel.sortingOrderSymbols =
          AppConst.CONST.sortOrderDesc;
      } else {
        this.appDataService.priceMovementSortingModel.sortingOrderSymbols =
          AppConst.CONST.sortOrderAsc;
      }
      this.appDataService.priceMovementSortingModel.finalSorting =
        this.appDataService.priceMovementSortingModel.sortingOrderSymbols;
    } else if (sortingKey == 'last') {
      if (
        this.appDataService.priceMovementSortingModel.sortingOrderPrice ===
        AppConst.CONST.sortOrderAsc
      ) {
        this.appDataService.priceMovementSortingModel.sortingOrderPrice =
          AppConst.CONST.sortOrderDesc;
      } else {
        this.appDataService.priceMovementSortingModel.sortingOrderPrice =
          AppConst.CONST.sortOrderAsc;
      }
      this.appDataService.priceMovementSortingModel.finalSorting =
        this.appDataService.priceMovementSortingModel.sortingOrderPrice;
    }

    let abc = [
      {
        sortingKey: sortingKey,
        sortingOrder:
          this.appDataService.priceMovementSortingModel.finalSorting,
        ignoreKey: [-1],
      },
      {
        sortingKey: extendKey,
        sortingOrder: AppConst.CONST.sortOrderAsc,
        ignoreKey: [],
      },
    ];

    const sortingParams = this.setPriceMovementSorting(
      this.appDataService.listSymbolsPriceForSelectedTimeFrameArray,
      abc
    );
    this.appDataService.sortingParamsModel = abc;
    this.appDataService.listSymbolsPriceForSelectedTimeFrameArray =
      Sort.sortUnique(sortingParams);
    return Sort.sortUnique(sortingParams);
  }

  setPriceMovementSorting(
    data: Array<any>,
    sortObj: SortObjectModel[]
  ): SortingModel {
    const len = sortObj.length - 1;
    const sortingModelData = new SortingModel();
    const sortLevelKey = [];

    sortObj.forEach((element, index) => {
      let sorting: SortingModel;
      if (index === 0) {
        sorting = sortingModelData;
        sorting.sortData = data;
      } else {
        sortingModelData.extendedData = new SortingModel();
        sorting = sortingModelData.extendedData;
        sorting.sortData = [];
      }

      sorting.sortKey = element.sortingKey;
      sorting.sortOrder = element.sortingOrder;
      sorting.ignoreDataKeys = element.ignoreKey;
      sorting.sortLevelKey = _.cloneDeep(sortLevelKey);
      sorting.ignoreData = [];
      sorting.isExtended = index === len ? false : true;

      sortLevelKey.push(element.sortingKey);
    });

    return sortingModelData;
  }

  removeList(index): void {
    // if(this.sendSelectedListData.readonly){
    //   this.toastService.showDanger("You can't modify Power Gauge Report Portfolio");
    //   return
    // }

    if (this.sendSelectedListData.readonly) {
      this.toastService.showDanger(
        "You can't modify" + this.sendSelectedListData.listName
      );
      return;
    }
    // if(this.sendSelectedListData.listName === 'PowerTactics Portfolio'){
    //   this.toastService.showDanger("You can't modify PowerTactics Portfolio");
    //   return
    // }
    const httpDeleteSymbolFromList$ = this.api.deleteSymbolFromList(
      index.symbol,
      this.userDetails.UID
    );
    httpDeleteSymbolFromList$
      .pipe(
        map((resp: { Status: boolean }) => {
          return resp.Status;
        })
      )
      .subscribe((status: boolean) => {
        if (status) {
          this.toastService.showSuccess('Successfully removed');
          this.dashboardPriceMoment('1day', this.selectedListId, '');
          this.dashBoardDataUpdate.emit(index.symbol);
          this.dataService.setProjectAccess(true);
        }
      });
  }

  loadInitialState(listId): void {
    this.dashboardPriceMoment('1day', listId, '');
  }

  redirectOnPgr(symbol): void {
    LocalStorage.setItem('mainSymbol', symbol);
    this.dataService.recentlyViewedData.setSymbol(symbol);
    this.router.navigate(['/']);
  }

  addActiveClass(): void {
    let d = document.getElementById('nav-alerts-tab');
    d.className += ' active';
    let element = document.getElementById('nav-performance-tab');
    element.classList.remove('active');
  }
}
