import { ChangeDetectorRef, Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import {
  AlertComponentData,
  AlertDataModel,
  AlertFilterType,
  AlertModel,
  ContextSummaryDataModel,
  HeaderDataModel,
  ListSymbolsPriceForSelectedTimeFrameModel,
  PriceMovementSorting,
  ProductAccessModel,
  SignalDataModel,
  SortingModel,
  SortObjectModel,
  StrengthCountObject,
  UserRoleMappingModel,
} from 'src/app/core/data-models/app-model';
import {
  LocalStorage,
  RedirectParameter,
  SharedUtil,
  Signal,
  Sort,
} from 'src/app/core/services/utilily/utility';
import {
  AlertsData,
  EarningsGlance,
  LoginDetailModel,
  SymbolDataRootObject,
  SymbolLookUp,
} from 'src/app/core/data-models/service-model';

import { forkJoin, Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { AppConst } from 'src/app/core/app-constants/app-const';
import * as _ from 'lodash';
import { MatDialog } from '@angular/material/dialog';
import { APIService } from 'src/app/core/http-services/api.service';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from 'src/app/core/services/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { ToastService } from 'src/app/core/services/toast-service';
import { AppDataService } from 'src/app/core/services/app-data.service';
import { AuthService } from 'src/app/core/http-services/auth.service';
import { ViewportScroller } from '@angular/common';
import { ConditionalExpr } from '@angular/compiler';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {

  headerDataModel: HeaderDataModel = new HeaderDataModel();
  @ViewChild('symbolSearch', { static: false }) searchElement: ElementRef;
  symbolLookupData: SymbolLookUp[] = [];
  etfSymbolLookupData: SymbolLookUp[] = [];
  StockSymbolLookupData: SymbolLookUp[] = [];
  sentToEarningCalendar: string;
  sentUpdateToIndustryExposure: string;
  @ViewChild('logoutPopup', { static: false }) logoutPopup: any;
  listSymbolsPriceForSelectedTimeFrameArray: ListSymbolsPriceForSelectedTimeFrameModel[] = [];
  strengthCountObjectData = new StrengthCountObject();
  selectedSymbol: string;
  selectedListId: any;
  selectedListName :string;
  sendSelectedListData:any;
  productsAccessCount: any;
  setPreferenceForCPP: boolean = false;
  showLogoutPopup: boolean = false;

 // isBullishSelected = true;
  productsList: any;
  documentClickEventRegister: boolean = false;
  productPopUpShow: boolean = false;
  userDetails: LoginDetailModel;
  isDefaultSorting: boolean = true;
  createLogoutXPosition: any = 0;
  createLogoutYPosition: any = 0;
  fixedLogoutYPosition: any = 0;
  saveUsername: boolean = false;
  dateForDashboard: any;
  checkCurrentListDataInterval: NodeJS.Timer;
  totalProductAccessArray = [];
  earningsGlanceObject = new EarningsGlance();
  refereshLastPriceDataOnIntervel: NodeJS.Timer;
  alertComponentData: AlertComponentData;
  earningCalenderDataCount:any;
  welcomePopUpCheckBoxValue: boolean;
  emailAlertModeValue : boolean;
  emailAlertModeValueFromHeader: boolean;

  //Alert 

  //   selectedList: SelectedListNavModel = {
  //     listId: undefined,
  //     listType: 'User',
  //     listName: '',
  // };
  // filterType: AlertFilterType[] = [
  //   {
  //     value: 'All',
  //     viewValue: 'All Alerts',
  //     comment: 'default to show all alerts without applying a filter',
  //   },
  //   {
  //     value: 'Signals',
  //     viewValue: 'Signals',
  //     comment: 'Signals only',
  //   },
  //   {
  //     value: 'Earnings',
  //     viewValue: 'Earnings Alerts',
  //     comment: 'including surprises and revisions',
  //   },
  //   {
  //     value: 'Rating',
  //     viewValue: 'Rating Changes',
  //     comment: 'Rating Changes only',
  //   },
  // ];
  // alertsData: AlertDataModel[] = [];
  
  // selectedAlertFilter = 'All';

  alertModel: AlertModel = new AlertModel();
  healthCheckRefresh$: Observable<boolean>;
  private destroyed$ = new Subject();
  // priceMovementSortingModel = new PriceMovementSorting;
  productAccessModel: ProductAccessModel;
  // selectedTimeFrame: any = '1day';
  updateSentToAlerts: string;
  updateSentToEarnings: string;
  projectAccessDashboard: any;
  updateSentToPriceMovement: string;
  isMobile = false;
  userAccess: UserRoleMappingModel;
  showWelcomePopUp: boolean;
  ngOnInit(): void {
    this.userAccess = this.appDataService.userRoleMappingModel;
    // this.productAccessModel = this.appDataService.productAccessModel;
    this.isMobile = this.appDataService.cookiesData.isMobile;
    // this.productsList = this.appDataService.productsListModel;
    // this.totalProductAccessArray = this.appDataService.productAccessModel.totalProductAccessArray[0];
    // if (!this.appDataService.productAccessModel.dashboard) {
    //   this.logOut();
    // }

    if (!this.userAccess.userPermission.dashboard) {
      this.logOut();
    }

    // if(this.productAccessModel.powerGaugeAnalytics && !this.isMobile){
    //   this.showWelcomePopUp = this.userDetails.enablePowerGaugeAnalyticsWelcomeGuide;
    //   this.welcomePopUpCheckBoxValue =  this.showWelcomePopUp;
    // }else{
    //   this.showWelcomePopUp = false;
    // }

    if(this.userAccess.userPermission.welcomePopup){
      this.showWelcomePopUp = this.userDetails.enablePowerGaugeAnalyticsWelcomeGuide;
      this.welcomePopUpCheckBoxValue =  this.showWelcomePopUp;
    }else{
      this.showWelcomePopUp = false;
    }

    // this.setProductName();
    this.dateForDashboard = moment()
      .tz('America/New_York')
      .format('MM/DD/YY HH:mmA z');

    if (LocalStorage.getItem('listId') === -1 || LocalStorage.getItem('listId') === undefined || !LocalStorage.getItem('listId')) {
      this.userDetails = LocalStorage.getItem('userDetail');
      this.selectedListId = this.userDetails.ListID;
    } else {
      this.selectedListId = LocalStorage.getItem('listId');
    }
    // this.refereshLastPrice();
    this.loadInitialState(this.selectedListId);
    //  this.init();
  }

  ngOnChanges(): void {
    if (LocalStorage.getItem('listId') === -1 || LocalStorage.getItem('listId') === undefined) {
      this.userDetails = LocalStorage.getItem('userDetail');
      this.selectedListId = this.userDetails.ListID;
    } else {
      this.selectedListId = LocalStorage.getItem('listId');
    }
    this.loadInitialState(this.selectedListId);
    // this.init();

  }

  constructor(
    private api: APIService,
    public dialog: MatDialog,
    private cookieService: CookieService,
    private dataService: DataService, public router: Router,
    private route: ActivatedRoute,
    public toastService: ToastService,
    public appDataService: AppDataService,
    public auth: AuthService,
    public viewportScroller: ViewportScroller,
    private detectorRef: ChangeDetectorRef,

  ) {
    if (!this.userDetails) {
      this.userDetails = this.getUserDetail();
    }
  }

  ngOnDestroy() {
    //	clearInterval(this.refereshLastPriceDataOnIntervel);
  }

  getUserDetail(): LoginDetailModel {
    const userDetail: LoginDetailModel = LocalStorage.getItem('userDetail');
    return userDetail;
  }
  getSymbolData(): void {
    let symbol = LocalStorage.getItem('mainSymbol');
    this.recentlyReviewSymbol(symbol);
  }

  getAlertData(alertData): void {
    this.alertComponentData = alertData;
    //this.alertModel = alertData.
  }

  recentlyReviewSymbol(symbol): void {
    this.route.queryParams.subscribe(params => {
      if (params["data"]) {
        const data = JSON.parse(params["data"]);
        const navigationExtras = RedirectParameter.getNavigationExtrasDataForResearch(symbol, null, null, null);
        this.dataService.recentlyViewedData.setSymbol(data.symbol);
        this.router.navigate(["pgr/#/"], navigationExtras);
      }
    });
  }

  getWatchListUpdate(event) {
    this.selectedListId = event;
    this.loadInitialState(this.selectedListId);
    // this.init();
  }

  resetSearchSymbol(): void {
    this.symbolLookupData = [];
    this.etfSymbolLookupData = [];
    this.StockSymbolLookupData = [];
  }

  onSymbolSelect(data: SymbolLookUp): void {
    LocalStorage.setItem('mainSymbol', data.Symbol);
    let priceMoveData = this.listSymbolsPriceForSelectedTimeFrameArray;
    let idx = priceMoveData.findIndex((item) => (item.symbol).toLowerCase() === data.Symbol.toLowerCase());
    if (idx < 0) {
      LocalStorage.setItem('listId', -1);
    } else {
      LocalStorage.setItem('listId', this.selectedListId);
    }

    this.dataService.recentlyViewedData.setSymbol(data.Symbol);
    //this.init();
    this.resetSearchSymbol();
    this.redirectOnPgr(data.Symbol);
    //  window.location.replace(window.location.protocol + "//" + window.location.host + "/pgr/#/");

  }

  loadInitialState(listId): void {
    //  this.dashboardPriceMoment('1day', listId);
    this.updateSentToAlerts = "update";
    this.updateSentToEarnings = "update";
    this.updateSentToPriceMovement = "update"
  //  this.sentUpdateToIndustryExposure = "update";
    // this.getAllAlerts(listId);
    this.getSymbolData();
    // if (this.productAccessModel.powerGaugeAnalytics) {
    //   this.earningsGlanceData();
    // }
    if (this.userAccess.userPermission.earning) {
      this.earningsGlanceData();
    }
  }

  updateSendToDashboard1(value) {
    this.updateSentToAlerts = value;
    this.updateSentToEarnings = value;
    this.sentUpdateToIndustryExposure = value;
  }


  openLogoutPopup(e: any) {
    this.productPopUpShow = !this.productPopUpShow
    e.stopPropagation();
    //this.userId = localStorage.getItem('email');
    if (!this.documentClickEventRegister) {
      document.addEventListener('click', this.offClickHandler.bind(this));
      this.documentClickEventRegister = true;
      this.onClickLogoutOpenPopup(e);
      this.openLogoutPopupForAccount();
    }
  }

  onClickLogoutOpenPopup(event: any) {
    this.createLogoutXPosition = event.clientX - 150;
    this.createLogoutYPosition = event.clientY + 20;
  }

  openLogoutPopupForAccount() {
    this.showLogoutPopup = true;
  }

  onCloseLogOutPopupEvent() {
    this.showLogoutPopup = false;
  }

  offClickHandler(event: any) {
    if (
      this.logoutPopup &&
      this.logoutPopup.nativeElement &&
      !this.logoutPopup.nativeElement.contains(event.target)
    ) {
      // check click origin
      this.showLogoutPopup = false;
      if (this.documentClickEventRegister) {
        document.removeEventListener('click', this.offClickHandler.bind(this));
        this.documentClickEventRegister = false;
      }
    }
  }

  updateProductPreferanceValue(data, value): void {
    let object = this.productsList;
    for (const key in object) {
      if (Object.prototype.hasOwnProperty.call(object, key)) {
        const element = object[key];
        if (element.name == data.name) {
          element.productPreference = true
        } else {
          element.productPreference = false;
        }
      }
    }
    if (value === true) {
      this.api.updateProductPreferance(data.name, this.userDetails.UID).subscribe((response) => {
        // console.log(response);
      });
    }
    if (value === false) {
      this.api.updateProductPreferance('NA', this.userDetails.UID).subscribe((response) => {
        //  console.log(response);
      });
    }
  }

  
  productAccessCount(productAccessCount): void {
    this.productsAccessCount = productAccessCount;
    let isPgrAccess = false;
    this.productsAccessCount.forEach(element => {
      if (element === "Power Pulse Premium") {
        isPgrAccess = true;
      }
    });
    if (!isPgrAccess) {
      this.logOut();
    }
  }

  logOut(): void {
    this.auth.killsessions(this.appDataService.cookiesData.email).subscribe((resp) => {
      if (resp["status"] === "success") {
        this.appDataService.removeAllData();
        this.cookieService.deleteAll('/', '.chaikinanalytics.com');
        window.location.replace(window.location.protocol + "//" + window.location.host + "/login/#/logout");
        // window.location.replace(window.location.protocol + "//" + window.location.host + "/login/");
      }
    });
  }
  oldSelectedListName :string;
  getSelectedList(data) {
    if (data.listId ) {
    //  console.log(data)
    this.sendSelectedListData =data;
      this.selectedListId = data.listId;
      if(this.oldSelectedListName !== data.listName){
      
        this.selectedListName = data.listName;
        this.oldSelectedListName  = this.selectedListName;
      }
    }
    this.updateSentToEarnings = data.symbolCount;
    this.updateSentToAlerts = data.symbolCount;
    this.sentUpdateToIndustryExposure = data.symbolCount;
    this.updateSentToPriceMovement = data.symbolCount;
    // if (this.productAccessModel.powerGaugeAnalytics) {
    //   this.earningsGlanceData();
    // }
    if (this.userAccess.userPermission.earning) {
      this.earningsGlanceData();
    }
  }


  redirectOnPgr(symbol): void {
    LocalStorage.setItem('mainSymbol', symbol);
    this.router.navigate(["/"]
      // window.location.replace(
      //   window.location.protocol +
      //   '//' +
      //   window.location.host +
      //   '/pgr/'
    );
  }

  earningsGlanceData(): void {
    this.api.getEarningsGlanceData(this.selectedListId).subscribe((resp) => {
      if (resp['status'] === 'true') {
        this.earningsGlanceObject.nextWeekEarningCount = resp.nextWeekEarningCount ? resp.nextWeekEarningCount : 0;
        this.earningsGlanceObject.thisWeekEarningCount = resp.thisWeekEarningCount ? resp.thisWeekEarningCount : 0;
        this.earningsGlanceObject.todayEarningCount = resp.todayEarningCount ? resp.todayEarningCount : 0;
      }
    });
  }

  sentToEarningCalnder(value): void {
      document.getElementById("earningCalender").scrollIntoView({
        behavior: "smooth",
        block: "start",
        inline: "nearest"
       });
      this.sentToEarningCalendar = value;
  }





 // ALert API
//  deleteForEnabledAlertList(): void {
//   this.api.deleteForEnabledPowerPulseAlertList(this.selectedListId, this.userDetails.UID).subscribe(resp => {
//     //  console.log(resp);
//   });
// }
// addListForEnableAlert(value): void {
//   this.saveUsername = value;
//   if (this.saveUsername) {
//     this.api.addListForEnablePowerPulseAlert(this.selectedListId).subscribe(resp => {
//       //   console.log(resp);
//     })
//   } else {
//     this.deleteForEnabledAlertList();
//   }
// }
  // private refereshLastPrice() {
  //   clearInterval(this.refereshLastPriceDataOnIntervel);
  //   this.refereshLastPriceDataOnIntervel = setInterval(async () => {
  //   //  this.dashboardPriceMoment(this.selectedTimeFrame, this.selectedListId);
  //  //  this.priceMovementSorting(this.selectedTimeFrame , this.selectedListId);
  //   }, 72000);
  // }

  // setProductName() {

  //   let keyValueOfProducts = [];
  //   let i = 0;
  //   for (let key in this.productsList) {
  //     keyValueOfProducts.push({ "key": key, "value": this.productsList[key] });
  //     i++
  //   }
  //   this.productDataValue = keyValueOfProducts;
  // //  console.log(this.productDataValue);
  // }

  // productData(data): void {
  //   this.productsList = data;
  //   this.setProductName();
  // }


  // init(): void {
  // this.getSymbolData();
  // this.getEnabledAlertListId();
  // if(this.productAccessModel.powerGaugeAnalytics){
  // this.earningsGlanceData();
  // }

  // }

  // dashboardPriceMoment(timeFrame, listId): void {
  //   this.selectedTimeFrame = timeFrame;
  //   this.dateForDashboard = moment()
  //   .tz('America/New_York')
  //   .format('MM/DD/YY HH:mmA z');
  //   this.listSymbolsPriceForSelectedTimeFrameArray = [];
  //   this.api.getListSymbolsPriceForSelectedTimeFrame(timeFrame, listId, this.userDetails.UID).subscribe((resp) => {


  //     resp.data.forEach(element => {
  //       const listSymbolsPriceForSelectedTimeFrame = new ListSymbolsPriceForSelectedTimeFrameModel();
  //       listSymbolsPriceForSelectedTimeFrame.change = element.change;
  //       listSymbolsPriceForSelectedTimeFrame.corrected_pgr = element.corrected_pgr;
  //       listSymbolsPriceForSelectedTimeFrame.is_etf = element.is_etf;
  //       listSymbolsPriceForSelectedTimeFrame.last = element.last;
  //       listSymbolsPriceForSelectedTimeFrame.percent_change = element.percent_change;
  //       listSymbolsPriceForSelectedTimeFrame.raw_pgr = element.raw_pgr;
  //       listSymbolsPriceForSelectedTimeFrame.symbol = element.symbol;
  //       listSymbolsPriceForSelectedTimeFrame.companyName = element.company_name;

  //       // return this.formatAlertData('Signals')
  //       // .concat(this.formatAlertData('Earnings'))
  //       // .concat(this.formatAlertData('Rating'));
  //       for (let alert of this.alertModel.data) {
  //         // this.alertsData.forEach(alert =>{

  //         // tslint:disable-next-line: max-line-length
  //         if (alert.symbol === element.symbol && (alert.alertName === 'Rating Change' || alert.alertInfo === 'Estimate Revision' || alert.alertInfo === 'Earnings Surprise') && (alert.value > 3 || alert.value < 3)) {
  //           listSymbolsPriceForSelectedTimeFrame.alertTrue = true;
  //           break;

  //           // console.log(listSymbolsPriceForSelectedTimeFrame);
  //         } else {
  //           listSymbolsPriceForSelectedTimeFrame.alertTrue = false;
  //           // console.log(listSymbolsPriceForSelectedTimeFrame);
  //         }
  //       }

  //       if (element.percent_change.toString().split('')[0] === '-') {
  //         listSymbolsPriceForSelectedTimeFrame.sliderValue = false;
  //       } else {
  //         listSymbolsPriceForSelectedTimeFrame.sliderValue = true;
  //       }

  //       listSymbolsPriceForSelectedTimeFrame.finalRating = SharedUtil.getPgrRating(
  //         element.corrected_pgr,
  //         element.raw_pgr
  //       );
  //       listSymbolsPriceForSelectedTimeFrame.selectedTimeFrame = timeFrame;
  //       listSymbolsPriceForSelectedTimeFrame.modifiedChangeValue = SharedUtil.formatChangeDashWat(element.percent_change)
  //       listSymbolsPriceForSelectedTimeFrame.calculateWidth = SharedUtil.calculateWidth(element.percent_change, resp.data);
  //       listSymbolsPriceForSelectedTimeFrame.imgSrc =
  //         SharedUtil.getPgrArcWithoutNeutralValue(element.corrected_pgr, element.raw_pgr, element.is_etf);
  //       this.listSymbolsPriceForSelectedTimeFrameArray.push(listSymbolsPriceForSelectedTimeFrame);
  //     });
  //     this.priceMovementSortingModel.sortingOrder = AppConst.CONST.sortOrderAsc;
  //     this.priceMovementSortingModel.sortingOrderPgr = AppConst.CONST.sortOrderAsc;
  //     this.priceMovementSortingModel.sortingOrderSymbols = AppConst.CONST.sortOrderDesc;
  //     this.listSymbolsPriceForSelectedTimeFrameArray = this.priceMovementSorting('percent_change', 'symbol');
  //   });

  //  // this.appDataService.dashboardData.priceMovementComponentData = this.listSymbolsPriceForSelectedTimeFrameArray;
  // }

  // selectTimeFrame(timeFrame): void {
  //   this.dashboardPriceMoment(timeFrame, this.selectedListId);
  // }


  // priceMovementSorting(sortingKey: string, extendKey: string) {

  //   // let sortingOrder: string;
  //   // let sortingOrderExtend: string;
  //   // if(this.priceMovementSortingModel.isDefaultSorting){
  //   //     this.priceMovementSortingModel.sortingOrder = AppConst.CONST.sortOrderAsc;
  //   //     this.priceMovementSortingModel.isDefaultSorting = false;
  //   // }else{
  //   if (sortingKey == 'percent_change') {
  //     if (this.priceMovementSortingModel.sortingOrder === AppConst.CONST.sortOrderAsc) {
  //       this.priceMovementSortingModel.sortingOrder = AppConst.CONST.sortOrderDesc;
  //     } else {
  //       this.priceMovementSortingModel.sortingOrder = AppConst.CONST.sortOrderAsc;
  //     }
  //     this.priceMovementSortingModel.finalSorting = this.priceMovementSortingModel.sortingOrder;
  //   } else if (sortingKey == 'finalRating') {
  //     if (this.priceMovementSortingModel.sortingOrderPgr === AppConst.CONST.sortOrderAsc) {
  //       this.priceMovementSortingModel.sortingOrderPgr = AppConst.CONST.sortOrderDesc;
  //     } else {
  //       this.priceMovementSortingModel.sortingOrderPgr = AppConst.CONST.sortOrderAsc;
  //     }
  //     this.priceMovementSortingModel.finalSorting = this.priceMovementSortingModel.sortingOrderPgr;
  //   } else if (sortingKey == 'symbol') {
  //     if (this.priceMovementSortingModel.sortingOrderSymbols === AppConst.CONST.sortOrderAsc) {
  //       this.priceMovementSortingModel.sortingOrderSymbols = AppConst.CONST.sortOrderDesc;

  //     } else {
  //       this.priceMovementSortingModel.sortingOrderSymbols = AppConst.CONST.sortOrderAsc;
  //     }
  //     this.priceMovementSortingModel.finalSorting = this.priceMovementSortingModel.sortingOrderSymbols;
  //   }

  //   //  }
  //   // if (sortingKey == 'symbol') {
  //   //     sortingOrder = AppConst.CONST.sortOrderAsc;
  //   //     sortingOrderExtend = AppConst.CONST.sortOrderDesc;
  //   // } else if (extendKey == 'symbol') {
  //   //     sortingOrder = AppConst.CONST.sortOrderDesc;
  //   //     sortingOrderExtend = AppConst.CONST.sortOrderAsc;
  //   // }

  //   const sortingParams = this.setPriceMovementSorting(this.listSymbolsPriceForSelectedTimeFrameArray, [
  //     {
  //       sortingKey: sortingKey,
  //       sortingOrder: this.priceMovementSortingModel.finalSorting,
  //       ignoreKey: [-1],
  //     },
  //     {
  //       sortingKey: extendKey,
  //       sortingOrder: AppConst.CONST.sortOrderAsc,
  //       ignoreKey: [],
  //     },
  //   ]);
  //   this.listSymbolsPriceForSelectedTimeFrameArray = Sort.sortUnique(sortingParams);
  //   return Sort.sortUnique(sortingParams);
  // }

  // setPriceMovementSorting(data: Array<any>, sortObj: SortObjectModel[]): SortingModel {
  //   const len = sortObj.length - 1;
  //   const sortingModelData = new SortingModel();
  //   const sortLevelKey = [];

  //   sortObj.forEach((element, index) => {
  //     let sorting: SortingModel;
  //     if (index === 0) {
  //       sorting = sortingModelData;
  //       sorting.sortData = data;
  //     } else {
  //       sortingModelData.extendedData = new SortingModel();
  //       sorting = sortingModelData.extendedData;
  //       sorting.sortData = [];
  //     }

  //     sorting.sortKey = element.sortingKey;
  //     sorting.sortOrder = element.sortingOrder;
  //     sorting.ignoreDataKeys = element.ignoreKey;
  //     sorting.sortLevelKey = _.cloneDeep(sortLevelKey);
  //     sorting.ignoreData = [];
  //     sorting.isExtended = index === len ? false : true;

  //     sortLevelKey.push(element.sortingKey);
  //   });

  //   return sortingModelData;
  // }
  // removeList(index) {

  //   const httpDeleteSymbolFromList$ = this.api.deleteSymbolFromList(
  //     index.symbol,this.userDetails.UID
  //   );

  //   httpDeleteSymbolFromList$
  //     .pipe(
  //       map((resp: { Status: boolean }) => {
  //         return resp.Status;
  //       })
  //     )
  //     .subscribe((status: boolean) => {

  //       if (status) {
  //         this.toastService.showSuccess('Successfully removed');
  //         this.dashboardPriceMoment('1day', this.selectedListId);
  //         this.updateSentToAlerts = "remove"
  //         this.updateSentToEarnings = "remove"
  //       //  this.getAllAlerts(this.selectedListId);
  //         this.dataService.setProjectAccess(true);
  //       }
  //     });

  // }

  // Alert 





  // getAllAlerts(listId): void {
  //   if (!listId) {
  //     listId = this.userDetails.ListID;
  //   }
  //   let alertComponentData = new AlertComponentData();
  //   // const listId = this.userDetails.ListID;
  //   const httpAllAlerts$ = this.api.getAllAlerts(listId, this.userDetails.UID);
  //   const httpSignalDataForList = this.api.getSignalDataForList(listId,this.userDetails.UID);
  //   forkJoin([httpAllAlerts$, httpSignalDataForList]).subscribe((resp) => {
  //     // console.log(resp);
  //     const apiAlertData = resp[0] as AlertsData;
  //     const apiSignalAlertData = resp[1];

  //     const signalAlert = this.prepareSignalAlertData(apiSignalAlertData);
  //     const otherAlert = this.prepareAlertData(apiAlertData);
  //     this.alertsData = signalAlert.concat(otherAlert);
  //     //  console.log(this.alertsData);
  //     this.alertModel.data = this.alertsData;
  //     let bullishCount = 0;
  //     let bearishCount = 0;

  //     bullishCount = this.alertModel.data
  //       // tslint:disable-next-line: max-line-length
  //       .filter((item) => (item.value > 3 && (item.alertName === 'Rating Change' || item.alertInfo === 'Estimate Revision' || item.alertInfo === 'Earnings Surprise')))
  //       .map((item) => item.symbol)
  //        .filter((value, index, self) => self.indexOf(value) === index).length;

  //     bearishCount = this.alertModel.data
  //       // tslint:disable-next-line: max-line-length
  //       .filter((item) => (item.value < 3 && (item.alertName === 'Rating Change' || item.alertInfo === 'Estimate Revision' || item.alertInfo === 'Earnings Surprise')))
  //       .map((item) => item.symbol)
  //      .filter((value, index, self) => self.indexOf(value) === index).length;
  //     this.alertModel.count.bullish = bullishCount;
  //     this.alertModel.count.bearish = bearishCount;
  //     this.onAlertFilterType(this.selectedAlertFilter);
  //     alertComponentData.alertModel =  this.alertModel
  //     alertComponentData.alertDataModel = this.alertsData;
  //     alertComponentData.totalAlertCount = bullishCount + bearishCount
  //     // this.appDataService.dashboardData.alertComponentData.alertDataModel = this.alertsData;
  //     // this.appDataService.dashboardData.alertComponentData.alertModel =  this.alertModel;
  //     // this.appDataService.dashboardData.alertComponentData.totalAlertCount = bullishCount + bearishCount
  //     this.alertComponentData1 = alertComponentData;
  //     // if(alertComponentData){
  //     //   this.appDataService.dashboardData.alertComponentData = alertComponentData;
  //     // }

  //     this.dashboardPriceMoment('1day', listId);
  //   });

  // }

  // onAlertFilterType(filterType: string): void {
  //   this.selectedAlertFilter = filterType;
  //   switch (filterType) {
  //     case 'All':
  //       this.alertsData = this.formatAlertData(filterType);

  //       this.alertsData = this.alertSorting(this.alertsData, 'symbol', 'date');

  //       break;
  //     case 'Signals':
  //       this.alertsData = this.formatAlertData(filterType);
  //       break;
  //     case 'Earnings':
  //       this.alertsData = this.formatAlertData(filterType);
  //       break;
  //     case 'Rating':
  //       this.alertsData = this.formatAlertData(filterType);
  //       break;
  //     default:
  //       this.alertsData = this.formatAlertData('');
  //   }
  // }


  // prepareSignalAlertData(signalData): AlertDataModel[] {
  //   const modifiedAlertData: AlertDataModel[] = [];
  //   let formattedDate = '';
  //   const signalDataModel = new SignalDataModel();
  //   signalData.forEach((element) => {
  //     signalDataModel.signal = this.convertToArray(element.Signals);
  //     signalDataModel.close = this.convertToArray(element.Close);
  //     signalDataModel.signalDate = this.convertToArray(element.SignalDate);
  //     signalDataModel.symbol = element.Symbol;

  //     signalDataModel.signal.forEach((signal, i) => {
  //       const parsedSignals = Signal.parseSignal(signal);

  //       parsedSignals.sellSignalNames.forEach((data) => {
  //         formattedDate = this.formatDate(signalDataModel.signalDate[i]);

  //         const alertDataModel = new AlertDataModel();
  //         alertDataModel.date = signalDataModel.signalDate[i];
  //         alertDataModel.formattedDate = formattedDate;
  //         alertDataModel.alertName = 'Sell Signal';
  //         alertDataModel.alertInfo = AppConst.sellSignalInfo[data].name;
  //         alertDataModel.isEtf = undefined;
  //         alertDataModel.pgr = undefined;
  //         alertDataModel.rawPGR = undefined;
  //         alertDataModel.symbol = signalDataModel.symbol;
  //         alertDataModel.value = 2;
  //         alertDataModel.oldValue = undefined;
  //         alertDataModel.newValue = undefined;
  //         alertDataModel.actualEps = undefined;
  //         alertDataModel.estimateEps = undefined;
  //         alertDataModel.changeInPer = undefined;
  //         alertDataModel.classForPerChange = undefined;
  //         alertDataModel.changeInPerFormatted = undefined;
  //         alertDataModel.close = signalDataModel.close[i];
  //         alertDataModel.rank = AppConst.sellSignalInfo[data].rank;
  //         modifiedAlertData.push(alertDataModel);
  //       });

  //       parsedSignals.buySignalNames.forEach((data) => {
  //         formattedDate = this.formatDate(signalDataModel.signalDate[i]);

  //         const alertDataModel = new AlertDataModel();
  //         alertDataModel.date = signalDataModel.signalDate[i];
  //         alertDataModel.formattedDate = formattedDate;
  //         alertDataModel.alertName = 'Buy Signal';
  //         alertDataModel.alertInfo = AppConst.buySignalInfo[data].name;
  //         alertDataModel.isEtf = undefined;
  //         alertDataModel.pgr = undefined;
  //         alertDataModel.rawPGR = undefined;
  //         alertDataModel.symbol = signalDataModel.symbol;
  //         alertDataModel.value = 4;
  //         alertDataModel.oldValue = undefined;
  //         alertDataModel.newValue = undefined;
  //         alertDataModel.actualEps = undefined;
  //         alertDataModel.estimateEps = undefined;
  //         alertDataModel.changeInPer = undefined;
  //         alertDataModel.classForPerChange = undefined;
  //         alertDataModel.changeInPerFormatted = undefined;
  //         alertDataModel.close = signalDataModel.close[i];
  //         alertDataModel.rank = AppConst.buySignalInfo[data].rank;
  //         modifiedAlertData.push(alertDataModel);
  //       });
  //     });
  //   });
  //   return modifiedAlertData;
  // }

  // prepareAlertData(data): AlertDataModel[] {
  //   const modifiedAlertData: AlertDataModel[] = [];
  //   for (const key in data) {
  //     if (Object.prototype.hasOwnProperty.call(data, key)) {
  //       const element = data[key];
  //       element.forEach((alertData) => {
  //         const alertDate = alertData.AlertDate;
  //         const formattedDate = this.formatDate(alertDate);
  //         let alertInfo = '';
  //         let alertName = '';
  //         let changeInPer: number;
  //         let changeInPerFormatted = '';
  //         let actualEps = '';
  //         let estimateEps = '';
  //         let oldValue = '';
  //         let newValue = '';
  //         let pgr: number;
  //         if (key === 'CTI') {
  //           alertInfo = alertData.Category;
  //         } else {
  //           alertInfo = alertData.Text;
  //         }
  //         alertInfo = alertInfo.trim();
  //         if (alertInfo === 'Earnings Surprise') {
  //           alertName = alertInfo;
  //           changeInPer = +alertData.PercentageSurprise;
  //           if (alertData.Value > 3) {
  //             changeInPerFormatted =
  //               alertData.PercentageSurprise.indexOf('+') === -1
  //                 ? '+' + alertData.PercentageSurprise
  //                 : alertData.PercentageSurprise;
  //           } else if (alertData.Value < 3) {
  //             changeInPerFormatted =
  //               alertData.PercentageSurprise.indexOf('-') === -1
  //                 ? '-' + alertData.PercentageSurprise
  //                 : alertData.PercentageSurprise;
  //           }
  //           changeInPerFormatted = SharedUtil.appendSymbol(
  //             changeInPerFormatted,
  //             '%'
  //           );
  //           changeInPerFormatted = SharedUtil.appendSymbol(
  //             changeInPerFormatted,
  //             '()'
  //           );
  //           actualEps = alertData.ActualEPS;
  //           estimateEps = alertData.ConsensusEstimate;
  //           pgr = alertData.pgrRating;
  //         } else if (alertInfo === 'Estimate Revision') {
  //           alertName = alertInfo;
  //           changeInPer = +alertData.ESTPercentageChange;
  //           if (alertData.Value > 3) {
  //             changeInPerFormatted =
  //               alertData.ESTPercentageChange.indexOf('+') === -1
  //                 ? '+' + alertData.ESTPercentageChange
  //                 : alertData.ESTPercentageChange;
  //           } else if (alertData.Value < 3) {
  //             changeInPerFormatted =
  //               alertData.ESTPercentageChange.indexOf('-') === -1
  //                 ? '-' + alertData.ESTPercentageChange
  //                 : alertData.ESTPercentageChange;
  //           }
  //           changeInPerFormatted = SharedUtil.appendSymbol(
  //             changeInPerFormatted,
  //             '%'
  //           );
  //           changeInPerFormatted = SharedUtil.appendSymbol(
  //             changeInPerFormatted,
  //             '()'
  //           );
  //           oldValue = alertData.MeanESTPreviousDay;
  //           newValue = alertData.MeanESTCurrentDay;
  //           pgr = alertData.pgrRating;
  //         } else if (key === 'PGR') {
  //           alertInfo = alertData.Text;
  //           alertName = 'Rating Change';
  //           pgr = alertData.Value;
  //         }

  //         pgr = !pgr ? 0 : alertData.pgrRating;

  //         const alertDataModel = new AlertDataModel();
  //         alertDataModel.date = alertDate;
  //         alertDataModel.formattedDate = formattedDate;
  //         alertDataModel.alertName = alertName;
  //         alertDataModel.alertInfo = alertInfo;
  //         alertDataModel.isEtf = alertData.is_etf;
  //         alertDataModel.pgr = pgr;
  //         alertDataModel.rawPGR = alertData.rawPgrRating;
  //         alertDataModel.symbol = alertData.Symbol;
  //         alertDataModel.value = alertData.Value;
  //         alertDataModel.oldValue = oldValue;
  //         alertDataModel.newValue = newValue;
  //         alertDataModel.actualEps = actualEps;
  //         alertDataModel.estimateEps = estimateEps;
  //         alertDataModel.changeInPer = changeInPer;
  //         alertDataModel.classForPerChange =
  //           changeInPer < 0 ? 'negative' : 'positive';
  //         alertDataModel.changeInPerFormatted = changeInPerFormatted;
  //         alertDataModel.close = undefined;
  //         alertDataModel.rank = undefined;
  //         modifiedAlertData.push(alertDataModel);
  //       });
  //     }
  //   }
  //   return modifiedAlertData;
  // }

  // formatAlertData(type: string): any[] {
  //   let totalAlerts: AlertDataModel[] = [];
  //   const data = this.alertModel.data;
  //   switch (type) {
  //     case 'All':
  //       totalAlerts = data.filter((alert) => {
  //         if (this.isBullishSelected && alert.value > 3) {
  //           return alert;
  //         } else if (!this.isBullishSelected && alert.value < 3) {
  //           return alert;
  //         }
  //       });
  //       totalAlerts = this.concatData();
  //       totalAlerts = this.alertSorting(totalAlerts, 'symbol', 'date');
  //       break;
  //     case 'Signals':
  //       totalAlerts = data.filter((alert) => {
  //         if (
  //           alert.alertName === 'Buy Signal' ||
  //           alert.alertName === 'Sell Signal'
  //         ) {
  //           if (this.isBullishSelected && alert.value > 3) {
  //             return alert;
  //           } else if (!this.isBullishSelected && alert.value < 3) {
  //             return alert;
  //           }
  //         }
  //       });
  //       //  totalAlerts = this.alertSorting(totalAlerts, 'date', 'symbol');
  //       break;
  //     case 'Earnings':
  //       totalAlerts = data.filter((alert) => {
  //         if (
  //           alert.alertName === 'Earnings Surprise' ||
  //           alert.alertName === 'Estimate Revision'
  //         ) {
  //           if (this.isBullishSelected && alert.value > 3) {
  //             return alert;
  //           } else if (!this.isBullishSelected && alert.value < 3) {
  //             return alert;
  //           }
  //         }
  //       });
  //       //  totalAlerts = this.alertSorting(totalAlerts, 'symbol', 'date');
  //       break;
  //     case 'Rating':
  //       totalAlerts = data.filter((alert) => {
  //         if (alert.alertName === 'Rating Change') {
  //           if (this.isBullishSelected && alert.value > 3) {
  //             return alert;
  //           } else if (!this.isBullishSelected && alert.value < 3) {
  //             return alert;
  //           }
  //         }
  //       });
  //       //  totalAlerts = this.alertSorting(totalAlerts, 'symbol', 'date');
  //       break;
  //     default:
  //   }

  //   return totalAlerts;
  // }
  // selectAlertType(selectType: any): void {

  //   const type = selectType;
  //   switch (type) {
  //     case 'bullishAlerts':
  //       this.isBullishSelected = true;
  //       this.onAlertFilterType(this.selectedAlertFilter);
  //       break;
  //     case 'bearishAlerts':
  //       this.isBullishSelected = false;
  //       this.onAlertFilterType(this.selectedAlertFilter);
  //       break;
  //     default:
  //       break;
  //   }
  // }

  // formatDate(stringDate: string): string {
  //   const months = [
  //     'JAN',
  //     'FEB',
  //     'MAR',
  //     'APR',
  //     'MAY',
  //     'JUN',
  //     'JUL',
  //     'AUG',
  //     'SEP',
  //     'OCT',
  //     'NOV',
  //     'DEC',
  //   ];
  //   const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  //   const currentDateTime = new Date(stringDate);
  //   const formattedDate =
  //     days[moment(stringDate).format('d')] +
  //     '. ' +
  //     months[((+moment(stringDate).format('M')) - 1)] +
  //     ' ' +
  //     moment(stringDate).format('D') +
  //     ', ' +
  //     moment(stringDate).format('Y').toString();
  //   return formattedDate;
  // }

  // convertToArray(str): any {
  //   const a = str.split(',');
  //   for (let i = 0; i < a.length; i++) {
  //     a[i] = a[i].replace(/\[/g, '').replace(/\]/g, '').replace(/ /g, '');
  //   }
  //   return a;
  // }



  // alertSorting(sectorData, sortingKey: string, extendKey: string) {
  //   let sortingOrder: string;
  //   let sortingOrderExtend: string;
  //   if (sortingKey == 'date') {
  //     sortingOrder = AppConst.CONST.sortOrderAsc;
  //     sortingOrderExtend = AppConst.CONST.sortOrderDesc;
  //   } else if (extendKey == 'date') {
  //     sortingOrder = AppConst.CONST.sortOrderDesc;
  //     sortingOrderExtend = AppConst.CONST.sortOrderAsc;
  //   }

  //   const sortingParams = this.setAlertSorting(sectorData, [
  //     {
  //       sortingKey: sortingKey,
  //       sortingOrder: AppConst.CONST.sortOrderAsc,
  //       ignoreKey: [-1],
  //     },
  //     {
  //       sortingKey: extendKey,
  //       sortingOrder: AppConst.CONST.sortOrderAsc,
  //       ignoreKey: [],
  //     },
  //   ]);
  //   return Sort.sortUnique(sortingParams);
  // }

  // setAlertSorting(data: Array<any>, sortObj: SortObjectModel[]): SortingModel {
  //   const len = sortObj.length - 1;
  //   const sortingModelData = new SortingModel();
  //   const sortLevelKey = [];

  //   sortObj.forEach((element, index) => {
  //     let sorting: SortingModel;
  //     if (index === 0) {
  //       sorting = sortingModelData;
  //       sorting.sortData = data;
  //     } else {
  //       sortingModelData.extendedData = new SortingModel();
  //       sorting = sortingModelData.extendedData;
  //       sorting.sortData = [];
  //     }

  //     sorting.sortKey = element.sortingKey;
  //     sorting.sortOrder = element.sortingOrder;
  //     sorting.ignoreDataKeys = element.ignoreKey;
  //     sorting.sortLevelKey = _.cloneDeep(sortLevelKey);
  //     sorting.ignoreData = [];
  //     sorting.isExtended = index === len ? false : true;

  //     sortLevelKey.push(element.sortingKey);
  //   });

  //   return sortingModelData;
  // }

  // Login Work

  
  setWelcomePopUpChecboxValue(showMeWelcomePopUp): void{
       this.welcomePopUpCheckBoxValue = showMeWelcomePopUp;
       this.api.disablePowerGaugeAnalyticsWelcomeGuide(this.userDetails.UID,showMeWelcomePopUp).subscribe(response => {
      })

    }

    hideWelcomePopUp():void{
     this.showWelcomePopUp = true;
    }

    showWelcomePopUpForPGA(event):void {
      this.showWelcomePopUp = !event;
    }

    getStrengthCountIndustryExposure(strengthCount: StrengthCountObject): void {
      let strong = strengthCount.strong;
      let weak =  strengthCount.weak;
      this.strengthCountObjectData.neutral = strengthCount.neutral;
      this.strengthCountObjectData.strong = strengthCount.strong;
      this.strengthCountObjectData.weak =  strengthCount.weak;
      if (strong > weak){
      this.strengthCountObjectData.strongSliderWidth = 100;
      // tslint:disable-next-line: max-line-length
      this.strengthCountObjectData.weakSliderWidth = Math.abs(Math.abs(weak) / (strong)) * 100;
    }else if (strong < weak){
      this.strengthCountObjectData.weakSliderWidth = 100;
      // tslint:disable-next-line: max-line-length
      this.strengthCountObjectData.strongSliderWidth = Math.abs(Math.abs(strong) / (weak)) * 100;
    }else if ( (strong === weak) && strong){
      this.strengthCountObjectData.weakSliderWidth = 100;
      this.strengthCountObjectData.strongSliderWidth = 100;
    } else if ((strong === weak) && !strong) {
      this.strengthCountObjectData.weakSliderWidth = 1;
      this.strengthCountObjectData.strongSliderWidth = 1;
    }
    // tslint:disable-next-line: max-line-length
   // this.strengthCountObjectData.strongSliderWidth = Math.abs(Math.abs(strengthCount.strong) / (strengthCount.strong + strengthCount.weak)) * 100;
    // tslint:disable-next-line: max-line-length
   // this.strengthCountObjectData.weakSliderWidth = Math.abs(Math.abs(strengthCount.weak) / (strengthCount.strong + strengthCount.weak)) * 100;
    }

   setScrollOnJump(value): void {
      if (value === 'IndustryExposure'){
        document.getElementById("IndustryExposure").scrollIntoView({
          behavior: "smooth",
          block: "start",
          inline: "nearest"
         });
      }
      if (value === 'EarningsCalendar'){
        document.getElementById("earningCalender").scrollIntoView({
          behavior: "smooth",
          block: "start",
          inline: "nearest"
         });
      }
    }

    getEmailAlertModeValue(event): void {
      this.emailAlertModeValue = event;
    }
    getEmailAlertModeValueFromHeader(event): void {
      this.emailAlertModeValueFromHeader = event;
    }

    getEarningCalenderDataLength(data):void{
      this.earningCalenderDataCount = data;
      this.detectorRef.detectChanges();

    }
  }