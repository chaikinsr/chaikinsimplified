import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSortModule } from '@angular/material/sort';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTableModule } from '@angular/material/table';

import { SharedModule } from '../shared/shared.module';
import { AppletHeaderModule } from 'src/app/layout/header/applet-header.module';

import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { AlertsPanelComponent } from './components/alerts-panel/alerts-panel.component';
import { AlertsTableComponent } from './components/alerts-table/alerts-table.component';
import { EarningsCalendarComponent } from './components/earnings-calendar/earnings-calendar.component';
import { CalendarHeaderComponent } from '../shared/earnings-utils/calendar-header.component';
import { PriceMovementComponent } from './components/price-movement/price-movement.component';
import { IndustryExposureComponent } from './components/industry-exposure/industry-exposure.component';





@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DashboardRoutingModule,
    HttpClientModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDialogModule,
    MatDividerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatSortModule,
    MatTooltipModule,
    MatTableModule,
    SharedModule,
    AppletHeaderModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
  ],
  declarations: [
    DashboardComponent,
    AlertsPanelComponent,
    AlertsTableComponent,
    EarningsCalendarComponent,
    CalendarHeaderComponent,
    PriceMovementComponent,
    IndustryExposureComponent
  ],
  bootstrap: [DashboardComponent],
})
export class DashboardModule {}