import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { LocalStorage, SharedUtil } from 'src/app/core/services/utilily/utility';
import { APIService } from 'src/app/core/http-services/api.service';
import { ExtendedListDataModel, MarketScannerDataDataModel, SubExtendedListDataModel } from 'src/app/core/data-models/app-model';
import { LoginDetailModel, MarketScannerDataObject } from 'src/app/core/data-models/service-model';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import * as am5 from '@amcharts/amcharts5';
import am5themes_Animated from '@amcharts/amcharts5/themes/Animated';
import * as am5hierarchy from '@amcharts/amcharts5/hierarchy';






@Component({
  selector: 'app-market-chart',
  templateUrl: './market-chart.component.html',
  styleUrls: ['./market-chart.component.scss']
})

export class MarketChartComponent implements OnInit {

  constructor(private api: APIService) { }
  marketScannerDataDataModel = new MarketScannerDataDataModel();
  userDetails: LoginDetailModel;
  @Output() marketScannerChartData = new EventEmitter<MarketScannerDataDataModel>();
  @Output() sendChartClickEvent = new EventEmitter<any>();
  neutralPerCountStyle: number;
  bearishPerCountStyle: number;
  bullishPerCountStyle: number;

  ngOnInit(): void {
    this.marketScannerChartDataApi();
  }

  marketScannerChartDataApi(): void {
    const httpGetIndustryExposureData$ = this.api.getMarketScannerChartData();
    httpGetIndustryExposureData$
      .pipe(
        map((resp) => {
          const marketScannerDataObject = resp as MarketScannerDataObject;
          this.prepareMarketScannerChartData(marketScannerDataObject.data);
        })).subscribe((resp) => {
        });
  }

  getListSymbolsData(e): void {
    this.sendChartClickEvent.emit(e.target.dataItem.dataContext);
  }

  prepareMarketScannerChartData(marketScannerDataObject): void {
    this.marketScannerDataDataModel.name = marketScannerDataObject.listName;
    this.marketScannerDataDataModel.shortName = marketScannerDataObject.listName;
    //  this.marketScannerDataDataModel.value += marketScannerDataObject.extendedList.length;
    this.marketScannerDataDataModel.value = 0.1;
    this.marketScannerDataDataModel.value1 = 0.1;
    this.marketScannerDataDataModel.color = '#4A5164';
    this.marketScannerDataDataModel.nodeSettings = { fill: am5.color('#4A5164') }
    marketScannerDataObject.extendedList.forEach(element => {
      let extendedListDataModel = new ExtendedListDataModel();
      extendedListDataModel.name = element.listName;
      extendedListDataModel.description = element.description;
      extendedListDataModel.splitName = (element.listName.split(' ', 2)[1]);
       extendedListDataModel.value = element.subExtendedList.length;
      extendedListDataModel.color = '#7399B8';
      extendedListDataModel.nodeSettings = { fill: am5.color('#7399B8') };
      extendedListDataModel.shortName = element.listName;
      if (element.subExtendedList.length > 0) {
        this.marketScannerDataDataModel.value += element.subExtendedList.length;
        element.subExtendedList.forEach(data => {
          let subExtendedListDataModel = new SubExtendedListDataModel();
          subExtendedListDataModel.name = data.sub_extended_list_name;
          if(extendedListDataModel.shortName === 'by Industry'){
            subExtendedListDataModel.shortName = './assets/industry-icons/'+data.sub_extended_list_name+'.svg';
            subExtendedListDataModel.image = './assets/industry-icons/'+data.sub_extended_list_name +'.svg';
          }else{
            subExtendedListDataModel.image = "";
            subExtendedListDataModel.shortName = data.short_name;
          }
        
                  //  /assets/industry-icons/icon_industry--Aerospace.svg
          subExtendedListDataModel.green = data.green;
          subExtendedListDataModel.list_id = data.list_id;
          subExtendedListDataModel.red = data.red;
          subExtendedListDataModel.symbolCount = data.symbolCount;
          subExtendedListDataModel.yellow = data.yellow;
          if(data.red > data.green){
            subExtendedListDataModel.stripClass = 'down-bg';
            subExtendedListDataModel.nodeSettings = { fill: am5.color('#e00000') }
            subExtendedListDataModel.color = '#e00000';
          }else if(data.red === data.green){
            subExtendedListDataModel.stripClass = 'neutral-bg';
            subExtendedListDataModel.nodeSettings = { fill: am5.color('#ffbe00') }
            subExtendedListDataModel.color = '#ffbe00';
          }else{
            subExtendedListDataModel.stripClass = 'up-bg';
            subExtendedListDataModel.nodeSettings = { fill: am5.color('#24a300') }
            subExtendedListDataModel.color = '#24a300';
          }

          //let totalSliderValue = data.green + data.red
          this.setPowerBarWidth(data);
          subExtendedListDataModel.widthOfGreenSlider = this.bullishPerCountStyle;
          subExtendedListDataModel.widthOfRedSlider = this.bearishPerCountStyle;
          subExtendedListDataModel.extendedListName = element.listName;
          extendedListDataModel.value += data.symbolCount;
          subExtendedListDataModel.value = data.symbolCount;
          this.marketScannerDataDataModel.value += data.symbolCount;
          extendedListDataModel.children.push(subExtendedListDataModel);
        });

      } else {
        // extendedListDataModel. = 1;
        extendedListDataModel.children = [];
      }
      this.marketScannerDataDataModel.children.push(extendedListDataModel);
    });
    this.marketScannerChartData.emit(this.marketScannerDataDataModel);
    this.am5ChartForMarketScanner(this.marketScannerDataDataModel)
  }
  
  am5ChartForMarketScanner(marketScannerDataDataModel) {
    let root = am5.Root.new('chartdiv');
    // Set themes
    // https://www.amcharts.com/docs/v5/concepts/themes/
    root.setThemes([
      am5themes_Animated.new(root)
    ]);

    // Create wrapper container
    let container = root.container.children.push(am5.Container.new(root, {
      width: am5.percent(100),
      height: am5.percent(100),
      layout: root.verticalLayout,
      paddingTop: 0,
      paddingBottom: 0,
      paddingLeft: 0,
      paddingRight: 0,
    }));

    // Create series
    // https://www.amcharts.com/docs/v5/charts/hierarchy/#Adding

    let series = container.children.push(am5hierarchy.ForceDirected.new(root, {
      singleBranchOnly: false,
      downDepth: 1,
      initialDepth: 3,
      valueField: 'value',
      categoryField: 'name',
      childDataField: 'children',
      centerStrength: 0.9,
      maxRadius: 80,
      minRadius: 25,
      xField: "x",
      yField: "y",
      manyBodyStrength: -10,
      nodePadding: 2,
    }));
    // 

    // Industry Nodes Only
      // minRadius: 25,
      // manyBodyStrength: -10,
      // nodePadding: 2,

    
    let setDisable = true;
    series.nodes.template.events.on("click", function(ev) {
      setDisable = false
      let targetNode = ev.target;
      if (!targetNode["_isHidden"]) {
        series.nodes.each(function(node) {
        
          if (targetNode !== node && !node["_isHidden"] && targetNode.dataItem._settings['depth'] == node.dataItem._settings['depth']) {
            node.set("disabled", true);
          }
        });
      }
    });


    series.nodes.template.on("disabled", function(disabled, targetNode) {
      let targetLevel
      if(targetNode.dataItem && setDisable){
         targetLevel = targetNode.dataItem._settings['depth'];

         if ( targetNode.dataItem._settings['depth'] === 1 && targetNode.dataItem.dataContext['shortName'] !== "by Sector") {
         setTimeout(() => {
          targetNode.set("disabled", true);
         }, 500); 
        }
      }
    })

    series.labels.template.setAll({
      text: '{shortName}',
      fontSize: 16
    });
    series.nodes.template.events.on('click', this.getListSymbolsData, this);
    series.get('colors').setAll({
      step: 0
    });
 
    series.circles.template.adapters.add('fill', function(fill, target) {
     return  am5.color(target.dataItem.dataContext['color']);
    });

    series.nodes.template.setup = function(target) {
      target.events.on('dataitemchanged', function(ev) {
        
      //  console.log(ev.target.dataItem.dataContext['children']['extendedListName'] === "by Industry")
        let icon = target.children.push(am5.Picture.new(root, {
          width: 28,
          height: 28,
          centerX: am5.percent(50),
          centerY: am5.percent(50),
          src: ev.target.dataItem.dataContext['image']
        }));
          return  ev.target.dataItem.dataContext['children'];
        });
    
    };
//     series.nodes.template.setup = function(target) {
//       target.events.on('dataitemchanged', function(ev) {
//         console.log(ev.target.dataItem.dataContext['children']);
// //     if(target.dataItem.dataContext['children'][1]){
// //       for ( let i = 0; i <  series.dataItems[0].dataContext['children'][1]['children'].length; i++ ) {
// //  //   node.dataItem.dataContext['children'][1]['children'].forEach(element => {
// //       if(series.dataItems[0].dataContext['children'][1]['children'][i]['extendedListName'] === "by Industry"){
// //        series.circles.template.adapters.add("radius", function(radius) {
// //       // ev.target.dataItem.dataContext['children']
// //       return 15
// //     });
// //     }
// //   }
// //   }
// });
// }
    series.links.template.set('strength', 0.5);
    series.data.setAll([this.marketScannerDataDataModel]);
    series.set('selectedDataItem', series.dataItems[0]);
    // Make stuff animate on load
    series.appear(1000, 100);

  }



  setPowerBarWidth(powerbar: any): void {
    if (powerbar) {
      const totalCount = (powerbar.red) + (powerbar.green) + (powerbar.yellow);
      // tslint:disable-next-line: radix
      const bearishPer = Math.round(totalCount === 0 ? 0 : ((powerbar.red) / totalCount) * 100);
      // tslint:disable-next-line: radix
      const neutralPer = Math.round(totalCount === 0 ? 0 : ((powerbar.yellow) / totalCount) * 100);
      // tslint:disable-next-line: radix
      const bullishPer = Math.round(totalCount === 0 ? 0 : ((powerbar.green) / totalCount) * 100);

      this.neutralPerCountStyle = 30;
      const bearishCountPer = (bearishPer === 0) ? 0 : (bearishPer / (bearishPer + bullishPer)) * 70;
      const bullishCountPer = (bullishPer === 0) ? 0 : (bullishPer / (bearishPer + bullishPer)) * 70;
      if (bearishCountPer === 0 && bullishCountPer === 0) {
         this.bearishPerCountStyle = 0;
         this.bullishPerCountStyle = 0;
      } else if (20 - bearishCountPer > 0) {
        this.bearishPerCountStyle = 20;
        this.bullishPerCountStyle = bullishCountPer - (20 - bearishCountPer);
      } else if (20 - bullishCountPer > 0) {
        this.bearishPerCountStyle = bearishCountPer - (20 - bullishCountPer);
        this.bullishPerCountStyle = 20;
      } else {
        this.bearishPerCountStyle = bearishCountPer;
        this.bullishPerCountStyle = bullishCountPer;
      }
      if (powerbar.red === 0) {
        this.bearishPerCountStyle = 0;
      } else if (powerbar.green === 0) {
        this.bullishPerCountStyle = 0;
      }
      //  }
    }
  }





}