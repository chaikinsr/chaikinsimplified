import {
  AfterViewInit,
  Component,
  Input,
  OnInit,
  OnDestroy,
  ViewChild
} from '@angular/core';
// import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { APIService } from 'src/app/core/http-services/api.service';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { LocalStorage, SharedUtil, Sort } from 'src/app/core/services/utilily/utility';
import { ExtendedListDataModel, GetListSymbolsDataModel, MarketScannerDataDataModel, SortingModel, SubExtendedListDataModel } from 'src/app/core/data-models/app-model';
import { GetListSymbols, GetListSymbolsDataObject, LoginDetailModel } from 'src/app/core/data-models/service-model';
import { AppConst } from 'src/app/core/app-constants/app-const';
import { element } from 'protractor';
import * as _ from 'lodash';
import { Router } from '@angular/router';


@Component({
  selector: 'app-sectors-panel',
  templateUrl: './sectors-panel.component.html',
  styleUrls: ['./sectors-panel.component.scss']
})

export class SectorsPanelComponent implements OnInit {

  constructor(private api: APIService, public router: Router) {
  }
  @Input() marketScannerDataDataModel: MarketScannerDataDataModel;
  @Input() chartClickSelectedData: any;
  extendedMarketScannerData: SubExtendedListDataModel;
  getListSymbolsDataModelArray: GetListSymbolsDataModel[] = [];
  userDetails: LoginDetailModel;
  extendedListDataModel: ExtendedListDataModel;
  showStockList = false;
  listName: string
  // sectorPanelSortingKey = ['strongest to weakest', 'weakest to strongest', 'A-Z', 'Z-A'];
  sectorPanelSortingKeyCategory1 = [
    { value: 'green', viewValue: 'Strong to Weak', selected: true, sortingOrder: AppConst.CONST.sortOrderDesc, sortingKey: 'green' },
    { value: 'red', viewValue: 'Weak to Strong', selected: false, sortingOrder: AppConst.CONST.sortOrderDesc, sortingKey: 'red' },
    { value: 'nameAsc', viewValue: 'A-Z', selected: false, sortingOrder: AppConst.CONST.sortOrderAsc, sortingKey: 'name' },
    { value: 'nameDsc', viewValue: 'Z-A', selected: false, sortingOrder: AppConst.CONST.sortOrderDesc, sortingKey: 'name' },
  ];

  sectorPanelSortingKeyCategory2 = [
    { value: 'rating', viewValue: 'Rating', selected: true, sortingOrder: AppConst.CONST.sortOrderDesc, sortingKey: 'rating' },
    { value: 'nameAsc', viewValue: 'A-Z', selected: false, sortingOrder: AppConst.CONST.sortOrderAsc, sortingKey: 'symbol' },
    { value: 'nameDsc', viewValue: 'Z-A', selected: false, sortingOrder: AppConst.CONST.sortOrderDesc, sortingKey: 'symbol' },
    { value: 'change', viewValue: 'Change', selected: false, sortingOrder: AppConst.CONST.sortOrderDesc, sortingKey: 'change' },
  ];

  ngOnInit(): void {
    // console.log(this.marketScannerDataDataModel,"sector")

  }

  ngOnChanges(): void {

    if (this.marketScannerDataDataModel) {
      this.showStockList = false;
      this.extendedListDataModel = this.marketScannerDataDataModel.children[4];
      this.listName = this.extendedListDataModel.name;
      this.sortListCategory1('green', this.extendedListDataModel.children);
     // console.log(this.extendedListDataModel.children, "extend");
    }
    if (this.chartClickSelectedData && this.chartClickSelectedData.list_id) {
      this.showStockList = false;
      this.extendedMarketScannerData = this.chartClickSelectedData;
      this.toggleShowStocks(this.extendedMarketScannerData);

    } else if (this.chartClickSelectedData && this.chartClickSelectedData.children && this.chartClickSelectedData.name !== 'US STOCK MARKET') {
      this.extendedListDataModel = this.chartClickSelectedData;
      this.listName = this.extendedListDataModel.name;
      this.sortListCategory1('green', this.chartClickSelectedData.children);
    }

  }

  sortListByPowerBar(a, b) {
    let totalA = a.green + a.red;// + pbA.neutral;
    let totalB = b.green + b.red;// + pbB.neutral;
    let pbAPositive = a.green;
    let pbBPositive = b.green;
    //also need neutral according to des-265,in case both rank and red are equal
    let pbANeutral = a.yellow;
    let pbBNeutral = b.yellow;

    //keep the list at end h have totalpowerbar 0
    let totalAPb = a.green + a.red + a.yellow;
    let totalBPb = b.green + b.red + b.yellow;
    if (totalAPb == 0 && totalBPb == 0) {
      //sort alphabetically
      return this.sortListByAlphabetically(a, b);
    }
    else if (totalAPb == 0 && totalBPb != 0) {
      return 1;
    }
    else if (totalAPb != 0 && totalBPb == 0) {
      return -1;
    }

    let ratingA = pbAPositive / ((totalA > 0) ? totalA : 1);
    let ratingB = pbBPositive / ((totalB > 0) ? totalB : 1);
    if (ratingA == ratingB) {
      //des-265
      return this.sortListsForEqualPGR(a, b, ratingA);
    }
    return (ratingA > ratingB) ? -1 : ((ratingA != ratingB) ? 1 : 0);

  }

  sortListsForEqualPGR(a, b, ratingA) {
    //DES-265

    if (ratingA == 0) {
      //sort by red and then neutral
      if (a.red > b.red) {
        return 1;
      }
      else if (a.red < b.red) {
        return -1;
      }
      else if (a.red == b.red) {
        //then sort by neutral
        if (a.yellow > b.yellow) {
          return 1;
        }
        else if (a.yellow < b.yellow) {
          return -1;
        }
        else if (a.yellow == b.yellow) {
          //sort by alphabet
          return this.sortListByAlphabetically(a, b);
        }
      }
    }
    else {
      //sort by green
      //sort positive pgr
      if (a.green > b.green) {
        return -1;
      }
      else if (a.green < b.green) {
        return 1;
      }
      else if (a.green == b.green) {
        //then sort by neutral
        if (a.yellow > b.yellow) {
          return 1;
        }
        else if (a.yellow < b.yellow) {
          return -1;
        }
        else if (a.yellow == b.yellow) {
          //sort by alphabet
          return this.sortListByAlphabetically(a, b);
        }
      }
    }
  }

  sortListByAlphabetically(a, b) {
    let FirstName = (a.name).toLowerCase();
    let SecondName = (b.name).toLowerCase();
    if (FirstName < SecondName) {
      return -1;
    } else if (FirstName > SecondName) {
      return 1;
    } else if (FirstName == SecondName) {
      return 0;
    }
  }

  // Toggle Checklist Shrink/Expand for mobile
  toggleShowStocks(extendedMarketScannerData) {
    this.extendedMarketScannerData = extendedMarketScannerData;
    this.showStockList = !this.showStockList;
    this.getListSymbolsData(extendedMarketScannerData);
  }

  toggleSector() {
    this.showStockList = !this.showStockList;
   // console.log(this.extendedMarketScannerData);
    this.marketScannerDataDataModel.children.find(element => {
      if (element.name === this.extendedMarketScannerData.extendedListName) {
        this.extendedListDataModel = element;
        this.listName = this.extendedListDataModel.name;
      }
    });
  }

  public getListSymbolsData(e): void {
    this.userDetails = LocalStorage.getItem('userDetail');
    // let listId ;
    // if(e.target){
    //   listId = e.target.dataItem.dataContext.list_id;
    //   console.log(e.target.dataItem.dataContext);
    // }else{
    //   listId=   this.userDetails.ListID
    // }
    const httpGetListSymbolsData$ = this.api.getListSymbols(e.list_id, this.userDetails.UID);
    httpGetListSymbolsData$
      .pipe(
        map((resp) => {
          const getListSymbolsDataObject = resp as GetListSymbols;
          this.prepareGetListSymbolsData(getListSymbolsDataObject);
        })).subscribe((resp) => {
        });
  }

  prepareGetListSymbolsData(getListSymbolsDataObject): void {
    this.getListSymbolsDataModelArray = [];
    // let stocksData = getListSymbolsDataObject.symbols as GetListSymbolsDataObject;
    if (getListSymbolsDataObject.symbols.length > 0) {
      getListSymbolsDataObject.symbols.forEach(element => {
        const getListSymbolsDataModel = new GetListSymbolsDataModel();
        getListSymbolsDataModel.change =  (element.Change).toFixed(2);

        getListSymbolsDataModel.changeFormatted = SharedUtil.formatPriceDashWat(getListSymbolsDataModel.change);
        getListSymbolsDataModel.divYield = element.div_yield;
        getListSymbolsDataModel.filter = element.filter;
        getListSymbolsDataModel.industryListID = element['industry_ListID '];
        getListSymbolsDataModel.industryName = element.industry_name;
        getListSymbolsDataModel.isEtfSymbol = element.is_etf_symbol;
        getListSymbolsDataModel.isNoteExist = element.isNoteExist;
        getListSymbolsDataModel.last = (element.Last).toFixed(2);;
        getListSymbolsDataModel.listRating = element.list_rating;
        getListSymbolsDataModel.marketCap = element.market_cap;
        getListSymbolsDataModel.name = element.name;
        getListSymbolsDataModel.percentage = element['Percentage '];
        getListSymbolsDataModel.pgr = element.PGR;
        getListSymbolsDataModel.pgrImage = SharedUtil.getPgrArcWithoutNeutralValue(element.PGR, element.raw_PGR, element.is_etf_symbol);
        getListSymbolsDataModel.rawPGR = element.raw_PGR;
        getListSymbolsDataModel.signals = element.signals;
        getListSymbolsDataModel.summaryRating = element['SummaryRating '];
        getListSymbolsDataModel.symbol = element.symbol;
        const a = element.name.split(',') ;
        getListSymbolsDataModel.modifiedName = a[1] ? (a[0]+a[1]) : a[0];
        SharedUtil.formatChangeDashWat(element.percent_change)
        getListSymbolsDataModel.technicalRating = element['TechnicalRating '];
        getListSymbolsDataModel.pgrName = SharedUtil.getPgrNameWithoutNeutralValues(SharedUtil.getPgrRating(element.PGR, element.raw_PGR));
        getListSymbolsDataModel.changeFormattedClass = element.Change > 0 ? 'up' : 'down';
        getListSymbolsDataModel.percentageFormattedClass = getListSymbolsDataModel.percentage > 0 ? 'up' : 'down';
        getListSymbolsDataModel.lastFormatted = '$' +  SharedUtil.formatPriceDashWat((element.Last).toFixed(2)).toString();
        getListSymbolsDataModel.percentageFormatted = '(' + ( SharedUtil.formatChangeDashWat(getListSymbolsDataModel.percentage)).toString() + ')';
        getListSymbolsDataModel.rating = SharedUtil.getPgrRating(element.PGR, element.raw_PGR);
        this.getListSymbolsDataModelArray.push(getListSymbolsDataModel);
      });
      this.sortListCategory2('rating', this.getListSymbolsDataModelArray);
      //  console.log(this.getListSymbolsDataModelArray);
    }
  }

  sortListCategory1(value, data): void {
    if(value === "green" || value ==="red"){
      data.sort(function (a, b) {
        return value === 'green' ? this.sortListByPowerBar(a, b) : this.sortListByPowerBar(b, a);
      }.bind(this))
      this.extendedListDataModel.children = data;
    }else{
 let sectorPanelSortingKeyObj;
    this.sectorPanelSortingKeyCategory1.forEach((element => {
        if (element.value === value){
          element.selected = true;
          sectorPanelSortingKeyObj =  element;
        }else{
          element.selected = false;
        }
      }));
    this.extendedListDataModel.children = this.sectorPanelSorting(data, sectorPanelSortingKeyObj);
    }
   
   
  }

  sortListCategory2(value, data): void {
    let sectorPanelSortingKeyObj;
    this.sectorPanelSortingKeyCategory2.forEach((element => {
      if (element.value === value) {
        sectorPanelSortingKeyObj = element;
      }
    }));
    this.getListSymbolsDataModelArray = this.sectorPanelSorting(data, sectorPanelSortingKeyObj);
  }



  sectorPanelSorting(listData, sortingObj): any {
    let sortingParams: any;
    if (sortingObj.sortingKey === 'rating' || sortingObj.sortingKey === 'change') {
      sortingParams = this.setSectorPanelSortingParams(listData, [
        {
          sortingKey: sortingObj.sortingKey,
          sortingOrder: sortingObj.sortingOrder,
          ignoreKey: [-1],
        },
        {
          sortingKey: 'symbol',
          sortingOrder: AppConst.CONST.sortOrderAsc,
          ignoreKey: [],
        },
      ]);
    } else {
      sortingParams = this.setSectorPanelSortingParams(listData, [
        {
          sortingKey: sortingObj.sortingKey,
          sortingOrder: sortingObj.sortingOrder,
          ignoreKey: [-1],
        },
      ]);
    }
    return Sort.sortUnique(sortingParams);
  }

  setSectorPanelSortingParams(data: Array<any>, sortObj): SortingModel {
    const len = sortObj.length - 1;
    const sortingModelData = new SortingModel();
    const sortLevelKey = [];

    sortObj.forEach((element, index) => {
      let sorting: SortingModel;
      if (index === 0) {
        sorting = sortingModelData;
        sorting.sortData = data;
      } else {
        sortingModelData.extendedData = new SortingModel();
        sorting = sortingModelData.extendedData;
        sorting.sortData = [];
      }

      sorting.sortKey = element.sortingKey;
      sorting.sortOrder = element.sortingOrder;
      sorting.ignoreDataKeys = element.ignoreKey;
      sorting.sortLevelKey = _.cloneDeep(sortLevelKey);
      sorting.ignoreData = [];
      sorting.isExtended = index === len ? false : true;
      sortLevelKey.push(element.sortingKey);
    });

    return sortingModelData;
  }

  redirectOnPgr(symbol): void {
    LocalStorage.setItem('mainSymbol', symbol);
    this.router.navigate(["/"]
      // window.location.replace(
      //   window.location.protocol +
      //   '//' +
      //   window.location.host +
      //   '/pgr/'
    );

  }


  // Export List
  exportListReport(): void {
    const exportResultData = this.getExportResultData();
    const fileName: string = this.getExportResultFileName();
    if (exportResultData.length > 0) {
      this.downloadFile(exportResultData, fileName);
    }
  } 
 
 
 private getExportResultData(): string[] {
    // let analysisChecklistData: AnalysisChecklistDataModel[] = [];
    const exportResultData: string[] = [];
    // const selectedData = this.checklistSelection.selected;
    // this.getListSymbolData.symbols =
    //   this.getListSymbolData.symbols.length > 0
    //     ? this.getListSymbolData.symbols
    //     : [];

    if (this.getListSymbolsDataModelArray) {
      exportResultData.push(
        'Ticker,Name,Rating,'
      );
      this.getListSymbolsDataModelArray.forEach((item) => {
       // console.log(item)
        //  const pgrNameBasedOnRating = item.pgrNameBasedOnRating;
        const csvString =
          item.symbol +
          ',' +
          item.modifiedName +
          ','
          +
          item.pgrName +
          ','
          ;


        exportResultData.push(csvString);
      });
    }
    return exportResultData;
  }

  getExportResultFileName(): string {
    const today = new Date();
    const dd = today.getDate();
    const mm = today.getMonth() + 1;
    const yyyy = today.getFullYear();
    const date: string = dd + '.' + mm + '.' + yyyy;
    const fileName = this.extendedMarketScannerData.name + ' ' + date + '.csv';
    return fileName;
  }

  private downloadFile(data: string[], fileName: string): void {
    
    const csvArray = data.join('\r\n');
    
    const blob = new Blob([csvArray], { type: 'text/csv' });
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = fileName;
    a.click();
    window.URL.revokeObjectURL(url);
    a.remove();
  }

}
