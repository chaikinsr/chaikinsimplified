import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { MarketScannerComponent } from "./market-scanner.component";


const routes: Routes = [
    { 
        path: '',
        component: MarketScannerComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: []
})
export class MarketScannerRoutingModule { }