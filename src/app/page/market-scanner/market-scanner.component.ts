import { Component, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { CookieService } from 'ngx-cookie-service';
import { ToastrService } from 'ngx-toastr';

import { DataService } from 'src/app/core/services/data.service';
import { AppDataService } from 'src/app/core/services/app-data.service';
import { AuthService } from 'src/app/core/http-services/auth.service';
import { MarketScannerDataDataModel, ProductAccessModel } from 'src/app/core/data-models/app-model';
import { LocalStorage, SharedUtil } from 'src/app/core/services/utilily/utility';
import { LoginDetailModel } from 'src/app/core/data-models/service-model';


@Component({
  selector: 'app-market-scanner',
  templateUrl: './market-scanner.component.html',
  styleUrls: ['./market-scanner.component.scss']
})

export class MarketScannerComponent implements OnInit, OnChanges {
  
  @ViewChild('logoutPopup', { static: false }) logoutPopup: any;
  setPreferenceForCPP: boolean = false;
  showLogoutPopup: boolean = false;
  productsList: any;
  productPopUpShow: boolean = false;
  productsAccessCount: any;
  productAccessModel: ProductAccessModel;
  totalProductAccessArray = [];
  userDetails: LoginDetailModel;
  documentClickEventRegister: boolean = false;
  createLogoutXPosition: any = 0;
  createLogoutYPosition: any = 0;
  fixedLogoutYPosition: any = 0;
  marketScannerDataDataModel :MarketScannerDataDataModel;
  chartClickSelectedData :any;

  ngOnChanges(): void {}
  ngOnInit(): void {}

  openLogoutPopup(e: any) {
    this.productPopUpShow = !this.productPopUpShow
    e.stopPropagation();
    //this.userId = localStorage.getItem('email');
    if (!this.documentClickEventRegister) {
      document.addEventListener('click', this.offClickHandler.bind(this));
      this.documentClickEventRegister = true;
      this.onClickLogoutOpenPopup(e);
      this.openLogoutPopupForAccount();
    }
  }
  onClickLogoutOpenPopup(event: any) {
    this.createLogoutXPosition = event.clientX - 150;
    this.createLogoutYPosition = event.clientY + 20;
  }
  openLogoutPopupForAccount() {
    this.showLogoutPopup = true;
  }
  onCloseLogOutPopupEvent() {
    this.showLogoutPopup = false;
  }
  offClickHandler(event: any) {
    if (
      this.logoutPopup &&
      this.logoutPopup.nativeElement &&
      !this.logoutPopup.nativeElement.contains(event.target)
    ) {
      // check click origin
      this.showLogoutPopup = false;
      if (this.documentClickEventRegister) {
        document.removeEventListener('click', this.offClickHandler.bind(this));
        this.documentClickEventRegister = false;
      }
    }
  }

  getMarketScannerChartData(marketScannerChartData:MarketScannerDataDataModel):void{
    this.marketScannerDataDataModel = marketScannerChartData;
  }

  getChartClickEvent(chartClickEvent:any):void{
    this.chartClickSelectedData = chartClickEvent;
  }
}
