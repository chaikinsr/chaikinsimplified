import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatNativeDateModule } from '@angular/material/core';
import { MatStepperModule } from '@angular/material/stepper';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';

import { AppletHeaderModule } from 'src/app/layout/header/applet-header.module';
import { SharedModule } from '../shared/shared.module';

import { ClickOutsideModule } from 'ng-click-outside';
import { MarketScannerComponent } from './market-scanner.component';
import { MarketScannerRoutingModule } from './market-scanner-routing.module';

import { MarketChartComponent } from './components/market-chart/market-chart.component';
import { SectorsPanelComponent } from './components/sectors-panel/sectors-panel.component';



@NgModule({
  imports: [
    MarketScannerRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDialogModule,
    MatDividerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    SharedModule,
    MatStepperModule,
    MatListModule,
    MatButtonToggleModule,
    MatSelectModule,
    MatTooltipModule,
    AppletHeaderModule,
    ClickOutsideModule,
  ],
  declarations: [
    MarketScannerComponent,
    MarketChartComponent,
    SectorsPanelComponent
  ],
  
  bootstrap: [MarketScannerComponent]
})
export class MarketScannerModule {}
