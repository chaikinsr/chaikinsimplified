import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { map } from 'rxjs/operators';
import {
  HeaderDataModel,
  QuaterlyEpsData,
  StockEarningsDataModel,
} from 'src/app/core/data-models/app-model';
import {
  EarningsContextSummary,
  StockEarningsDataObject,
} from 'src/app/core/data-models/service-model';
import { APIService } from 'src/app/core/http-services/api.service';
import { LocalStorage } from 'src/app/core/services/utilily/utility';

@Component({
  selector: 'app-earning',
  templateUrl: './earnings.component.html',
  styleUrls: ['./earnings.component.scss'],
})
export class EarningComponent implements OnInit, OnChanges {
  @Input() earningsContextSummary: EarningsContextSummary =
    new EarningsContextSummary();
  @Input() headerDataModel: HeaderDataModel = new HeaderDataModel();
  stockEarningsData: StockEarningsDataModel = new StockEarningsDataModel();

  ngOnInit(): void {}
  constructor(private api: APIService) {}
  ngOnChanges(changes: SimpleChanges): void {
    this.getStockEarningsData();
  }

  getStockEarningsData(): void {
    let symbol = LocalStorage.getItem('mainSymbol');
    const httpGetStockEarningsData$ = this.api.getStockEarningsData(symbol);
    httpGetStockEarningsData$
      .pipe(
        map((resp) => {
          const stockEarningsData = resp as StockEarningsDataObject;
          return this.prepareStockEarningsData(stockEarningsData);
        })
      )
      .subscribe((stockEarningsData: StockEarningsDataModel) => {
        this.stockEarningsData = stockEarningsData;
      });
  }

  prepareStockEarningsData(data: StockEarningsDataObject): StockEarningsDataModel {
    let stockEarningsData = new StockEarningsDataModel();
    let quarterEPS = data.quarterEPS;
    stockEarningsData.quaterlyEpsData = [];
    quarterEPS.forEach(epsData => {
      let dataForChart = new QuaterlyEpsData();
      dataForChart.category = epsData.year;
      dataForChart.first = +epsData.q1;
      dataForChart.second = +epsData.q2;
      dataForChart.third = +epsData.q3;
      dataForChart.four = +epsData.q4;
      stockEarningsData.quaterlyEpsData.push(dataForChart);
    });
    return stockEarningsData;
  }

}
