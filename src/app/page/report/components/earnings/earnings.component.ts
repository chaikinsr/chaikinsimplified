import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import {
  AnnualEps,
  AnnualRevenueData,
  EarningAnnouncementData,
  HeaderDataModel,
  QuaterlyEpsData,
  StockEarningsDataModel,
} from 'src/app/core/data-models/app-model';
import {
  AnnualEP,
  AnnualRevenue,
  EarningsContextSummary,
  StockEarningsDataObject,
  WordpressIdMapping,
} from 'src/app/core/data-models/service-model';
import { APIService } from 'src/app/core/http-services/api.service';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import { LocalStorage } from 'src/app/core/services/utilily/utility';
import { map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataService } from 'src/app/core/services/data.service';

@Component({
  selector: 'app-earning',
  templateUrl: './earnings.component.html',
  styleUrls: ['./earnings.component.scss'],
})
export class EarningComponent implements OnInit, OnChanges {
  @Input() earningsContextSummary: EarningsContextSummary =
    new EarningsContextSummary();
  @Input() headerDataModel: HeaderDataModel = new HeaderDataModel();
  stockEarningsData: StockEarningsDataModel = new StockEarningsDataModel();
  chartReg: any = {};
  documentClickEventRegister: boolean = false;
  @ViewChild('qickDescriptionPopup', { static: false })
  qickDescriptionPopup: any;
  showQuickScanDescription: boolean = false;
  createQickDescriptionXPosition: any = 0;
  createQickDescriptionYPosition: any = 0;
  fixedCreateQickDescriptionYPosition: any = 0;
  quickDescriptionId: string;
  quickDescriptionIdCheck: string;
  quickDescriptionDefaultText: string;
  wordpressIdMapping: WordpressIdMapping = new WordpressIdMapping();
  fixedScrollTop: any = 0;
  private destroyed$ = new Subject();
  ngOnInit(): void {
    this.dataService.showAllPopup.pipe(takeUntil(this.destroyed$)).subscribe(status => {
      if(status) {
        let earningDescription:HTMLElement = document.getElementById('earningDescription') as HTMLElement;
        earningDescription.click();
      } else {
        this.showQuickScanDescription = false;
        this.quickDescriptionIdCheck = '0';
      }
        
    });
  }

  constructor(private api: APIService, private dataService: DataService) {}

  ngOnChanges(changes: SimpleChanges): void {
    this.getStockEarningsData();
  }

  offClickHandler(event: any) {
    if (
      this.qickDescriptionPopup &&
      this.qickDescriptionPopup.nativeElement &&
      !this.qickDescriptionPopup.nativeElement.contains(event.target)
    ) {
      // check click origin
      this.showQuickScanDescription = false;
      if (this.documentClickEventRegister) {
        document.removeEventListener('click', this.offClickHandler.bind(this));
        this.documentClickEventRegister = false;
      }
    }
  }

  openQuickDescriptionPopup(id: string, defaultText: string, event) {
    event.stopPropagation();
    // if (!this.documentClickEventRegister) {
    //   document.addEventListener('click', this.offClickHandler.bind(this));
    //   this.documentClickEventRegister = true;
    // }
    if (!this.showQuickScanDescription) {
      this.onClickPortfolioOpenPopup(event);
      this.quickDescriptionId = id;
      this.quickDescriptionDefaultText = defaultText;
      this.openQuickDescription();
    } else {
      this.showQuickScanDescription = false;
      this.quickDescriptionIdCheck = '0';
    }
    
  }

  onClickPortfolioOpenPopup(event: any) {
    this.createQickDescriptionXPosition = -1;
    this.createQickDescriptionYPosition = -1;
    this.fixedCreateQickDescriptionYPosition =
      this.createQickDescriptionYPosition;
    // this.fixedScrollTop = this.currentScrollTop;
  }

  openQuickDescription() {
    if (this.quickDescriptionId !== this.quickDescriptionIdCheck) {
      this.showQuickScanDescription = true;
      this.quickDescriptionIdCheck = this.quickDescriptionId;
    } else {
      this.showQuickScanDescription = false;
      this.quickDescriptionIdCheck = '0';
    }
  }

  onCloseQuickDescriptionPopupEvent() {
    this.showQuickScanDescription = false;
    this.quickDescriptionIdCheck = '';
  }

  disposeChart(chartDiv): void {
    if (this.chartReg[chartDiv]) {
      this.chartReg[chartDiv].dispose();
    }
  }

  getStockEarningsData(): void {
    let symbol = LocalStorage.getItem('mainSymbol');
    const httpGetStockEarningsData$ = this.api.getStockEarningsData(symbol);
    httpGetStockEarningsData$
      .pipe(
        map((resp) => {
          const stockEarningsData = resp as StockEarningsDataObject;
          return this.prepareStockEarningsData(stockEarningsData);
        })
      )
      .subscribe((stockEarningsData: StockEarningsDataModel) => {
        this.stockEarningsData = stockEarningsData;
        this.quarterlyEpsChart(this.stockEarningsData);
        this.AnnualEPSChart(this.stockEarningsData);
        this.annualRevenuChart(this.stockEarningsData);
        this.earningAnnouncementChart(this.stockEarningsData);
      });
  }
  prepareStockEarningsData(
    data: StockEarningsDataObject
  ): StockEarningsDataModel {
    let stockEarningsData = new StockEarningsDataModel();
    let quarterEPS = data.quarterEPS;
    let annualEPS = data.annualEPS;
    let annualRevenue = data.annualRevenue;
    let earningAnnouncement = data.earningAnnouncement.reverse();
    stockEarningsData.quaterlyEpsData = [];
    stockEarningsData.annualEPS = [];
    stockEarningsData.annualRevenue = [];
    stockEarningsData.earningAnnouncement = [];
    quarterEPS.forEach((epsData) => {
      let dataForChart = new QuaterlyEpsData();
      dataForChart.category = epsData.year;
      if (epsData.q1 != '-') {
        dataForChart.first = +epsData.q1;
      } else {
        dataForChart.first = 0;
      }
      if (epsData.q2 != '-') {
        dataForChart.second = +epsData.q2;
      } else {
        dataForChart.second = 0;
      }
      if (epsData.q3 != '-') {
        dataForChart.third = +epsData.q3;
      } else {
        dataForChart.third = 0;
      }
      if (epsData.q4 != '-') {
        dataForChart.four = +epsData.q4;
      } else {
        dataForChart.four = 0;
      }

      stockEarningsData.quaterlyEpsData.push(dataForChart);
    });

    annualEPS.forEach((annualEpsData) => {
      let dataForAnnualEps = new AnnualEps();
      if (annualEpsData.eps != '-') {
        dataForAnnualEps.eps = +annualEpsData.eps;
      } else {
        dataForAnnualEps.eps = 0;
      }
      dataForAnnualEps.year = annualEpsData.year;
      stockEarningsData.annualEPS.push(dataForAnnualEps);
    });

    annualRevenue.forEach((annualRevData) => {
      let annualRevenueData = new AnnualRevenueData();
      if (annualRevData.revenue != '-') {
        let numberWithoutCommas = annualRevData.revenue.replace(',', '');
        annualRevenueData.revenue = +numberWithoutCommas;
      } else {
        annualRevenueData.revenue = 0;
      }
      annualRevenueData.year = annualRevData.year;
      stockEarningsData.annualRevenue.push(annualRevenueData);
    });
    let labelArr = ['3 Qtrs ago', '2 Qtrs ago', '1 Qtrs ago', 'Latest Qtrs'];
    earningAnnouncement.forEach((element, index) => {
      let earningData = new EarningAnnouncementData();
      earningData.actualEps = +element.actualEps;
      earningData.estimatedEps = element.estimatedEps;
      earningData.quarter = labelArr[index];
      stockEarningsData.earningAnnouncement.push(earningData);
    });

    return stockEarningsData;
  }

  earningAnnouncementChart(earningData: StockEarningsDataModel): void {
    let data = earningData.earningAnnouncement;
    let actualEps = Object.keys(data).map((value) => {
      let eps = data[value]['actualEps'];
      return eps;
    });
    let estimatedEps = Object.keys(data).map((value) => {
      let eps = data[value]['estimatedEps'];
      return eps;
    });
    const mergeArr = actualEps.concat(estimatedEps);
    const min = Math.min.apply(Math, mergeArr);
    const max = Math.max.apply(Math, mergeArr);
    let chartDiv = 'earningAnnouncementChart';
    this.disposeChart(chartDiv);
    const chart = am4core.create(chartDiv, am4charts.XYChart);
    chart.colors.step = 2;

    const valueAxisX = chart.xAxes.push(new am4charts.CategoryAxis());
    valueAxisX.dataFields.category = 'quarter';
    valueAxisX.renderer.grid.template.location = 0;
    valueAxisX.renderer.minGridDistance = 30;
    valueAxisX.renderer.grid.template.disabled = true;
    valueAxisX.renderer.labels.template.fontSize = 14;

    var valueAxisY = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxisY.renderer.ticks.template.disabled = true;
    valueAxisY.renderer.axisFills.template.disabled = true;
    valueAxisY.renderer.grid.template.disabled = false;
    valueAxisY.renderer.grid.template.stroke = am4core.color('#888888');
    valueAxisY.renderer.labels.template.fontSize = 14;

    if (Math.abs(max - min) <= 2) {
      valueAxisY.min = min - 0.1;
      valueAxisY.max = max + 0.1;
      valueAxisY.baseValue = min - 1;
    } else if (Math.abs(max - min) < 2 && Math.abs(max - min) <= 5) {
      valueAxisY.min = min - 0.3;
      valueAxisY.max = max + 0.3;
      valueAxisY.baseValue = min - 1;
    } else {
      valueAxisY.min = min - 0.5;
      valueAxisY.max = max + 0.5;
      valueAxisY.baseValue = min - 1;
    }

    var series = chart.series.push(new am4charts.LineSeries());
    series.dataFields.categoryX = 'quarter';
    series.dataFields.valueY = 'estimatedEps';
    series.dataFields.value = 'weight';
    series.strokeOpacity = 0;
    series.sequencedInterpolation = true;
    series.tooltip.pointerOrientation = 'vertical';

    var bullet = series.bullets.push(new am4core.Circle());
    bullet.fill = am4core.color('#67B7DC');
    //bullet.propertyFields.fill = 'color';
    bullet.strokeOpacity = 0;
    bullet.strokeWidth = 0;
    bullet.fillOpacity = 0.75;
    bullet.stroke = am4core.color('#ffffff');
    bullet.hiddenState.properties.opacity = 0;

    bullet.tooltipText = 'Estimate: ${valueY}';

    var series2 = chart.series.push(new am4charts.LineSeries());
    series2.dataFields.categoryX = 'quarter';
    series2.dataFields.valueY = 'actualEps';
    series2.dataFields.value = 'weight';
    series2.strokeOpacity = 0;
    series2.sequencedInterpolation = true;
    series2.tooltip.pointerOrientation = 'vertical';

    var bullet2 = series2.bullets.push(new am4core.Circle());
    bullet2.fill = am4core.color('#a367dc');
    //bullet2.propertyFields.fill = 'color';
    bullet2.strokeOpacity = 0;
    bullet2.strokeWidth = 0;
    bullet2.fillOpacity = 0.75;
    bullet2.stroke = am4core.color('#ffffff');
    bullet2.hiddenState.properties.opacity = 0;

    bullet2.tooltipText = 'Actual: ${valueY}';
    series.heatRules.push({
      target: bullet,
      min: 2,
      max: 20,
      property: 'radius',
    });

    series2.heatRules.push({
      target: bullet2,
      min: 2,
      max: 20,
      property: 'radius',
    });
    chart.data = data;

    this.chartReg[chartDiv] = chart;
  }

  quarterlyEpsChart(quartelyData: StockEarningsDataModel): void {
    let data = quartelyData.quaterlyEpsData;
    let chartDiv = 'quarterlyEpsChart';
    this.disposeChart(chartDiv);
    const chart = am4core.create(chartDiv, am4charts.XYChart);
    chart.colors.step = 2;
    chart.legend = new am4charts.Legend();
    //chart.legend.position = 'top';
    //chart.legend.paddingBottom = 20;
    //chart.legend.labels.template.maxWidth = 95;
    chart.legend.markers.template.disabled = true;
    const xAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    xAxis.dataFields.category = 'category';
    xAxis.renderer.cellStartLocation = 0.1;
    xAxis.renderer.cellEndLocation = 0.9;
    xAxis.renderer.labels.template.fontSize = 14;

    //xAxis.renderer.grid.template.location = 0;
    xAxis.renderer.grid.template.disabled = true;
    const yAxis = chart.yAxes.push(new am4charts.ValueAxis());
    yAxis.renderer.grid.template.disabled = false;
    yAxis.renderer.grid.template.stroke = am4core.color('#888888');
    yAxis.renderer.labels.template.fontSize = 14;
   // yAxis.min = -0.12;
    function createSeries(value, name): any {
      const series = chart.series.push(new am4charts.ColumnSeries());
      series.dataFields.valueY = value;
      series.dataFields.categoryX = 'category';
      // series.name = name;
      series.columns.template.fillOpacity = 0.8;
      series.columns.template.tooltipText = "${valueY.formatNumber('#.00')}[/]";
      return series;
    }
    chart.data = data;

    createSeries('first', 'The First');
    createSeries('second', 'The Second');
    createSeries('third', 'The Third');
    createSeries('four', 'The Four');

    this.chartReg[chartDiv] = chart;
  }

  rounding(num) {
    return parseFloat(num).toFixed(2);
    // return (Math.round(num * 100) / 100);
  }

  annualRevenuChart(stockEarningsDataModel: StockEarningsDataModel): void {
    // Create chart instance
    let chartDiv = 'annualRevenueChart';
    let annualRevenueData = stockEarningsDataModel.annualRevenue;
    this.disposeChart(chartDiv);
    const chart = am4core.create(chartDiv, am4charts.XYChart);
    let annualRev = Object.keys(stockEarningsDataModel.annualRevenue).map(
      (value) => {
        let rev = stockEarningsDataModel.annualRevenue[value]['revenue'];
        return rev;
      }
    );
    let revDates = Object.keys(stockEarningsDataModel.annualRevenue).map(
      (value) => {
        let date = stockEarningsDataModel.annualRevenue[value]['year'];
        return date;
      }
    );

    if (Math.min.apply(Math, annualRev) > 1000) {
      annualRev = annualRev.map((value) => +this.rounding(value / 1000));
      chart.numberFormatter.numberFormat = "#.#'B'";
    } else {
      annualRev = annualRev;
      chart.numberFormatter.numberFormat = "#.#'M'";
    }

    chart.data = revDates.map((value, index) => {
      return {
        year: value,
        revenue: annualRev[index],
      };
    });

    // Add data
    // chart.data = annualRevenueData;

    const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = 'year';
    // categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.grid.template.disabled = true;
    categoryAxis.renderer.minGridDistance = 30;
    categoryAxis.renderer.labels.template.fontSize = 14;
    /* 
categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
  if (target.dataItem && target.dataItem.index & 2 == 2) {
    return dy + 25;
  }
  return dy;
}); */

    const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

    // Create series
    const series = chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueY = 'revenue';
    series.dataFields.categoryX = 'year';
    series.name = 'revenue';
    series.columns.template.tooltipText = '{categoryX}: [bold]{valueY}[/]';
    series.columns.template.fillOpacity = 0.8;
    valueAxis.renderer.grid.template.disabled = false;
    valueAxis.renderer.grid.template.stroke = am4core.color('#888888');
    valueAxis.renderer.labels.template.fontSize = 14;

    const columnTemplate = series.columns.template;
    columnTemplate.strokeWidth = 2;
    columnTemplate.strokeOpacity = 0;
    this.chartReg[chartDiv] = chart;
  }

  AnnualEPSChart(stockEarningsDataModel: StockEarningsDataModel): void {
    let chartDiv = 'annualEpsChart';
    this.disposeChart(chartDiv);
    let annualData = stockEarningsDataModel.annualEPS;
    const chart = am4core.create(chartDiv, am4charts.XYChart);

    // Add data
    chart.data = annualData;

    const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = 'year';
    //categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 30;
    categoryAxis.renderer.grid.template.disabled = true;
    categoryAxis.renderer.labels.template.fontSize = 14;
    /* 
  categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
    if (target.dataItem && target.dataItem.index & 2 == 2) {
      return dy + 25;
    }
    return dy;
  }); */

    const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

    // Create series
    const series = chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueY = 'eps';
    series.dataFields.categoryX = 'year';
    series.name = 'eps';
    series.columns.template.tooltipText = '{categoryX}: [bold]{valueY}[/]';
    series.columns.template.fillOpacity = 0.8;
    valueAxis.renderer.grid.template.disabled = false;
    valueAxis.renderer.grid.template.stroke = am4core.color('#888888');
    valueAxis.renderer.labels.template.fontSize = 14;
    const columnTemplate = series.columns.template;
    columnTemplate.strokeWidth = 2;
    columnTemplate.strokeOpacity = 0;
    this.chartReg[chartDiv] = chart;
  }
}
