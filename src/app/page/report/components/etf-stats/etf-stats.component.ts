import { Component, Input, OnChanges, OnInit, SimpleChanges } from "@angular/core";
import { map } from "rxjs/operators";
import { EtfStatDataModel, HeaderDataModel } from "src/app/core/data-models/app-model";
import { EtfStatData } from "src/app/core/data-models/service-model";
import { APIService } from "src/app/core/http-services/api.service";
import { LocalStorage, SharedUtil } from "src/app/core/services/utilily/utility";


@Component({
    selector: 'app-etf-stats',
    templateUrl: './etf-stats.component.html',
    styleUrls: ['./etf-stats.component.scss']
  })
  
  export class EtfStatsComponent implements OnInit, OnChanges {
    @Input() headerDataModel: HeaderDataModel = new HeaderDataModel();
    etfTickerData: EtfStatDataModel = new EtfStatDataModel();
    ngOnInit(): void {}
    constructor(
      private api: APIService
    ) {}
  ngOnChanges(changes: SimpleChanges): void {
    this.getEtfTickerData()
  }

  getEtfTickerData() {
    let symbol = LocalStorage.getItem('mainSymbol');
    const httpGetEtfTickerData$ = this.api.getEtfTickerData(symbol);
    httpGetEtfTickerData$
      .pipe(
        map((resp) => {
          const etfTickerData = resp as EtfStatData;
          return this.prepareEtfTickerData(etfTickerData);
        })
      )
      .subscribe((etfTickerData: EtfStatDataModel) => {
        this.etfTickerData = etfTickerData;
       
      });
  }

  prepareEtfTickerData(data: EtfStatData): EtfStatDataModel{
    let etfTickerData = new EtfStatDataModel();
    etfTickerData.assets = data.assets;
    let assetsInt = SharedUtil.convertStringToNumber(etfTickerData.assets);
    if(!isNaN(assetsInt)) {
    let assetsString = SharedUtil.convertMillonIntoBillion(assetsInt);
    etfTickerData.assetsFormatted =  SharedUtil.appendSymbol(assetsString, '$');
    } else {
      etfTickerData.assetsFormatted = etfTickerData.assets;
    }
   
    etfTickerData.expenseRatio = data.expenseRatio;
    if (etfTickerData.expenseRatio !== 'NA') {
      etfTickerData.expenseRatioFormatted = SharedUtil.appendSymbol(etfTickerData.expenseRatio, '%');
    } else {
      etfTickerData.expenseRatioFormatted = etfTickerData.expenseRatio;
    }
    
    etfTickerData.management = data.management;
    etfTickerData.etfGroup = data.etfGroup;
    etfTickerData.strategy = data.strategy;
    etfTickerData.holdings = data.holdings;
    etfTickerData.beta = data.beta;
    etfTickerData.dividentYield = data.dividentYield;
    if (etfTickerData.dividentYield !== 'NA') {
      etfTickerData.dividentYieldFormatted = SharedUtil.appendSymbol(etfTickerData.dividentYield, '%');
    } else {
      etfTickerData.dividentYieldFormatted = etfTickerData.dividentYield;
    }
    etfTickerData.avgDailyVolume = data.avgDailyVolume;
    let avgDailyVolumeInt = SharedUtil.convertStringToNumber(etfTickerData.avgDailyVolume);
    if(!isNaN(avgDailyVolumeInt)) {
      etfTickerData.avgDailyVolumeFormatted = SharedUtil.convertIntoMB(avgDailyVolumeInt);
    } else {
      etfTickerData.avgDailyVolumeFormatted =  etfTickerData.avgDailyVolume;
    }
    
    return etfTickerData;
  }
}