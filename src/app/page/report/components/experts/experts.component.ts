import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from "@angular/core";
import { Subject } from "rxjs";
import { map, takeUntil } from "rxjs/operators";
import { ExpertDataModel, HeaderDataModel } from "src/app/core/data-models/app-model";
import { ExpertOpnionsContextSummary, StockExpertDataObject, WordpressIdMapping } from "src/app/core/data-models/service-model";
import { APIService } from "src/app/core/http-services/api.service";
import { DataService } from "src/app/core/services/data.service";
import { LocalStorage, SharedUtil } from "src/app/core/services/utilily/utility";


@Component({
    selector: 'app-expert',
    templateUrl: './experts.component.html',
    styleUrls: ['./experts.component.scss']
  })
  
  export class ExpertsComponent implements OnInit, OnChanges {
    @Input() expertOpnionsContextSummary: ExpertOpnionsContextSummary = new ExpertOpnionsContextSummary();
    @Input() headerDataModel: HeaderDataModel = new HeaderDataModel();
    expertDataModel: ExpertDataModel = new ExpertDataModel();
    ngOnInit(): void {}
    documentClickEventRegister: boolean = false;
  @ViewChild('qickDescriptionPopup', { static: false })
  qickDescriptionPopup: any;
  showQuickScanDescription: boolean = false;
  createQickDescriptionXPosition: any = 0;
  createQickDescriptionYPosition: any = 0;
  fixedCreateQickDescriptionYPosition: any = 0;
  quickDescriptionId: string;
  quickDescriptionIdCheck: string;
  quickDescriptionDefaultText: string;
  wordpressIdMapping: WordpressIdMapping = new WordpressIdMapping();
  fixedScrollTop: any = 0;
  private destroyed$ = new Subject();
    constructor(
      private api: APIService,
      private dataService: DataService
    ) {
       // this.getStockExpertData();
       this.dataService.showAllPopup.pipe(takeUntil(this.destroyed$)).subscribe(status => {
        if(status) {
          let expertDescription:HTMLElement = document.getElementById('expertDescription') as HTMLElement;
          expertDescription.click();
         // this.moneyFlow.nativeElement.click();
        } else {
          this.showQuickScanDescription = false;
          this.quickDescriptionIdCheck = '0';
        }
          
      });
    }
  ngOnChanges(changes: SimpleChanges): void {
    this.getStockExpertData();
    if (this.expertOpnionsContextSummary.explanatorySentence && this.headerDataModel.industry) {
      let industry = this.headerDataModel.industry;
      this.expertOpnionsContextSummary.explanatorySentence = this.expertOpnionsContextSummary.explanatorySentence.replace(/<industry>/g, '');
      // this.expertOpnionsContextSummary.explanatorySentence = this.expertOpnionsContextSummary.explanatorySentence.replace(/<industry>/g, industry);
    }
  }

  offClickHandler(event: any) {
    if (
      this.qickDescriptionPopup &&
      this.qickDescriptionPopup.nativeElement &&
      !this.qickDescriptionPopup.nativeElement.contains(event.target)
    ) {
      // check click origin
      this.showQuickScanDescription = false;
      if (this.documentClickEventRegister) {
        document.removeEventListener('click', this.offClickHandler.bind(this));
        this.documentClickEventRegister = false;
      }
    }
  }

  openQuickDescriptionPopup(id: string, defaultText: string, event) {
    event.stopPropagation();
    // if (!this.documentClickEventRegister) {
    //   document.addEventListener('click', this.offClickHandler.bind(this));
    //   this.documentClickEventRegister = true;
    // }
    if (!this.showQuickScanDescription) {
      this.onClickPortfolioOpenPopup(event);
      this.quickDescriptionId = id;
      this.quickDescriptionDefaultText = defaultText;
      this.openQuickDescription();
    } else {
      this.showQuickScanDescription = false;
      this.quickDescriptionIdCheck = '0';
    }
    
  }

  onClickPortfolioOpenPopup(event: any) {
    this.createQickDescriptionXPosition = -1;
    this.createQickDescriptionYPosition = -1;
    this.fixedCreateQickDescriptionYPosition =
      this.createQickDescriptionYPosition;
    // this.fixedScrollTop = this.currentScrollTop;
  }

  openQuickDescription() {
    if (this.quickDescriptionId !== this.quickDescriptionIdCheck) {
      this.showQuickScanDescription = true;
      this.quickDescriptionIdCheck = this.quickDescriptionId;
    } else {
      this.showQuickScanDescription = false;
      this.quickDescriptionIdCheck = '0';
    }
  }

  onCloseQuickDescriptionPopupEvent() {
    this.showQuickScanDescription = false;
    this.quickDescriptionIdCheck = '';
  }
    getStockExpertData(): void {
        let symbol = LocalStorage.getItem('mainSymbol');
        const httpGetExpertData$ = this.api.getStockExpertData(symbol);
        httpGetExpertData$
          .pipe(
            map((resp) => {
              const expertData = resp as StockExpertDataObject;
              return this.prepareExpertData(expertData);
            })
          )
          .subscribe((expertDataModel: ExpertDataModel) => {
            this.expertDataModel = expertDataModel;
          
          });
      }
    
      prepareExpertData(data: StockExpertDataObject): ExpertDataModel {
        let expertDataModel = new ExpertDataModel();
        expertDataModel.currentQuarterCurrent = data.earningEstimateRevision.currentQuarterCurrent;
        expertDataModel.currentQuarter30Dago = data.earningEstimateRevision.currentQuarter30Dago;
        expertDataModel.currentQuarterPercentChange = data.earningEstimateRevision.currentQuarterPercentChange;
        expertDataModel.nextQuarterCurrent = data.earningEstimateRevision.nextQuarterCurrent;
        expertDataModel.nextQuarter7Dago = data.earningEstimateRevision.nextQuarter30Dago;
        expertDataModel.nextQuarterPercentChange = data.earningEstimateRevision.nextQuarterPercentChange;
        expertDataModel.shortIntrest = data.shortIntrest;
        if(expertDataModel.shortIntrest<3) {
            expertDataModel.shortIntrestHighColor = 'red';
            expertDataModel.shortIntrestMediumColor = 'grey thin';
            expertDataModel.shortIntrestLowColor = 'grey thin';
        } else if (expertDataModel.shortIntrest === 3) {
            expertDataModel.shortIntrestHighColor = 'grey thin';
            expertDataModel.shortIntrestMediumColor = 'neutral';
            expertDataModel.shortIntrestLowColor = 'grey thin';
        } else {
            expertDataModel.shortIntrestHighColor = 'grey thin';
            expertDataModel.shortIntrestMediumColor = 'grey thin';
            expertDataModel.shortIntrestLowColor = 'green';
        }
        expertDataModel.meanThisWeek = data.recomandations.meanThisWeek;
        expertDataModel.meanLastWeek = data.recomandations.meanLastWeek;
        expertDataModel.fiveweekago = data.recomandations.fiveweekago;
        expertDataModel.meanThisWeekColor = SharedUtil.getColorForRating(expertDataModel.meanThisWeek) + '-label';
        expertDataModel.meanLastWeekColor = SharedUtil.getColorForRating(expertDataModel.meanLastWeek) + '-label';
        expertDataModel.fiveweekagoColor = SharedUtil.getColorForRating(expertDataModel.fiveweekago) + '-label';
        return expertDataModel;
      }
    
      checkForNaNandAppendSymbol(value: string, symbol: string): string {
        return value === 'NA' ? value : SharedUtil.appendSymbol(value, symbol);
      }
}