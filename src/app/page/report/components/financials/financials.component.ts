import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { FinancialsDataModel, HeaderDataModel } from 'src/app/core/data-models/app-model';
import { FinancialContextSummary, StockFinancialsDataObject, WordpressIdMapping } from 'src/app/core/data-models/service-model';
import { APIService } from 'src/app/core/http-services/api.service';
import { DataService } from 'src/app/core/services/data.service';
import { LocalStorage, SharedUtil } from 'src/app/core/services/utilily/utility';

@Component({
  selector: 'app-financial',
  templateUrl: './financials.component.html',
  styleUrls: ['./financials.component.scss'],
})
export class FinancialComponent implements OnInit, OnChanges {
  @Input() headerDataModel: HeaderDataModel = new HeaderDataModel();
  @Input() financialContextSummary: FinancialContextSummary = new FinancialContextSummary();
  financialData: FinancialsDataModel = new FinancialsDataModel();
  documentClickEventRegister: boolean = false;
  @ViewChild('qickDescriptionPopup', { static: false }) qickDescriptionPopup: any;
  showQuickScanDescription: boolean = false;
  createQickDescriptionXPosition: any = 0;
  createQickDescriptionYPosition: any = 0;
  fixedCreateQickDescriptionYPosition: any = 0;
  quickDescriptionId: string;
  quickDescriptionIdCheck: string;
  quickDescriptionDefaultText: string;
  wordpressIdMapping: WordpressIdMapping = new WordpressIdMapping();
  fixedScrollTop: any = 0;
  private destroyed$ = new Subject();
  
  constructor(private api: APIService, private dataService: DataService) {
   // this.getStockFinancialsData();
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.getStockFinancialsData();
  }

  ngOnInit(): void {
    this.dataService.showAllPopup.pipe(takeUntil(this.destroyed$)).subscribe(status => {
      if(status) {
        let financialDescription:HTMLElement = document.getElementById('financialDescription') as HTMLElement;
        financialDescription.click();
       // this.moneyFlow.nativeElement.click();
      } else {
        this.showQuickScanDescription = false;
        this.quickDescriptionIdCheck = '0';
      }
        
    });
  }

  offClickHandler(event: any) {
    if (
      this.qickDescriptionPopup &&
      this.qickDescriptionPopup.nativeElement &&
      !this.qickDescriptionPopup.nativeElement.contains(event.target)
    ) {
      // check click origin
      this.showQuickScanDescription = false;
      if (this.documentClickEventRegister) {
        document.removeEventListener('click', this.offClickHandler.bind(this));
        this.documentClickEventRegister = false;
      }
    }
  }

  openQuickDescriptionPopup(id: string, defaultText: string, event) {
    event.stopPropagation();
    // if (!this.documentClickEventRegister) {
    //   document.addEventListener('click', this.offClickHandler.bind(this));
    //   this.documentClickEventRegister = true;
    // }
    if (!this.showQuickScanDescription) {
      this.onClickPortfolioOpenPopup(event);
      this.quickDescriptionId = id;
      this.quickDescriptionDefaultText = defaultText;
      this.openQuickDescription();
    } else {
      this.showQuickScanDescription = false;
      this.quickDescriptionIdCheck = '0';
    }
    
  }

  onClickPortfolioOpenPopup(event: any) {
    this.createQickDescriptionXPosition = -1;
    this.createQickDescriptionYPosition = -1;
    this.fixedCreateQickDescriptionYPosition =
      this.createQickDescriptionYPosition;
    // this.fixedScrollTop = this.currentScrollTop;
  }

  openQuickDescription() {
    if (this.quickDescriptionId !== this.quickDescriptionIdCheck) {
      this.showQuickScanDescription = true;
      this.quickDescriptionIdCheck = this.quickDescriptionId;
    } else {
      this.showQuickScanDescription = false;
      this.quickDescriptionIdCheck = '0';
    }
  }

  onCloseQuickDescriptionPopupEvent() {
    this.showQuickScanDescription = false;
    this.quickDescriptionIdCheck = '';
  }

  getStockFinancialsData(): void {
    let symbol = LocalStorage.getItem('mainSymbol');
    const httpGetFinancialData$ = this.api.getStockFinancialsData(symbol);
    httpGetFinancialData$
      .pipe(
        map((resp) => {
          const financialData = resp as StockFinancialsDataObject;
          return this.prepareFinancialData(financialData);
        })
      )
      .subscribe((financialData: FinancialsDataModel) => {
        this.financialData = financialData;
      
      });
  }

  prepareFinancialData(data: StockFinancialsDataObject): FinancialsDataModel {
    let financialData: FinancialsDataModel = new FinancialsDataModel();
    financialData.ticker = data.ticker;
    
    // valuation
    financialData.peg = data.valuation.peg;
    financialData.pegFormatted = SharedUtil.formatedString(financialData.peg);
    financialData.priceToSale = data.valuation.priceToSale;
    financialData.priceToSaleFormatted = SharedUtil.formatedString(financialData.priceToSale);
    financialData.priceEarning = data.valuation.priceEarning;
    financialData.priceEarningFormatted = SharedUtil.formatedString(financialData.priceEarning);
    financialData.priceToBook = data.valuation.priceToBook;
    financialData.priceToBookFormatted = SharedUtil.formatedString(financialData.priceToBook);

    // assetAndLiability
    financialData.bookValue = data.assetAndLiability.bookValue;
    let bookValueFormatted = SharedUtil.formatedString(financialData.bookValue);
    financialData.bookValueFormatted = this.checkForNaNandAppendSymbol(bookValueFormatted, '$');
    financialData.current_ratio = data.assetAndLiability.current_ratio;
    financialData.current_ratioFormatted = SharedUtil.formatedString(financialData.current_ratio);
    financialData.debtEquity = data.assetAndLiability.debtEquity;
    financialData.debtEquityFormatted = SharedUtil.formatedString(financialData.debtEquity);
    financialData.marketCap = data.assetAndLiability.marketCap;
    let marketCapInt = SharedUtil.convertStringToNumber(financialData.marketCap);
    let marketCapString = SharedUtil.convertMillonIntoBillion(marketCapInt);
    financialData.marketCapFormatted = SharedUtil.appendSymbol(marketCapString, '$');
    financialData.revenue = data.assetAndLiability.revenue;
    let revenueInt = SharedUtil.convertStringToNumber(financialData.revenue);
    let revenueString = SharedUtil.convertMillonIntoBillion(revenueInt);
    financialData.revenueFormatted = SharedUtil.appendSymbol(revenueString, '$');
  
    // dividends
    financialData.yield = data.dividends.yield;
    let yieldFormatted = SharedUtil.formatedString(financialData.yield);
    financialData.yieldFormatted = this.checkForNaNandAppendSymbol(yieldFormatted, '%');
    financialData.dividendPerShare = data.dividends.dividendPerShare;
    let dividendPerShareFormatted = SharedUtil.formatedString(financialData.dividendPerShare);
    financialData.dividendPerShareFormatted = this.checkForNaNandAppendSymbol(dividendPerShareFormatted, '$');
    financialData.payoutRatio = data.dividends.payoutRatio;
    let payoutRatioFormatted = SharedUtil.formatedString(financialData.payoutRatio);
    financialData.payoutRatioFormatted = this.checkForNaNandAppendSymbol(payoutRatioFormatted, '%');
    financialData.growthRate5Year = data.dividends.growthRate5Year;
    let growthRate5YearFormatted = SharedUtil.formatedString(financialData.growthRate5Year);
    financialData.growthRate5YearFormatted = this.checkForNaNandAppendSymbol(growthRate5YearFormatted, '%');

    // returns
    financialData.returnOneMonth = data.returns.returnOneMonth;
    let returnOneMonthFormatted = SharedUtil.formatedString(financialData.returnOneMonth);
    financialData.returnOneMonthFormatted = this.checkForNaNandAppendSymbol(returnOneMonthFormatted, '%');
    financialData.returnOnEquity = data.returns.returnOnEquity;
    let returnOnEquityFormatted = SharedUtil.formatedString(financialData.returnOnEquity);
    financialData.returnOnEquityFormatted = this.checkForNaNandAppendSymbol(returnOnEquityFormatted, '%');
    financialData.returnThreeMonth = data.returns.returnThreeMonth;
    let returnThreeMonthFormatted = SharedUtil.formatedString(financialData.returnThreeMonth);
    financialData.returnThreeMonthFormatted = this.checkForNaNandAppendSymbol(returnThreeMonthFormatted, '%');
    financialData.returnOnInvestment = data.returns.returnOnInvestment;
    let returnOnInvestmentFormatted = SharedUtil.formatedString(financialData.returnOnInvestment);
    financialData.returnOnInvestmentFormatted = this.checkForNaNandAppendSymbol(returnOnInvestmentFormatted, '%');

    return financialData
  } 

  checkForNaNandAppendSymbol(value: string, symbol: string): string {
    return (value === 'NA') ? value : SharedUtil.appendSymbol(value, symbol);
  }

}
