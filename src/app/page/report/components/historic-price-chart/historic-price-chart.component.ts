import {
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { APIService } from 'src/app/core/http-services/api.service';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import {
  FundamentalData,
  LoginDetailModel,
  PGRs,
  SectorsPerformanceChart,
  SymbolDataRootObject,
  SymbolInfo,
  WordpressIdMapping,
} from 'src/app/core/data-models/service-model';
import { forkJoin, Observable, Subject, Subscription } from 'rxjs';
import { AuthService } from 'src/app/core/http-services/auth.service';
import {
  BenchMarkRelativeStrengthChartData,
  HistoricPriceChartData,
  UISetting,
} from 'src/app/core/data-models/app-model';
import { LocalStorage, SharedUtil } from 'src/app/core/services/utilily/utility';
import { DataService } from 'src/app/core/services/data.service';
import { takeUntil } from 'rxjs/operators';
import { AppDataService } from 'src/app/core/services/app-data.service';
export interface Performance {
  value: string;
  viewValue: string;
}
export interface Benchmark {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-historic-price-chart',
  templateUrl: './historic-price-chart.component.html',
  styleUrls: ['./historic-price-chart.component.scss'],
})
export class HistoricPriceChartComponent
  implements OnInit, OnChanges, OnDestroy
{
  @Input() symbolstr: string;
  @Input() isETF: boolean;
  @Input() name: string;

  selectedPeriod: string = 'year';
  selectedPeriodText: string = '1 year';
  isChartLoaded: boolean = false;
  isPageLoaded: boolean = false;
  symbolPriceUp: boolean = false;
  symbolPriceChange: number;
  symbolPriceChangePerc: number;
  isDailyWeeklyToggleVisible: boolean;
  isWeeklyHistoricRating: boolean = true;
  timespanPerChange: string;
  timespanPriceChange: string;
  changeMovement: boolean = true;

  private mainchart: am4charts.XYChart;
  uiSetting: UISetting = new UISetting();
  pgrsValue: PGRs = new PGRs();

  private BMRSChartDataSub: Subscription;
  private chartDataSub: Subscription;

  historicPrices: Performance[] = [
    { value: '1month', viewValue: '1 month' },
    { value: '3month', viewValue: '3 months' },
    { value: '6month', viewValue: '6 months' },
    { value: 'ytd', viewValue: 'YTD' },
    { value: 'year', viewValue: '1 year' },
    { value: '3year', viewValue: '3 years' },
    { value: '5year', viewValue: '5 years' },
  ];

  performances: Performance[] = this.historicPrices;
  benchmarks: Benchmark[] = [
    { value: 'SPY', viewValue: 'SPY' },
    { value: 'MDY', viewValue: 'MDY' },
    { value: 'IWM', viewValue: 'IWM' },
    { value: 'AGG', viewValue: 'AGG' },
    { value: 'VEA', viewValue: 'VEA' },
  ];

  relbenchmarks: Benchmark[] = [
    { value: 'SPY', viewValue: 'SPY' },
    { value: 'MDY', viewValue: 'MDY' },
    { value: 'IWM', viewValue: 'IWM' },
    { value: 'AGG', viewValue: 'AGG' },
    { value: 'VEA', viewValue: 'VEA' },
  ];
  perfchart: any;
  dateAxisChart: any;
  isChartDataAvailableForSelectedPeriod: boolean = true;
  // @Input() isPrintMode: boolean;
  //@Input() sectorsPerformanceTableData: SectorsPerformanceTableData;

  documentClickEventRegister: boolean = false;
  @ViewChild('qickDescriptionPopup', { static: false })
  qickDescriptionPopup: any;
  showQuickScanDescription: boolean = false;
  createQickDescriptionXPosition: any = 0;
  createQickDescriptionYPosition: any = 0;
  fixedCreateQickDescriptionYPosition: any = 0;
  quickDescriptionId: string;
  quickDescriptionIdCheck: string;
  quickDescriptionDefaultText: string;
  wordpressIdMapping: WordpressIdMapping = new WordpressIdMapping();
  fixedScrollTop: any = 0;
  currentScrollTop: any = 0;
  private subscription: Subscription;

  documentClickEventRegisterMoneyFlow: boolean = false;
  @ViewChild('qickDescriptionPopupMoneyFlow', { static: false })
  // @ViewChild('moneyFlow') moneyFlow: ElementRef<HTMLElement>;
  qickDescriptionPopupMoneyFlow: any;
  showQuickScanDescriptionMoneyFlow: boolean = false;
  createQickDescriptionXPositionMoneyFlow: any = 0;
  createQickDescriptionYPositionMoneyFlow: any = 0;
  fixedCreateQickDescriptionYPositionMoneyFlow: any = 0;
  quickDescriptionIdMoneyFlow: string;
  quickDescriptionIdCheckMoneyFlow: string;
  quickDescriptionDefaultTextMoneyFlow: string;

  documentClickEventRegisterRelStr: boolean = false;
  @ViewChild('qickDescriptionPopupRelStr', { static: false })
  qickDescriptionPopupRelStr: any;
  showQuickScanDescriptionRelStr: boolean = false;
  createQickDescriptionXPositionRelStr: any = 0;
  createQickDescriptionYPositionRelStr: any = 0;
  fixedCreateQickDescriptionYPositionRelStr: any = 0;
  quickDescriptionIdRelStr: string;
  quickDescriptionIdCheckRelStr: string;
  quickDescriptionDefaultTextRelStr: string;
  private destroyed$ = new Subject();
  ngOnInit(): void {
    this.matSideNavContentScroll();
    this.dataService.showAllPopup
      .pipe(takeUntil(this.destroyed$))
      .subscribe((status) => {
        if (status) {
          let historicChartPopup: HTMLElement = document.getElementById(
            'historicChartPopup'
          ) as HTMLElement;
          let moneyFlowPopup: HTMLElement = document.getElementById(
            'moneyFlowPopup'
          ) as HTMLElement;
          let relStrPopup: HTMLElement = document.getElementById(
            'relStrPopup'
          ) as HTMLElement;
          historicChartPopup.click();
          moneyFlowPopup.click();
          relStrPopup.click();
          // this.moneyFlow.nativeElement.click();
        } else {
          this.showQuickScanDescription = status;
          this.showQuickScanDescriptionMoneyFlow = status;
          this.showQuickScanDescriptionRelStr = status;
          this.quickDescriptionIdCheck = '0';
          this.quickDescriptionIdCheckMoneyFlow = '0';
          this.quickDescriptionIdCheckRelStr = '0';
        }
      });
  }
  constructor(
    private api: APIService,
    private auth: AuthService,
    private dataService: DataService,
    public appDataService: AppDataService,
  ) {}
  ngOnDestroy(): void {
    this.destroyed$.next();
    this.destroyed$.complete();
    if (this.chartDataSub) {
      this.chartDataSub.unsubscribe();
    }

    if (this.BMRSChartDataSub) {
      this.BMRSChartDataSub.unsubscribe();
    }

    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (this.symbolstr === 'SPY') {
      this.relbenchmarks = [
        { value: 'IWV', viewValue: 'IWV' },
        { value: 'MDY', viewValue: 'MDY' },
        { value: 'IWM', viewValue: 'IWM' },
        { value: 'AGG', viewValue: 'AGG' },
        { value: 'VEA', viewValue: 'VEA' },
      ];
      if (this.uiSetting.selectedRelstrengthBenchmark == 'SPY') {
        this.uiSetting.selectedRelstrengthBenchmark = 'IWV';
      }
    } else {
      if (this.uiSetting.selectedRelstrengthBenchmark == 'IWV') {
        this.uiSetting.selectedRelstrengthBenchmark = 'SPY';
      }
    }

    if (this.symbolstr) {
      this.prepareMainChart();
    }
  }

  matSideNavContentScroll(): void {
    this.subscription =
      this.dataService.matSideNavContentScrollChange.subscribe((event) => {
        this.currentScrollTop = event.srcElement.scrollTop;
        if (this.showQuickScanDescription) {
          const scrollTopPos = event.srcElement.scrollTop - this.fixedScrollTop;
          this.createQickDescriptionYPosition =
            this.fixedCreateQickDescriptionYPosition - scrollTopPos;
        }
      });
  }

  offClickHandler(event: any) {
    if (
      this.qickDescriptionPopup &&
      this.qickDescriptionPopup.nativeElement &&
      !this.qickDescriptionPopup.nativeElement.contains(event.target)
    ) {
      // check click origin
      this.showQuickScanDescription = false;
      if (this.documentClickEventRegister) {
        document.removeEventListener('click', this.offClickHandler.bind(this));
        this.documentClickEventRegister = false;
      }
    }
  }

  openQuickDescriptionPopup(id: string, defaultText: string, event) {
    event.stopPropagation();
    // if (!this.documentClickEventRegister) {
    //   document.addEventListener('click', this.offClickHandler.bind(this));
    //   this.documentClickEventRegister = true;
    // }
    if (!this.showQuickScanDescription) {
      this.onClickPortfolioOpenPopup(event);
      this.quickDescriptionId = id;
      this.quickDescriptionDefaultText = defaultText;

      this.openQuickDescription();
    } else {
      this.showQuickScanDescription = false;
      this.quickDescriptionIdCheck = '0';
    }
  }

  onClickPortfolioOpenPopup(event: any) {
    this.createQickDescriptionXPosition = 30;
    this.createQickDescriptionYPosition = 30;
    this.fixedCreateQickDescriptionYPosition =
      this.createQickDescriptionYPosition;
    this.fixedScrollTop = this.currentScrollTop;
  }

  openQuickDescription() {
    if (this.quickDescriptionId !== this.quickDescriptionIdCheck) {
      this.showQuickScanDescription = true;
      this.quickDescriptionIdCheck = this.quickDescriptionId;
    } else {
      this.showQuickScanDescription = false;
      this.quickDescriptionIdCheck = '0';
    }
  }

  onCloseQuickDescriptionPopupEvent() {
    this.showQuickScanDescription = false;
    // this.quickDescriptionIdCheck = '';
  }

  // money flow chart

  offClickHandlerMoneyFlow(event: any) {
    if (
      this.qickDescriptionPopupMoneyFlow &&
      this.qickDescriptionPopupMoneyFlow.nativeElement &&
      !this.qickDescriptionPopupMoneyFlow.nativeElement.contains(event.target)
    ) {
      // check click origin
      this.showQuickScanDescriptionMoneyFlow = false;
      if (this.documentClickEventRegisterMoneyFlow) {
        document.removeEventListener(
          'click',
          this.offClickHandlerMoneyFlow.bind(this)
        );
        this.documentClickEventRegisterMoneyFlow = false;
      }
    }
  }

  openQuickDescriptionPopupMoneyFlow(id: string, defaultText: string, event) {
    event.stopPropagation();
    // if (!this.documentClickEventRegisterMoneyFlow) {
    //   document.addEventListener('click', this.offClickHandlerMoneyFlow.bind(this));
    //   this.documentClickEventRegisterMoneyFlow = true;
    // }
    if (!this.showQuickScanDescriptionMoneyFlow) {
      this.onClickPortfolioOpenPopupMoneyFlow(event);
      this.quickDescriptionIdMoneyFlow = id;
      this.quickDescriptionDefaultTextMoneyFlow = defaultText;

      this.openQuickDescriptionMoneyFlow();
    } else {
      this.showQuickScanDescriptionMoneyFlow = false;
      this.quickDescriptionIdCheckMoneyFlow = '0';
    }
  }

  onClickPortfolioOpenPopupMoneyFlow(event: any) {
    this.createQickDescriptionXPositionMoneyFlow = 30;
    this.createQickDescriptionYPositionMoneyFlow = 30;
    this.fixedCreateQickDescriptionYPositionMoneyFlow =
      this.createQickDescriptionYPositionMoneyFlow;
    //this.fixedScrollTop = this.currentScrollTop;
  }

  openQuickDescriptionMoneyFlow() {
    if (
      this.quickDescriptionIdMoneyFlow !== this.quickDescriptionIdCheckMoneyFlow
    ) {
      this.showQuickScanDescriptionMoneyFlow = true;
      this.quickDescriptionIdCheckMoneyFlow = this.quickDescriptionIdMoneyFlow;
    } else {
      this.showQuickScanDescriptionMoneyFlow = false;
      this.quickDescriptionIdCheckMoneyFlow = '0';
    }
  }

  onCloseQuickDescriptionPopupEventMoneyFlow() {
    this.showQuickScanDescriptionMoneyFlow = false;
    this.quickDescriptionIdCheckMoneyFlow = '';
  }

  // rel strength

  offClickHandlerRelStr(event: any) {
    if (
      this.qickDescriptionPopupRelStr &&
      this.qickDescriptionPopupRelStr.nativeElement &&
      !this.qickDescriptionPopupRelStr.nativeElement.contains(event.target)
    ) {
      // check click origin
      this.showQuickScanDescriptionRelStr = false;
      if (this.documentClickEventRegisterRelStr) {
        document.removeEventListener(
          'click',
          this.offClickHandlerRelStr.bind(this)
        );
        this.documentClickEventRegisterRelStr = false;
        this.showQuickScanDescriptionRelStr = false;
        this.quickDescriptionIdCheckRelStr = '0';
      }
    }
  }

  openQuickDescriptionPopupRelStr(id: string, defaultText: string, event) {
    event.stopPropagation();
    // if (!this.documentClickEventRegisterRelStr) {
    //   document.addEventListener('click', this.offClickHandlerRelStr.bind(this));
    //   this.documentClickEventRegisterRelStr = true;
    // }
    if (!this.showQuickScanDescriptionRelStr) {
      this.onClickPortfolioOpenPopupRelStr(event);
      this.quickDescriptionIdRelStr = id;
      this.quickDescriptionDefaultTextRelStr = defaultText;

      this.openQuickDescriptionRelStr();
    } else {
      this.showQuickScanDescriptionRelStr = false;
      this.quickDescriptionIdCheckRelStr = '0';
    }
  }

  onClickPortfolioOpenPopupRelStr(event: any) {
    this.createQickDescriptionXPositionRelStr = 30;
    this.createQickDescriptionYPositionRelStr = 30;
    this.fixedCreateQickDescriptionYPositionRelStr =
      this.createQickDescriptionYPositionRelStr;
    //this.fixedScrollTop = this.currentScrollTop;
  }

  openQuickDescriptionRelStr() {
    if (this.quickDescriptionIdRelStr !== this.quickDescriptionIdCheckRelStr) {
      this.showQuickScanDescriptionRelStr = true;
      this.quickDescriptionIdCheckRelStr = this.quickDescriptionIdRelStr;
    } else {
      this.showQuickScanDescriptionRelStr = false;
      this.quickDescriptionIdCheckRelStr = '0';
    }
  }

  onCloseQuickDescriptionPopupEventRelStr() {
    this.showQuickScanDescriptionRelStr = false;
    this.quickDescriptionIdCheckRelStr = '';
  }

  calculatePricePerChange(firstClose: number, lastClose: number): number {
    return ((lastClose - firstClose) / firstClose) * 100;
  }

  calculatePriceChange(firstClose: number, lastClose: number): number {
    return lastClose - firstClose;
  }

  prepareMainChart = () => {
    if (this.isETF) {
      this.isDailyWeeklyToggleVisible = false;
    } else {
      if (this.selectedPeriod == '5year' || this.selectedPeriod == '3year') {
        this.isDailyWeeklyToggleVisible = false;
      } else {
        this.isDailyWeeklyToggleVisible = true;
      }
    }
    this.isChartLoaded = true;
    this.chartDataSub = forkJoin([
      this.getHistoricPriceChartData(),
      this.getPerformanceChartData(),
    ]).subscribe(
      ([histChartData, perfChartData]) => {
       this.isChartDataAvailableForSelectedPeriod = true;
       if (histChartData.length === 0 || perfChartData.length === 0) {
          this.isChartDataAvailableForSelectedPeriod = false;
          this.isChartLoaded = true;
          this.isPageLoaded = false;
          this.uiSetting.printChartData = [];
          return;
        }
        let chartData = [];
        let hstartIndex = 0;
        let hendIndex = histChartData.length - 1;
        let pstartIndex = 0;
        let pendIndex = perfChartData.length - 1;

        let shdate: Date = histChartData[0].date;
        let spdate: Date = perfChartData[0].date;
     //   console.log("historic date ", shdate);
    //    console.log("performance chart date ", spdate);
        if (Date.parse(spdate.toString()) > Date.parse(shdate.toString())) {
          let index = histChartData.findIndex(
            (item) =>
              Date.parse(item.date.toString()) == Date.parse(spdate.toString())
          );
          if (index > -1) {
            hstartIndex = index;
          }
        } else {
          let index = perfChartData.findIndex(
            (item) =>
              Date.parse(item.date.toString()) == Date.parse(shdate.toString())
          );
          if (index > -1) {
            pstartIndex = index;
          }
        }

        let ehdate: Date = histChartData[histChartData.length - 1].date;
        let epdate: Date = perfChartData[perfChartData.length - 1].date;
     //   console.log("historic end date ", ehdate);
     //   console.log("performance chart end date ", epdate);
        if (Date.parse(epdate.toString()) < Date.parse(ehdate.toString())) {
          let index = histChartData.findIndex(
            (item) =>
              Date.parse(item.date.toString()) == Date.parse(epdate.toString())
          );
          if (index > -1) {
            hendIndex = index;
          }
        }

        let tHistChartData = histChartData.slice(hstartIndex, hendIndex + 1);
        let tperfChartData = perfChartData.slice(pstartIndex);
    //    console.log("sliced Historic Data ", tHistChartData); 
    //    console.log("sliced perf Data ", tperfChartData);  
        tHistChartData.forEach((hchartData: any, i: number) => {
          let prefData = tperfChartData.find(
            (item) =>
              Date.parse(item.date.toString()) ==
              Date.parse(hchartData.date.toString())
          );
          if (prefData) {
            chartData.push({ ...prefData, ...hchartData });
          }
        });
        let timespanPerChange = this.calculatePricePerChange(
          chartData[0]['closingprice'],
          chartData[chartData.length - 1]['closingprice']
        );
        this.timespanPerChange = SharedUtil.formatNumber(timespanPerChange);
        let timespanPriceChange = this.calculatePriceChange(
          chartData[0]['closingprice'],
          chartData[chartData.length - 1]['closingprice']
        );
        this.timespanPriceChange = SharedUtil.formatNumber(timespanPriceChange);
        if (timespanPerChange > 0) {
          this.changeMovement = true;
        } else {
          this.changeMovement = false;
        }
    //    console.log("chartData");
    //   console.log(chartData);
        this.renderHistoricPriceChart(chartData);
        // this.renderDateAxisChart(chartData);
        // this.uiSetting.printChartData = chartData;
        // this.uiSetting.histPrice ?
        //   this.renderHistoricPriceChart(chartData) :
        //   this.renderPerformanceChart(chartData);
        this.renderDateAxisChart(chartData);

        // setTimeout(() => {
        //   this.isChartLoaded = true;
        //   this.isPageLoaded = true;
        //   this.updateUI();
        //   this.changeDetectorRef.reattach();
        // }, 10);
      },
      (err) => {
        //console.log("Error in chart data api call", err);
      }
    );
  };

  getPerformanceChartData = () => {
    // return new Observable<any>(observer => {

    //     observer.next([]);
    //     observer.complete();

    // });
    return new Observable<any>((observer) => {
      forkJoin([
        this.fetchPerformanceChartData(this.symbolstr),
        this.fetchPerformanceChartData(
          this.uiSetting.selectedPerformanceBenchmark
        ),
      ]).subscribe(
        ([result1, result2]) => {
          let chartData: any = this.generatePerformanceChartData(
            result1,
            result2
          );
          observer.next(chartData);
          observer.complete();
        },
        (err) => {
          observer.error(err);
        }
      );
    });
  };

  private generatePerformanceChartData(
    perData: SectorsPerformanceChart,
    bmPerData: SectorsPerformanceChart
  ) {
    let chartData = [];
    const { dates, points } = perData.data;
    let rdates = dates.slice(); //.reverse();
    let rpoints = points.slice(); //.reverse();
    let rbmPoints = bmPerData.data.points.slice(); //.reverse();
    const len = rdates.length;

    for (var i = 0; i < len; i++) {
      let date = this.ConvertToDate(rdates[i]);
      let point = rpoints[i];
      let bmpoint = 0;
      if (rbmPoints[i]) {
        bmpoint = rbmPoints[i];
        if (isNaN(bmpoint)) {
          bmpoint = 0;
        }
      }
      if (!isNaN(point)) {
        chartData.push({
          date: date,
          point: point,
          bmpoint: bmpoint,
        });
      }
    }

    return chartData;
  }

  private renderHistoricPriceChart(chartData: any[]) {
    let lastClosingPrice = chartData[chartData.length - 1].closingprice;
    let firstClosingPrice = chartData[0].closingprice;
    let lastPoint = chartData[chartData.length - 1].point;
    // if (!this.isETF) {
    //   if (lastClosingPrice > firstClosingPrice) {
    //     this.symbolPriceChange = lastClosingPrice - firstClosingPrice;
    //     this.symbolPriceUp = true;
    //   }
    //   else {
    //     this.symbolPriceChange = firstClosingPrice - lastClosingPrice;
    //     this.symbolPriceUp = false;
    //   }
    //   this.symbolPriceChangePerc = lastPoint;
    // }

    if (this.mainchart) {
      this.mainchart.dispose();
    }

    // Create chart
    let chartId = 'main-chart';
    let chart = am4core.create(chartId, am4charts.XYChart);
    if( this.appDataService.cookiesData.isMobile){
      chart.cursor = new am4charts.XYCursor();
      chart.cursor.behavior = "none";
    }
    chart.data = chartData;
    chart.dateFormatter.dateFormat = 'yyyy-MM-dd';
    chart.padding(0, 5, 0, 5);
    chart.leftAxesContainer.layout = 'vertical';
    chart.rightAxesContainer.layout = 'vertical';
    chart.leftAxesContainer.reverseOrder = true;
    chart.rightAxesContainer.reverseOrder = true;
    chart.seriesContainer.draggable = false;
    chart.seriesContainer.resizable = false;


    // Historic PRICE axis
    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.tooltip.disabled = true;
    valueAxis.height = am4core.percent(50);
    valueAxis.renderer.opposite = true;
    valueAxis.renderer.baseGrid.disabled = true;
    valueAxis.renderer.grid.template.strokeOpacity = 1;
    valueAxis.renderer.grid.template.stroke = am4core.color('#f4f4f4');
    valueAxis.renderer.grid.template.strokeWidth = 1;
    valueAxis.renderer.maxLabelPosition = 0.99;
    valueAxis.renderer.labels.template.dy = -5;
    valueAxis.renderer.labels.template.fontSize = 14;

    // Historic DATE AXIS
    let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    dateAxis.renderer.minGridDistance = 50;
    dateAxis.renderer.axisFills.template.disabled = false;
    dateAxis.renderer.axisFills.template.fillOpacity = 1;
    dateAxis.renderer.axisFills.template.fill = am4core.color('#F8FBFE');
    dateAxis.renderer.grid.template.strokeWidth = 0;
    dateAxis.renderer.minLabelPosition = 0.01;
    dateAxis.renderer.maxLabelPosition = 0.99;
    dateAxis.renderer.labels.template.fontSize = 14;
    dateAxis.skipEmptyPeriods = true;
    //dateAxis.groupData = true;
    dateAxis.baseInterval = {
      timeUnit: 'day',
      count: 1,
    };
    dateAxis.hidden = true;

    // Pinch to zoom diabled
    pinchZoom: false;


    // let months =[];
    // dateAxis.renderer.labels.template.adapter.add("text", (label, target, key) => {
    //  let item = target.dataItem;
    //   // if (target.dataItem &&  label ) {
    //   //   let month = label.split(" ")[0];
    //   //   let item = months.find( lbl => lbl == month);
    //   //   if(!item){
    //   //     months.push(month);
    //   //     return month;
    //   //   }
    //   // }
    //   return label;
    // });

    // dateAxis.gridIntervals.setAll([
    //   { timeUnit: "day", count: 1 }

    // ]);

    // dateAxis.dateFormats.setKey("month", "MMMM");

    let series = chart.series.push(new am4charts.LineSeries());
    series.dataFields.dateX = 'date';
    series.dataFields.valueY = 'closingprice';
    series.name = 'Closingprice';
    series.defaultState.transitionDuration = 0;
    series.fillOpacity = 0.05;
    series.strokeWidth = 1;
    series.minBulletDistance = 10;
    series.tooltipText = 'Close: ${valueY}';
    series.tooltip.pointerOrientation = 'vertical';
    series.tooltip.background.cornerRadius = 3;
    series.tooltip.background.fillOpacity = 0.85;
    series.tooltip.label.padding(6, 12, 4, 12);

    if (lastClosingPrice > firstClosingPrice) {
      series.fill = am4core.color('#13B64D');
      series.stroke = am4core.color('#13B64D');
    } else {
      series.fill = am4core.color('#FD154A');
      series.stroke = am4core.color('#FD154A');
    }

    if (this.uiSetting.includeTermAvg) {
      let series = chart.series.push(new am4charts.LineSeries());
      series.dataFields.valueY = 'chainkinTrend';
      series.dataFields.dateX = 'date';
      series.strokeWidth = 1;
      series.stroke = am4core.color('#67b7dc');
      series.minBulletDistance = 10;
      series.tooltipText = 'LT Trend: ${valueY}';
      series.tooltip.pointerOrientation = 'vertical';
      series.tooltip.background.cornerRadius = 3;
      series.tooltip.background.fillOpacity = 0.85;

      series.tooltip.getFillFromObject = false;
      series.tooltip.background.fill = am4core.color('#67b7dc');
      series.tooltip.label.padding(6, 12, 4, 12);
    }

    // Add cursor
    chart.cursor = new am4charts.XYCursor();
    chart.cursor.behavior = "none";
   // chart.zoomOutButton.set("forceHidden", true);
    // chart.cursor.xAxis = dateAxis;
    // chart.cursor.snapToSeries = series;

    this.renderHistPGRAndRelStrengthInMainChart(chart);

    this.mainchart = chart;
  }

  private renderHistPGRAndRelStrengthInMainChart(chart: any) {
    // Historic PGR chart
    let valueAxis3 = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis3.renderer.cellStartLocation = 0;
    valueAxis3.renderer.cellEndLocation = 1;
    valueAxis3.renderer.labels.template.disabled = true;
    valueAxis3.renderer.baseGrid.disabled = true;
    valueAxis3.renderer.grid.template.strokeWidth = 0;
    valueAxis3.renderer.opposite = true;
    valueAxis3.height = am4core.percent(4);
    valueAxis3.tooltip.disabled = true;
    valueAxis3.marginTop = -15;
    valueAxis3.renderer.labels.template.fontSize = 12;

    // Historic PGR chart
    let series3 = chart.series.push(new am4charts.ColumnSeries());
    series3.dataFields.valueY = 'val';
    series3.dataFields.dateX = 'date';
    series3.yAxis = valueAxis3;
    series3.columns.template.propertyFields.fill = 'color';
    series3.columns.template.propertyFields.stroke = 'color';
    series3.columns.template.width = am4core.percent(100);

    if (this.isETF) {
      // series3.columns.template.tooltipText = "Historic Rating: {pgrtext}";
      series3.columns.template.tooltipText = 'Rating*: {pgrtext}';
    } else {
      if (this.isWeeklyHistoricRating) {
        // series3.columns.template.tooltipText = "Weekly Historic Rating: {pgrtext}";
        // series3.columns.template.tooltipText = "Weekly Rating*: {pgrtext}";
        series3.columns.template.tooltipText = 'Rating*: {pgrtext}';
      } else {
        // series3.columns.template.tooltipText = "Daily Historic Rating: {pgrtext}";
        series3.columns.template.tooltipText = 'Daily Rating*: {pgrtext}';
      }
    }

    // Money flow chart
    let valueAxis4 = chart.yAxes.push(new am4charts.ValueAxis());

    valueAxis4.height = am4core.percent(20);
    valueAxis4.zIndex = 3;
    valueAxis4.marginTop = 30;
    valueAxis4.renderer.baseGrid.disabled = true;
    valueAxis4.renderer.grid.template.strokeWidth = 0;
    valueAxis4.renderer.minGridDistance = 40;
    valueAxis4.baseValue = 0;
    valueAxis4.renderer.opposite = true;
    valueAxis4.renderer.labels.template.fontSize = 12;
    // valueAxis4.min = 0;
    // valueAxis4.max = 1;

    // Money flow chart
    let series4 = chart.series.push(new am4charts.LineSeries());
    series4.dataFields.dateX = 'date';
    series4.dataFields.valueY = 'moneyFlow';
    series4.yAxis = valueAxis4;
    series4.tooltipText = 'Money flow: {valueY}';
    series4.name = 'Money';
    series4.fillOpacity = 0.4;
    series4.fill = am4core.color('#13B64D');
    series4.stroke = am4core.color('#13B64D');
    series4.strokeWidth = 1;
    series4.strokeOpacity = 1;
    series4.minBulletDistance = 10;
    series4.tooltip.pointerOrientation = 'vertical';
    series4.tooltip.background.cornerRadius = 3;
    series4.tooltip.background.fillOpacity = 0.85;
    series4.tooltip.label.padding(6, 12, 4, 12);
    series4.tooltip.getFillFromObject = false;
    series4.tooltip.background.fill = am4core.color('#13B64D');

    valueAxis4.renderer.labels.template.adapter.add(
      'textOutput',
      function (text, target, key) {
        if (text > 0) {
          return 'Strong';
        } else if (text < 0) {
          return 'Weak';
        }
        return text;
      }
    );

    series4.adapter.add('tooltipText', (tooltipText) => {
      series4.tooltipDataItem.dataContext['moneyFlow'] >= 0
        ? (series4.tooltip.background.fill = am4core.color('#13B64D'))
        : (series4.tooltip.background.fill = am4core.color('#FD154A'));

      return tooltipText;
    });
    let range4 = valueAxis4.createSeriesRange(series4);
    range4.value = 0;
    range4.endValue = -1000;
    range4.contents.stroke = am4core.color('#FD154A');
    range4.contents.fill = am4core.color('#FD154A');
    range4.contents.strokeOpacity = 1;
    range4.contents.fillOpacity = 0.4;

    // Rel strength chart
    let valueAxis2 = chart.yAxes.push(new am4charts.ValueAxis());

    valueAxis2.height = am4core.percent(20);
    valueAxis2.zIndex = 3;
    valueAxis2.marginTop = 30;
    valueAxis2.renderer.baseGrid.disabled = true;
    valueAxis2.renderer.grid.template.strokeWidth = 0;
    valueAxis2.renderer.minGridDistance = 40;
    valueAxis2.renderer.labels.template.fontSize = 12;
    valueAxis2.baseValue = 0.5;
    valueAxis2.renderer.opposite = true;
    valueAxis2.min = 0;
    valueAxis2.max = 1;

    // Rel strength chart
    let series2 = chart.series.push(new am4charts.LineSeries());
    series2.dataFields.dateX = 'date';
    series2.dataFields.valueY = 'relativeStrength';
    series2.yAxis = valueAxis2;
    series2.tooltipText = 'Relative strength: {valueY}';
    series2.name = 'Rel';
    series2.fillOpacity = 0.4;
    series2.fill = am4core.color('#13B64D');
    series2.stroke = am4core.color('#13B64D');
    series2.strokeWidth = 1;
    series2.strokeOpacity = 1;
    series2.minBulletDistance = 10;
    series2.tooltip.pointerOrientation = 'vertical';
    series2.tooltip.background.cornerRadius = 3;
    series2.tooltip.background.fillOpacity = 0.85;
    series2.tooltip.label.padding(6, 12, 4, 12);
    series2.tooltip.getFillFromObject = false;
    series2.tooltip.background.fill = am4core.color('#13B64D');

    valueAxis2.renderer.labels.template.adapter.add(
      'textOutput',
      function (text, target, key) {
        if (text > 0.5) {
          return 'Strong';
        } else if (text < 0.5) {
          return 'Weak';
        }
        return text;
      }
    );

    series2.adapter.add('tooltipText', (tooltipText) => {
      series2.tooltipDataItem.dataContext['relativeStrength'] >= 0.5
        ? (series2.tooltip.background.fill = am4core.color('#13B64D'))
        : (series2.tooltip.background.fill = am4core.color('#FD154A'));

      return tooltipText;
    });

    let range = valueAxis2.createSeriesRange(series2);
    range.value = 0.5;
    range.endValue = -1000;
    range.contents.stroke = am4core.color('#FD154A');
    range.contents.fill = am4core.color('#FD154A');
    range.contents.strokeOpacity = 1;
    range.contents.fillOpacity = 0.4;
    setTimeout(() => {
      this.isChartLoaded = false;
    }, 1000);
  }

  onPricePeriodChange(period: string) {
    this.isChartDataAvailableForSelectedPeriod = true;
    this.selectedPeriod = period;
    this.selectedPeriod = period;
    this.prepareMainChart();
    //this.updateUI();
  }

  getHistoricPriceChartData = () => {
    return new Observable<any>((observer) => {
      this.fetchChartData(this.symbolstr).subscribe(
        (data: HistoricPriceChartData) => {
          let histChartData: HistoricPriceChartData =
            this.getSlicedHisPriceData(data);
     //       console.log("Historic Price Data ",histChartData);
          let bmrsData: BenchMarkRelativeStrengthChartData = null;
          let chartData = [];
          if (
            this.uiSetting.selectedRelstrengthBenchmark == 'SPY' ||
            this.uiSetting.selectedRelstrengthBenchmark == 'IWV'
          ) {
            chartData = this.generateHistoricPriceChartData(
              histChartData,
              bmrsData
            );

            observer.next(chartData);
            observer.complete();
          } else {
            this.BMRSChartDataSub =
              this.fetchBenchMarkRelativeStrengthChartData(
                this.symbolstr
              ).subscribe((data: BenchMarkRelativeStrengthChartData) => {
                bmrsData = this.getSlicedBMRSData(data);
                chartData = this.generateHistoricPriceChartData(
                  histChartData,
                  bmrsData
                );
                observer.next(chartData);
                observer.complete();
              });
          }
        }
      );
    });
  };

  private getSlicedBMRSData(
    data: BenchMarkRelativeStrengthChartData
  ): BenchMarkRelativeStrengthChartData {
    let parts = data.dates[0].split('-');
    let year = parseInt(parts[0]);
    let month = parseInt(parts[1]) - 1;
    let day = parseInt(parts[2]);
    let endDate: Date;

    switch (this.selectedPeriod) {
      case '1month':
        endDate = new Date(year, month - 1, day - 1);
        break;
      case '3month':
        endDate = new Date(year, month - 3, day - 1);
        break;
      case '6month':
        endDate = new Date(year, month - 6, day);
        break;
      case 'ytd':
        endDate = new Date(year, 0, 1);
        break;
      case 'year':
        endDate = null;
        break;
      case '3year':
        endDate = new Date(year - 3, month, day);
        break;
      case '5year':
        endDate = null;
        break;
      default:
        break;
    }
    if (endDate == null) {
      return data;
    }

    let newSlicedData: BenchMarkRelativeStrengthChartData =
      new BenchMarkRelativeStrengthChartData();

    newSlicedData.dates = data.dates.filter((strdate) => {
      let pts = strdate.split('-');
      let dt = new Date(
        parseInt(pts[0]),
        parseInt(pts[1]) - 1,
        parseInt(pts[2])
      );
      return dt >= endDate;
    });

    let lastIndex = newSlicedData.dates.length;

    newSlicedData.relative_strength = data.relative_strength.slice(
      0,
      lastIndex
    );

    return newSlicedData;
  }

  private fetchBenchMarkRelativeStrengthChartData(
    symbol: string
  ): Observable<BenchMarkRelativeStrengthChartData> {
    return new Observable<BenchMarkRelativeStrengthChartData>((observer) => {
      let interval = '1D';
      if (this.selectedPeriod === '3year' || this.selectedPeriod === '5year') {
        interval = '1W';
      }
      let benchmarkRelStrenthChartData: BenchMarkRelativeStrengthChartData =
        null;
      // let resHubData: ResearchHubData = this.appDataService.researchHubData;
      benchmarkRelStrenthChartData = null; // resHubData.getBenchMarkRelStrengthData(this.symbolstr, this.uiSetting.selectedRelstrengthBenchmark, interval);

      if (benchmarkRelStrenthChartData) {
        observer.next(benchmarkRelStrenthChartData);
        observer.complete();
      } else {
        this.api
          .getBenchMarkRelativeStrengthChart(
            symbol,
            this.uiSetting.selectedRelstrengthBenchmark,
            interval
          )
          .subscribe(
            (resp: any) => {
              benchmarkRelStrenthChartData =
                new BenchMarkRelativeStrengthChartData();
              benchmarkRelStrenthChartData.dates = resp.dates;
              benchmarkRelStrenthChartData.relative_strength =
                resp.relative_strength;
              benchmarkRelStrenthChartData.symbol = symbol;
              benchmarkRelStrenthChartData.benchmark =
                this.uiSetting.selectedRelstrengthBenchmark;
              benchmarkRelStrenthChartData.interval = interval;
              // resHubData.setBenchMarkRelStrengthData(benchmarkRelStrenthChartData);
              observer.next(benchmarkRelStrenthChartData);
              observer.complete();
            },
            (err) => {
              console.log(
                'Error in fetching getBenchMarkRelativeStrengthChart Api call data '
              );
              observer.error('Error');
            }
          );
      }
    });
  }

  private fetchChartData(symbol: string): Observable<HistoricPriceChartData> {
    return new Observable<HistoricPriceChartData>((observer) => {
      const userDetail: LoginDetailModel = LocalStorage.getItem('userDetail');
      let interval = '1D';
      if (this.selectedPeriod === '3year' || this.selectedPeriod === '5year') {
        interval = '1W';
      }
      let historicChartData: HistoricPriceChartData = null;

      historicChartData = null;

      this.api.getChartData(symbol, interval , userDetail.UID).subscribe(
        (resp) => {
          let dates = resp.dates;
          let prices = resp.slices[0].indicators[4].data;
          let trends = resp.slices[0].indicators[3].data;
          let spyrelativeStrengths = resp.slices[3].indicators[0].data;
          let moneyFlow = resp.slices[1].indicators[0].data;
          let powerGauges = [];
          if (this.isETF) {
            powerGauges = resp.slices[4].indicators[0].data;
          } else {
            if (resp.slices[4].indicators.length) {
              powerGauges = resp.slices[4].indicators[0].data;
            }
          }

          let closingPrices = prices.map(
            (price: string) => price.split(',')[2]
          );
          historicChartData = new HistoricPriceChartData();
          historicChartData.closingprices = closingPrices;
          historicChartData.dates = dates;
          historicChartData.chainkinTrends = trends;
          historicChartData.spyrelativeStrengths = spyrelativeStrengths;
          historicChartData.powerGauges = powerGauges;
          historicChartData.symbol = symbol;
          historicChartData.interval = interval;
          historicChartData.moneyFlow = moneyFlow;

          observer.next(historicChartData);
          observer.complete();
        },
        (err) => {
          console.log('Error in fetching Chart data');
          observer.error('Error');
        }
      );
    });
  }

  private getSlicedHisPriceData(
    data: HistoricPriceChartData
  ): HistoricPriceChartData {
    this.fetchSymbolData().subscribe((resp) => {
      this.priceChangeAndPeriodData(resp.fundamentalData);
    });
    let closingprices = data.closingprices;
   // console.log(closingprices);
    let closeIndex = 0;
    for(let i=0; i<closingprices.length; i++) {
      if (closingprices[i] != 'null') {
        break;
      } else {
        closeIndex++;
      }
    }
    let parts = data.dates[closeIndex].split('-');
    let year = parseInt(parts[0]);
    let month = parseInt(parts[1]) - 1;
    let day = parseInt(parts[2]);
    let endDate: Date;
 //   console.log(parts);
 //   console.log(year, month, day);
    switch (this.selectedPeriod) {
      case '1month':
        endDate = new Date(year, month - 1, day);
        break;
      case '3month':
        endDate = new Date(year, month - 3, day );
        break;
      case '6month':
        endDate = new Date(year, month - 6, day);
        break;
      case 'ytd':
        endDate = new Date(year, 0, 1);
        break;
      case 'year':
        endDate = null;
        break;
      case '3year':
        endDate = new Date(year - 3, month, day);
        break;
      case '5year':
        endDate = null;
        break;
      default:
        break;
    }

    if (endDate == null) {
      return data;
    }

    let newHistChartData: HistoricPriceChartData = new HistoricPriceChartData();

    newHistChartData.dates = data.dates.filter((strdate) => {
      let pts = strdate.split('-');
      let dt = new Date(
        parseInt(pts[0]),
        parseInt(pts[1]) - 1,
        parseInt(pts[2])
      );
      return dt >= endDate;
    });

   
    let lastIndex = newHistChartData.dates.length;
    let startIndex = closeIndex;
    newHistChartData.closingprices = data.closingprices.slice(startIndex, lastIndex);
    newHistChartData.chainkinTrends = data.chainkinTrends.slice(startIndex, lastIndex);
    newHistChartData.spyrelativeStrengths = data.spyrelativeStrengths.slice(
      startIndex,
      lastIndex
    );
    newHistChartData.powerGauges = data.powerGauges.slice(startIndex, lastIndex);
    newHistChartData.moneyFlow = data.moneyFlow.slice(startIndex, lastIndex);
    return newHistChartData;
  }

  private fetchSymbolData(): Observable<SymbolInfo> {
    const userDetail: LoginDetailModel = LocalStorage.getItem('userDetail');
    return new Observable<SymbolInfo>((observer) => {
      let symbolInfo: SymbolInfo;
      let component = 'pgr,metaInfo,fundamentalData,EPSData';
      this.api.getSymbolData(this.symbolstr, component , userDetail.UID ).subscribe(
        (resp) => {
          symbolInfo = new SymbolInfo();
          const metaInfo = resp.metaInfo[0];
          symbolInfo.metaInfo = metaInfo;
          symbolInfo.EPSData = resp.EPSData;
          symbolInfo.fundamentalData = resp.fundamentalData;
          symbolInfo.pgr = resp.pgr;
          symbolInfo.status = resp.status;

          symbolInfo.metaInfo.Percentage = metaInfo['Percentage '];
          symbolInfo.metaInfo.technicalRating = metaInfo['TechnicalRating '];
          symbolInfo.metaInfo.industry_ListID = metaInfo['industry_ListID '];
          symbolInfo.metaInfo.summaryRating = metaInfo['SummaryRating '];
          //resHubData.setSymbolInfo(symbolInfo);
          observer.next(symbolInfo);
          observer.complete();
        },
        (err) => {
          console.log('Error in fetching Symbol data');
          observer.error('Error');
        }
      );
    });
  }

  private priceChangeAndPeriodData(data: FundamentalData) {
    let symbolPriceChangePerc = data
      ? parseFloat(data.year_pct_change)
      : undefined;
    switch (this.selectedPeriod) {
      case '1month':
        this.selectedPeriodText = '1 month';
        symbolPriceChangePerc = data
          ? parseFloat(data.one_month_pct_change)
          : undefined;
        break;
      case '3month':
        this.selectedPeriodText = '3 months';
        symbolPriceChangePerc = data
          ? parseFloat(data.three_month_pct_change)
          : undefined;
        break;
      case '6month':
        this.selectedPeriodText = '6 months';
        symbolPriceChangePerc = data
          ? parseFloat(data.six_month_pct_change)
          : undefined;
        break;
      case 'ytd':
        this.selectedPeriodText = 'YTD';
        symbolPriceChangePerc = data
          ? parseFloat(data.ytd_pct_change)
          : undefined;
        break;
      case 'year':
        this.selectedPeriodText = '1 year';
        symbolPriceChangePerc = data
          ? parseFloat(data.year_pct_change)
          : undefined;
        break;
      case '3year':
        this.selectedPeriodText = '3 years';
        symbolPriceChangePerc = data
          ? parseFloat(data.three_year_pct_change)
          : undefined;
        break;
      case '5year':
        this.selectedPeriodText = '5 years';
        symbolPriceChangePerc = data
          ? parseFloat(data.five_year_pct_change)
          : undefined;
        break;
      default:
        break;
    }

    if(!symbolPriceChangePerc){
      symbolPriceChangePerc =   data ? parseFloat(data.pct_change_from_trade_date) : undefined;
    }
    

    if (symbolPriceChangePerc >= 0) {
      this.symbolPriceUp = true;
    } else {
      this.symbolPriceUp = false;
    }

    
    this.symbolPriceChangePerc = Math.abs(symbolPriceChangePerc);
  }

  private ConvertToDate(strDate: string) {
    let parts = strDate.split('-');
    let year = parseInt(parts[0]);
    let month = parseInt(parts[1]) - 1;
    let day = parseInt(parts[2]);

    let date: Date = new Date(year, month, day);

    return date;
  }

  private generateHistoricPriceChartData(
    historicChartData: HistoricPriceChartData,
    bmrsData: BenchMarkRelativeStrengthChartData
  ) {
    let chartData = [];
    const {
      dates,
      closingprices,
      chainkinTrends,
      spyrelativeStrengths,
      powerGauges,
      moneyFlow,
    } = historicChartData;
    let rdates = dates.slice().reverse();
    let rclosingprices = closingprices.slice().reverse();
    let rchainkinTrends = chainkinTrends.slice().reverse();
    let rpowerGauges = powerGauges.slice().reverse();
    let rmoneyFlow = moneyFlow.slice().reverse();

    let revrelativeStrengths = [];

    if (
      this.uiSetting.selectedRelstrengthBenchmark == 'SPY' ||
      this.uiSetting.selectedRelstrengthBenchmark == 'IWV'
    ) {
      revrelativeStrengths = spyrelativeStrengths.slice().reverse();
    } else {
      const { relative_strength } = bmrsData;
      revrelativeStrengths = relative_strength.slice().reverse();
    }

    const len = rdates.length;
    let color: any = am4core.color('#FFBE00');

    for (var i = 0; i < len; i++) {
      let date = this.ConvertToDate(rdates[i]);
      let closingPrice = parseFloat(rclosingprices[i]);
      let chainkinTrend = parseFloat(rchainkinTrends[i]);
      let rel = parseFloat(revrelativeStrengths[i]);
      let histPGR = parseInt(rpowerGauges[i]);
      let hisMoneyFlow = parseFloat(rmoneyFlow[i]);
      let pgrText = 'none';
      switch (histPGR) {
        case this.pgrsValue.VERY_BULLISH:
          color = am4core.color('#24A300');
          pgrText = 'Very Bullish';
          break;
        case this.pgrsValue.BULLISH:
          color = am4core.color('#24A300');
          pgrText = 'Bullish';
          break;
        case this.pgrsValue.NEUTRAL:
          color = am4core.color('#FFBE00');
          pgrText = 'Neutral';
          break;
        case this.pgrsValue.BEARISH:
          color = am4core.color('#E00000');
          pgrText = 'Bearish';
          break;
        case this.pgrsValue.VERY_BEARISH:
          color = am4core.color('#E00000');
          pgrText = 'Very Bearish';
          break;
        default:
          color = am4core.color('#91989E');
          pgrText = 'none';
          break;
      }

      if (this.uiSetting.includeTermAvg) {
        if (!isNaN(closingPrice) && !isNaN(chainkinTrend)) {
          chartData.push({
            date: date,
            closingprice: closingPrice,
            chainkinTrend: chainkinTrend,
            relativeStrength: rel,
            pgr: histPGR,
            pgrtext: pgrText,
            val: 1,
            color: color,
            moneyFlow: hisMoneyFlow,
          });
        } else if (!isNaN(closingPrice) && isNaN(chainkinTrend)) {
          chartData.push({
            date: date,
            closingprice: closingPrice,
            // chainkinTrend: chainkinTrend,
            relativeStrength: rel,
            pgr: histPGR,
            pgrtext: pgrText,
            val: 1,
            color: color,
            moneyFlow: hisMoneyFlow,
          });
        }
      } else {
        if (!isNaN(closingPrice)) {
          chartData.push({
            date: date,
            closingprice: closingPrice,
            chainkinTrend: chainkinTrend,
            relativeStrength: rel,
            pgr: histPGR,
            pgrtext: pgrText,
            val: 1,
            color: color,
            moneyFlow: hisMoneyFlow,
          });
        }
      }
    }

    return chartData;
  }

  private fetchPerformanceChartData(
    symbol: string
  ): Observable<SectorsPerformanceChart> {
    const userDetail: LoginDetailModel = LocalStorage.getItem('userDetail');
    return new Observable<SectorsPerformanceChart>((observer) => {
      let performanceChartData: SectorsPerformanceChart = null;
      // let resHubData: ResearchHubData = this.appDataService.researchHubData;

      performanceChartData = null; // resHubData.getPerformanceData(symbol, this.selectedPeriod);
      if (performanceChartData) {
        observer.next(performanceChartData);
        observer.complete();
      } else {
        this.api
          .getSectorsPerformanceChart(symbol, this.selectedPeriod , userDetail.UID)
          .subscribe(
            (resp) => {
              let results: SectorsPerformanceChart[] = resp as any;
              if (results.length) {
                let result: SectorsPerformanceChart = results[0];
                result.period = this.selectedPeriod;
                // resHubData.setPerformanceData(result);
                observer.next(result);
                observer.complete();
              } else {
                observer.error('No data fetched');
              }
            },
            (err) => {
              observer.error(err);
            }
          );
      }
    });
  }

  private renderDateAxisChart(chartData: any[]) {
    
    if (this.dateAxisChart) {
      this.dateAxisChart.dispose();
    }
    let chart = am4core.create('datex-chart', am4charts.XYChart);
    chart.data = chartData;

    // chart.dateFormatter.dateFormat = "yyyy-MM-dd";
    // chart.numberFormatter.numberFormat = "#.00";
    //chart.padding(0, 5, 5, 0);

    chart.leftAxesContainer.layout = 'vertical';
    chart.rightAxesContainer.layout = 'vertical';
    chart.leftAxesContainer.reverseOrder = true;
    chart.rightAxesContainer.reverseOrder = true;
    chart.padding(0, 5, 0, 5);

    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.renderer.opposite = true;
    // valueAxis.tooltip.disabled = true;
    valueAxis.height = am4core.percent(0);
    valueAxis.renderer.grid.template.strokeOpacity = 0;
    valueAxis.renderer.grid.template.stroke = am4core.color('#ffffff');
    valueAxis.renderer.grid.template.strokeWidth = 1;
    valueAxis.renderer.maxLabelPosition = 0.99;
    valueAxis.renderer.labels.template.dy = -5;
    valueAxis.renderer.labels.template.fontSize = 14;

    valueAxis.hidden = true;

    let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    
    dateAxis.renderer.minGridDistance = 50;
    dateAxis.renderer.axisFills.template.disabled = false;
    dateAxis.renderer.axisFills.template.fillOpacity = 1;
    dateAxis.renderer.axisFills.template.fill = am4core.color('#ffffff');
    dateAxis.renderer.grid.template.strokeWidth = 0;
    // dateAxis.renderer.labels.template.dy = 5;
    dateAxis.renderer.labels.template.fontSize = 14;
    dateAxis.renderer.minLabelPosition = 0.01;
    dateAxis.renderer.maxLabelPosition = 0.99;
    dateAxis.skipEmptyPeriods = true;
    dateAxis.baseInterval = {
      timeUnit: 'day',
      count: 1,
    };
    // dateAxis.startLocation = 0.5;
    // dateAxis.endLocation = 0.5;
    // dateAxis.renderer.minLabelPosition = 0.05;
    // dateAxis.renderer.maxLabelPosition = 0.95;
    // dateAxis.renderer.inside = true;
    dateAxis.zIndex = 1;
    dateAxis.marginTop = 5;

    // Create series
    let series = chart.series.push(new am4charts.LineSeries());
    series.dataFields.valueY = 'point';
    series.dataFields.dateX = 'date';
    series.fillOpacity = 0;
    series.strokeWidth = 1.5;
    series.minBulletDistance = 10;
    series.tooltipText = 'Performance:{valueY}';
    series.tooltip.pointerOrientation = 'vertical';
    series.tooltip.background.cornerRadius = 3;
    series.tooltip.background.fillOpacity = 0.5;
    series.tooltip.label.fill = am4core.color('#ffffff');
    series.tooltip.label.padding(6, 12, 4, 12);
    series.hidden = true;
    this.dateAxisChart = chart;
  }

  onCheckboxChange(e): void {
    this.dataService.updatePopup(e.target.checked);
  }
}
