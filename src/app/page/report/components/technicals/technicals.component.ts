import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import {
  HeaderDataModel,
  TechnicalDataModel,
} from 'src/app/core/data-models/app-model';
import {
  PriceVolumeContextSummary,
  StockTechnicalDataObject,
  WordpressIdMapping,
} from 'src/app/core/data-models/service-model';
import { APIService } from 'src/app/core/http-services/api.service';
import { DataService } from 'src/app/core/services/data.service';
import {
  LocalStorage,
  SharedUtil,
} from 'src/app/core/services/utilily/utility';

@Component({
  selector: 'app-technical',
  templateUrl: './technicals.component.html',
  styleUrls: ['./technicals.component.scss'],
})
export class TechnicalComponent implements OnInit,OnChanges {
  @Input() priceVolumeContextSummary: PriceVolumeContextSummary =
    new PriceVolumeContextSummary();
  @Input() headerDataModel: HeaderDataModel = new HeaderDataModel();
  technicalDataModel: TechnicalDataModel = new TechnicalDataModel();

  documentClickEventRegister: boolean = false;
  @ViewChild('qickDescriptionPopup', { static: false })
  qickDescriptionPopup: any;
  showQuickScanDescription: boolean = false;
  createQickDescriptionXPosition: any = 0;
  createQickDescriptionYPosition: any = 0;
  fixedCreateQickDescriptionYPosition: any = 0;
  quickDescriptionId: string;
  quickDescriptionIdCheck: string;
  quickDescriptionDefaultText: string;
  wordpressIdMapping: WordpressIdMapping = new WordpressIdMapping();
  fixedScrollTop: any = 0;
  private destroyed$ = new Subject();
  constructor(private api: APIService, private dataService: DataService) {
    //this.getStockTechnicalData();
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.getStockTechnicalData();
  }

  ngOnInit(): void {
    this.dataService.showAllPopup.pipe(takeUntil(this.destroyed$)).subscribe(status => {
      if(status) {
        let techDescription:HTMLElement = document.getElementById('techDescription') as HTMLElement;
        techDescription.click();
       // this.moneyFlow.nativeElement.click();
      } else {
        this.showQuickScanDescription = false;
        this.quickDescriptionIdCheck = '0';
      }
        
    });
  }

  offClickHandler(event: any) {
    if (
      this.qickDescriptionPopup &&
      this.qickDescriptionPopup.nativeElement &&
      !this.qickDescriptionPopup.nativeElement.contains(event.target)
    ) {
      // check click origin
      this.showQuickScanDescription = false;
      if (this.documentClickEventRegister) {
        document.removeEventListener('click', this.offClickHandler.bind(this));
        this.documentClickEventRegister = false;
      }
    }
  }

  openQuickDescriptionPopup(id: string, defaultText: string, event) {
    event.stopPropagation();
    // if (!this.documentClickEventRegister) {
    //   document.addEventListener('click', this.offClickHandler.bind(this));
    //   this.documentClickEventRegister = true;
    // }
    if (!this.showQuickScanDescription) {
      this.onClickPortfolioOpenPopup(event);
    this.quickDescriptionId = id;
    this.quickDescriptionDefaultText = defaultText;
    
    this.openQuickDescription();
    } else {
      this.showQuickScanDescription = false;
      this.quickDescriptionIdCheck = '0';
    }
    
  }

  onClickPortfolioOpenPopup(event: any) {
    this.createQickDescriptionXPosition = -1;
    this.createQickDescriptionYPosition = -1;
    this.fixedCreateQickDescriptionYPosition =
      this.createQickDescriptionYPosition;
    // this.fixedScrollTop = this.currentScrollTop;
  }

  openQuickDescription() {
    if (this.quickDescriptionId !== this.quickDescriptionIdCheck) {
      this.showQuickScanDescription = true;
      this.quickDescriptionIdCheck = this.quickDescriptionId;
    } else {
      this.showQuickScanDescription = false;
      this.quickDescriptionIdCheck = '0';
    }
  }

  onCloseQuickDescriptionPopupEvent() {
    this.showQuickScanDescription = false;
    this.quickDescriptionIdCheck = '';
  }

  getStockTechnicalData(): void {
    let symbol = LocalStorage.getItem('mainSymbol');
    const httpGetTechnicalData$ = this.api.getStockTechnicalData(symbol);
    httpGetTechnicalData$
      .pipe(
        map((resp) => {
          const technicalData = resp as StockTechnicalDataObject;
          return this.prepareTechnicalData(technicalData);
        })
      )
      .subscribe((technicalDataModel: TechnicalDataModel) => {
        this.technicalDataModel = technicalDataModel;
        
      });
  }

  prepareTechnicalData(data: StockTechnicalDataObject): TechnicalDataModel {
    let technicalDataModel = new TechnicalDataModel();
    technicalDataModel.ticker = data.ticker;
    technicalDataModel.chg24weekRelativeToSPY =
      data.priceChangePtc.chg24weekRelativeToSPY;
    let chg24weekRelativeToSPYFormatted = SharedUtil.formatedString(
      technicalDataModel.chg24weekRelativeToSPY
    );
    technicalDataModel.chg24weekRelativeToSPYFormatted =
      this.checkForNaNandAppendSymbol(chg24weekRelativeToSPYFormatted, '%');

    technicalDataModel.chg4weekRelativeToSPY =
      data.priceChangePtc.chg4weekRelativeToSPY;
    let chg4weekRelativeToSPYFormatted = SharedUtil.formatedString(
      technicalDataModel.chg4weekRelativeToSPY
    );
    technicalDataModel.chg4weekRelativeToSPYFormatted =
      this.checkForNaNandAppendSymbol(chg4weekRelativeToSPYFormatted, '%');

    technicalDataModel.beta = data.volatilityRelativeToMarket.beta;
    technicalDataModel.betaFormatted = SharedUtil.formatedString(
      technicalDataModel.beta
    );

    technicalDataModel.volatility = data.volatilityRelativeToMarket.volatility;
    if (isNaN(Number(technicalDataModel.beta))) {
      technicalDataModel.volatilityFormatted = 'NA';
    } else {
      let numberValue = SharedUtil.convertStringToNumber(
        technicalDataModel.beta
      );
      if (numberValue < 1) {
        technicalDataModel.volatilityFormatted = 'Less Volatile';
      } else if (numberValue > 1) {
        technicalDataModel.volatilityFormatted = 'More Volatile';
      } else {
        technicalDataModel.volatilityFormatted = 'Equally Volatile';
      }
    }

    technicalDataModel.avgVolume90Days = data.volumeActivity.avgVolume90Days;
    let avgVolume90Days = SharedUtil.convertStringToNumber(technicalDataModel.avgVolume90Days);
    if(!isNaN(avgVolume90Days)) {
      avgVolume90Days = Math.round(avgVolume90Days);
      technicalDataModel.avgVolume90DaysFormatted = avgVolume90Days.toLocaleString()
    } else {
      let avgVolume90DaysFormatted = SharedUtil.formatedString(
        technicalDataModel.avgVolume90Days
      );
      technicalDataModel.avgVolume90DaysFormatted = avgVolume90DaysFormatted;
    }
    
    technicalDataModel.avgVolume20Days = data.volumeActivity.avgVolume20Days;
    let avgVolume20Days = SharedUtil.convertStringToNumber(technicalDataModel.avgVolume20Days);
    if(!isNaN(avgVolume20Days)) {
      avgVolume20Days = Math.round(avgVolume20Days);
      technicalDataModel.avgVolume20DaysFormatted = avgVolume20Days.toLocaleString()
    } else {
      let avgVolume20DaysFormatted = SharedUtil.formatedString(
        technicalDataModel.avgVolume20Days
      );
      technicalDataModel.avgVolume20DaysFormatted = avgVolume20DaysFormatted;
    }
    

    technicalDataModel.high52Week = data.priceActivity.high52Week;
    technicalDataModel.high52WeekFormatted = SharedUtil.formatedString(
      technicalDataModel.high52Week
    );

    technicalDataModel.low52Week = data.priceActivity.low52Week;
    technicalDataModel.low52WeekFormatted = SharedUtil.formatedString(
        technicalDataModel.low52Week
      );

    return technicalDataModel;
  }

  checkForNaNandAppendSymbol(value: string, symbol: string): string {
    return value === 'NA' ? value : SharedUtil.appendSymbol(value, symbol);
  }
}
