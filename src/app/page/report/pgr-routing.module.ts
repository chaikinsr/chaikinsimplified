import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { PowerGaugeReportComponent } from "./pgr.component";


const routes: Routes = [
    { 
        path: '',
        component: PowerGaugeReportComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: []
})
export class PowerGaugeRoutingModule { }