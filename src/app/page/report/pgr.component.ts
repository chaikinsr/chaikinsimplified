import {
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { map, mergeMap, shareReplay, tap, timeout } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import * as moment from 'moment-timezone';
// import { any } from '@amcharts/amcharts4/.internal/core/utils/Array';
// import { ListDisposer } from '@amcharts/amcharts4/core';
import { CookieService } from 'ngx-cookie-service';

import { APIService } from 'src/app/core/http-services/api.service';
import {
  AuthorizationModel,
  CompanyProfileObject,
  ContextSummaryRootObject,
  EarningsContextSummary,
  ExpertOpnionsContextSummary,
  FinancialContextSummary,
  GetListSymbolsData,
  GetSymbolsDataAccToList,
  LoginDetailModel,
  NavigationListWithPowbar,
  PgrContextSummary,
  PriceVolumeContextSummary,
  SymbolDataRootObject,
  SymbolLookUp,
  SymbolPriceObject,
} from 'src/app/core/data-models/service-model';
import {
  ListNavigationSort,
  LocalStorage,
  SharedUtil,
} from 'src/app/core/services/utilily/utility';
import {
  ContextSummaryDataModel,
  GlobalListWithPowbarModel,
  HeaderDataModel,
  NavigationListWithPowbarModel,
  ProductAccessModel,
  SymbolPriceModel,
  TabbedStructureData,
  UserRoleMappingModel,
} from 'src/app/core/data-models/app-model';
import { DataService } from 'src/app/core/services/data.service';

// Components
import { EditListDialogComponent } from 'src/app/page/shared/components/edit-list-dialog/edit-list-dialog.component';
import { InfoDialogComponent } from 'src/app/page/shared/components/info-dialog/info-dialog.component';
import { ToastService } from 'src/app/core/services/toast-service';
import { AppDataService } from 'src/app/core/services/app-data.service';
import { AuthService } from 'src/app/core/http-services/auth.service';
import { ThisReceiver, ThrowStmt } from '@angular/compiler';




@Component({
  selector: 'app-power-gauge-report',
  templateUrl: './pgr.component.html',
  styleUrls: ['./pgr.component.scss'],
})
export class PowerGaugeReportComponent implements OnInit, OnChanges, OnDestroy {
  userDetail: LoginDetailModel;
  headerDataModel: HeaderDataModel = new HeaderDataModel();
  pgrContextSummary: PgrContextSummary = new PgrContextSummary();
  financialContextSummary = new FinancialContextSummary();
  earningsContextSummary = new EarningsContextSummary();
  priceVolumeContextSummary = new PriceVolumeContextSummary();
  expertOpnionsContextSummary = new ExpertOpnionsContextSummary();
  companyProfileObject = new CompanyProfileObject();
  reportDate: string;
  userId: string;
  productDataValue: any;
  productsList: any;
  symbolCsv: any;
  accessPgrSectionAccordingToPubCode: boolean = false;
  productsAccessCount: any;
  productPopUpShow: boolean = false;
  //@Input() setProductPopUpValue :boolean
  @ViewChild('symbolSearch', { static: false }) searchElement: ElementRef;
  symbolLookupData: SymbolLookUp[] = [];
  etfSymbolLookupData: SymbolLookUp[] = [];
  StockSymbolLookupData: SymbolLookUp[] = [];
  searchSymbolText = '';
  selectedSymbol: string;
  showSearch = false;

  //@ViewChild('setProductPopUpValue',{static: false })setProductPopUpValue:any;
  @ViewChild('logoutPopup', { static: false }) logoutPopup: any;
  showLogoutPopup: boolean = false;
  documentClickEventRegister: boolean = false;
  createLogoutXPosition: any = 0;
  createLogoutYPosition: any = 0;
  fixedLogoutYPosition: any = 0;
  setInterval: any;
  totalProductAccessArray = [];
  env: string = environment.env;
  pulseChaikinDataModel: GlobalListWithPowbarModel = new GlobalListWithPowbarModel();
  watchlistDataModel: GlobalListWithPowbarModel =
    new GlobalListWithPowbarModel();
  selectedListIdData = {
    listName: 'My Stocks',
    listId: 1420416,
    symbolCount: 11,
  };
  getListSymbolData = new GetListSymbolsData();
  productAccessModel: ProductAccessModel;
  userAccess: UserRoleMappingModel;
  tabbedStructureData =  new TabbedStructureData()

  constructor(private api: APIService, private cookieService: CookieService, private dataService: DataService, public auth: AuthService,
    public appDataService: AppDataService, private router: Router, public dialog: MatDialog
    , public toastService: ToastService) {
    if (!this.userDetail) {
      this.userDetail = this.getUserDetail();
    }
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.tabbedStructureData.activeTabForHome = false;
    // setTimeout(() => {
    // let storeMe = {
    //   myBool: false
    // }
    // localStorage.setItem('projectAccess', JSON.stringify(storeMe));
    // },20000);
    // this.dataService.setProjectAccess(false);
  }
  mobileTooltipFlag = false;

  ngOnDestroy() { }
  ngOnInit(): void {
    this.tabbedStructureData.activeTabForHome = false;
    this.pulseChaikinDataModel = new GlobalListWithPowbarModel();
    if (this.appDataService.cookiesData.isMobile) {
      this.mobileTooltipFlag = true;
      setTimeout(() => {
        this.mobileTooltipFlag = false;
      }, 5000);
    }

    let listId = LocalStorage.getItem('listId');
    if(listId === -1){
      this.selectRecentViewed();
    }
    this.userAccess = this.appDataService.userRoleMappingModel;

    // this.productAccessModel = this.appDataService.productAccessModel;
    // this.productsList = this.appDataService.productsListModel;
    // this.totalProductAccessArray = this.appDataService.productAccessModel.totalProductAccessArray[0];
    // if (!this.appDataService.productAccessModel.report) {
    //   this.logOut();
    // };
    if (!this.userAccess.userPermission.reportPage) {
      this.logOut();
    };

    setTimeout(() => {
      if (this.searchElement) {
        this.searchElement.nativeElement.focus();
      }
    }, 0);

    if(this.userAccess.userPermission.chaikinList){
      this.getPulseChaikinListData();
    }
    // if(this.productAccessModel && this.productAccessModel.productsSubscriptionList.length === 2){
    //   this.getPulseChaikinListData();
    // }
    this.getUserWatchlistData();

    this.init();

  }

  onClickedOutside(e: Event) {
   // console.log("outside");
    this.mobileTooltipFlag = false;
  }

  getUserDetail(): LoginDetailModel {
    const userDetail: LoginDetailModel = LocalStorage.getItem('userDetail');
    return userDetail;
  }

  init(): void {
    this.reportDate = moment()
      .tz('America/New_York')
      .format('MM/DD/YY HH:mmA z');
    this.getSymbolData();
    this.getPgrDataAndContextSummary();
    this.router.navigate(['/']);
  }

  offClickHandler(event: any) {
    if (
      this.logoutPopup &&
      this.logoutPopup.nativeElement &&
      !this.logoutPopup.nativeElement.contains(event.target)
    ) {
      // check click origin
      this.showLogoutPopup = false;
      if (this.documentClickEventRegister) {
        document.removeEventListener('click', this.offClickHandler.bind(this));
        this.documentClickEventRegister = false;
      }
    }
  }


  getPulseChaikinListData(): void {
    const httpPulseChaikinList$ = this.api.getPulseChaikinList();
    httpPulseChaikinList$
      .pipe(
        map((resp) => {

          if (resp.status) {
            const listData = resp.data as NavigationListWithPowbar[];
            return this.perparePulseChaikinListData(listData);
          }
          return [];
        })
      )
      .subscribe((pulseList: GlobalListWithPowbarModel) => {
        this.pulseChaikinDataModel.data.push(pulseList.data[0]);
        this.deleteDuplicateValueFRomArray()
        this.pulseChaikinDataModel.title = pulseList.title;
      //  this.pulseChaikinDataModel = pulseList;
      });
  }

  getUserWatchlistData(): void {
    const httpUserWatchlist$ = this.api.getUserWatchlistData(this.userDetail.UID);
    httpUserWatchlist$
      .pipe(
        map((resp) => {
          if (resp.status) {
            const userWatchlistData = resp.data as NavigationListWithPowbar[];
            this.appDataService.userList = resp.data;
            return this.perpareUserWatchlistData(userWatchlistData);
          }
          return [];
        })
      )
      .subscribe((userList: GlobalListWithPowbarModel) => {
       // this.appDataService.userList = userList;
        // console.log("userList",userList);
        // console.log( this.selectedListIdData);
      });
  }

  perparePulseChaikinListData(listData: NavigationListWithPowbar[]): GlobalListWithPowbarModel {
    let pulseChaikinDataModel = new GlobalListWithPowbarModel();
    pulseChaikinDataModel.data = [];
    let listId = LocalStorage.getItem('listId');
    if (listData) {
      listData.forEach(data => {
        const navigationListWithPowbarModel = new NavigationListWithPowbarModel();
        if (+listId === data.listId) {
          this.selectedListIdData.listId = data.listId;
          this.selectedListIdData.listName = data.listName;
          this.selectedListIdData.symbolCount = data.symbolCount;

          // this.setPowerBarWidth(data.powerBar);
        }
        navigationListWithPowbarModel.listId = data.listId;
        navigationListWithPowbarModel.hasChart = data.hasChart;
        navigationListWithPowbarModel.isExtenable = data.isExtenable;
        navigationListWithPowbarModel.description = data.description;
        navigationListWithPowbarModel.listName = data.listName;
        navigationListWithPowbarModel.listType = data.listType;
        navigationListWithPowbarModel.symbolCount = data.symbolCount;
        navigationListWithPowbarModel.powerBar = data.powerBar;
        navigationListWithPowbarModel.extendedData = undefined;
        navigationListWithPowbarModel.powerBarBullish = data.powerBar.green;
        navigationListWithPowbarModel.listNameFormatted = data.listName.toLowerCase();
        pulseChaikinDataModel.data.push(navigationListWithPowbarModel);
      });

    }
    return pulseChaikinDataModel;
  }


  perpareUserWatchlistData(
    listData: NavigationListWithPowbar[]
  ): GlobalListWithPowbarModel {
    this.watchlistDataModel.data = [];

    if (listData) {
      let listId = LocalStorage.getItem('listId');
      listData.forEach((data) => {
        const navigationListWithPowbarModel =
          new NavigationListWithPowbarModel();
        const userDetails = LocalStorage.getItem('userDetail');

        if (+listId === data.listId) {
          this.selectedListIdData.listId = data.listId;
          this.selectedListIdData.listName = data.listName;
          this.selectedListIdData.symbolCount = data.symbolCount;

          // this.setPowerBarWidth(data.powerBar);
        }

       
        // console.log( this.selectedListIdData);
        navigationListWithPowbarModel.listId = data.listId;
        navigationListWithPowbarModel.hasChart = data.hasChart;
        navigationListWithPowbarModel.isExtenable = data.isExtenable;
        navigationListWithPowbarModel.description = data.description;
        navigationListWithPowbarModel.listName = data.listName;
        navigationListWithPowbarModel.listType = data.listType;
        navigationListWithPowbarModel.symbolCount = data.symbolCount;
        navigationListWithPowbarModel.powerBar = data.powerBar;
        navigationListWithPowbarModel.extendedData = undefined;
        navigationListWithPowbarModel.powerBarBullish = data.powerBar.green;
        navigationListWithPowbarModel.listNameFormatted =
          data.listName.toLowerCase();
        navigationListWithPowbarModel.powerBar = data.powerBar;
        navigationListWithPowbarModel.priorityList = data.priorityList;
        if (data.priorityList){
           this.pulseChaikinDataModel.data.push(navigationListWithPowbarModel);
        //  this.pulseChaikinDataModel.data.push(navigationListWithPowbarModel);
        }else{
          this.watchlistDataModel.data.push(navigationListWithPowbarModel);
        }
      });
    //  this.pulseChaikinDataModel.data =  this.pulseChaikinDataModel.data.filter((element, index, array) => array.indexOf(element) !== index)
    this.deleteDuplicateValueFRomArray()
      if (+listId === -1) {
        // this.selectedListIdData.listId = -1;
        this.selectRecentViewed();
      } else {
        this.getListSymbolsData("");
      }

    }
    const sortedData = ListNavigationSort.listNavSorting(
      this.watchlistDataModel.data,
      'listName'
    );
    this.watchlistDataModel.data = sortedData;
    this.watchlistDataModel.title = 'My Lists';
    // console.log(this.watchlistDataModel);

    return this.watchlistDataModel;
  }

  deleteDuplicateValueFRomArray():any{
    let originalArray = this.pulseChaikinDataModel.data
    let prop = 'listName'
       var newArray = [];
       var lookupObject  = {};
   
       for(var i in originalArray) {
          lookupObject[originalArray[i][prop]] = originalArray[i];
       }
   
       for(i in lookupObject) {
           newArray.push(lookupObject[i]);
       }
       this.pulseChaikinDataModel.data = newArray
  }

  loadWatchlistData(loadWatchlistData): void {
    //  this.selectedListIdData = loadWatchlistData;
    this.selectedListIdData.listId = loadWatchlistData.listId;
    this.appDataService.priceMovementRenderCount = 0;
    this.selectedListIdData.listName = loadWatchlistData.listName;
    this.selectedListIdData.symbolCount = loadWatchlistData.symbolCount;
    LocalStorage.setItem('listId', loadWatchlistData.listId);
    this.getListSymbolsData("loadFirstSymbol");
    // this.selectedWatchListId.emit(loadWatchlistData);
  }

  selectRecentViewed(): void {
    this.selectedListIdData.listId = -1;
    this.selectedListIdData.listName = "Recently Viewed";
    let recentlyViewSymbol = LocalStorage.getItem('PGR_RecentlyViewedSymbols');
    let csvSymbols = recentlyViewSymbol.join(',');
    this.getTicketCsvPriceData(csvSymbols);
  }

  getTicketCsvPriceData(csvSymbols: string): void {
    //  console.log(csvSymbols);
    const httpCsvPriceUpdate$ = this.api.getSymbolsCSVPrice(csvSymbols, this.userDetail.UID);
    httpCsvPriceUpdate$
      .pipe(
        map((resp) => {
          if (resp.length >= 0) {
            const priceData = resp as SymbolPriceObject[];
            return this.perparePriceData(priceData);
          }
          return [];
        })
      )
      .subscribe((data: SymbolPriceModel[]) => {
        LocalStorage.setItem('mainSymbol', data[0].symbol);
        this.init();
        this.getListSymbolData.symbols = data;
      });
    // this.api.getSymbolsCSVPrice(csvSymbols).subscribe(resp => {
    //   console.log(resp);;
    // });
  }

  perparePriceData(priceData: SymbolPriceObject[]): SymbolPriceModel[] {
    let symbolPriceData: SymbolPriceModel[] = [];
    priceData.forEach(element => {
      let data = new SymbolPriceModel();

      data.last = element.Last;
      let lastFormattedString = SharedUtil.formatNumber(data.last);
      data.lastFormattedString = SharedUtil.appendSymbol(
        lastFormattedString,
        '$'
      );
      data.rawPgr = element.raw_pgr;
      data.symbol = element.Symbol;
      data.pgrRating = element.corrected_pgr;
      data.name = element.company;
      data.change = element.Change;
      data.percentage = element['Percentage '];
      data.isEtf = element.is_etf;

      data.pgr = SharedUtil.getPgrRating(
        data.pgrRating,
        data.rawPgr
      );
      data.ratingType = SharedUtil.getPgrNameWithoutNeutralValues(
        data.pgr
      );
      data.ratingTypeColorLabel =
        SharedUtil.getColorForRating(data.ratingType) + '-label';
      data.ratingTypeColorBg =
        SharedUtil.getColorForRating(data.ratingType) + '-bg';
      data.changeFormattedString = SharedUtil.formatNumber(
        data.change
      );


      let percentageChangeFormattedString = SharedUtil.formatNumber(
        data.percentage
      );
      data.percentageChangeFormattedString = SharedUtil.appendSymbol(
        percentageChangeFormattedString,
        '%'
      );
      if (percentageChangeFormattedString !== 'NA') {
        if (data.percentage > 0) {
          data.percentageChangeFormattedString =
            '+' + data.percentageChangeFormattedString + '';
          data.percentageChangeColor = 'green';
        } else if (data.percentage < 0) {
          data.percentageChangeFormattedString =
            '' + data.percentageChangeFormattedString + '';
          data.percentageChangeColor = 'red';
        } else {
          data.percentageChangeFormattedString =
            '' + data.percentageChangeFormattedString + '';
          data.percentageChangeColor = 'grey';
        }
      }
      data.pgrImageSrc = SharedUtil.getPgrArcWithoutNeutralValue(
        data.pgrRating,
        data.rawPgr,
        data.isEtf
      );

      symbolPriceData.push(data);
    });

    return symbolPriceData;
  }

  openLogoutPopup(e: any) {
    this.productPopUpShow = !this.productPopUpShow;
    e.stopPropagation();
    this.userId = localStorage.getItem('email');
    if (!this.documentClickEventRegister) {
      document.addEventListener('click', this.offClickHandler.bind(this));
      this.documentClickEventRegister = true;
      this.onClickLogoutOpenPopup(e);
      this.openLogoutPopupForAccount();
    }
  }
  onClickLogoutOpenPopup(event: any) {
    this.createLogoutXPosition = event.clientX - 150;
    this.createLogoutYPosition = event.clientY + 20;
    // setTimeout(() => {
    //   let box = document.getElementById("logoutPopupComponent").offsetWidth
    // console.log(box);
    // this.createLogoutXPosition = event.clientX - box;
    // this.createLogoutYPosition = event.clientY + 20;
    // }, 50);

    // this.fixedLogoutYPosition = this.createLogoutYPosition;
  }

  openLogoutPopupForAccount() {
    this.showLogoutPopup = true;
  }

  onCloseLogOutPopupEvent() {
    this.showLogoutPopup = false;
  }

  getWatchListUpdate(event) {
    if(event){
    this.selectedListIdData.listId = event;
    this.getUserWatchlistData();
    }
  }

  getPgrDataAndContextSummary(): void {
    let symbol = LocalStorage.getItem('mainSymbol');
    let industry = null;
    const httpGetContextSummaryData$ = this.api.getContextSummaryData(
      symbol,
      industry
    );
    httpGetContextSummaryData$
      .pipe(
        map((resp) => {
          const summaryData = resp as ContextSummaryRootObject;
          return this.prepareContextSummaryData(summaryData);
        })
      )
      .subscribe((summaryData: ContextSummaryDataModel) => {
        this.pgrContextSummary = Object.assign(
          {},
          summaryData.pgrContextSummary
        );
        this.financialContextSummary = Object.assign(
          {},
          summaryData.financialContextSummary
        );
        this.earningsContextSummary = Object.assign(
          {},
          summaryData.earningsContextSummary
        );
        this.priceVolumeContextSummary = Object.assign(
          {},
          summaryData.priceVolumeContextSummary
        );
        this.expertOpnionsContextSummary = Object.assign(
          {},
          summaryData.expertOpnionsContextSummary
        );
      });
  }

  prepareContextSummaryData(
      data: ContextSummaryRootObject
    ): ContextSummaryDataModel {
    let contextSummaryDataMadel = new ContextSummaryDataModel();
    contextSummaryDataMadel.pgrContextSummary = data.pgrContextSummary[0];
    contextSummaryDataMadel.pgrContextSummary.mainSentence =
      contextSummaryDataMadel.pgrContextSummary.mainSentence.replace(
        /<TRADEMARK>/g,
        ''
      );
    contextSummaryDataMadel.financialContextSummary =
      data.financialContextSummary[0];
    contextSummaryDataMadel.financialRatingBgColorClass =
      SharedUtil.getColorForRating(
        contextSummaryDataMadel.financialContextSummary.status
      ) + '-bg';
    contextSummaryDataMadel.financialRatingLableColorClass =
      SharedUtil.getColorForRating(
        contextSummaryDataMadel.financialContextSummary.status
      ) + '-label';
    contextSummaryDataMadel.financialContextSummary.financialRatingBgColorClass =
      contextSummaryDataMadel.financialRatingBgColorClass;
    contextSummaryDataMadel.financialContextSummary.financialRatingLableColorClass =
      contextSummaryDataMadel.financialRatingLableColorClass;

    contextSummaryDataMadel.earningsContextSummary =
      data.earningsContextSummary[0];
    contextSummaryDataMadel.earningRatingBgColorClass =
      SharedUtil.getColorForRating(
        contextSummaryDataMadel.earningsContextSummary.status
      ) + '-bg';
    contextSummaryDataMadel.earningRatingLableColorClass =
      SharedUtil.getColorForRating(
        contextSummaryDataMadel.earningsContextSummary.status
      ) + '-label';
    contextSummaryDataMadel.earningsContextSummary.earningRatingBgColorClass =
      contextSummaryDataMadel.earningRatingBgColorClass;
    contextSummaryDataMadel.earningsContextSummary.earningRatingLableColorClass =
      contextSummaryDataMadel.earningRatingLableColorClass;

    contextSummaryDataMadel.priceVolumeContextSummary =
      data.priceVolumeContextSummary[0];
    contextSummaryDataMadel.priceVolumeRatingBgColorClass =
      SharedUtil.getColorForRating(
        contextSummaryDataMadel.priceVolumeContextSummary.status
      ) + '-bg';
    contextSummaryDataMadel.priceVolumeRatingLableColorClass =
      SharedUtil.getColorForRating(
        contextSummaryDataMadel.priceVolumeContextSummary.status
      ) + '-label';
    contextSummaryDataMadel.priceVolumeContextSummary.priceVolumeRatingBgColorClass =
      contextSummaryDataMadel.priceVolumeRatingBgColorClass;
    contextSummaryDataMadel.priceVolumeContextSummary.priceVolumeRatingLableColorClass =
      contextSummaryDataMadel.priceVolumeRatingLableColorClass;

    contextSummaryDataMadel.expertOpnionsContextSummary =
      data.expertOpnionsContextSummary[0];
    contextSummaryDataMadel.expertRatingBgColorClass =
      SharedUtil.getColorForRating(
        contextSummaryDataMadel.expertOpnionsContextSummary.status
      ) + '-bg';
    contextSummaryDataMadel.expertRatingLableColorClass =
      SharedUtil.getColorForRating(
        contextSummaryDataMadel.expertOpnionsContextSummary.status
      ) + '-label';
    contextSummaryDataMadel.expertOpnionsContextSummary.expertRatingBgColorClass =
      contextSummaryDataMadel.expertRatingBgColorClass;
    contextSummaryDataMadel.expertOpnionsContextSummary.expertRatingLableColorClass =
      contextSummaryDataMadel.expertRatingLableColorClass;

    return contextSummaryDataMadel;
  }

  getSymbolData(): void {
    let symbol = LocalStorage.getItem('mainSymbol');
    let components = 'metaInfo,EPSData,pgr';
    const httpGetSymbolData$ = this.api.getSymbolData(symbol, components, this.userDetail.UID);
    httpGetSymbolData$
      .pipe(
        map((resp) => {
          const symbolData = resp as SymbolDataRootObject;
          return this.prepareHeaderData(symbolData);
        })
      )
      .subscribe((symbolData: HeaderDataModel) => {
        this.headerDataModel = Object.assign({}, symbolData);
        this.companyInformation();
      });
  }

  prepareHeaderData(data: SymbolDataRootObject): HeaderDataModel {
    let headerData = new HeaderDataModel();
    headerData.reportDate = 'As of ' + this.reportDate;
    headerData.generalDate = 'As of ' + this.reportDate.split(' ')[0];
    if (data.status === 'success') {
      let metainfo = data.metaInfo[0];
      let epsData = data.EPSData;
      let pgr = data.pgr;
      headerData.pgrRating = metainfo.PGR;
      headerData.rawPgrRating = metainfo.raw_PGR;
      headerData.pgr = SharedUtil.getPgrRating(
        headerData.pgrRating,
        headerData.rawPgrRating
      );
      headerData.ratingType = SharedUtil.getPgrNameWithoutNeutralValues(
        headerData.pgr
      );
      headerData.ratingTypeColorLabel =
        SharedUtil.getColorForRating(headerData.ratingType) + '-label';
      headerData.ratingTypeColorBg =
        SharedUtil.getColorForRating(headerData.ratingType) + '-bg';
      headerData.symbol = metainfo.symbol;
      headerData.name = metainfo.name;
      headerData.isEtf = metainfo.is_etf_symbol;
      headerData.last = metainfo.Last;
      headerData.listRating = metainfo.listRating;
      // isNaN(headerData.listRating)
      headerData.imgSrcBind = './assets/industry-icons/'+metainfo['industry_logo_name']+'.svg'
      headerData.svgColorSet = headerData.listRating > 50 ? 'svg-greencolor-set' : 'svg-redcolor-set' ;

      headerData.industryColor = (!metainfo.industry_name)
        ? 'grey'
        : headerData.listRating > 50
          ? 'green'
          : 'red';
      let lastFormattedString = SharedUtil.formatNumber(headerData.last);
      headerData.lastFormattedString = SharedUtil.appendSymbol(
        lastFormattedString,
        '$'
      );
      headerData.change = metainfo.Change;
      headerData.changeFormattedString = SharedUtil.formatNumber(
        headerData.change
      );
      headerData.percentageChange = metainfo['Percentage '];
      let percentageChangeFormattedString = SharedUtil.formatNumber(
        headerData.percentageChange
      );
      headerData.percentageChangeFormattedString = SharedUtil.appendSymbol(
        percentageChangeFormattedString,
        '%'
      );
      if (percentageChangeFormattedString !== 'NA') {
        if (headerData.percentageChange > 0) {
          headerData.percentageChangeFormattedString =
            '(+' + headerData.percentageChangeFormattedString + ')';
          headerData.percentageChangeColor = 'green';
        } else if (headerData.percentageChange < 0) {
          headerData.percentageChangeFormattedString =
            '(-' + headerData.percentageChangeFormattedString + ')';
          headerData.percentageChangeColor = 'red';
        } else {
          headerData.percentageChangeFormattedString =
            '(' + headerData.percentageChangeFormattedString + ')';
          headerData.percentageChangeColor = 'grey';
        }
      }
      headerData.pgrImageSrc = SharedUtil.getPgrArcWithoutNeutralValue(
        headerData.pgrRating,
        headerData.rawPgrRating,
        headerData.isEtf
      );
      headerData.pgrImageSrcGradient = SharedUtil.getPgrGradientArcWithoutNeutralValue(   
        headerData.pgrRating,
        headerData.rawPgrRating,
        headerData.isEtf
      );

      const powerBarArr = pgr[6]['power_bar'];
      headerData.red = +powerBarArr[0];
      headerData.yellow = +powerBarArr[1];
      headerData.green = +powerBarArr[2];
      this.setPowerBarWidth(
        headerData.red,
        headerData.yellow,
        headerData.green,
        headerData
      );

      headerData.industry = (metainfo.industry_name) ? metainfo.industry_name : "None";
      headerData.financialRating = pgr[1]['Financials'][0]['Value'];
      headerData.earningRating = pgr[2]['Earnings'][0]['Value'];
      headerData.technicalRating = pgr[3]['Technicals'][0]['Value'];
      headerData.expertRating = pgr[4]['Experts'][0]['Value'];

      let financial = pgr[1]['Financials'];
      headerData.financialDebtToEquity = financial[1]['LT Debt to Equity'];
      headerData.financialPriceToBook = financial[2]['Price to Book'];
      headerData.financialReturnOnEquity = financial[3]['Return on Equity'];
      headerData.financialPriceToSales = financial[4]['Price to Sales'];
      headerData.financialFreeCashFlow = financial[5]['Free Cash Flow'];

      let earning = pgr[2]['Earnings'];
      headerData.earningGrowth = earning[1]['Earnings Growth'];
      headerData.earningSurprise = earning[2]['Earnings Surprise'];
      headerData.earningTrend = earning[3]['Earnings Trend'];
      headerData.earningProfitToEarning = earning[4]['Projected P/E'];
      headerData.earningConsistency = earning[5]['Earnings Consistency'];

      let technical = pgr[3]['Technicals'];
      headerData.technicalRelativeStrengthVsMarket =
        technical[1]['Rel Strength vs Market'];
      headerData.technicalChaikinMoneyFlow = technical[2]['Chaikin Money Flow'];
      headerData.technicalPriceStrength = technical[3]['Price Strength'];
      headerData.technicalPriceTrendRoc = technical[4]['Price Trend ROC'];
      headerData.technicalVolumeTrend = technical[5]['Volume Trend'];

      let expert = pgr[4]['Experts'];
      headerData.expertEstimateTrend = expert[1]['Estimate Trend'];
      headerData.expertShortInterest = expert[2]['Short Interest'];
      headerData.expertInsiderActivity = expert[3]['Insider Activity'];
      headerData.expertRatingTrend = expert[4]['Analyst Rating Trend'];
      headerData.expertIndustrialRelStrength =
        expert[5]['Industry Rel Strength'];

      headerData.lastReportDate = data.EPSData.last_report_date;
      headerData.nextReportDate = data.EPSData.next_report_date;
      headerData.lastQuarterConsensusEstimate =
        data.EPSData.last_quarter_consensus_estimate;
      headerData.epsDiffDescription = data.EPSData.eps_diff_description;
      headerData.currentQuarter = data.EPSData.current_quarter;
      headerData.currentQuarterConsensusEstimate =
        data.EPSData.current_quarter_consensus_estimate;
      headerData.earningsReportInfo = data.EPSData.earnings_report_info;
      headerData.preReportInfo = data.EPSData.pre_report_info;
      headerData.estimateRevisionInfo = data.EPSData.estimate_revision_info;
    }
    return headerData;
  }

  getListSymbolsData(symbolData: string): void {
    this.api
      .getListSymbols(this.selectedListIdData.listId, this.userDetail.UID)
      .subscribe((resp) => {
        // let getListSymbolsData = new GetListSymbolsData
        this.getListSymbolData.PowerBar = resp.PowerBar;
        this.getListSymbolData.listRating = resp.listRating;
        this.getListSymbolData.list_id = resp.list_id;
        //const symbol  = this.prepareListSymbolData(resp.symbols);
        this.getListSymbolData.symbols = this.prepareListSymbolData(
          resp.symbols
        );
        if (symbolData) {

          if (symbolData === 'loadFirstSymbol') {
            LocalStorage.setItem('mainSymbol', this.getListSymbolData.symbols[0].symbol);
            this.init();
          }
        }
        // console.log(resp);
      });
  }

  prepareListSymbolData(data): GetSymbolsDataAccToList[] {
    let getSymbolsDataAccToListArray: GetSymbolsDataAccToList[] = [];
    let a = [];
    data.forEach((data) => {
      const getSymbolsDataAccToListObject = new GetSymbolsDataAccToList();
      getSymbolsDataAccToListObject.symbol = data.symbol;
      a.push(data.symbol);
      getSymbolsDataAccToListObject.Change = data.Change;

      getSymbolsDataAccToListObject.Last = data.Last;
      let lastFormattedString = SharedUtil.formatNumber(data.Last);
      getSymbolsDataAccToListObject.lastFormattedString = SharedUtil.appendSymbol(
        lastFormattedString,
        '$'
      );
      getSymbolsDataAccToListObject.PGR = data.PGR;
      getSymbolsDataAccToListObject.Percentage = data['Percentage '];
      let percentageChangeFormattedString = SharedUtil.formatNumber(
        getSymbolsDataAccToListObject.Percentage
      );
      getSymbolsDataAccToListObject.percentageChangeFormattedString = SharedUtil.appendSymbol(
        percentageChangeFormattedString,
        '%'
      );
      if (percentageChangeFormattedString !== 'NA') {
        if (getSymbolsDataAccToListObject.Percentage > 0) {
          getSymbolsDataAccToListObject.percentageChangeFormattedString =
            '+' + getSymbolsDataAccToListObject.percentageChangeFormattedString;
          getSymbolsDataAccToListObject.percentageChangeColor = 'green';
        } else if (getSymbolsDataAccToListObject.Percentage < 0) {
          getSymbolsDataAccToListObject.percentageChangeFormattedString =
            '' + getSymbolsDataAccToListObject.percentageChangeFormattedString;
          getSymbolsDataAccToListObject.percentageChangeColor = 'red';
        } else {
          getSymbolsDataAccToListObject.percentageChangeFormattedString =
            '' + getSymbolsDataAccToListObject.percentageChangeFormattedString;
          getSymbolsDataAccToListObject.percentageChangeColor = 'grey';
        }
      }
      getSymbolsDataAccToListObject.SummaryRating = data.SummaryRating;
      getSymbolsDataAccToListObject.TechnicalRating = data.TechnicalRating;
      getSymbolsDataAccToListObject.div_yield = data.div_yield;
      getSymbolsDataAccToListObject.filter = data.filter;
      getSymbolsDataAccToListObject.industry_ListID = data.industry_ListID;
      getSymbolsDataAccToListObject.industry_name = data.industry_name;
      getSymbolsDataAccToListObject.isNoteExist = data.isNoteExist;
      getSymbolsDataAccToListObject.is_etf_symbol = data.is_etf_symbol;
      getSymbolsDataAccToListObject.list_rating = data.list_rating;
      getSymbolsDataAccToListObject.market_cap = data.market_cap;
      getSymbolsDataAccToListObject.name = data.name;
      getSymbolsDataAccToListObject.raw_PGR = data.raw_PGR;
      getSymbolsDataAccToListObject.signals = data.signals;
      getSymbolsDataAccToListObject.pgr = SharedUtil.getPgrRating(
        data.PGR,
        data.raw_PGR
      );
      getSymbolsDataAccToListObject.ratingType = SharedUtil.getPgrNameWithoutNeutralValues(
        data.PGR
      );
      getSymbolsDataAccToListObject.ratingTypeColorLabel =
        SharedUtil.getColorForRating(getSymbolsDataAccToListObject.ratingType) + '-label';
      getSymbolsDataAccToListObject.ratingTypeColorBg =
        SharedUtil.getColorForRating(getSymbolsDataAccToListObject.ratingType) + '-bg';

      getSymbolsDataAccToListObject.pgrImageSrc = SharedUtil.getPgrSmallArcWithoutNeutralValue(
        data.PGR,
        data.raw_PGR,
        data.is_etf_symbol
      );
      // is_etf_symbol

      getSymbolsDataAccToListArray.push(getSymbolsDataAccToListObject);
    });
    const sortedData = ListNavigationSort.quickBarSorting(
      getSymbolsDataAccToListArray,
      'symbol'
    );

    // getSymbolsDataAccToListArray = sortedData;
    this.getListSymbolData.symbols = sortedData;

    this.symbolCsv = a.join(",");

    return sortedData;
  }

  setPowerBarWidth(red, yellow, green, headerData): void {
    let bearishPerCountStyle = 0;
    let neutralPerCountStyle = 0;
    let bullishPerCountStyle = 0;

    const totalCount = red + yellow + green;
    const bearishPer = Math.round(
      totalCount === 0 ? 0 : (red / totalCount) * 100
    );
    const neutralPer = Math.round(
      totalCount === 0 ? 0 : (yellow / totalCount) * 100
    );
    const bullishPer = Math.round(
      totalCount === 0 ? 0 : (green / totalCount) * 100
    );

    neutralPerCountStyle = 7;
    const bearishCountPer =
      bearishPer === 0 ? 0 : (bearishPer / (bearishPer + bullishPer)) * 70;
    const bullishCountPer =
      bullishPer === 0 ? 0 : (bullishPer / (bearishPer + bullishPer)) * 70;
    if (bearishCountPer === 0 && bullishCountPer === 0) {
      bearishPerCountStyle = 35;
      bullishPerCountStyle = 35;
    } else if (20 - bearishCountPer > 0) {
      bearishPerCountStyle = 20;
      bullishPerCountStyle = bullishCountPer - (20 - bearishCountPer);
    } else if (20 - bullishCountPer > 0) {
      bearishPerCountStyle = bearishCountPer - (20 - bullishCountPer);
      bullishPerCountStyle = 20;
    } else {
      bearishPerCountStyle = bearishCountPer;
      bullishPerCountStyle = bullishCountPer;
    }
    headerData.bearishPerCountStyle = bearishPerCountStyle;
    headerData.neutralPerCountStyle = neutralPerCountStyle;
    headerData.bullishPerCountStyle = bullishPerCountStyle;
  }

  lookupSymbol(symbol: string): void {
    if (symbol == '') {
      this.resetSearchSymbol();
    }
    setTimeout(() => {
      this.api.symbolLookup(symbol, 'symbol').subscribe((resp) => {
        this.symbolLookupData = resp as any;
        this.etfSymbolLookupData = this.symbolLookupData.filter(
          (symbol) => symbol.is_etf
        );
        this.StockSymbolLookupData = this.symbolLookupData.filter(
          (symbol) => !symbol.is_etf
        );
      });
    }, 100);
  }

  toggleSearch() {
    //clear data before open
    if (!this.showSearch) {
      this.resetSearchSymbol();
    }

    this.showSearch = !this.showSearch;
    // this.switchSideMode = !this.switchSideMode;
    if (this.showSearch) {
      setTimeout(() => {
        if (this.searchElement.nativeElement) {
          this.searchElement.nativeElement.focus();
        }
      }, 0);
    }
  }

  goToDashboard() {
    this.router.navigate(['/dashboard']);
  }

  resetSearchSymbol(): void {
    this.searchSymbolText = '';
    this.symbolLookupData = [];
    this.etfSymbolLookupData = [];
    this.StockSymbolLookupData = [];
  }

  onSymbolSelect(data: SymbolLookUp): void {
    LocalStorage.setItem('mainSymbol', data.Symbol);
    this.dataService.recentlyViewedData.setSymbol(data.Symbol);
    let idx = (this.getListSymbolData.symbols).findIndex((item) => item.symbol.toLowerCase() === data.Symbol.toLowerCase());
    if (idx < 0) {
      this.selectRecentViewed();
    }
    this.init();
    this.resetSearchSymbol();
  }

  selectSymbolForReport(symbol: string): void {
    LocalStorage.setItem('mainSymbol', symbol);
    this.dataService.recentlyViewedData.setSymbol(symbol);
    this.init();
  }

  setCheckListAccess = false;

  setProductName() {
    let keyValueOfProducts = [];
    let i = 0;
    for (let key in this.productsList) {
      keyValueOfProducts.push({ key: key, value: this.productsList[key] });
      i++;
    }
    this.productDataValue = keyValueOfProducts;

    for (let element of this.productDataValue) {
      if ((element.value.name === 'Power Pulse Premium') && (element.value.productAccess === true)) {
        this.accessPgrSectionAccordingToPubCode = true;
        break;
      } else {
        this.accessPgrSectionAccordingToPubCode = false;
      }
    }
    for (let element of this.productDataValue) {
      if ((element.value.name === 'Checklist') && (element.value.productAccess === true)) {
        this.setCheckListAccess = true;
        break;
      } else {
        this.setCheckListAccess = false;
      }
      // this.accessPgrSectionAccordingToPubCode = true;
    };
   // console.log(this.setCheckListAccess);
    if (this.env === 'local') {
      this.accessPgrSectionAccordingToPubCode = true;
    }
  }

  // productData(data): void {
  //   this.productsList = data;
  //   console.log(this.productsList);
  //   this.setProductName();
  // }

  updateProductPreferanceValue(data, value): void {
    // let data = this.selectValue;
    let object = this.productsList;
    for (const key in object) {
      if (Object.prototype.hasOwnProperty.call(object, key)) {
        const element = object[key];
        if (element.name == data.name) {
          element.productPreference = true;
        } else {
          element.productPreference = false;
        }
      }
    }
    if (value === true) {
      this.api.updateProductPreferance(data.name, this.userDetail.UID).subscribe((response) => {
        //      console.log(response);
      });
    }
    if (value === false) {
      this.api.updateProductPreferance('NA', this.userDetail.UID).subscribe((response) => {
        //      console.log(response);
      });
    }
  }
  // productAccessCount(productAccessCount): void {
  //   this.productsAccessCount = productAccessCount;
  //   let isPgrAccess = false;
  //   this.productsAccessCount.forEach(element => {
  //     if (element === "Power Gauge Report") {
  //       isPgrAccess = true;
  //     }
  //   });
  //   if (!isPgrAccess) {
  //     this.logOut();
  //   }

  // }

  logOut(): void {
    this.auth.killsessions(this.appDataService.cookiesData.email).subscribe((resp) => {
      if (resp["status"] === "success") {
        this.appDataService.removeAllData();
        this.cookieService.deleteAll('/', '.chaikinanalytics.com');
        window.location.replace(window.location.protocol + "//" + window.location.host + "/login/#/logout");
        // window.location.replace(window.location.protocol + "//" + window.location.host + "/login/");
      }
    });
  }

  // quick bar
  startIndex = 0;
  moveBarToRightDirection(): void {
    const visibleBarArea = document.getElementById('viewed-area-of-bar');
    const leftPos = visibleBarArea.scrollLeft;
    const symbolArr = (this.symbolCsv).split(',');
    const elementRefId = 'chyron-item-' + symbolArr[this.startIndex];
    const elementRefBoundaryValue = document.getElementById(elementRefId).getBoundingClientRect();
    visibleBarArea.scrollLeft = leftPos + elementRefBoundaryValue.width;
    this.startIndex = (this.startIndex >= (symbolArr.length - 1)) ? symbolArr.length - 1 : this.startIndex + 1;
  }

  moveBarToLeftDirection(): void {
    const visibleBarArea = document.getElementById('viewed-area-of-bar');
    const leftPos = visibleBarArea.scrollLeft;
    const symbolArr = (this.symbolCsv).split(',');
    this.startIndex = this.startIndex === 0 ? 0 : this.startIndex - 1;
    const elementRefId = 'chyron-item-' + symbolArr[this.startIndex];
    const elementRefBoundaryValue = document.getElementById(elementRefId).getBoundingClientRect();
    visibleBarArea.scrollLeft = leftPos - elementRefBoundaryValue.width;
  }

  editList(event: any, listData) {
    let list: any;
    this.watchlistDataModel.data.forEach(element => {
      if ((element.listId) === (listData.listId)) {
        list = element;
      }
    });

    event.stopPropagation();
    const headerText = "Update Watchlist";
    const componentTaskText = `Enter a new name for your "${list.listName}"`;
    const matInputLabelText = "Enter new name";
    const btnCancelText = "Cancel";
    const btnSaveText = "Update";
    const screenName = list.listName;

    const dialogRef = this.dialog.open(EditListDialogComponent, {
      autoFocus: true,
      data: {
        headerText: headerText, componentTaskText: componentTaskText,
        matInputLabelText: matInputLabelText, btnCancelText: btnCancelText,
        btnSaveText: btnSaveText, screenName: screenName
      }
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      const userDetails: LoginDetailModel = LocalStorage.getItem('userDetail');
      const result = dialogResult;
      if (result) {
        this.api.editSymbolList(list.listId, result, userDetails.UID).subscribe((resp) => {
          if (resp['msgForUpdateListName'] === 'successfully rename list') {
            const msg = `"${screenName}" successfully renamed to "${result}".`;
            this.getUserWatchlistData();
            this.toastService.showSuccess(msg);
            // this.store.dispatch(
            //   ListNavigationAction.updateUserLists({
            //     isUserListUpdate: false,
            //   })
            //  );
          }
        });
        // setTimeout(() => {
        //   // if (this.selectedListNavModel['listId'] === list.listId) {
        //   //   const selectedListNavModel = new SelectedListNavModel();
        //   //   const selectedListObj = this.listData.data.find(data => data.listId === this.selectedListNavModel['listId']);
        //   //   selectedListNavModel.listId = selectedListObj.listId;
        //   //   selectedListNavModel.listType = selectedListObj.listType;
        //   //   selectedListNavModel.listName = result;
        //   //   this.selectedList.emit(selectedListNavModel);
        //   // }
        // }, 1000);
      }
    });
  }

  deleteList(event: any, listData) {
    event.stopPropagation();
    this.watchlistDataModel.data.forEach(element => {
      if ((element.listId) === (listData.listId)) {
        this.getConfirmationFromUserToDelete(element);
      }
    });

  }

  watchlistDataUpdate(event: any) {
    if(event){
    this.getUserWatchlistData();
    }
  }

  getConfirmationFromUserToDelete(list: NavigationListWithPowbarModel): void {
    const msg = 'Are you sure you want to delete this list?';
    const dialogRef = this.dialog.open(InfoDialogComponent, {
      data: {
        message: msg,
        noButtonText: 'No, I changed my mind',
        yesButtonText: 'Yes, delete',
      },
    });

    dialogRef.afterClosed().subscribe((dialogResult) => {
      const result = dialogResult;
      if (result) {
        // if (this.selectedListNavModel['listId'] === list.listId) {
        //   const selectedListNavModel = new SelectedListNavModel();
        //   const selectedListObj = this.listData.data.find(data => data.listId === this.userDetailListId);
        //   selectedListNavModel.listId = selectedListObj.listId;
        //   selectedListNavModel.listType = selectedListObj.listType;
        //   selectedListNavModel.listName = selectedListObj.listName;
        //   this.selectedList.emit(selectedListNavModel);
        // }
        this.deleteUserList(list);
      }
    });
  }

  deleteUserList(list: NavigationListWithPowbarModel): void {
    const listId = list.listId;
    const httpDeleteUserList$ = this.api.deleteStockList(
      listId, this.userDetail.UID
    );

    httpDeleteUserList$
      .pipe(
        map((resp: any) => {
          return resp.Status;
        })
      )
      .subscribe((status: boolean) => {
        if (status) {
          this.getUserWatchlistData();
          this.toastService.showSuccess('Successfully removed');
          // this.store.dispatch(
          //   ListNavigationAction.updateUserLists({
          //     isUserListUpdate: false,
          //   })
          //  );
        } else {
          //  this.toastService.showDanger('Please try again');
        }
      });
  }

  companyInformation(): void {
    this.api.getCompanyInformation(this.headerDataModel.symbol).subscribe((resp) => {
      if (resp.status === "true") {
        this.companyProfileObject.Address1 = resp.Address1;
        this.companyProfileObject.ChineseProfile = resp.ChineseProfile;
        this.companyProfileObject.CompanyName = resp.CompanyName;
        this.companyProfileObject.EmployeesCount = (resp.EmployeesCount) ? resp.EmployeesCount : "unavailable";
        this.companyProfileObject.Profile = resp.Profile;
        this.companyProfileObject.Sector = (resp.Sector) ? resp.Sector : "unavailable";
        this.companyProfileObject.formattedEmployeesCount = (Math.round(resp.EmployeesCount * 100) / 100);
        this.companyProfileObject.msg = resp.msg;
      }
    })
  }



}
