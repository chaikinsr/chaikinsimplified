import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatNativeDateModule } from '@angular/material/core';
import { MatStepperModule } from '@angular/material/stepper';
import { MatSelectModule } from '@angular/material/select';

import { EtfStatsComponent } from './components/etf-stats/etf-stats.component';
import { HistoricPriceChartComponent } from './components/historic-price-chart/historic-price-chart.component';
import { EarningComponent } from './components/earnings/earnings.component';
import { ExpertsComponent } from './components/experts/experts.component';
import { FinancialComponent } from './components/financials/financials.component';
import { TechnicalComponent } from './components/technicals/technicals.component';

import { PowerGaugeRoutingModule } from './pgr-routing.module';
import { PowerGaugeReportComponent } from './pgr.component';
import { AppletHeaderModule } from 'src/app/layout/header/applet-header.module';
import { SharedModule } from '../shared/shared.module';
import { ClickOutsideModule } from 'ng-click-outside';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PowerGaugeRoutingModule,
    HttpClientModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDialogModule,
    MatDividerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    SharedModule,
    MatStepperModule,
    MatListModule,
    MatButtonToggleModule,
    MatSelectModule,
    AppletHeaderModule,
    ClickOutsideModule,
    
  ],
  declarations: [
    PowerGaugeReportComponent,
    FinancialComponent,
    EarningComponent,
    TechnicalComponent,
    ExpertsComponent,
    EtfStatsComponent,
    HistoricPriceChartComponent,
  ],
  
  bootstrap: [PowerGaugeReportComponent],
})
export class PowerGaugeReportModule {}
