import {
  Component,
  ElementRef,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LoginDetailModel } from 'src/app/core/data-models/service-model';
import { APIService } from 'src/app/core/http-services/api.service';
import { LocalStorage } from 'src/app/core/services/utilily/utility';

import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-tickers-to-list-dialog',
  templateUrl: './add-tickers-to-list.component.html',
  styleUrls: ['./add-tickers-to-list.component.scss'],
})
export class AddTickersToListComponent implements OnInit {
  selectedWatchlistId = -1;
  isLoading = false;
  onTickerSaveErrMsg: string;
  @ViewChild('txttickers', { static: false }) txtBoxTickers: ElementRef;
  constructor(
    private api: APIService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<AddTickersToListComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    if (data.listId) {
      this.selectedWatchlistId = data.listId;
    }
  }

  ngOnInit(): void {}

  onBulkAddTickersInList(csvTickers: string): void {
    this.paresCSVTickers(csvTickers);
  }

  paresCSVTickers(csvTickers: string): void {
    csvTickers = csvTickers.replace(/ +/g, '').trim();
    let arrCsvTickers = [];
    if (csvTickers) {
      const lastChar = csvTickers.slice(-1);
      if (lastChar === ',') {
        csvTickers = csvTickers.slice(0, -1);
      }

      arrCsvTickers = csvTickers
        .split('\n')
        .join(',')
        .split(',')
        .filter(value => value.trim() != "")
        .filter((value, index, self) => self.indexOf(value) == index) // distinct
        .map((symbol) => symbol.toUpperCase());
      csvTickers = arrCsvTickers.join(',');
      const maxTickerLimit = 1000;
      const totalTickersCount = arrCsvTickers.length;

      if (totalTickersCount > maxTickerLimit) {
        this.openConfirmationPopup(maxTickerLimit, arrCsvTickers);
      } else {
        this.addTickerIntoList(csvTickers);
      }
    } else {
      this.onTickerSaveErrMsg = 'Add comma separated tickers to save.';
    }
  }

  openConfirmationPopup(maxTickerLimit: number, arrCsvTickers: string[]): void {
    const msg = `The number of tickers separated by comma exceeds the maximum number of ${maxTickerLimit}.
        To accept the first ${maxTickerLimit} tickers for this list click Yes. To terminate the request click No.`;

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '500px',
      data: {
        title: 'Confirm action',
        message: msg,
        noButtonText: 'No',
        yesButtonText: 'Yes',
      },
    });

    dialogRef.afterClosed().subscribe((dialogResult) => {
      const result = dialogResult;
      if (result) {
        const csvTickers = arrCsvTickers.slice(0, maxTickerLimit).join(',');
        this.addTickerIntoList(csvTickers);
      }
    });
  }

  addTickerIntoList(csvTickers): void {
    const userDetail: LoginDetailModel = LocalStorage.getItem('userDetail');
    this.api.addStockIntoList(csvTickers, this.selectedWatchlistId,userDetail.UID).subscribe(
      (resp: any) => {
        this.dialogRef.close(this.selectedWatchlistId);
      },
      (err) => {
        this.onTickerSaveErrMsg = 'Api error in adding tickeks to list.';
      }
    );
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
