import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatStepper } from '@angular/material/stepper';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';
import { LoginDetailModel, NavigationListWithPowbar } from 'src/app/core/data-models/service-model';
import { APIService } from 'src/app/core/http-services/api.service';
import { AppDataService } from 'src/app/core/services/app-data.service';

import { LocalStorage } from 'src/app/core/services/utilily/utility';

import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-create-list-dialog',
  templateUrl: './create-list-dialog.component.html',
  styleUrls: ['./create-list-dialog.component.scss'],
})
export class CreateListDialogComponent implements OnInit {
  listType = 'import';
  watchlistName: string = '';
  watchlistNameExistMessage: string = '';
  manualSaveMsg: string = '';
  invalidFileMsg: string = '';
  importInvalidFileMsg: string = '';
  updationMsg = '';
  hideButton:boolean = false;
  myStepper: MatStepper;
  isLoading: boolean = false;
  isNameStepBtnDisabled = false;
  isUploadStepBtnDisabled = false;
  isFileImportBtnDisabled = false;
  columnForTickers = 0;
  fileToUpload: File = null;
  fileData: any;
  filePreviewData: any[] = [];
  fileHdrs = [];
  newWatchlistId: number;
  symbol:string;
  @ViewChild('fileInput', { static: false }) fileInputVar: ElementRef;
  @ViewChild('listName') listName:ElementRef;
  constructor(
    private api: APIService,
    public dialog: MatDialog,
    public appDataService: AppDataService,
    public dialogRef: MatDialogRef<CreateListDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data
  ) {
    this.symbol = data;
  }
  ngOnInit(): void {
//console.log(this.symbol);
  }

  onlistTypeChange(val: string) {
    this.listType = val;
  }

  onNameStepContinue(stepper: MatStepper, listName: string) {
    if(!listName){
      listName = this.listName.nativeElement.value;
    }
   //  console.log()
   // console.log(valueInput);
    const userDetail: LoginDetailModel = LocalStorage.getItem('userDetail');
    this.watchlistName = listName.trim();
    this.myStepper = stepper;
    if (this.watchlistName) {
      this.isNameStepBtnDisabled = true;
      this.isLoading = true;
      this.api.checkListNameExistence(this.watchlistName,userDetail.UID).subscribe(
        (resp) => {
          this.isLoading = false;
          this.isNameStepBtnDisabled = false;
          const listNameExists = resp.listNameExists === 'true';
          this.isNameStepBtnDisabled = true;
          this.isLoading = true;
          if (listNameExists) {
            this.hideButton = true;
            this.watchlistNameExistMessage =
              'You already have a watchlist with that name. Please choose a different name.';
             // this.myStepper.selectedIndex = 1;
         // }

          } else {
            this.hideButton = false;
            if(this.symbol){
              this.createNewList(this.symbol);
             // this.dialogRef.close(this.newWatchlistId);
            }else{
              stepper.next();
            }
           
           // this.myStepper.selectedIndex = 2;
            // this.listType === 'manual'
            //   ? stepper.next()
            //   : (stepper.selectedIndex = 3);

            // this.watchlistNameExistMessage = '';
            // this.manualSaveMsg = '';
          }
        },
        (err) => {
          console.log('Error in checkListNameExistence api call');
          this.isNameStepBtnDisabled = false;
          this.watchlistNameExistMessage =
            'Error in checking the list name existance. Try again later.';
        }
      );
    } else {
      this.watchlistNameExistMessage = 'Please enter watchlist name';
    }
  }

  onManualSave(csvTickers: string): void {
    this.paresCSVTickers(csvTickers);
  }

  paresCSVTickers(csvTickers: string): void {
    console.log(csvTickers);
    csvTickers = csvTickers.replace(/ +/g, '').trim();
    let arrCsvTickers = [];
    if (csvTickers) {
      const lastChar = csvTickers.slice(-1);
      if (lastChar === ',') {
        csvTickers = csvTickers.slice(0, -1);
      }

      arrCsvTickers = csvTickers
        .split('\n')
        .join(',')
        .split(',')
        .filter((value) => value.trim() != '')
        .filter((value, index, self) => self.indexOf(value) == index) // distinct
        .map((symbol) => symbol.toUpperCase());

      csvTickers = arrCsvTickers.join(',');
      this.manualSaveMsg = '';
      if (csvTickers.trim() == '') {
        this.manualSaveMsg = 'Add comma separated tickers to continue.';
        return;
      }

      const maxTickerLimit = 1000;
      const totalTickersCount = arrCsvTickers.length;

      if (totalTickersCount > maxTickerLimit) {
        this.openConfirmationPopup(maxTickerLimit, arrCsvTickers);
      } else {
        this.createNewList(csvTickers);
      }
    } else {
      this.manualSaveMsg = 'Add comma separated tickers to save.';
    }
  }

  openConfirmationPopup(maxTickerLimit: number, arrCsvTickers: string[]): void {
    const msg = `The number of tickers separated by comma exceeds the maximum number of ${maxTickerLimit}.
        To accept the first ${maxTickerLimit} tickers for this list click Yes. To terminate the request click No.`;

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '500px',
      data: {
        title: 'Confirm action',
        message: msg,
        noButtonText: 'No',
        yesButtonText: 'Yes',
      },
    });

    dialogRef.afterClosed().subscribe((dialogResult) => {
      const result = dialogResult;
      if (result) {
        const csvTickers = arrCsvTickers.slice(0, maxTickerLimit).join(',');
        this.createNewList(csvTickers);
      }
    });
  }

  createNewList(csvTickers: string): void {
    const userDetail: LoginDetailModel = LocalStorage.getItem('userDetail');
    this.isLoading = true;
    this.api.addListToPortfolio(this.watchlistName , userDetail.UID).subscribe((resp) => {
      const status: boolean = resp['Status'] as boolean;
      if (status) {
        const userObj = this.getUserWatchlistData();
        userObj.subscribe((userListResp) => {
          const userlist: NavigationListWithPowbar = userListResp.find(
            (data) => data.listName === this.watchlistName
          );

        //  console.log(userlist);
          if (userlist) {
            const listId = userlist.listId;
            this.newWatchlistId = listId;
            LocalStorage.setItem('listId', listId);
            this.api
              .uploadListDataToRedis(listId, userlist.listName, userDetail.UID )
              .subscribe();
            this.addTickerIntoList(csvTickers, listId ,userListResp);
          }
        });
      }
    });
  }

  addTickerIntoList(csvTickers: string, listId: number , watchlistData:any): void {
    const userDetail: LoginDetailModel = LocalStorage.getItem('userDetail');
    this.api.addStockIntoList(csvTickers, listId,userDetail.UID).subscribe(
      (resp: any) => {
      //  console.log(resp,"addTickerIntoList");
        this.dialogRef.close(listId);
        this.isLoading = false;
      },
      (err) => {
       // this.toastService.showDanger('Error, Please try again later');
        this.onNoClick();
      }
    );
  }

  getUserWatchlistData(): Observable<NavigationListWithPowbar[]> {
    const userDetail: LoginDetailModel = LocalStorage.getItem('userDetail');
    return new Observable<NavigationListWithPowbar[]>((observer) => {
      const httpUserWatchlist$ = this.api.getUserWatchlistData(userDetail.UID);
      httpUserWatchlist$
        .pipe(
          map((resp) => {
            if (resp.status) {
              const userWatchlistData = resp.data as NavigationListWithPowbar[];
              this.appDataService.userList =  resp.data;
              return userWatchlistData;
            }
            return [];
          })
        )
        .subscribe(
          (userList: NavigationListWithPowbar[]) => {
            observer.next(userList);
            observer.complete();
           
          },
          (err) => {
            console.log('Error in User data', err);
            observer.next([]);
            observer.complete();
          }
        );
    });
  }

  onuploadFileChange(files: File[]) {
    this.columnForTickers = 0;
    let file = files[0];

    let isCSVFile = file.name.endsWith('.csv');

    if (isCSVFile) {
      this.fileToUpload = file;
      this.invalidFileMsg = '';
    } else {
      this.fileInputVar.nativeElement.value = '';
      this.invalidFileMsg = 'Select a valid  file to upload';
      this.fileToUpload = null;
    }

    this.isUploadStepBtnDisabled = false;
  }

  onUploadStepContinue() {
    this.importInvalidFileMsg = '';
    if (!this.fileToUpload) {
      this.invalidFileMsg = 'Select a valid csv file to continue.';
      return;
    }

    //  let resp:any =
    //  {"filePreviewData":[["Synbol","comp"],["VGT","AA"],["XLK","AA"],["FNDX","BB"],["IWP","CC"],["SPY","AA"],["VIG","CC"],["VGR","AA"],["XLP","DD"],["FENY","BB"],["IEZ","CC"],["JMIN","DD"]]};

    this.isUploadStepBtnDisabled = true;
    let file = this.fileToUpload;
    let reader = new FileReader();
    reader.readAsArrayBuffer(file);
    reader.onload = (e) => {
      let data: any = e.target['result']; //e.target.result;// reader.result;
      //console.log(data);
      let isMatch = true;
      var data1 = new Uint8Array(data);
      // console.log(data1);
      var dataFile = '';
      var arr = new Array();
      for (let i = 0; i != data1.length; ++i) {
        arr[i] = String.fromCharCode(data1[i]);
        // console.log(arr[i]);
        let match = /\r/.exec(arr[i]); //CPT-2302
        let match1 = /\n/.exec(String.fromCharCode(data1[i + 1]));
        //	console.log(match, match1);
        if (match1) {
          if (i + 2 == data1.length) {
            isMatch = false;
          }
        }
        if (match && !isMatch) {
          arr[i] = '\r\n';
        }
      }
      // console.log(arr);
      dataFile = arr.join('');

      this.fileData = dataFile;
      this.filterFileData(true, 0, dataFile);
    };
  }

  async filterFileData(isNextStep: boolean, columnIndex: number, dataFile) {
    let fileData = dataFile;
    let fileDataArr = [];
    let fileDataArrTemp = fileData
      .split(/\r?\n/)
      .filter((value) => value.trim() != '')
      .filter((value, index, self) => self.indexOf(value) == index) // distinct
      .map((item) => item.toUpperCase());

    let fileDataArr1 = fileDataArrTemp.map((data) => {
      return data.split(',')[columnIndex];
    });

    fileDataArr1 = fileDataArr1
      .filter((value) => value.trim() != '')
      .filter((value, index, self) => self.indexOf(value) === index) // distinct
      .map((item) => item.toUpperCase());

    this.uploadFormData(isNextStep, fileData);
  }

  uploadFormData(isNextStep: boolean, fileData: string) {
    const userDetail: LoginDetailModel = LocalStorage.getItem('userDetail');
    this.isLoading = true;
    this.api.uploadFileData(fileData, this.watchlistName , userDetail.UID).subscribe(
      (resp: any) => {
        const uploadStatus: string = resp.fileUploadStatus;
        if (uploadStatus.toLowerCase() === 'success') {
          this.api.getFormattedFileData(userDetail.UID).subscribe(
            (resp: any) => {
              this.filePreviewData = resp.filePreviewData;
              let maxHdrs = [
                'A',
                'B',
                'C',
                'D',
                'E',
                'F',
                'G',
                'H',
                'I',
                'J',
                'K',
                'L',
                'M',
                'N',
                'O',
                'P',
                'Q',
                'R',
                'S',
                'T',
                'U',
                'V',
                'W',
                'X',
                'Y',
                'Z',
              ];
              if (this.filePreviewData.length === 0) {
                this.invalidFileMsg = 'Select a valid  file to upload';
              } else {
                let hlen = this.filePreviewData[0].length;
                this.fileHdrs = [];
                for (let i = 0; i < hlen; i++) {
                  this.fileHdrs.push(maxHdrs[i]);
                }
                // this.columnForTickers = this.fileHdrs[0];
                if (isNextStep) {
                  this.myStepper.next();
                  this.isUploadStepBtnDisabled = false;
                }
              }

              this.isLoading = false;
            },
            (err) => {
              console.log('Error in getFormattedFileData api call');
              this.isUploadStepBtnDisabled = false;
              this.isLoading = false;
            }
          );
        }
      },
      (err) => {
        console.log('Error in uploadFileData api call');
        this.isUploadStepBtnDisabled = false;
        this.isLoading = false;
      }
    );
  }

  onColumnSelectionChange(event: any) {
    this.columnForTickers = event.value;
    this.filterFileData(false, this.columnForTickers, this.fileData);
    this.isFileImportBtnDisabled = false;
    this.importInvalidFileMsg = '';
  }

  importBack() {
    this.isFileImportBtnDisabled = false;
    this.isUploadStepBtnDisabled = false;
    this.importInvalidFileMsg = '';
    this.invalidFileMsg = '';
  }

  onFileImportClick() {
    const userDetail: LoginDetailModel = LocalStorage.getItem('userDetail');
    let tickersStartRow = 1;
    let colForTickres = this.columnForTickers;

    this.isFileImportBtnDisabled = true;
    this.isLoading = true;
    this.api
      .validateAndUploadTickersForList(
        colForTickres,
        tickersStartRow,
        false,
        this.watchlistName,userDetail.UID
      )
      .subscribe(
        (resp: any) => {
          // console.log(resp);

          if (resp.updationStatus) {
            this.updationMsg = resp.updationMsg;
            this.clearRawDataAndGetAuthList();
          } else {
            this.isLoading = false;
            if (resp.actionCode == '4') {
              let msg = resp.updationMsg;

              const dialogRef = this.dialog.open(ConfirmDialogComponent, {
                maxWidth: '500px',
                data: { title: 'Confirm action', message: msg },
              });

              dialogRef.afterClosed().subscribe((dialogResult) => {
                const userDetail: LoginDetailModel = LocalStorage.getItem('userDetail');
                let result = dialogResult;
                if (result) {
                  this.isLoading = true;
                  this.api
                    .validateAndUploadTickersForList(
                      colForTickres,
                      tickersStartRow,
                      true,
                      this.watchlistName, userDetail.UID
                    )
                    .subscribe(
                      (resp: any) => {
                        if (resp.updationStatus) {
                          this.updationMsg = resp.updationMsg;
                          this.clearRawDataAndGetAuthList();
                        }
                      },
                      (err) => {
                        console.log(
                          'Error in validateAndUploadTickersForList api call'
                        );
                        this.isFileImportBtnDisabled = false;
                        this.isLoading = false;
                      }
                    );
                } else {
                  //this.dialogRef.close();
                }
              });
            } else {
              this.importInvalidFileMsg = resp.updationMsg;
            }
          }
        },
        (err) => {
          console.log('Error in validateAndUploadTickersForList api call');
          this.isFileImportBtnDisabled = false;
          this.isLoading = false;
        }
      );
  }

  clearRawDataAndGetAuthList() {
    const userDetails :LoginDetailModel = LocalStorage.getItem('userDetail');
    this.isFileImportBtnDisabled = false;
    this.isLoading = true;
    this.api.clearRawListData(userDetails.UID).subscribe(
      (resp: any) => {
        const userObj = this.getUserWatchlistData();
        userObj.subscribe((userListResp) => {
          this.isLoading = false;
          const userlist: NavigationListWithPowbar = userListResp.find(
            (data) => data.listName === this.watchlistName
          );
          if (userlist) {
            const listId = userlist.listId;
            this.newWatchlistId = listId;
            let searchOpenBracker = '[';
            let searchCloseBracket = ']';

            if (
              this.updationMsg.indexOf(searchOpenBracker) > -1 ||
              this.updationMsg.indexOf(searchCloseBracket) > -1
            ) {
              let msg = `The ${this.watchlistName} list is created and is available.But the following symbols were not recognized.`;
              let msgExtra = this.updationMsg.substring(
                this.updationMsg.indexOf(searchOpenBracker)
              );
              this.updationMsg = msg + msgExtra;
              this.myStepper.next();
            } else {
              this.dialogRef.close(listId);
            }

            // this.api
            //   .uploadListDataToRedis(listId, userlist.listName)
            //   .subscribe();
            // this.addTickerIntoList(csvTickers, listId);
          }
        });
      },
      (err) => {
        console.log('Error in clearRawListData api call');
        this.isLoading = false;
      }
    );
  }

  onCompleteFileImport() {
    this.dialogRef.close(this.newWatchlistId);
  }

  onAckStepBack() {
    this.isFileImportBtnDisabled = false;
    this.myStepper.previous();
  }

  onUploadStepBack() {
    this.myStepper.selectedIndex = 1;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
