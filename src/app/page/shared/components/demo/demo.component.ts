import { Component, OnInit } from "@angular/core";
import { APIService } from "src/app/core/http-services/api.service";


@Component({
    selector: 'app-demo',
    templateUrl: './demo.component.html',
    styleUrls: ['./demo.component.scss']
  })
  
  export class DemoComponent implements OnInit {
    ngOnInit(): void {}
    constructor(
      private api: APIService
    ) {}
}