import { Component, OnInit } from "@angular/core";
import { APIService } from "src/app/core/http-services/api.service";


@Component({
    selector: 'app-disclaimer',
    templateUrl: './disclaimer.component.html',
    styleUrls: ['./disclaimer.component.scss']
  })
  
  export class DisclaimerComponent implements OnInit {
    ngOnInit(): void {}
    constructor(
      private api: APIService
    ) {}
}