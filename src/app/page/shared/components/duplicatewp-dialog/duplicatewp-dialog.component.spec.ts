import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DuplicatewpDialogComponent } from './duplicatewp-dialog.component';

describe('DuplicatewpDialogComponent', () => {
  let component: DuplicatewpDialogComponent;
  let fixture: ComponentFixture<DuplicatewpDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DuplicatewpDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DuplicatewpDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
