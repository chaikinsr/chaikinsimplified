import { Component, OnInit, Inject, ViewChild } from '@angular/core';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatStepper } from '@angular/material/stepper';
import { LoginDetailModel } from 'src/app/core/data-models/service-model';
import { APIService } from 'src/app/core/http-services/api.service';
import { LocalStorage } from 'src/app/core/services/utilily/utility';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-duplicatewp-dialog',
  templateUrl: './duplicatewp-dialog.component.html',
  styleUrls: ['./duplicatewp-dialog.component.scss']
})
export class DuplicatewpDialogComponent implements OnInit {
  isWatchListSelected: any;
  watchlist: any;
  watchlistSymbolsData: any;
  portfolioData: any;
  stepZeroHeaderText: string = 'Duplicate this list to create a new:';

  title: string = "";
  listType: string = 'watchlist';
  listName: string = "";
  csvTickers: string = "";
  arrTickers = [];
  onSaveErrMsg: string = "";
  listNameExistsMessage: string = "";
  listId: number;
  userDetail: LoginDetailModel
  isLoading: boolean = false;
  //newWatchlist: AuthorizedListData;
  allocationCount: number = 0;
  allocationMsg: string = "";
  isCreatePortfolioBtnDisabled: boolean = false;
  @ViewChild('stepper') stepper: MatStepper;

  constructor(private api: APIService,
    //private appDataService: AppDataService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<DuplicatewpDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
     
    this.isWatchListSelected = data.isWatchListSelected;
    this.watchlist = data.watchlist;
    this.watchlistSymbolsData = data.watchlistSymbolsData;
    this.portfolioData = data.portfolioData;
    if (data.stepZeroHeaderText) {
      this.stepZeroHeaderText = data.stepZeroHeaderText;
    }
    if (!this.isWatchListSelected) {
      this.listType = 'portfolio'
    }
  }
  ngOnInit() {
     this.userDetail = LocalStorage.getItem('userDetail');
  }

  onNoClick() {
    this.dialogRef.close();
  }

  onlistTypeChange(val: string) {
    this.listType = val;
  }

  onStep1Continue() {
    this.stepper.next();
  }

  onNameStepBack() {
    this.stepper.previous();
  }

  onCreateWatchlist(listName: string) {
    this.listName = listName.trim();
    if (this.listName) {
      this.isLoading = true;
      this.api.checkListNameExistence(this.listName , this.userDetail.UID).subscribe(resp => {
        this.isLoading = false;
        const listNameExists = (resp.listNameExists === "true");
        if (listNameExists) {
          this.listNameExistsMessage = "You already have a watchlist with that name. Please choose a different name.";
        }
        else {
         // if (this.isWatchListSelected) {
           // this.dialogRef.close(true);
            this.createWatchlistFromWatchlist();
        //  }
          

        }
      },
        err => {
          console.log("Error in checkListNameExistence api call");
          this.listNameExistsMessage = "System error. Your request could not be completed. Please refresh and try again or contact us through support.";
          this.isLoading = false;
        });
    }
    else {
      this.listNameExistsMessage = "Please enter watchlist name";
    }

  }
  // onPortfolioNameStepContinue(listName: string) {
  //   this.listName = listName.trim();
  //   if (this.listName) {
  //     this.isLoading = true;
  //     this.api.checkSandboxPortfolioListNameExistence(this.listName).subscribe(resp => {
  //       this.isLoading = false;
  //       const listNameExists = (resp.listNameExists === "true");
  //       if (listNameExists) {
  //         this.listNameExistsMessage = "You already have a portfolio with that name. Please choose a different name.";
  //       }
  //       else {
  //         this.listNameExistsMessage = "";
  //         if (this.isWatchListSelected) {
  //           this.creatPortfolioFromWatchlist();
  //         }
  //         else {
  //           this.creatPortfolioFromPortfolio();
  //         }
  //         // this.stepper.next();
  //       }

  //     },
  //       err => {
  //         console.log("Error in checkListNameExistence api call");
  //         this.isLoading = false;
  //         this.listNameExistsMessage = "System error. Your request could not be completed. Please refresh and try again or contact us through support."
  //       });

  //   }
  //   else {
  //     this.listNameExistsMessage = "Please enter portfolio name";
  //   }


  // }

  onCreatePortfolioBack() {
    this.stepper.previous();
  }

  

  checkAllocation() {
    console.log(this.arrTickers);
    const allocationCount = this.arrTickers
      .map(item => parseFloat(item.weight.toFixed(2)))
      .reduce((prev, current) => prev + current, 0);
    this.allocationCount = parseFloat(allocationCount.toFixed(2));

    if (this.allocationCount > 100) {
      this.allocationMsg = "Error: Allocation exceeds 100%";
      this.isCreatePortfolioBtnDisabled = true;
    }
    else {
      this.allocationMsg = "";
      this.isCreatePortfolioBtnDisabled = false;
    }

  }

  removeETF(index: number) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: "500px",
      data: {
        title: "Confirm action",
        message: "Are you sure you want to delete this etf/stock from portfolio?",
        noButtonText: "No, I changed my mind",
        yesButtonText: "Yes,delete"
      }
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      let result = dialogResult;
      if (result) {
        this.arrTickers = this.arrTickers.filter((item, i) => i !== index);
        this.checkAllocation();

      }
    });

  }

  // Private methods

  private createWatchlistFromWatchlist() {
    console.log(this.portfolioData);
    let csvTickers = this.portfolioData.symbols
      .map(symbol => symbol.symbol)
      .join(',');
      console.log(csvTickers);
    this.addListToPortfolio(csvTickers);
  }

  private createWatchlistFromPortfolio() {
    let csvTickers = this.portfolioData["etfSymbols"]
      .map(item => item.composite_ticker)
      .join(",");
    this.addListToPortfolio(csvTickers);
  }

  // private creatPortfolioFromWatchlist() {
  //   if (this.arrTickers.length == 0) {
  //     this.arrTickers = .symbols
  //       .filter(symbol => symbol.is_etf_symbol)
  //       .map(symbol => { return { ticker: symbol.symbol, weight: 0 } });

  //     this.checkAllocation();

  //   }
  //   this.stepper.next();

  // }
  private creatPortfolioFromPortfolio() {

    // if (this.arrTickers.length == 0) {
    //   if (this.portfolioData.list_type == "sandbox") {
    //     this.arrTickers = this.portfolioData["etfSymbols"]
    //       .map(item => { return { ticker: item.composite_ticker, weight: parseFloat(item.weight.toFixed(2)) } });
    //   } else {
    //     this.arrTickers = this.portfolioData["etfSymbols"]
    //       .map(item => { return { ticker: item.composite_ticker, weight: parseFloat((item["est_allocation"] * 100).toFixed(2)) } });
    //   }
       this.checkAllocation();
    // }
    this.stepper.next();
  }


  private addListToPortfolio(csvTickers: string) {
    const userDetail: LoginDetailModel = LocalStorage.getItem('userDetail');
    this.isLoading = true;
    this.api.addListToPortfolio(this.listName , this.userDetail.UID).subscribe((resp: any) => {
      const status: boolean = resp["Status"] as boolean;
      let userlist
      if (status) {
        this.isLoading = false;
        this.api.getUserWatchlistData(userDetail.UID).subscribe(resp => {
          if (resp) {
          //  let authorizedLists = this.setAuthorisedList(resp);
          //  const userlist = resp.data.find(data => data.name === this.listName);
              resp.data.forEach(element => {
              if( element.listName === this.listName){
                 userlist = element;
              }
            });
            console.log(userlist);
            if (userlist) {
              this.api.uploadListDataToRedis(userlist.listId, userlist.listName , this.userDetail.UID).subscribe();
              const listId: number = userlist.listId;
             // this.newWatchlist = userlist;
              this.api.addStockIntoList(csvTickers, listId , this.userDetail.UID).subscribe((resp: any) => {
                this.isLoading = false;
                this.dialogRef.close(userlist);
              },
                err => {
                  console.log("Error in addStockIntoList api call");
                  this.isLoading = false;
                });
            }
          }
        },
          err => {
            console.log("Error in getAuthorizedLists api call");
            this.isLoading = false;
          })
      }
    },
      err => {
        console.log("Error in addListToPortfolio api call");
        this.isLoading = false;
      });
  }

  // private setAuthorisedList(resp: any) {

  //   let authorizedLists = new AuthorizedListsResp();

  //   for (let i = 0; i < resp.length; i++) {
  //     if (resp[i]["User Lists"]) {
  //       authorizedLists.UserLists = resp[i]["User Lists"];
  //     } else if (resp[i]["Clients"]) {
  //       authorizedLists.Clients = resp[i]["Clients"];
  //     } else if (resp[i]["Industries"]) {
  //       authorizedLists.Industries = resp[i]["Industries"];
  //     } else if (resp[i]["Indices"]) {
  //       authorizedLists.Indices = resp[i]["Indices"];
  //     } else if (resp[i]["ETFs"]) {
  //       authorizedLists.ETFs = resp[i]["ETFs"];
  //     } else if (resp[i]["Chaikin HotLists"]) {
  //       authorizedLists.ChaikinHotLists = resp[i]["Chaikin HotLists"];
  //     }
  //   }

  //  // this.appDataService.dashboardData.setAuthorizedLists(authorizedLists);

  //   return authorizedLists;

  // }

}
