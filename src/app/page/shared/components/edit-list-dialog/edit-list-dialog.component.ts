import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LoginDetailModel } from 'src/app/core/data-models/service-model';
import { APIService } from 'src/app/core/http-services/api.service';
import { LocalStorage } from 'src/app/core/services/utilily/utility';


@Component({
  selector: 'app-edit-list-dialog',
  templateUrl: './edit-list-dialog.component.html',
  styleUrls: ['./edit-list-dialog.component.scss'],
})
export class EditListDialogComponent implements OnInit {
  headerText: string = 'Save a screen';
  componentTaskText: string;
  //componentTaskText: string = "Enter a name for your new screen";
  matInputLabelText: string = 'New saved screen';
  btnCancelText: string = 'Cancel';
  btnSaveText: string = 'Save';
  screenName: string;
  screens: [];
  errorDisplay: string;
  oldScreenName: string;

  constructor(
    private api: APIService,
    private dialogRef: MatDialogRef<EditListDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data
  ) {
    if (data.headerText) {
      this.headerText = data.headerText;
    }
    if (data.componentTaskText) {
      this.componentTaskText = data.componentTaskText;
    }
    if (data.matInputLabelText) {
      this.matInputLabelText = data.matInputLabelText;
    }
    if (data.btnCancelText) {
      this.btnCancelText = data.btnCancelText;
    }
    if (data.btnSaveText) {
      this.btnSaveText = data.btnSaveText;
    }

    if (data.screenName) {
      this.oldScreenName = this.screenName = data.screenName;
    }
    this.screens = data.screens;
  }

  ngOnInit() {}

  save() {
    const userDetail: LoginDetailModel = LocalStorage.getItem('userDetail');
    if (!this.screenName) {
      this.errorDisplay = 'Screen name should not be empty';
    } else {
      this.api.checkListNameExistence(this.screenName,userDetail.UID ).subscribe(
        (resp) => {
          const listNameExists = resp.listNameExists === 'true';
          if (listNameExists) {
            this.errorDisplay =
              'You already have a watchlist with that name. Please choose a different name.';
          } else {
            this.dialogRef.close(this.screenName);
          }
        },
        (err) => {
          console.log('Error in checkListNameExistence api call');
          this.errorDisplay =
            'Error in checking the list name existance. Try again later.';
        }
      );
    }
  }

  close() {
    this.dialogRef.close();
  }
}
