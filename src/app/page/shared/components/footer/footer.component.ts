import { Component, OnInit } from "@angular/core";
import { APIService } from "src/app/core/http-services/api.service";


@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
  })
  
  export class FooterComponent implements OnInit {
    ngOnInit(): void {}
    constructor(
      private api: APIService
    ) {}
}