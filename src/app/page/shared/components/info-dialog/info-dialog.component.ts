import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


@Component({
    selector: 'app-info-dialog',
    templateUrl: './info-dialog.component.html',
    styleUrls: ['./info-dialog.component.scss']
})
export class InfoDialogComponent implements OnInit {

    title: string;
    errorMessage: string;
    message: string;
    noButtonText: string ;
    yesButtonText: string;

    constructor(
        public dialogRef: MatDialogRef<InfoDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) {
        if (data.title) {
            this.title = data.title;
        }
        if (data.message) {
            this.message = data.message;
        }
        if (data.errorMessage) {
            this.errorMessage = data.errorMessage;
        }
        if (data.yesButtonText) {
            this.yesButtonText = data.yesButtonText;
        }
        if (data.noButtonText) {
            this.noButtonText = data.noButtonText;
        }
    }


    ngOnInit(): void {
    }

    onConfirm(): void {
        this.dialogRef.close(true);
    }

    onDismiss(): void {
        this.dialogRef.close(false);
    }
}
