import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from "@angular/core";
import { APIService } from "src/app/core/http-services/api.service";

@Component({
    selector: 'app-logout-popup',
    templateUrl: './logout-popup.component.html',
    styleUrls: ['./logout-popup.component.scss']
})
export class LogoutComponent implements OnInit, OnChanges {
    @Input() id: string;
    //id: string = "hardeep";
    @Output() messageToClose = new EventEmitter<any>();
    constructor(private api: APIService) { }
    ngOnChanges() {
    }

    ngOnInit(): void {
    }
    onClose() {
        this.messageToClose.emit(null);
    } 

   
}