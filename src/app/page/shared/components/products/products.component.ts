import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from "@angular/core";
import { CookieService } from "ngx-cookie-service";
import { ProductAccessModel } from "src/app/core/data-models/app-model";
import { ProductCodesAndPreferenceObject } from "src/app/core/data-models/service-model";
import { APIService } from "src/app/core/http-services/api.service";
import { AppDataService } from "src/app/core/services/app-data.service";
import { SharedUtil } from "src/app/core/services/utilily/utility";
import { environment } from 'src/environments/environment';
// import {APIService} from "./app/core/http-services/api.service.ts"



@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})

export class ProductsComponent implements OnInit, OnChanges {

  constructor(
    private cookieService: CookieService,
    private apiService: APIService,
    public appDataService: AppDataService,
  ) { }
  env: string = environment.env;
  subscription = {};
  uid: any;
  token: string;
  @Input() productPopUpShow: boolean;

  // @Output() productList1 = new EventEmitter<any>();
  // @Output() productAccessArrayCount = new EventEmitter<any>();
  productAccessModelObject = new ProductAccessModel();
  productsList: any;
  productsSubscriptionList = [];
  hideProductDiv: boolean;
  // productsList = {
  //   powerGaugeInvestor: {
  //     name: 'Power Gauge Investor',
  //     subscriptionCode: ['CPGI'],
  //     productAccess: false,
  //     productPreference: false,
  //   },
  //   chaikinAnalytics: {
  //     name: 'Chaikin Analytics',
  //     subscriptionCode: ['CHAP', 'CHA', 'CAMS', 'CAPW', 'BCHAP', 'BCHAPF'],
  //     productAccess: false,
  //     productPreference: false,
  //   },
  //   checklist: {
  //     name: 'Checklist',
  //     subscriptionCode: ['CLST'],
  //     productAccess: false,
  //   },
  //   powerGaugeReport: {
  //     name: 'Power Gauge Report',
  //     subscriptionCode: ['CPGR'],
  //     productAccess: false,
  //     productPreference: false,
  //   },
  //   // powerPulse: {
  //   //   name: 'Power Pulse Premium',
  //   //   subscriptionCode: ['CPP', 'CPOW'],
  //   //   productAccess: false,
  //   //   productPreference: false,
  //   // },
  //   powerPulse: {
  //     name: 'Power Pulse Premium',
  //     subscriptionCode: ['CPP', 'CPOW'],
  //     productAccess: false,
  //     productPreference: false,
  //   },
  //   // portfolioWise: {
  //   //   name: 'PortfolioWise',
  //   //   subscriptionCode: ['CPW', 'CAPW'],
  //   //   productAccess: false,
  //   // },
  //   // powerGaugeRating: {
  //   //   name: 'PowerGauge Rating',
  //   //   subscriptionCode: [],
  //   //   productAccess: false,
  //   // },
  // };
  ngOnChanges(): void {
    this.hideProductDiv = this.productPopUpShow;
  }
  ngOnInit(): void {
    this.productsList = this.appDataService.productsListModel;
   // this.emitProductListData(this.appDataService.productsListModel);
    // console.log(this.appDataService.productsListModel , "product");
    // this.token = this.getToken();
    // if (!this.token) {
    //   this.token = this.cookieService.get('pgi-token');
    // }
    // if (this.env === 'local') {
    //   this.token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFrYXNoLmt1bWFyQHBheGNlbC5uZXQiLCJzbmFpZCI6IiIsImN1c3RvbWVyTnVtYmVyIjoiU0FDMDAyNDMxNTI0OSIsImNvbnRhY3RpZCI6IiIsInN1YnNjcmlwdGlvbnMiOiJDSEFQLENMU1QsQ1BHSSxDUFAsQ1BHUixDUFciLCJpYXQiOjE2NDM3MTI0MjcsImV4cCI6MTY0Mzc5ODgyN30.nJaW7h4-U7BFR3baBLoYJeFzbOdDc09SLZbY2QvNJes"
  
    // }

    // this.apiService.getProductCodesAndPreference(this.token).subscribe((response) => {
    // //  let productCodesAndPreferenceObject = new ProductCodesAndPreferenceObject();
    //   if (response["status"] === "true") {
    //     this.productsSubscriptionList = response['productCodes'];
    //     let productPreference = response['productPreference']
    //     this.unlockUserSubscriptions(productPreference);
    //     this.uid = response['uid'];
    //     // this.apiService.updateProductPreferance('NA',this.uid).subscribe((response)=>{
    //     //   console.log(response)
    //     // })
    //   } else {
    //     if (this.env != 'local') {
    //       this.deleteAllCookies();
    //       window.location.replace(window.location.protocol + "//" + window.location.host + "/login/");
    //     }
    //   }
    //   this.emitProductListData(this.productsList)

    // });
  }




  // emitProductListData(data: any): void {
  //   this.productList1.emit(data);
  // }
  // getToken(): any {
  //   if ('dev-login.chaikinanalytics.com' === window.location.hostname || 'dev.chaikinanalytics.com' === window.location.hostname) {
  //     this.uid = this.cookieService.get('dev-uid');
  //     return this.cookieService.get('dev-token');
  //   } else if ('localhost' === window.location.hostname) {
  //     this.uid = this.cookieService.get('local-uid');
  //     return this.cookieService.get('local-token');

  //   } else if ('qa.chaikinanalytics.com' === window.location.hostname) {
  //     this.uid = this.cookieService.get('qa-uid');
  //     return this.cookieService.get('qa-token');

  //   }
  //   else if ('staging.chaikinanalytics.com' === window.location.hostname) {
  //     this.uid = this.cookieService.get('staging-uid');
  //     return this.cookieService.get('staging-token');

  //   }
  //   else if ('app.chaikinanalytics.com' === window.location.hostname) {
  //     this.uid = this.cookieService.get('app-uid');
  //     return this.cookieService.get('app-token');

  //   }

  // }

  // deleteAllCookies() {
  //   var cookies = document.cookie.split(";");

  //   for (var i = 0; i < cookies.length; i++) {
  //     var cookie = cookies[i];
  //     var eqPos = cookie.indexOf("=");
  //     var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
  //     document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
  //   }
  // }


  // unlockUserSubscriptions(productPreference): void {
  //   let countAccess = [];
  //   let object = this.productsList;
  //   for (const key in object) {
  //     if (Object.prototype.hasOwnProperty.call(object, key)) {
  //       const element = object[key];
  //       // element.subscriptionCode;
  //       this.productsSubscriptionList.forEach((productCode) => {
  //         this.prepareProductAccessObject(productCode);
  //         let index = element.subscriptionCode.indexOf(productCode);
  //         if (element.name == productPreference) {
  //           element.productPreference = true;
  //         }
  //         if (index > -1) {
  //           countAccess.push(element.name);
  //           element.productAccess = true;
  //         }

  //         if (productCode === 'CPP') {
  //           countAccess.push('Power Gauge Report');
  //         }

  //       });

       
  //     }
  //   }
  //   this.appDataService.productAccessModel = this.productAccessModelObject;
  //    console.log(this.appDataService.productAccessModel);
  //   this.productAccessArrayCount.emit(countAccess);
  // }

prepareProductAccessObject(value): void{
  switch (value) {
    case 'CPP':
      this.productAccessModelObject.dashboard = true;
      this.productAccessModelObject.report = true;
      break;
    case 'CPGR':
      this.productAccessModelObject.report = true;
      break;
    case 'CLST':
      this.productAccessModelObject.checklist = true;
      break;
    default:
      break;
  }
}

  loginToPgi() {
    this.cookieService.set('role', 'Power Gauge Analytics', { path: '/', domain: '.chaikinanalytics.com' });
    // this.callGetAuthorization('Power Gauge analytics');
    window.location.replace("https://pgi.chaikinanalytics.com/");
  }

  loginToTac(){
    this.cookieService.set('role','Chaikin Power Tactics' ,{path: '/', domain: '.chaikinanalytics.com'});
    window.location.replace("https://tac.chaikinanalytics.com/");
  }

  loginToCPTR(){
    this.cookieService.set('role','Chaikin Power Trader' ,{path: '/', domain: '.chaikinanalytics.com'});
    window.location.replace("https://cptr.chaikinanalytics.com/");
  }
  loginToPowerGaugeAnalytics(productDetail: any): void {
    this.callGetAuthorization('Chaikin Analytics');
  }

  loginToPulse(productDetail: any): void {
    this.callGetAuthorization('Power Pulse');
  }

  loginToPgr(productDetail: any): void {
    this.cookieService.set('role', 'Power Gauge Report', { path: '/', domain: '.chaikinanalytics.com' });
    this.cookieService.set('pgr-role', 'Power Gauge Report', { path: '/', domain: '.chaikinanalytics.com' });
    window.location.replace("https://pgr.chaikinanalytics.com/");
  }

  loginToPortfoliowise(productDetail: any): void {
    this.callGetAuthorization('PortfolioWise');
  }

  loginToChaikinAnalytics(productDetail: any): void {
    this.callGetAuthorization('Chaikin Analytics');
  }

  callGetAuthorization(productName: string): any {
    let acquireSessionForcibly = true;
    this.apiService.getAuthorizationCall(acquireSessionForcibly, productName, this.appDataService.cookiesData.token).subscribe(resp => {
      
      // this.cookieService.set( 'JSESSIONID', JSESSIONID, {path: '/', domain: '.chaikinanalytics.com'});
      // return resp;
      if (resp.loggedInStatus === 'true') {
        let sessionId = resp.sessionId;
        window.localStorage.setItem('sessionId', sessionId);
        if (productName === 'Chaikin Analytics') {
          this.onMobileLogin();
          // this.cookieService.set('role', 'Chaikin Analytics', { path: '/', domain: '.chaikinanalytics.com' });
          // window.location.replace(window.location.protocol + "//" + window.location.host + "/");

        } else if (productName === 'Power Pulse') {
          this.cookieService.set('role', 'Power Pulse', { path: '/', domain: '.chaikinanalytics.com' });
          // window.location.replace(window.location.protocol + "//" + window.location.host + "/pulse/");
          window.location.replace(window.location.protocol + "//" + window.location.host + "/pgr/#/dashboard");

        } else if (productName === 'Power Gauge Investor') {
          this.cookieService.set('role', 'Power Gauge Analytics', { path: '/', domain: '.chaikinanalytics.com' });
          window.location.replace("https://pgi.chaikinanalytics.com/");
        }
        // else if(productName === 'Power Gauge Report'){
        //   this.cookieService.set('role','Power Gauge Analytics' ,{path: '/', domain: '.chaikinanalytics.com'});
        //   window.location.replace(window.location.protocol+"//"+window.location.host+"/pgr/");
        // }

      }
    })
  }

  onMobileLogin():void{
    let _isNotMobile = (function() {
      let check = false;
      (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor);
      return !check;
  })(); 
  
  if(!_isNotMobile){
   // this.showPopOnCAMobile = true;
   // this.cookieService.set('role','Chaikin Analytics',{path: '/' , domain: '.chaikinanalytics.com'});
  //  this.cookieService.set( 'app-email',window.localStorage.getItem('email') , {path: '/' , domain: '.chaikinanalytics.com'});
   // this.callGetAuthorization('Power Gauge Analytics',this.tokenValue);
    window.location.replace(window.location.protocol + "//" + window.location.host + "/pgr/#/dashboard");
  }else{
     //    this.showPopOnCAMobile = false;
      //   this.cookieService.set( 'app-email',window.localStorage.getItem('email') , {path: '/' , domain: '.chaikinanalytics.com'});
      //  this.cookieService.set('role', 'Chaikin Analytics', { path: '/', domain: '.chaikinanalytics.com' });
          window.location.replace(window.location.protocol + "//" + window.location.host + "/");
          
  }
  }

  hideProductPopUp(): void {
    this.hideProductDiv = false;
    // this.productPopUpShow = false
  }


}

