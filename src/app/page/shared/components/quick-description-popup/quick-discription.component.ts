import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from "@angular/core";
import { WordpressIdMapping } from "src/app/core/data-models/service-model";
import { APIService } from "src/app/core/http-services/api.service";

@Component({
    selector: 'app-quick-description',
    templateUrl: './quick-description.component.html',
    styleUrls: ['./quick-description.component.scss']
})
export class QuickDescriptionComponent implements OnInit, OnChanges {
    description: string = '';
    @Input() id: string;
    @Input() defaultTextForPopup: string;
    @Output() messageToClose = new EventEmitter<any>();
    @Output() openQuickDescription = new EventEmitter<any>();
    WordpressIdMapping: WordpressIdMapping = new WordpressIdMapping();
    constructor(private api: APIService) { }

    ngOnChanges() {
        this.description = '';
        this.insightData();
    }

    ngOnInit(): void {
    }

    onClose() {
        this.messageToClose.emit(null);
    }

    insightData() {
        this.api.getInsightContent(66).subscribe(resp => {
            this.checkCaseForDescription(resp, this.id, this.defaultTextForPopup);
        }, error => {
            const resp = {
                "0": {
                "66": [
                {
                "ID": 27613,
                "post_author": "8",
                "post_date": "2021-08-17 18:34:24",
                "post_date_gmt": "2021-08-17 22:34:24",
                "post_content": "[power_gauge_rating] The Chaikin Power Gauge Stock Rating combines 20 factors into a simple, reliable indication of a stock’s potential performance relative to the market over the next 1-6 months. The stock rating ranges from Very Bearish (likely to underperform) to Very Bullish (likely to outperform). <a href=\"https://chaikinanalytics.zendesk.com/hc/en-us/articles/360020677833-What-is-the-Chaikin-Power-Gauge-rating-\">Learn more</a>.\r\n\r\n[chaikin_power_bar] The Chaikin Power Bar gives the count of Bullish, Neutral, and Bearish stocks in an ETF or list. The width of the Neutrals always stays the same, while the Bullish and Bearish counts expand according to which has more. This makes it easy to compare the number of Bullish vs Bearish stocks.\r\n\r\n[industry] This is the industry to which the stock is associated according to our data provider, S&amp;P Global Market Intelligence. The industry will be colored green (strong) if it has a higher number of bullish than bearish stocks, and red (weak) if it has a lower number of bullish than bearish stocks.\r\n\r\n[historic_price_chart] The area chart shows the symbol’s historic, adjusted (for splits and corporate actions) closing price for the corresponding date. The blue line graph is the Long Term Trend (200-day Double-Exponential Moving Average), and the bar at the bottom of the chart is the Rating Trend, which represents a pro forma projection of the latest model’s rating results for the corresponding date.\r\n\r\n[money_flow] Chaikin Money Flow is a proprietary measure of net accumulation (green) or distribution (red). This is a simple, effective way of including volume in your analysis, and has become an industry standard tool. CMF ranges from +100 to -100. A stock which closes at its high 21 days in a row will have a value of 100. A value of +25 or -25 is considered strong accumulation or distribution.\r\n\r\n[rel_strength] This indicator provides a consistently scaled indication of a stock’s recent performance relative to the S&amp;P 500. When the gradient gets to green, this indicates the stock has been strongly outperforming the market. Relative Strength on the SPY chart, will compare SPY to the IWV—the Russell 3000 index which includes a blend of large, mid, and small cap stocks.\r\n\r\n[financials] The Power Gauge considers 5 key factors when rating a company’s financial standing – Long Term Debt to Equity, Price to Book, Return on Equity, Price to Sales, and Free Cash Flow. The exact calculations and weightings are proprietary, but all factors in the Power Gauge have been normalized so that GREEN IS GOOD and RED IS RISK.\r\n\r\n[earnings] The Power Gauge considers 5 key factors when rating a company’s earnings performance – Earnings Growth, Earnings Surprise, Earnings Trend, Projected Price to Earnings, and Earnings Consistency. The exact calculations and weightings are proprietary, but all factors in the Power Gauge have been normalized so that GREEN IS GOOD and RED IS RISK.\r\n\r\n[technicals] The Power Gauge considers 5 key factors when rating a company’s stock price and volume actions (technicals) – Relative Strength vs the Market, Chaikin Money Flow, Price Strength, Price Trend Rate of Change, and Volume Trend. The exact calculations and weightings are proprietary, but all factors in the Power Gauge have been normalized so that GREEN IS GOOD and RED IS RISK.\r\n\r\n[experts] The Power Gauge considers 5 key factors when rating a company’s Expert activity – Estimate Trend, Short Interest, Insider Activity, Analyst Rating Trend, and Industry Relative Strength. The exact calculations and weightings are proprietary, but all factors in the Power Gauge have been normalized so that GREEN IS GOOD and RED IS RISK.",
                "post_title": "Power Pulse Report Explanations",
                "post_excerpt": "",
                "post_status": "publish",
                "comment_status": "closed",
                "ping_status": "closed",
                "post_password": "",
                "post_name": "27613",
                "to_ping": "",
                "pinged": "",
                "post_modified": "2021-09-09 14:57:34",
                "post_modified_gmt": "2021-09-09 18:57:34",
                "post_content_filtered": "",
                "post_parent": 0,
                "guid": "https://dev.chaikinanalytics.com/insights/?p=27613",
                "menu_order": 0,
                "post_type": "post",
                "post_mime_type": "",
                "comment_count": "0",
                "filter": "raw"
                }
                ]
                },
                "status": "ok"
                }
            this.checkCaseForDescription(resp, this.id, this.defaultTextForPopup);
        });
    }

    checkCaseForDescription(resp, id, defaultText) {
        if (resp['status'] === 'ok') {
            if (resp['0'] && resp['0']['66'] && resp['0']['66'][0] && resp['0']['66'][0]) {
                const descriptionObj = resp['0']['66'][0]['post_content'];
                const postContent = descriptionObj.split('\r\n\r\n');
                if (descriptionObj.includes(id)) {
                    this.description = this.searchStringFromArray(postContent, id);
                } else {
                    this.description = defaultText;
                }
            }

        } else {
            this.description = defaultText;
        }
    }

    searchStringFromArray(postContent, stringToSearch) {
        let postString = '';
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < postContent.length; i++) {
            if (postContent[i].includes(stringToSearch)) {
                postString = postContent[i].split(']')[1];
                break;
            }
        }
        if (postString.includes('<a')) {
            postString = postString.replace('<a', '<br><a');
        }
        return postString;
    }
}