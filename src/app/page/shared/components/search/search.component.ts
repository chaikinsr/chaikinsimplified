import { Component, Input, OnChanges, OnInit, ViewChild, ElementRef, Output, EventEmitter } from "@angular/core";
import { LoginDetailModel, NavigationListWithPowbar, SymbolDataRootObject, SymbolLookUp } from 'src/app/core/data-models/service-model';
import { APIService } from 'src/app/core/http-services/api.service';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from 'src/app/core/services/data.service';
import { AppDataService } from 'src/app/core/services/app-data.service';
import { AuthService } from 'src/app/core/http-services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import {
  ListNavigationSort,
  LocalStorage,
  RedirectParameter,
  SharedUtil,
  Sort,
} from 'src/app/core/services/utilily/utility';
import { GlobalListWithPowbarModel, NavigationListWithPowbarModel, ProductAccessModel, UserRoleMappingModel } from "src/app/core/data-models/app-model";
import { CreateListDialogComponent } from "../create-list-dialog/create-list-dialog.component";
import { MatDialog } from "@angular/material/dialog";
import { map } from "rxjs/operators";
import { ToastService } from "src/app/core/services/toast-service";



@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
    @ViewChild('symbolSearch', { static: false }) searchElement: ElementRef;
    symbolLookupData: SymbolLookUp[] = [];
    etfSymbolLookupData: SymbolLookUp[] = [];
    StockSymbolLookupData: SymbolLookUp[] = [];
    userList: GlobalListWithPowbarModel;
    productAccessModel: ProductAccessModel;
    @Output()selectedSymbolOnLookUp  = new EventEmitter<any>();
    @Output()selectedListId  = new EventEmitter<any>();
    searchSymbolText = '';
    selectedSymbol: string;
    showSearch = false;
    watchlistDataModel: GlobalListWithPowbarModel = new GlobalListWithPowbarModel();
    userAccess: UserRoleMappingModel;


    ngOnInit(): void {
      this.userAccess = this.appDataService.userRoleMappingModel;
    
   //   this.productAccessModel = this.appDataService.productAccessModel;
    //   setTimeout(() => {
    //   this.userList = this.appDataService.userList;
    // },  500);
    }

    ngOnChanges(): void {


//this.userList = this.appDataService.userList;

      setTimeout(() => {
        if (this.searchElement) {
          this.searchElement.nativeElement.focus();
        }
      },  10);
      this.init();
  //  }
  }

  init(): void {
    setTimeout(() => {
      this.userList = this.appDataService.userList;
    },  300);
  //  this.userList =  this.appDataService.userList;
    this.getSymbolData();

  }
    constructor(
        private api: APIService,
        private cookieService: CookieService,
        private dataService: DataService, public router: Router,
        private route: ActivatedRoute,
        public appDataService: AppDataService,
        public auth :AuthService,
        public dialog: MatDialog ,
        public toastService: ToastService
    ) {
        // if (!this.userDetails) {
        //   this.userDetails = this.getUserDetail();
        // }
       // console.log("dashboard cons", this.userDetails);
    }

    getSymbolData(): void {
        let symbol = LocalStorage.getItem('mainSymbol');
        this.recentlyReviewSymbol(symbol);
      }
      recentlyReviewSymbol(symbol): void {
    this.route.queryParams.subscribe(params => {
      if (params["data"]) {
        const data = JSON.parse(params["data"]);
        const navigationExtras = RedirectParameter.getNavigationExtrasDataForResearch(symbol, null, null, null);
        this.dataService.recentlyViewedData.setSymbol(data.symbol);
        this.router.navigate(["pgr/#/"], navigationExtras);
      }
    });
  }
    lookupSymbol(symbol: string): void {
        if (symbol == '') {
          this.resetSearchSymbol();
        }
        setTimeout(() => {
          this.api.symbolLookup(symbol, 'symbol').subscribe((resp) => {
            this.symbolLookupData = resp as SymbolLookUp[]; 
            this.symbolLookupData.forEach(element => {
              element.rating = SharedUtil.getPgrRating(
                element.PGR,
                element.raw_PGR
              );
              element.imageSrc = SharedUtil.getPgrArcWithoutNeutralValue(element.PGR, element.raw_PGR, element.is_etf);
            });
            // console.log(this.symbolLookupData);
            this.userList = this.appDataService.userList;
            this.etfSymbolLookupData = this.symbolLookupData.filter(
              (symbol) => symbol.is_etf
            );
            this.StockSymbolLookupData = this.symbolLookupData.filter(
              (symbol) => !symbol.is_etf
            );
          });
      this.showBox = true;

        }, 100);
       
    }

    symbolLoadOnEnter() {
      if (this.symbolLookupData.length === 1) {
        this.selectedSymbolOnLookUp.emit(this.symbolLookupData[0]);
        this.resetSearchSymbol();
      }
    }

    resetSearchSymbol(): void {
        this.searchSymbolText = '';
        this.symbolLookupData = [];
        this.etfSymbolLookupData = [];
        this.StockSymbolLookupData = [];
    }

    onSymbolSelect(event, data: SymbolLookUp): void {
      this.selectedSymbolOnLookUp.emit(data);
    //  LocalStorage.setItem('mainSymbol', data.Symbol);
     // let priceMoveData = this.listSymbolsPriceForSelectedTimeFrameArray;
     // let idx = priceMoveData.findIndex((item) => (item.symbol).toLowerCase() == data.Symbol.toLowerCase());
      // if(idx<0) {
      //   LocalStorage.setItem('listId', -1);
      // } else {
      //   LocalStorage.setItem('listId', this.selectedListId);
      // }
     // this.dataService.recentlyViewedData.setSymbol(data.Symbol);
      //this.init();
       this.resetSearchSymbol();
       
      // this.redirectOnPgr(data.Symbol);
    //  window.location.replace(window.location.protocol + "//" + window.location.host + "/pgr/#/");
    }

    
  showBox = true;
    onClickedOutside(e: Event) {
      this.showBox = false;
    }
    createNewList(event: any , symbol): void {
      console.log("CreateList" , symbol);
    //  event.stopPropagation();
      const dialogRef = this.dialog.open(CreateListDialogComponent, {
        data : symbol
      });
      dialogRef.afterClosed().subscribe(listId => {
        if (listId) {
          this.selectedListId.emit(listId);
          LocalStorage.setItem('listId' , listId);
          if (!symbol){
            this.getUserWatchlistData('');
          
           // this.perpareUserWatchlistData(listId,"onchange" );
          }
         // this.selectedWatchListId.emit(LocalStorage.getItem('listId'));
         // this.watchlistDataUpdate.emit(this.watchlistDataModel);
          // this.getUserWatchlistData('');
          this.toastService.showSuccess('successfully added');
        }
      });
    }
    addTickerIntoList(symbol , list): void {
  
      const userDetail: LoginDetailModel = LocalStorage.getItem('userDetail');
      this.api.addStockIntoList(symbol,list.listId
        ,userDetail.UID).subscribe(resp => {
          if(resp){
          LocalStorage.setItem('listId', list.listId);
          LocalStorage.setItem('mainSymbol', symbol);
          this.selectedListId.emit(list.listId);
       //   this.watchlistDataUpdate.emit(true);
          this.toastService.showSuccess('Successfully added');
        //  this.getUserWatchlistData('');
          }
        } )
    }

    redirectOnPgr(symbol): void {
      LocalStorage.setItem('mainSymbol', symbol);
      this.router.navigate(["/"]
      // window.location.replace(
      //   window.location.protocol +
      //   '//' +
      //   window.location.host +
      //   '/pgr/'
      );
    }

    getUserWatchlistData(value): void {
      let userDetails = LocalStorage.getItem('userDetail');
      const httpUserWatchlist$ = this.api.getUserWatchlistData(userDetails.UID);
      httpUserWatchlist$
        .pipe(
          map((resp) => {
          
            if (resp.status) {
              const userWatchlistData = resp.data as NavigationListWithPowbar[];
              this.appDataService.userList = resp.data;
              return this.perpareUserWatchlistData(userWatchlistData, value);
            }
            return [];
          })
        )
        .subscribe((userList: GlobalListWithPowbarModel) => {
          
          //  console.log("userList",userList);
  
        });
    }
  
    perpareUserWatchlistData(
      listData: NavigationListWithPowbar[] , value
    ): GlobalListWithPowbarModel {
      this.watchlistDataModel = new GlobalListWithPowbarModel();
      this.watchlistDataModel.data = [];
    //  this.setListIdOnListChange(listData);
      if (listData) {
        listData.forEach(data => {
  
          const navigationListWithPowbarModel = new NavigationListWithPowbarModel();
          // const selectedData = new specialList();
          const userDetails = LocalStorage.getItem('userDetail');
        //   if(data.listName === 'Power Gauge Investor Portfolio'  || data.listName === 'Power Gauge Report'){
        //       selectedData.listName =  data.listName;
        //       selectedData.listId = data.listId;
        //       selectedData.symbolCount = data.symbolCount;
        //  }
        
         // this.loadWatchlistData(this.selectedListIdData);
          // console.log( this.selectedListIdData);
          navigationListWithPowbarModel.listId = data.listId;
          navigationListWithPowbarModel.hasChart = data.hasChart;
          navigationListWithPowbarModel.isExtenable = data.isExtenable;
          navigationListWithPowbarModel.description = data.description;
          navigationListWithPowbarModel.listName = data.listName;
          navigationListWithPowbarModel.listType = data.listType;
          navigationListWithPowbarModel.symbolCount = data.symbolCount;
          navigationListWithPowbarModel.powerBar = data.powerBar;
          navigationListWithPowbarModel.extendedData = undefined;
          navigationListWithPowbarModel.powerBarBullish = data.powerBar.green;
          navigationListWithPowbarModel.listNameFormatted = data.listName.toLowerCase();
          navigationListWithPowbarModel.powerBar = data.powerBar;
          // if(selectedData.listName){
          //   this.specialData.push(selectedData);
          // }
          this.watchlistDataModel.data.push(navigationListWithPowbarModel);
        });
  
      //   if(value !== 'onchange')
      //   this.loadWatchlistData(this.selectedListIdData);
      // }
      const sortedData = ListNavigationSort.listNavSorting(
        this.watchlistDataModel.data,
        'listName'
      );
      this.watchlistDataModel.data = sortedData;
      this.watchlistDataModel.title = 'My Lists';
     // this.setListIdOnListChange(listData);
      // console.log(this.watchlistDataModel);'
      return this.watchlistDataModel;
    }
    // selectRecentViewed(): void {
    //   this.selectedListIdData.listId = -1;
    //   this.selectedListIdData.listName = "Recently Viewed";
    //   let recentlyViewSymbol = LocalStorage.getItem('PGR_RecentlyViewedSymbols');
    //   let csvSymbols = recentlyViewSymbol.join(',');
    //   this.getTicketCsvPriceData(csvSymbols);
    // }
}
}