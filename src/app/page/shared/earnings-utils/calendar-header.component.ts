import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CalendarMonthViewDay, CalendarView } from 'angular-calendar';
import { addDays, addMonths, addWeeks, endOfDay, endOfMonth, endOfWeek, startOfDay, startOfMonth, startOfWeek, subDays, subMonths, subWeeks } from 'date-fns';

type CalendarPeriod = 'day' | 'week' | 'month';
function addPeriod(period: CalendarPeriod, date: Date, amount: number): Date {
  return {
    day: addDays,
    week: addWeeks,
    month: addMonths,
  }[period](date, amount);
}

function subPeriod(period: CalendarPeriod, date: Date, amount: number): Date {
  return {
    day: subDays,
    week: subWeeks,
    month: subMonths,
  }[period](date, amount);
}

function startOfPeriod(period: CalendarPeriod, date: Date): Date {
  return {
    day: startOfDay,
    week: startOfWeek,
    month: startOfMonth,
  }[period](date);
}

function endOfPeriod(period: CalendarPeriod, date: Date): Date {
  return {
    day: endOfDay,
    week: endOfWeek,
    month: endOfMonth,
  }[period](date);
}
@Component({
  selector: 'mwl-demo-utils-calendar-header',
  styles: [
    `
      .cal-disabled {
        background-color: #eee;
        pointer-events: none;
      }

      .cal-disabled .cal-day-number {
        opacity: 0.1;
      }
    `,
  ],
  template: `
    <div class="d-flex py-2 toggle-toolbar">
        <nav class="nav nav-pills" id="pills-tab" role="tablist">
            <a class="nav-link active" id="pills-cal-weekly-tab" data-bs-toggle="pill" href="#pills-cal" role="tab"
           aria-controls="pills-cal-weekly" aria-selected="true"  (click)="viewChange.emit('week')"
            [class.active]="view === 'week'">
               Week
             </a>
            <a class="nav-link " id="pills-cal-monthly-tab" data-bs-toggle="pill" href="#pills-cal" role="tab"
           aria-controls="pills-cal-monthly" aria-selected="true" (click)="viewChange.emit('month')"
            [class.active]="view === 'month'">
               Month
             </a>
        </nav>
        <button class="btn border btn-border-toggleStyle d-flex ms-2"
          mwlCalendarToday
          [(viewDate)]="viewDate"
          (viewDateChange)="viewDateChange.next(viewDate)">
          <span class="label">Today</span>
        </button>
    </div>
    <div class="d-flex flex-row align-items-center justify-content-center">
      <div class="d-flex p-2">
        <button 
          class="btn border btn-border-xlarge d-flex flex-column" 
          type="button"
          mwlCalendarPreviousView
          [view]="view"
          [(viewDate)]="viewDate"
          (click)="decrement()"
          [disabled]="prevBtnDisabled"
          (viewDateChange)="viewDateChange.next(viewDate)">
          <i class="bi bi-chevron-double-left"></i>
          <span class="label">Prev</span>
        </button>
      </div>
      <div class="d-flex flex-column justify-content-center align-items-center py-2 px-0 px-sm-4">
        <h2 style="font-size: 24px; font-weight:500; margin:0;">{{ viewDate | calendarDate:(view + 'ViewTitle'):locale }}</h2>
      </div>
      <div class="d-flex p-2">
        <button 
          class="btn border btn-border-xlarge d-flex flex-column"
          type="button"
          mwlCalendarNextView
          [view]="view"
          [(viewDate)]="viewDate"
          (click)="increment()"
          [disabled]="nextBtnDisabled"
          (viewDateChange)="viewDateChange.next(viewDate)">
          <i class="bi bi-chevron-double-right"></i>
          <span class="label">Next</span>
        </button>
      </div>
    </div>
  `
})
export class CalendarHeaderComponent {
  @Input() view: string;
  view1: CalendarView | CalendarPeriod = CalendarView.Week;
  @Input() viewDate: Date;
  @Input() locale: string = 'en';
  @Input() sentToEarningCalendar: any;
  @Output() viewChange: EventEmitter<string> = new EventEmitter();
  @Output() viewDateChange: EventEmitter<Date> = new EventEmitter();
  minDate: Date = subMonths(new Date(), 3);
  maxDate: Date = addMonths(new Date(), 3);
  prevBtnDisabled: boolean = false;
  nextBtnDisabled: boolean = false;
  count = 0;
  defaultValue: Date;

  ngOnInit(): void {
    this.defaultValue = this.viewDate;
  }

  ngOnChanges(): void {
    if (this.sentToEarningCalendar === 'nextweek' && this.count === 0) {
      this.count = 1;
      this.increment();
      this.viewDateChange.next(this.viewDate);
    } else if ((this.sentToEarningCalendar === "today" || this.sentToEarningCalendar === "week") && this.count === 1) {
      this.count = 0;
      this.viewDateChange.next(this.defaultValue);
    }
  }



  increment(): void {
    this.changeDate(addPeriod(this.view1, this.viewDate, 1));
  }

  decrement(): void {
    this.changeDate(subPeriod(this.view1, this.viewDate, 1));
  }

  today(): void {
    this.changeDate(new Date());
  }

  dateIsValid(date: Date): boolean {
    return date >= this.minDate && date <= this.maxDate;
  }

  changeDate(date: Date): void {
    this.viewDate = date;
    this.dateOrViewChanged();
  }

  dateOrViewChanged(): void {
    this.prevBtnDisabled = !this.dateIsValid(
      endOfPeriod(this.view1, subPeriod(this.view1, this.viewDate, 1))
    );
    this.nextBtnDisabled = !this.dateIsValid(
      startOfPeriod(this.view1, addPeriod(this.view1, this.viewDate, 1))
    );
    if (this.viewDate < this.minDate) {
      this.changeDate(this.minDate);
    } else if (this.viewDate > this.maxDate) {
      this.changeDate(this.maxDate);
    }
  }

  beforeMonthViewRender({ body }: { body: CalendarMonthViewDay[] }): void {
    body.forEach((day) => {
      if (!this.dateIsValid(day.date)) {
        day.cssClass = 'cal-disabled';
      }
    });
  }
}
