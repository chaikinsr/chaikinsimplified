import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from "@angular/core";
import { Subject } from "rxjs";
import { map, takeUntil } from "rxjs/operators";
import { ChecklistModel, HeaderDataModel, ProductAccessModel, UserRoleMappingModel } from "src/app/core/data-models/app-model";
import { ChecklistObject, PgrContextSummary, WordpressIdMapping } from "src/app/core/data-models/service-model";
import { APIService } from "src/app/core/http-services/api.service";
import { AppDataService } from "src/app/core/services/app-data.service";
import { DataService } from "src/app/core/services/data.service";
import { SharedUtil } from "src/app/core/services/utilily/utility";


@Component({
    selector: 'app-checklist',
    templateUrl: './checklist.component.html',
    styleUrls: ['./checklist.component.scss']
  })
  
  export class ChecklistComponent implements OnInit, OnChanges {
    @Input() headerDataModel: HeaderDataModel = new HeaderDataModel();
    @Input() pgrContextSummary: PgrContextSummary = new PgrContextSummary();
    @Input() setCheckListAccess : boolean;
    documentClickEventRegister: boolean = false;
    checklistModel: ChecklistModel;

    @ViewChild('qickDescriptionPopup', { static: false }) qickDescriptionPopup: any;
    showQuickScanDescription: boolean = false;
    createQickDescriptionXPosition: any = 0;
    createQickDescriptionYPosition: any = 0;
    fixedCreateQickDescriptionYPosition: any = 0;
    quickDescriptionId: string;
    quickDescriptionIdCheck: string;
    quickDescriptionDefaultText: string;
    wordpressIdMapping: WordpressIdMapping = new WordpressIdMapping();
    fixedScrollTop: any = 0;
    currentScrollTop: any = 0;
    setWidth: any = 50;
    productAccessModel: ProductAccessModel;
    userAccess: UserRoleMappingModel;

    private destroyed$ = new Subject();
    ngOnInit(): void {
      this.userAccess = this.appDataService.userRoleMappingModel;
      // this.productAccessModel =  this.appDataService.productAccessModel;
     // this.setCheckListAccess = true;
      this.dataService.showAllPopup.pipe(takeUntil(this.destroyed$)).subscribe(status => {
        if(status) {
          let powerGaugeDescription:HTMLElement = document.getElementById('powerGaugeDescription') as HTMLElement;
          powerGaugeDescription.click();
         // this.moneyFlow.nativeElement.click();
        } else {
          this.onCloseQuickDescriptionPopupEvent();
        }
          
      });

      if(!this.setCheckListAccess){
        this.setWidth = 100;
      }
    }
    constructor(
      private api: APIService,
      private dataService: DataService,
      public appDataService: AppDataService,
    ){}

    ngOnChanges(changes: SimpleChanges): void {
      this.getChecklistStocks();
    }

    getChecklistStocks() {
      const httpChecklistStocks$ = this.api.getChecklistStocks(this.headerDataModel.symbol)

      httpChecklistStocks$
      .pipe(
          map((resp) => {
          
              const checklistData = resp as unknown as ChecklistObject;
              let checklistMappingData = this.setChecklistDataMapping(checklistData);
              return checklistMappingData;
          
          })
      )
      .subscribe((data: ChecklistModel) => {
        this.checklistModel = data;
       // console.log(data);
          // this.appDataService.productPreference = data;
          // this.productPreference = data;
          // this.productMapping = this.getMappingForProduct(data);
          // this.appDataService.productMapping = this.productMapping;
          // this.setTopHeaderData();
      });
    }

    setChecklistDataMapping(checklistData: ChecklistObject): ChecklistModel {
      let data = new ChecklistModel();
    //  console.log(this.headerDataModel);
      if(checklistData){
      data.relativeStrength = checklistData.relativeStrength;
      data.ltTrend = checklistData.ltTrend;
      data.pgr = checklistData.pgr;
      data.industry = checklistData.industry;
      data.overboughtOversold = checklistData.overboughtOversold;
      data.moneyFlow = checklistData.moneyFlow;
      data.stockStatus = checklistData.stockStatus;
      data.sentence1 = checklistData.sentence1;
      data.sentence2 = checklistData.sentence2;
      data.sentence3a = checklistData.sentence3a;
      data.sentence3b = checklistData.sentence3b;
      data.lastPrice = checklistData.lastPrice;
      data.change = checklistData.change;
      data.changePercentage = checklistData.changePercentage;
      data.symbol = checklistData.symbol;
      data.pgrRating = checklistData.pgrRating;
      data.rawPgrRating = checklistData.rawPgrRating;
      data.sentence4 = checklistData.sentence4;
      data.strengthCount = checklistData.strengthCount;
      if(data.strengthCount === 3) {
        data.strengthCountClass = 'positive';
      } else if (data.strengthCount === 2) {
        data.strengthCountClass = 'neutral-label';
      } else {
        data.strengthCountClass = 'negative';
      }
     // data.strengthCountClass = data.strengthCount > 2 ? 'positive' : 'negative';
      data.strengthOverallColor = data.strengthCount > 1 ? 'up-bg' : 'down-bg';
      data.timingCount = checklistData.timingCount;

      if(data.timingCount === 3) {
        data.timingCountClass = 'positive';
      } else if (data.timingCount === 2) {
        data.timingCountClass = 'neutral-label';
      } else {
        data.timingCountClass = 'negative';
      } 

    //  data.timingCountClass = data.timingCount > 1 ? 'positive' : 'negative';
      data.timingOverallColor = data.timingCount > 1 ? 'up-bg' : 'down-bg';
      data.pgrColor = SharedUtil.getColorForRating(data.pgr) + '-label';
      data.pgrIconColor = this.getColorClassForChecklist(data.pgr);
      data.pgrIconClass = this.getIconClassForChecklist(data.pgr);
      
      data.relativeStrengthColor = this.getColorClassForChecklist(data.relativeStrength);
      data.relativeStrengthIconColor = this.getColorClassForChecklist(data.relativeStrength);
      data.relativeStrengthIconClass = this.getIconClassForChecklist(data.relativeStrength);

      data.industryColor = this.getColorClassForChecklist(data.industry);
      data.industryIconColor = this.getColorClassForChecklist(data.industry);
      data.industryIconClass = this.getIconClassForChecklist(data.industry);

      data.overboughtOversoldColor = this.getColorClassForChecklist(data.overboughtOversold);
      data.overboughtOversoldIconColor = this.getColorClassForChecklist(data.overboughtOversold);
      data.overboughtOversoldIconClass = this.getIconClassForChecklist(data.overboughtOversold);

      data.ltTrendColor = this.getColorClassForChecklist(data.ltTrend);
      data.ltTrendIconColor = this.getColorClassForChecklist(data.ltTrend);
      data.ltTrendIconClass = this.getIconClassForChecklist(data.ltTrend);

      data.moneyFlowColor = this.getColorClassForChecklist(data.moneyFlow);
      data.moneyFlowIconColor = this.getColorClassForChecklist(data.moneyFlow);
      data.moneyFlowIconClass = this.getIconClassForChecklist(data.moneyFlow);

      if(data.sentence1) {
        let arr = data.sentence1.split(/[<<\>>]/);
        [data.sentenceBreakFirst,data.sentenceBreakSecond,data.sentenceBreakThird,
          data.sentenceBreakFourth, data.sentenceBreakFifth] = arr.filter(n => n);
      }



      }

      return data;
    }

    getColorClassForChecklist(rating: string) {
      let color = 'grey';
      switch (rating) {
        case 'bullish':
        case 'Bullish':
        case 'Buy':
        case 'Very Bullish':
        case 'Strong Buy':
        case 'Strong':
        case 'Early':
        case 'Optimal':
          color = 'positive';
          break;
        case 'bearish':
        case 'Bearish':
        case 'Sell':
        case 'Very Bearish':
        case 'Strong Sell':
        case 'Weak':
        case 'Wait':
          color = "negative";
          break;
        case 'neutral':
        case 'Neutral':
        case 'Neutral+':
        case 'Neutral-':
        case 'Hold':
          color = 'neutral-label';
          break;
        default:
          color  = 'grey';
          break;
      }
      return color;
    }

    getIconClassForChecklist(rating: string) {
      let iconClass = 'bi-check-circle-fill';
      switch (rating) {
        case 'bullish':
        case 'Bullish':
        case 'Buy':
        case 'Very Bullish':
        case 'Strong Buy':
        case 'Strong':
        case 'Early':
        case 'Optimal':
          iconClass = 'bi-check-circle-fill';
          break;
        case 'bearish':
        case 'Bearish':
        case 'Sell':
        case 'Very Bearish':
        case 'Strong Sell':
        case 'Weak':
        case 'Wait':
          iconClass = "bi-x-circle-fill";
          break;
        case 'neutral':
        case 'Neutral':
        case 'Neutral+':
        case 'Neutral-':
        case 'Hold':
          iconClass = 'bi-dash-circle-fill';
          break;
        default:
          iconClass  = 'bi-dash-circle-fill';
          break;
      }
      return iconClass;
    }

    offClickHandler(event: any) {
      if (
        this.qickDescriptionPopup &&
        this.qickDescriptionPopup.nativeElement &&
        !this.qickDescriptionPopup.nativeElement.contains(event.target)
      ) {
        // check click origin
        this.showQuickScanDescription = false;
        if (this.documentClickEventRegister) {
          let ele = document.getElementById('powerGaugeDescription');
          document.removeEventListener('click', this.offClickHandler.bind(this));
          this.documentClickEventRegister = false;
          this.showQuickScanDescription = false;
          this.quickDescriptionIdCheck = '0';
        }
      }
    }
  
    openQuickDescriptionPopup(id: string, defaultText: string, event) {
      event.stopPropagation();
      // if (!this.documentClickEventRegister) {
      //   let ele = document.getElementById('powerGaugeDescription');
      //   document.addEventListener('click', this.offClickHandler.bind(this));
      //   this.documentClickEventRegister = true;
      // }

      if(!this.showQuickScanDescription) {
        this.onClickPortfolioOpenPopup(event);
        this.quickDescriptionId = id;
        this.quickDescriptionDefaultText = defaultText;
    
        this.openQuickDescription();
      } else {
        this.showQuickScanDescription = false;
        this.quickDescriptionIdCheck = '0';
      }
      
    }
  
    onClickPortfolioOpenPopup(event: any) {
      this.createQickDescriptionXPosition = -1;
      this.createQickDescriptionYPosition = -1;
      this.fixedCreateQickDescriptionYPosition =
      this.createQickDescriptionYPosition;
      this.fixedScrollTop = this.currentScrollTop;
    }
  
    openQuickDescription() {
      if (this.quickDescriptionId !== this.quickDescriptionIdCheck) {
        this.showQuickScanDescription = true;
        this.quickDescriptionIdCheck = this.quickDescriptionId;
      } else {
        this.showQuickScanDescription = false;
        this.quickDescriptionIdCheck = '0';
      }
    }
  
    onCloseQuickDescriptionPopupEvent() {
      this.showQuickScanDescription = false;
      this.quickDescriptionIdCheck = '';
    }


    //Toggle Checklist Shrink/Expand for mobile
    shrinkChecklist = false;  
     
    toggleChecklistShrink() {  
      this.shrinkChecklist = !this.shrinkChecklist;  
    }  
  
}