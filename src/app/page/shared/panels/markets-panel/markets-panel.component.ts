import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { map } from 'rxjs/operators';
import { MarketPanelData } from 'src/app/core/data-models/app-model';
import { GetMarketPanelDataObject } from 'src/app/core/data-models/service-model';
import { APIService } from 'src/app/core/http-services/api.service';
import { LocalStorage, SharedUtil } from 'src/app/core/services/utilily/utility';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';



@Component({
    selector: 'app-markets-panel',
    templateUrl: './markets-panel.component.html',
    styleUrls: ['./markets-panel.component.scss'],
    encapsulation: ViewEncapsulation.None
  })
  
  export class MarketsPanelComponent implements OnInit {
    hideSentancePopUp = true;
    html: SafeHtml;
    marketPanelDataObjectArray: MarketPanelData[] = [];
    getMarketPanelDataObject = new GetMarketPanelDataObject();
    todayDate = moment().format("h:mm a");
    ngOnInit(): void {

    const httpGetMarketPanelData$ =   this.api.getMarketPanelData();
    httpGetMarketPanelData$.pipe(map((resp) => {
     const marketPanelData =  resp as GetMarketPanelDataObject;
     this.html = this._sanitizer.bypassSecurityTrustHtml(marketPanelData.summarySentance);
     this.getMarketPanelDataObject = marketPanelData;
    return this.prepareMarketPanelData(marketPanelData);
    })).subscribe(resp => {
      this.marketPanelDataObjectArray = resp;
    });

    }

    constructor(
      private api: APIService,
      public router: Router,protected _sanitizer: DomSanitizer
    ) {
     
    }

   prepareMarketPanelData(marketPanelData): MarketPanelData[]{
    let marketPanelDataArray: MarketPanelData[] = [];
    marketPanelData.data.forEach(element => {
const marketPanelDataObject = new MarketPanelData();
marketPanelDataObject.symbol = element.Symbol;
marketPanelDataObject.change = element.Change;
marketPanelDataObject.last = element.Last;
marketPanelDataObject.name = element.name;

const lastInString = SharedUtil.formatNumber(element.Last);
marketPanelDataObject.lastInString = lastInString;
marketPanelDataObject.percentage = element['Percentage '];
if (element['Percentage ']) {
  let percentageChangeInString = SharedUtil.formatNumber(
    element['Percentage ']
  );
  percentageChangeInString = SharedUtil.appendSymbol(
    percentageChangeInString,
    '%'
  );
  marketPanelDataObject.percentageChangeInString = SharedUtil.appendSymbol(
    percentageChangeInString,
    ''
  );
} else {
  marketPanelDataObject.percentageChangeInString = 'N/A';
}

marketPanelDataObject.correctedPgr = element.corrected_pgr;
marketPanelDataObject.isEtf = element.is_etf;
marketPanelDataObject.rawPgr = element.raw_pgr;

if (marketPanelDataObject.percentage > 0) {
  marketPanelDataObject.colorClass = 'up';
  marketPanelDataObject.stripeClass = 'up-bg';
  marketPanelDataObject.arrowDirection = 'arrow_upward';
} else if (marketPanelDataObject.percentage < 0) {
  marketPanelDataObject.colorClass = 'down';
  marketPanelDataObject.stripeClass = 'down-bg';
  marketPanelDataObject.arrowDirection = 'arrow_downward';
}
// else {
//   marketPanelDataObject.colorClass = 'no-movement';
//   marketPanelDataObject.arrowDirection = 'minimize';
// }
marketPanelDataArray.push(marketPanelDataObject);
});
    return marketPanelDataArray;
    }

    hideSentancePopUpOnIcon(): void {
     this.hideSentancePopUp = false;
    }

    redirectOnPgr(symbol): void {
      LocalStorage.setItem('mainSymbol', symbol);
      this.router.navigate(["/"]
      );
    }
};