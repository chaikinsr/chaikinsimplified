import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

import { APIService } from 'src/app/core/http-services/api.service';
import { AppDataService } from 'src/app/core/services/app-data.service';
import { GetPowerFeedInsightDataObject } from 'src/app/core/data-models/service-model';
import {
  PowerFeedInsightData,
  UserRoleMappingModel,
} from 'src/app/core/data-models/app-model';

interface IPowerFeedData {
  postAuthor: string;
  postDate: string;
  postTitle: string;
  postContent: string;
}

interface IIssuesAndUpdatesData {
  publicationName: string;
  publicationDate: string;
  publicationTitle: string;
  publicationLink: string;
}

@Component({
  selector: 'app-pub-panel',
  templateUrl: './pub-panel.component.html',
  styleUrls: ['./pub-panel.component.scss'],
})
export class PubPanelComponent implements OnInit {
  powerFeedInsightDataObject = new PowerFeedInsightData();
  powerFeedData: IPowerFeedData;
  issuesAndUpdatesDataList: IIssuesAndUpdatesData[];
  userAccess: UserRoleMappingModel;

  constructor(
    private api: APIService,
    private appDataService: AppDataService
  ) {}

  /**
   * formatDate - Format date to Month DD, YYYY
   * @param stringDate {string} - Date in string format
   * @returns {string} - Formatted date (Month DD, YYYY)
   */
  private formatDate(stringDate: string): string {
    const months = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ];

    const formattedDate =
      months[+moment(stringDate).format('M') - 1] +
      ' ' +
      moment(stringDate).format('D') +
      ', ' +
      moment(stringDate).format('Y').toString();
    return formattedDate;
  }

  /**
   * preparePowerFeedData - Prepare Power Feed Data for UI
   * @param latestPowerFeedData incoming data from API
   * @returns powerFeedData object with formatted date required for UI
   */
  private preparePowerFeedData(
    latestPowerFeedData: GetPowerFeedInsightDataObject
  ): IPowerFeedData {
    return {
      postAuthor: latestPowerFeedData.post_author,
      postDate: this.formatDate(latestPowerFeedData.post_date),
      postTitle: latestPowerFeedData.post_title,
      postContent: latestPowerFeedData.post_content,
    };
  }

  /**
   * prepareIssuesAndUpdatesData - Prepare Issues and Updates Data for UI
   * @param responseIssuesAndUpdatesData Incoming data from API
   * @returns {IIssuesAndUpdatesData} issuesAndUpdatesData object with formatted date required for UI
   */
  private prepareIssuesAndUpdatesData(
    responseIssuesAndUpdatesData: Record<string, string>
  ): IIssuesAndUpdatesData {
    return {
      publicationName: responseIssuesAndUpdatesData.publicationName,
      publicationDate: this.formatDate(responseIssuesAndUpdatesData.date),
      publicationTitle: responseIssuesAndUpdatesData.title,
      publicationLink: responseIssuesAndUpdatesData.link,
    };
  }

  /**
   * getPowerFeedInsightData - Get Power Feed Insight Data from API
   */
  private getPowerFeedInsightData() {
  this.api.getPowerFeedInsightData().subscribe((resp) => {


    //  let  resp ={
    //     "status": "ok",
    //     "0": {
    //       "2": [
    //         {
    //           "ID": 30554,
    //           "post_author": "11",
    //           "post_date": "2023-02-14 13:45:11",
    //           "post_date_gmt": "2023-02-14 18:45:11",
    //           "post_content": "<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">In a special update on January 17, I noted that the market flashed a critical buy signal for only the 19th time since 1950. Since then, the S&P 500 Index has climbed to as high as the 4,200 level during intraday trading. This further price strength confirms the momentum signal we saw last month.<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Meanwhile, the S&P 500 closed yesterday at 4,137. It's up roughly 7.8% so far this year. And it's around 3.7% higher than where it traded on January 17.<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">This morning, the U.S. Bureau of Labor Statistics released the January update of the Consumer Price Index (\"CPI\"). The data showed that the CPI climbed 6.4% last month over the previous year. That's down from 6.5% in December. And it's the seventh straight month of cooling inflation.<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Leading up to the CPI release, fundamental bears were nervous about an uptick in this key inflation gauge. But their fears proved to be overblown.<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">While the Federal Reserve's official position is that interest rates need to be raised higher for longer, Fed Chair Jerome Powell made more measured comments last week. He's trying hard not to telegraph his next move.<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">If the decline in the CPI since its peak last June continues, it will drop below the federal funds rate as soon as next month. In other words, interest rates could be nearing their peak. That's another catalyst for higher stock prices.<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">And ultimately, that would give Powell the cover he needs to stop raising rates. And in turn, it would help him avoid creating a deep recession in the U.S. economy.<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<p style=\"font-weight: 400;\"><span style=\"font-style: inherit; font-weight: inherit;\">\u00a0<\/span><\/p>\r\n<a href=\"https:\/\/insights.chaikinanalytics.com\/Here's-Why-the-Fundamental-Bears-Are-Wrong\/\">Continue Reading &gt;&gt;\u00a0<\/a>",
    //           "post_title": "Here's Why the Fundamental Bears Are Wrong",
    //           "post_excerpt": "",
    //           "post_status": "publish",
    //           "comment_status": "closed",
    //           "ping_status": "closed",
    //           "post_password": "",
    //           "post_name": "sp-500-index-grinds-higher-as-tech-rally-continues-while-energy-rebounds-2-3-3-2-2-2-2-2-2-2-2-2-2-3-2-2-2-6-3-2-2-2-3-2-3-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-2-3-2-2-4-3-2-2-3-2-2-2-2-2-3-2-139",
    //           "to_ping": "",
    //           "pinged": "",
    //           "post_modified": "2023-02-14 13:45:11",
    //           "post_modified_gmt": "2023-02-14 18:45:11",
    //           "post_content_filtered": "",
    //           "post_parent": 0,
    //           "guid": "https:\/\/app.chaikinanalytics.com\/insights\/?p=30554",
    //           "menu_order": 0,
    //           "post_type": "post",
    //           "post_mime_type": "",
    //           "comment_count": "0",
    //           "filter": "raw"
    //         },
    //         {
    //           "ID": 30462,
    //           "post_author": "11",
    //           "post_date": "2023-01-09 13:05:14",
    //           "post_date_gmt": "2023-01-09 18:05:14",
    //           "post_content": "<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">After one week, 2023 is off to a positive start. On Friday, the S&amp;P 500 Index closed at 3,895. That\u2019s roughly 1.4% higher than where it ended last year. But will it last?<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Since my last Market Insights update on December 12, the benchmark index is down 1%. And it\u2019s nearly 19% below its all-time high from a year ago.<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">In this month\u2019s Market Insights update, we\u2019ll discuss some recent economic reports that strongly suggest the U.S. economy is in a recession. And yet, the stock market isn\u2019t concerned after the first week of the year. For example, on Friday, a \u201cgoldilocks\u201d December jobs report triggered a fast 2.3% rally.<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">The December update of the Consumer Price Index and the report on initial jobless claims for the month will both come out this Thursday. These key reports will help guide the Federal Reserve\u2019s February interest-rate decision. But is the market focusing too much on yesterday\u2019s war on inflation instead of future earnings right now?<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">You see, another earnings season begins in earnest on Friday, when major banks Bank of America (BAC) and JPMorgan Chase (JPM) report. And it\u2019s \u201cpay the piper\u201d time for the private-equity debt that they\u2019ve kept on their balance sheets for months from the likes of Twitter and Citrix Systems. How will the market react to big write-downs?<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">At the end of the day, corporate earnings move the stock market. And 2023 will be a bad year for corporate profits and margins. So as we dig into this month\u2019s Market Insights update, remember this\u2026<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Economic recessions do lead to earnings shortfalls. So\u2026 buyers beware.<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<p style=\"font-weight: 400;\"><span style=\"font-style: inherit; font-weight: inherit;\">\u00a0<\/span><\/p>\r\n<a href=\"https:\/\/insights.chaikinanalytics.com\/stocks-are-focused-on-inflation-and-interest-rates-but-buyers-beware\/\">Continue Reading &gt;&gt;\u00a0<\/a>",
    //           "post_title": "Stocks Are Focused on Inflation and Interest Rates... But Buyers Beware",
    //           "post_excerpt": "",
    //           "post_status": "publish",
    //           "comment_status": "closed",
    //           "ping_status": "closed",
    //           "post_password": "",
    //           "post_name": "sp-500-index-grinds-higher-as-tech-rally-continues-while-energy-rebounds-2-3-3-2-2-2-2-2-2-2-2-2-2-3-2-2-2-6-3-2-2-2-3-2-3-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-2-3-2-2-4-3-2-2-3-2-2-2-2-2-3-2-114",
    //           "to_ping": "",
    //           "pinged": "",
    //           "post_modified": "2023-01-10 08:24:09",
    //           "post_modified_gmt": "2023-01-10 13:24:09",
    //           "post_content_filtered": "",
    //           "post_parent": 0,
    //           "guid": "https:\/\/app.chaikinanalytics.com\/insights\/?p=30462",
    //           "menu_order": 0,
    //           "post_type": "post",
    //           "post_mime_type": "",
    //           "comment_count": "0",
    //           "filter": "raw"
    //         },
    //         {
    //           "ID": 30383,
    //           "post_author": "11",
    //           "post_date": "2022-12-12 13:18:54",
    //           "post_date_gmt": "2022-12-12 18:18:54",
    //           "post_content": "<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">The S&P 500 Index closed at 3,934 on Friday. Since my last Market Insights update on November 14, the benchmark index is down about 1.5%. And as we head into the final weeks of 2022, it's down around 18% on the year.\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">In the short term, one broad topic is driving the stock market. I'm talking about the latest headlines related to inflation and analysts' guesses and estimates on how it could alter the Federal Reserve's interest-rate policies.\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">For example, on Friday, a slightly worse than expected November update of the Producer Price Index triggered a steep, late-day sell-off. The news was just the latest proof that investors are jittery today, leading to a lot of choppy action in the markets.\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">This week, the November update of the Consumer Price Index (\"CPI\") will come out on Tuesday at 8:30 a.m. Then, on Wednesday at 2 p.m., we'll get the latest announcement from the Fed about its interest-rate hikes and whether we can expect further tightening of monetary conditions.\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Computer algorithms will parse key words from those announcements and mindlessly push the markets around. So with that in mind, the week will likely be volatile in both the stock and bond markets.\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">The way it has looked lately, a typical year-end rally \u2013 which is often called a \"Santa Claus rally\" due to the time of year \u2013 appears likely. However, if the CPI news or Fed's announcement Wednesday are viewed as \"bearish\" by the markets, all bets for a rally are off. In that case, get ready for a rough ride.\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">At the end of the day, earnings move the stock market over the medium to longer terms. And 2023 isn't expected to be a good year for corporate profits and margins. So with that in mind, things could get ugly.\r\n<\/li>\r\n        <li style=\"font-weight: 400;\" aria-level=\"1\">Even if a full-blown economic recession doesn't happen in 2023, a so-called \"earnings recession\" will most likely occur. So today, we'll discuss how that will play out in comparison with my \"roadmap\" for midterm election years.\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<p style=\"font-weight: 400;\"><span style=\"font-style: inherit; font-weight: inherit;\">\u00a0<\/span><\/p>\r\n<a href=\"https:\/\/insights.chaikinanalytics.com\/expect-a-year-end-rally-unless-this-happens-this-week\/\/\/\">Continue Reading &gt;&gt;\u00a0<\/a>",
    //           "post_title": "Expect a Year-End Rally... Unless This Happens This Week",
    //           "post_excerpt": "",
    //           "post_status": "publish",
    //           "comment_status": "closed",
    //           "ping_status": "closed",
    //           "post_password": "",
    //           "post_name": "sp-500-index-grinds-higher-as-tech-rally-continues-while-energy-rebounds-2-3-3-2-2-2-2-2-2-2-2-2-2-3-2-2-2-6-3-2-2-2-3-2-3-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-2-3-2-2-4-3-2-2-3-2-2-2-2-2-3-2-95",
    //           "to_ping": "",
    //           "pinged": "",
    //           "post_modified": "2022-12-12 13:18:54",
    //           "post_modified_gmt": "2022-12-12 18:18:54",
    //           "post_content_filtered": "",
    //           "post_parent": 0,
    //           "guid": "https:\/\/app.chaikinanalytics.com\/insights\/?p=30383",
    //           "menu_order": 0,
    //           "post_type": "post",
    //           "post_mime_type": "",
    //           "comment_count": "0",
    //           "filter": "raw"
    //         },
    //         {
    //           "ID": 30323,
    //           "post_author": "11",
    //           "post_date": "2022-11-14 14:17:21",
    //           "post_date_gmt": "2022-11-14 19:17:21",
    //           "post_content": "<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">On Friday, the S&amp;P 500 Index closed just shy of 4,000. That means the benchmark index climbed almost 6% on the week. And since my last Market Insights update on October 10, it's up roughly 10%.<\/li>\r\n\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Meanwhile, the Federal Reserve hasn't changed its tune. The central bank remains serious about its fight against inflation. And we just got a sign that it could be working. Last Thursday, a slightly better-than-expected October update of the Consumer Price Index (\"CPI\") triggered a massive short-covering rally.<\/li>\r\n\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Throughout most of 2022, I've shared my \"roadmap\" for the stock market in this mid-term election year. And it has played out as expected...\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">To recap, in 23 of 27 mid-term election years since 1908, stocks have either been in a bear market or just emerged from one. We expected the market to bottom in mid-to-late October. And then, we anticipated a rally of 30% or more.\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">In fact, I made a critical observation just last month. As I said, \"My bottom line is simple... the bear market is not over. The low isn't in yet, but a significant turn could still occur in late October.\"\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">So with the S&P 500 up almost 15% from its low, the key questions now are... Have we seen the market bottom? Are we in the early throes of a new bull market?\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">We'll answer those questions in this month's Market Insights update. But for those of you who can't wait... the next bull market isn't here yet.\r\n<\/li>\r\n \t\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<p style=\"font-weight: 400;\"><span style=\"font-style: inherit; font-weight: inherit;\">\u00a0<\/span><\/p>\r\n<a href=\"https:\/\/insights.chaikinanalytics.com\/stocks-will-go-where-they-want-and-the-fed-will-do-what-it-needs-to\/\/\">Continue Reading &gt;&gt;\u00a0<\/a>",
    //           "post_title": "Stocks Will Go Where They Want... And the Fed Will Do What It Needs to",
    //           "post_excerpt": "",
    //           "post_status": "publish",
    //           "comment_status": "closed",
    //           "ping_status": "closed",
    //           "post_password": "",
    //           "post_name": "sp-500-index-grinds-higher-as-tech-rally-continues-while-energy-rebounds-2-3-3-2-2-2-2-2-2-2-2-2-2-3-2-2-2-6-3-2-2-2-3-2-3-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-2-3-2-2-4-3-2-2-3-2-2-2-2-2-3-2-86",
    //           "to_ping": "",
    //           "pinged": "",
    //           "post_modified": "2022-11-14 14:17:21",
    //           "post_modified_gmt": "2022-11-14 19:17:21",
    //           "post_content_filtered": "",
    //           "post_parent": 0,
    //           "guid": "https:\/\/app.chaikinanalytics.com\/insights\/?p=30323",
    //           "menu_order": 0,
    //           "post_type": "post",
    //           "post_mime_type": "",
    //           "comment_count": "0",
    //           "filter": "raw"
    //         },
    //         {
    //           "ID": 30225,
    //           "post_author": "11",
    //           "post_date": "2022-10-10 15:06:02",
    //           "post_date_gmt": "2022-10-10 19:06:02",
    //           "post_content": "<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">The S&P 500 Index closed at roughly 3,640 on Friday. That means it's down about 10.5% since my last Market Insights update on September 12. (And as we go to press, it's down another 1.1% today.)\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Last week, the Federal Reserve told us once again that it's serious about reining in inflation. And then, the relatively optimistic September employment update on Friday likely strengthened the central bank's resolve.\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Since February, we've continued to share our roadmap for the stock market in this mid-term election year with you. And it has played out true to form.\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">To recap, in 23 of 27 mid-term election years since 1908, stocks have either been in a bear market or just come out of one. That's a pretty compelling statistic. So we've expected the stock market to bottom in the mid-to-late October. And then, we believed a robust rally of 30% or more would occur.\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">The most important questions now are, \"Where (and when) will the bottom form? And what will trigger it?\" We'll get into that this month.\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">This week, third-quarter earnings season begins. So we'll also discuss why it could potentially move the market in a big way.\r\n<\/li>\r\n        <li style=\"font-weight: 400;\" aria-level=\"1\">Also this week, the U.S. Bureau of Labor Statistics will release its next updates of the Producer Price Index and the Consumer Price Index on Wednesday and Thursday, respectively. The Cleveland Fed's \"Nowcast\" currently projects higher inflation numbers than Wall Street. For what it's worth, the market has reacted badly to \"negative surprises\" in recent months.\r\n<\/li>\r\n        <li style=\"font-weight: 400;\" aria-level=\"1\">My bottom line is simple... the bear market is not over. The low isn't in yet, but a significant turn could still occur in late October.\r\n<\/li>\r\n        <li style=\"font-weight: 400;\" aria-level=\"1\">So for now, \"weatherproof\" your portfolio. Make sure you can withstand any \"shocks\" to the system as the consequences of the Fed's tight monetary policy continues to impact the U.S. and global economies.\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<p style=\"font-weight: 400;\"><span style=\"font-style: inherit; font-weight: inherit;\">\u00a0<\/span><\/p>\r\n<a href=\"https:\/\/insights.chaikinanalytics.com\/its-october-and-its-time-to-weatherproof-your-portfolio\/\/\">Continue Reading &gt;&gt;\u00a0<\/a>",
    //           "post_title": "It's October... And It's Time to 'Weatherproof' Your Portfolio",
    //           "post_excerpt": "",
    //           "post_status": "publish",
    //           "comment_status": "closed",
    //           "ping_status": "closed",
    //           "post_password": "",
    //           "post_name": "sp-500-index-grinds-higher-as-tech-rally-continues-while-energy-rebounds-2-3-3-2-2-2-2-2-2-2-2-2-2-3-2-2-2-6-3-2-2-2-3-2-3-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-2-3-2-2-4-3-2-2-3-2-2-2-2-2-3-2-71",
    //           "to_ping": "",
    //           "pinged": "",
    //           "post_modified": "2022-10-10 15:06:02",
    //           "post_modified_gmt": "2022-10-10 19:06:02",
    //           "post_content_filtered": "",
    //           "post_parent": 0,
    //           "guid": "https:\/\/app.chaikinanalytics.com\/insights\/?p=30225",
    //           "menu_order": 0,
    //           "post_type": "post",
    //           "post_mime_type": "",
    //           "comment_count": "0",
    //           "filter": "raw"
    //         },
    //         {
    //           "ID": 30165,
    //           "post_author": "11",
    //           "post_date": "2022-09-12 10:44:37",
    //           "post_date_gmt": "2022-09-12 14:44:37",
    //           "post_content": "<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">The benchmark S&amp;P 500 Index closed at 4,067 on Friday. So it\u2019s down about 1.9% since my last Market Insights update on August 8. And that performance includes a roughly 3.6% rally last week.<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Amid the back-and-forth market, Federal Reserve Chair Jerome Powell doubled down on his tough talk with interest rates in late August at the Jackson Hole Economic Symposium. \u201cRestoring price stability will likely require maintaining a restrictive policy stance for some time,\u201d Powell said. \u201cThe historical record cautions strongly against prematurely loosening policy.\u201d<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Powell appears determined to win the inflation fight at all costs. With that in mind, the Fed is expected to raise the benchmark interest rate by another 75 basis points this month and continue to unwind its balance sheet at a pace of $90 billion per month.<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">The government will release its August updates for the Consumer Price Index and the Producer Price Index on Tuesday and Wednesday, respectively. The expectation is that inflation has peaked on a year-over-year basis. However, the Cleveland Fed\u2019s \u201cNowcast\u201d suggests an uptick in inflation is possible. That would be a \u201cbearish\u201d surprise for the market, so we\u2019ll watch the releases closely.<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Commodity prices are down from their highs and inflation is showing signs of peaking. So a lot of folks are wondering\u2026 is the bear market in stocks over?<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">We\u2019ll cover that question in detail below. But the short answer is\u2026 I don\u2019t think the bear market is over. Let\u2019s be on the alert for an October bottom.<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<p style=\"font-weight: 400;\"><span style=\"font-style: inherit; font-weight: inherit;\">\u00a0<\/span><\/p>\r\n<a href=\"https:\/\/insights.chaikinanalytics.com\/inflation-could-be-peaking-but-the-fed-keeps-talking-tough\/\">Continue Reading &gt;&gt;\u00a0<\/a>",
    //           "post_title": "Inflation Could Be Peaking... But the Fed Keeps Talking Tough",
    //           "post_excerpt": "",
    //           "post_status": "publish",
    //           "comment_status": "closed",
    //           "ping_status": "closed",
    //           "post_password": "",
    //           "post_name": "sp-500-index-grinds-higher-as-tech-rally-continues-while-energy-rebounds-2-3-3-2-2-2-2-2-2-2-2-2-2-3-2-2-2-6-3-2-2-2-3-2-3-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-2-3-2-2-4-3-2-2-3-2-2-2-2-2-3-2-61",
    //           "to_ping": "",
    //           "pinged": "",
    //           "post_modified": "2022-09-12 10:44:37",
    //           "post_modified_gmt": "2022-09-12 14:44:37",
    //           "post_content_filtered": "",
    //           "post_parent": 0,
    //           "guid": "https:\/\/app.chaikinanalytics.com\/insights\/?p=30165",
    //           "menu_order": 0,
    //           "post_type": "post",
    //           "post_mime_type": "",
    //           "comment_count": "0",
    //           "filter": "raw"
    //         },
    //         {
    //           "ID": 30071,
    //           "post_author": "11",
    //           "post_date": "2022-08-08 14:23:51",
    //           "post_date_gmt": "2022-08-08 18:23:51",
    //           "post_content": "<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">On Friday, the S&P 500 Index closed at 4,145. That means the benchmark index is up about 6% since my last Market Insights update on July 11. And it has rallied around 14% from its intraday low of 3,636 on June 17.\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Three interest rate hikes from the Federal Reserve haven\u2019t taken the starch out of the U.S. economy. That\u2019s evident through last Friday\u2019s very strong jobs report, which showed 528,000 jobs were created in July. It might seem like a good thing to see an increase in jobs, but it\u2019s not good news for investors.\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">You see, the Fed will need to keep its foot on the brake pedal in order to cool off the economy and bring inflation down to its 2% target. And meanwhile, the \u201cinverted yield curve\u201d \u2013 two-year U.S. Treasurys yielding 3.22% while 10-year Treasurys only yield 2.81% \u2013 tells us that a recession is coming. However, the July jobs report says the Fed still has more work to do.\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">This week is full of key inflation data that will influence future Fed rate hikes. The government will release its July updates for the Consumer Price Index and the Producer Price Index on Wednesday and Thursday, respectively. They\u2019re likely to show that inflation is slowing \u2013 but still not turning over.\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">And finally, Wall Street analysts have come to their senses and begun to lower earnings estimates. If the Fed continues to hike interest rates, then companies\u2019 price-to-earnings ratios will start to come down to reflect slower earnings growth in 2022 and 2023.\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<p style=\"font-weight: 400;\"><span style=\"font-style: inherit; font-weight: inherit;\">\u00a0<\/span><\/p>\r\n<a href=\"https:\/\/insights.chaikinanalytics.com\/the-feds-rate-hikes-fail-to-cool-down-the-economy\/\">Continue Reading &gt;&gt;\u00a0<\/a>",
    //           "post_title": "The Fed's Rate Hikes Fail to Cool Down the Economy",
    //           "post_excerpt": "",
    //           "post_status": "publish",
    //           "comment_status": "closed",
    //           "ping_status": "closed",
    //           "post_password": "",
    //           "post_name": "sp-500-index-grinds-higher-as-tech-rally-continues-while-energy-rebounds-2-3-3-2-2-2-2-2-2-2-2-2-2-3-2-2-2-6-3-2-2-2-3-2-3-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-2-3-2-2-4-3-2-2-3-2-2-2-2-2-3-2-47",
    //           "to_ping": "",
    //           "pinged": "",
    //           "post_modified": "2022-08-08 14:23:51",
    //           "post_modified_gmt": "2022-08-08 18:23:51",
    //           "post_content_filtered": "",
    //           "post_parent": 0,
    //           "guid": "https:\/\/app.chaikinanalytics.com\/insights\/?p=30071",
    //           "menu_order": 0,
    //           "post_type": "post",
    //           "post_mime_type": "",
    //           "comment_count": "0",
    //           "filter": "raw"
    //         },
    //         {
    //           "ID": 29916,
    //           "post_author": "11",
    //           "post_date": "2022-07-11 13:49:37",
    //           "post_date_gmt": "2022-07-11 17:49:37",
    //           "post_content": "<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">The S&amp;P 500 Index closed at 3,899 on Friday. The benchmark index is down 5.4% since my last Market Insights update on June 6.<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">The past month was a volatile roller-coaster. Of course, that\u2019s typical action during a bear market. The S&amp;P 500 broke support at 4,000\u2026 then dropped sharply to a new low of 3,637 on June 17. However, the index then rallied in a three-wave move up to 3,931 \u2013 a gain of 7.3%. Notably, that\u2019s well within the 6% to 10% range of expected rebounds within a bear market.<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Second-quarter earnings season begins Thursday when financial-services giant JPMorgan Chase (JPM) reports before the opening bell. And remember, the company guidance that accompanies these reports are the real market-moving forces. This quarter\u2019s guidance should be sharply skewed to the downside.<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Analysts have been aggressively lowering their earnings estimates ahead of earnings season. Normally when analysts lower the bar like this, the market goes on to post gains during the six-week earnings-reporting period. So in that regard, perhaps the rally can gain some steam up to the key resistance level of 4,000 on the S&amp;P 500.<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">This week also brings very important economic data that will weigh heavily on future Federal Reserve interest-rate hikes. Key inflation gauges of the June Consumer Price Index (CPI) and the June Producer Price Index (PPI) will post on Wednesday and Thursday, respectively. Then, on Friday, we\u2019ll get the latest monthly retail sales report.<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<p style=\"font-weight: 400;\"><span style=\"font-style: inherit; font-weight: inherit;\">\u00a0<\/span><\/p>\r\n<a href=\"https:\/\/insights.chaikinanalytics.com\/the-bear-market-rally-is-about-to-hit-a-wall\/\">Continue Reading &gt;&gt;\u00a0<\/a>",
    //           "post_title": "The Bear Market Rally Is About to Hit a Wall",
    //           "post_excerpt": "",
    //           "post_status": "publish",
    //           "comment_status": "closed",
    //           "ping_status": "closed",
    //           "post_password": "",
    //           "post_name": "sp-500-index-grinds-higher-as-tech-rally-continues-while-energy-rebounds-2-3-3-2-2-2-2-2-2-2-2-2-2-3-2-2-2-6-3-2-2-2-3-2-3-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-2-3-2-2-4-3-2-2-3-2-2-2-2-2-3-2-32",
    //           "to_ping": "",
    //           "pinged": "",
    //           "post_modified": "2022-07-11 13:49:37",
    //           "post_modified_gmt": "2022-07-11 17:49:37",
    //           "post_content_filtered": "",
    //           "post_parent": 0,
    //           "guid": "https:\/\/app.chaikinanalytics.com\/insights\/?p=29916",
    //           "menu_order": 0,
    //           "post_type": "post",
    //           "post_mime_type": "",
    //           "comment_count": "0",
    //           "filter": "raw"
    //         },
    //         {
    //           "ID": 29832,
    //           "post_author": "11",
    //           "post_date": "2022-06-06 15:06:44",
    //           "post_date_gmt": "2022-06-06 19:06:44",
    //           "post_content": "<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">The benchmark S&amp;P 500 Index closed at 4,109 on Friday. In other words, the benchmark index is virtually unchanged from its level of 4,123 from my last Market Insights update on May 9.<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">However, the price action was anything but quiet. After breaking support at 4,000, the S&amp;P 500 dipped briefly into bear market territory below 3,850 on May 20. But then, the index rallied in a straight line up to almost 4,200. And as I mentioned, it\u2019s hovering at a little more than 4,100 today.<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">As Federal Reserve officials try to tame inflation, business leaders from JPMorgan Chase (JPM) CEO Jamie Dimon to Tesla (TSLA) CEO Elon Musk have weighed in with dire predictions about the U.S. economy. For example, Dimon said, \u201cBrace yourself for a financial hurricane\u2026 so much for the hoped for soft landing.\u201d<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Right now, reining in rampant inflation is the Fed\u2019s stated mission. And it won\u2019t be easy. After 15 years of feasting on \u201ceasy money\u201d and historically low interest rates, the markets are faced with the reality that the party is over and the bartender has announced last call.<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">After a 40-year bull market in bonds \u2013 during which inflation wasn\u2019t a serious problem \u2013 rising interest rates and the reality of higher food, energy, and housing costs are weighing on the stock market. However, at least for now, Wall Street analysts are still optimistic about corporate earnings. That could change during second-quarter earnings season in July.<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Last month we said, \u201cIn the end, the attractive buying opportunity we\u2019ve been looking for in 2022 will likely come at much lower levels than previously expected\u2026 3,800 is now in play for the S&amp;P 500, with 3,600 a realistic target on the downside.\u201d That continues to be our viewpoint. So with that in mind, investors need to plan accordingly.<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">The current bear market rally, which could extend up to 4,200 or 4,300, is an opportunity to get your portfolios in order. Sell underperforming mega-cap companies and the growth-without -earnings stocks that were the darlings of the post-COVID-19 bull market. Reposition your portfolios into the market-leading sectors like energy, materials, and pharmaceuticals.<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">You might\u2019ve noticed that you\u2019re receiving this month\u2019s Market Insights note a week earlier than normal. I\u2019ll be on vacation next week. But I still wanted to update you during these uncertain times.<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<p style=\"font-weight: 400;\"><span style=\"font-style: inherit; font-weight: inherit;\">\u00a0<\/span><\/p>\r\n<a href=\"https:\/\/insights.chaikinanalytics.com\/the-stock-market-is-following-our-script-and-now-lower-lows-are-likely\/\">Continue Reading &gt;&gt;\u00a0<\/a>",
    //           "post_title": "The Stock Market Is Following Our Script... And Now, Lower Lows Are Likely",
    //           "post_excerpt": "",
    //           "post_status": "publish",
    //           "comment_status": "closed",
    //           "ping_status": "closed",
    //           "post_password": "",
    //           "post_name": "sp-500-index-grinds-higher-as-tech-rally-continues-while-energy-rebounds-2-3-3-2-2-2-2-2-2-2-2-2-2-3-2-2-2-6-3-2-2-2-3-2-3-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-2-3-2-2-4-3-2-2-3-2-2-2-2-2-3-2-21",
    //           "to_ping": "",
    //           "pinged": "",
    //           "post_modified": "2022-06-06 15:06:44",
    //           "post_modified_gmt": "2022-06-06 19:06:44",
    //           "post_content_filtered": "",
    //           "post_parent": 0,
    //           "guid": "https:\/\/app.chaikinanalytics.com\/insights\/?p=29832",
    //           "menu_order": 0,
    //           "post_type": "post",
    //           "post_mime_type": "",
    //           "comment_count": "0",
    //           "filter": "raw"
    //         }
    //       ]
    //     },
    //     "1": {
    //       "10": [
    //         {
    //           "ID": 30016,
    //           "post_author": "11",
    //           "post_date": "2022-07-29 14:08:25",
    //           "post_date_gmt": "2022-07-29 18:08:25",
    //           "post_content": "<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">After the markets closed yesterday, tech giant Apple (AAPL) delivered strong earnings that beat expectations. Massive online retailer Amazon (AMZN) also delivered strong earnings and issued better guidance. That one-two punch of good news relieved many investors.\r\n<\/li>\r\n        <li style=\"font-weight: 400;\" aria-level=\"1\">Both stocks carry a lot of weight in technology-focused exchange-traded funds (\u201cETFs\u201d), as well as the S&P 500 and the Nasdaq 100 indexes. So as a result, the markets reflected a surge of upside trading when they opened this morning.\r\n<\/li>\r\n        <li style=\"font-weight: 400;\" aria-level=\"1\">The S&P 500 closed about 4,050 yesterday. And this morning\u2019s surge has taken the index to higher than 4,100. We\u2019ll discuss these levels further in today\u2019s note.\r\n<\/li>\r\n        <li style=\"font-weight: 400;\" aria-level=\"1\">Please review yesterday\u2019s video for coverage of the index levels to watch and other key indicators. And today, we\u2019ll look at market breadth.\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<p style=\"font-weight: 400;\"><span style=\"font-style: inherit; font-weight: inherit;\">\u00a0<\/span><\/p>\r\n<a href=\"https:\/\/insights.chaikinanalytics.com\/the-two-a-stocks-in-the-original-faang-deliver-on-earnings\/\">Continue Reading &gt;&gt;\u00a0<\/a>",
    //           "post_title": "The Two 'A' Stocks in the Original 'FAANG' Deliver on Earnings",
    //           "post_excerpt": "",
    //           "post_status": "publish",
    //           "comment_status": "closed",
    //           "ping_status": "closed",
    //           "post_password": "",
    //           "post_name": "sp-500-index-grinds-higher-as-tech-rally-continues-while-energy-rebounds-2-3-3-2-2-2-2-2-2-2-2-2-2-3-2-2-2-6-3-2-2-2-3-2-3-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-2-3-2-2-4-3-2-2-3-2-2-2-2-2-3-2-4-5",
    //           "to_ping": "",
    //           "pinged": "",
    //           "post_modified": "2022-07-29 14:08:25",
    //           "post_modified_gmt": "2022-07-29 18:08:25",
    //           "post_content_filtered": "",
    //           "post_parent": 0,
    //           "guid": "https:\/\/app.chaikinanalytics.com\/insights\/?p=30016",
    //           "menu_order": 0,
    //           "post_type": "post",
    //           "post_mime_type": "",
    //           "comment_count": "0",
    //           "filter": "raw"
    //         },
    //         {
    //           "ID": 29999,
    //           "post_author": "11",
    //           "post_date": "2022-07-28 14:15:49",
    //           "post_date_gmt": "2022-07-28 18:15:49",
    //           "post_content": "<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">In today\u2019s <em>Insights<\/em> video, Pete Carmasino discusses the Federal Reserve\u2019s decision to raise the benchmark interest rate by 75 basis points. And now, U.S. gross domestic product has declined for two straight quarters. That means we\u2019re officially in a recession. Get Pete\u2019s latest thoughts on what\u2019s happening in the video.<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<p style=\"font-weight: 400;\"><span style=\"font-style: inherit; font-weight: inherit;\">\u00a0<\/span><\/p>\r\n<a href=\"https:\/\/insights.chaikinanalytics.com\/looking-deeper-into-interest-rates-and-gdp\/\">Continue Reading &gt;&gt;\u00a0<\/a>",
    //           "post_title": "Looking Deeper Into Interest Rates and GDP",
    //           "post_excerpt": "",
    //           "post_status": "publish",
    //           "comment_status": "closed",
    //           "ping_status": "closed",
    //           "post_password": "",
    //           "post_name": "sp-500-index-grinds-higher-as-tech-rally-continues-while-energy-rebounds-2-3-3-2-2-2-2-2-2-2-2-2-2-3-2-2-2-6-3-2-2-2-3-2-3-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-2-3-2-2-4-3-2-2-3-2-2-2-2-2-3-2-4-4",
    //           "to_ping": "",
    //           "pinged": "",
    //           "post_modified": "2022-07-28 14:15:49",
    //           "post_modified_gmt": "2022-07-28 18:15:49",
    //           "post_content_filtered": "",
    //           "post_parent": 0,
    //           "guid": "https:\/\/app.chaikinanalytics.com\/insights\/?p=29999",
    //           "menu_order": 0,
    //           "post_type": "post",
    //           "post_mime_type": "",
    //           "comment_count": "0",
    //           "filter": "raw"
    //         },
    //         {
    //           "ID": 29989,
    //           "post_author": "11",
    //           "post_date": "2022-07-27 12:23:39",
    //           "post_date_gmt": "2022-07-27 16:23:39",
    //           "post_content": "<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Microsoft (MSFT) missed on its earnings report last night, but the Big Tech company delivered good guidance. The company reported 35% year-over-year commercial bookings growth. And overall, things weren\u2019t as bad as feared.\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Meanwhile, Alphabet (GOOGL) reported strong search numbers related to travel and retail. However, the company\u2019s operating losses persisted.\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">It\u2019s another \u201cFed Day\u201d today. By that, I\u2019m talking about how the markets will likely grind to a halt leading into the Federal Reserve\u2019s interest-rate announcement at 2 p.m. Eastern time.\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Again, though, bad news seems like good news\u2026 The markets are recalibrating this morning for a pivot in tone from Fed Chair Jerome Powell.\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Finally, it\u2019s Wednesday. So we\u2019ll review sector rotation in today\u2019s note.\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<p style=\"font-weight: 400;\"><span style=\"font-style: inherit; font-weight: inherit;\">\u00a0<\/span><\/p>\r\n<a href=\"https:\/\/insights.chaikinanalytics.com\/big-tech-guidance-delivers-hope\/\">Continue Reading &gt;&gt;\u00a0<\/a>",
    //           "post_title": "Big Tech Guidance Delivers Hope",
    //           "post_excerpt": "",
    //           "post_status": "publish",
    //           "comment_status": "closed",
    //           "ping_status": "closed",
    //           "post_password": "",
    //           "post_name": "sp-500-index-grinds-higher-as-tech-rally-continues-while-energy-rebounds-2-3-3-2-2-2-2-2-2-2-2-2-2-3-2-2-2-6-3-2-2-2-3-2-3-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-2-3-2-2-4-3-2-2-3-2-2-2-2-2-3-2-4-3",
    //           "to_ping": "",
    //           "pinged": "",
    //           "post_modified": "2022-07-27 12:23:39",
    //           "post_modified_gmt": "2022-07-27 16:23:39",
    //           "post_content_filtered": "",
    //           "post_parent": 0,
    //           "guid": "https:\/\/app.chaikinanalytics.com\/insights\/?p=29989",
    //           "menu_order": 0,
    //           "post_type": "post",
    //           "post_mime_type": "",
    //           "comment_count": "0",
    //           "filter": "raw"
    //         },
    //         {
    //           "ID": 29985,
    //           "post_author": "11",
    //           "post_date": "2022-07-26 12:15:51",
    //           "post_date_gmt": "2022-07-26 16:15:51",
    //           "post_content": "<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">The headline of our note one week ago was, \u201cUncertainty Breeds (More) Uncertainty\u2026 and Creates Fear.\u201d And late yesterday, retail juggernaut Walmart (WMT) fed into that headline when it told the truth\u2026 A demand issue exists.\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Walmart\u2019s stock is down as we go to press. And its announcement is creating a ripple effect across the rest of the market. All the major indexes are down so far today.\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Meanwhile, the Federal Reserve\u2019s next interest-rate announcement is front and center this week. And whether we enter an \u201cofficial\u201d recession or not is a theme to watch as well.\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">The market is proving resilient under the current levels of stress. But so far, earnings season isn\u2019t exciting investors. So we\u2019ll wait and see how long the current rally lasts.\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Before we dive into today\u2019s note, make sure you watch yesterday\u2019s video. Among other things, we discussed the current outlook in the commodity market.\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<p style=\"font-weight: 400;\"><span style=\"font-style: inherit; font-weight: inherit;\">\u00a0<\/span><\/p>\r\n<a href=\"https:\/\/insights.chaikinanalytics.com\/walmart-guides-the-market-lower\/\">Continue Reading &gt;&gt;\u00a0<\/a>",
    //           "post_title": "Walmart Guides the Market Lower",
    //           "post_excerpt": "",
    //           "post_status": "publish",
    //           "comment_status": "closed",
    //           "ping_status": "closed",
    //           "post_password": "",
    //           "post_name": "sp-500-index-grinds-higher-as-tech-rally-continues-while-energy-rebounds-2-3-3-2-2-2-2-2-2-2-2-2-2-3-2-2-2-6-3-2-2-2-3-2-3-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-2-3-2-2-4-3-2-2-3-2-2-2-2-2-3-2-42",
    //           "to_ping": "",
    //           "pinged": "",
    //           "post_modified": "2022-07-26 12:15:51",
    //           "post_modified_gmt": "2022-07-26 16:15:51",
    //           "post_content_filtered": "",
    //           "post_parent": 0,
    //           "guid": "https:\/\/app.chaikinanalytics.com\/insights\/?p=29985",
    //           "menu_order": 0,
    //           "post_type": "post",
    //           "post_mime_type": "",
    //           "comment_count": "0",
    //           "filter": "raw"
    //         },
    //         {
    //           "ID": 29982,
    //           "post_author": "11",
    //           "post_date": "2022-07-25 13:44:12",
    //           "post_date_gmt": "2022-07-25 17:44:12",
    //           "post_content": "<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">In today\u2019s <em>Insights<\/em> video, Pete Carmasino looks ahead to this week\u2019s interest-rate announcement from the Federal Reserve. The central bank is widely expected to raise the benchmark interest rate by 75 basis points. But could the Fed be less aggressive than expected?\r\n\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<p style=\"font-weight: 400;\"><span style=\"font-style: inherit; font-weight: inherit;\">\u00a0<\/span><\/p>\r\n<a href=\"https:\/\/insights.chaikinanalytics.com\/this-week-the-federal-reserve-is-the-focus-again\/\">Continue Reading &gt;&gt;\u00a0<\/a>",
    //           "post_title": "This Week, the Federal Reserve Is the Focus (Again)",
    //           "post_excerpt": "",
    //           "post_status": "publish",
    //           "comment_status": "closed",
    //           "ping_status": "closed",
    //           "post_password": "",
    //           "post_name": "sp-500-index-grinds-higher-as-tech-rally-continues-while-energy-rebounds-2-3-3-2-2-2-2-2-2-2-2-2-2-3-2-2-2-6-3-2-2-2-3-2-3-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-2-3-2-2-4-3-2-2-3-2-2-2-2-2-3-2-4-2",
    //           "to_ping": "",
    //           "pinged": "",
    //           "post_modified": "2022-07-25 13:44:12",
    //           "post_modified_gmt": "2022-07-25 17:44:12",
    //           "post_content_filtered": "",
    //           "post_parent": 0,
    //           "guid": "https:\/\/app.chaikinanalytics.com\/insights\/?p=29982",
    //           "menu_order": 0,
    //           "post_type": "post",
    //           "post_mime_type": "",
    //           "comment_count": "0",
    //           "filter": "raw"
    //         },
    //         {
    //           "ID": 29963,
    //           "post_author": "11",
    //           "post_date": "2022-07-22 14:31:17",
    //           "post_date_gmt": "2022-07-22 18:31:17",
    //           "post_content": "<p>&nbsp;<\/p>\r\n<!-- wp:paragraph -->\r\n<h3><strong> Market Commentary \/ Looking Ahead<\/strong><\/h3>\r\n<!-- \/wp:paragraph -->\t\r\n\r\n\r\n<!-- wp:paragraph -->\r\n<p>This recession might be the most expected one in the past 40 years.<\/p>\r\n<!-- \/wp:paragraph -->\r\n\r\n\r\n<!-- wp:paragraph -->\r\n<p>But it brings up an important question\u2026<\/p>\r\n<!-- \/wp:paragraph -->\r\n\r\n\r\n<!-- wp:paragraph -->\r\n<p>When everyone is expecting something, what\u2019s the payoff?<\/p>\r\n<!-- \/wp:paragraph -->\r\n\r\n\r\n<!-- wp:paragraph -->\r\n<p>If you\u2019ve ever been to a horse race or watched one on TV, you know what I mean\u2026<\/p>\r\n<!-- \/wp:paragraph -->\r\n\r\n\r\n<!-- wp:paragraph -->\r\n<p>The \u201cfavorite\u201d is the horse that is \u201cexpected\u201d to win. That horse has the lowest odds for bettors to make money.<\/p>\r\n<!-- \/wp:paragraph -->\r\n\r\n\r\n<!-- wp:paragraph -->\r\n<p>That\u2019s the case in the markets today\u2026<\/p>\r\n<!-- \/wp:paragraph -->\r\n\r\n\r\n<!-- wp:paragraph -->\r\n<p>Everyone is expecting a recession. It\u2019s the favorite to win this horse race.<\/p>\r\n<!-- \/wp:paragraph -->\r\n\r\n\r\n<!-- wp:paragraph -->\r\n<p>So in turn, it has the lowest odds for any potential payoff. The bigger profits would occur if folks got surprised by what happens next.<\/p>\r\n<!-- \/wp:paragraph -->\r\n\r\n\r\n<!-- wp:paragraph -->\r\n<p>Meanwhile, we called the sell-off in bonds back in December 2021\u2026<\/p>\r\n<!-- \/wp:paragraph -->\r\n\r\n\r\n<!-- wp:paragraph -->\r\n<p>That sell-off played out over the past seven months. Now, the bond market looks to be forming a bottom. It remains to be seen whether it\u2019s temporary or not.<\/p>\r\n<!-- \/wp:paragraph -->\r\n\r\n\r\n<!-- wp:paragraph -->\r\n<p>The sell-off didn\u2019t get all the way to the levels I had projected. However, it did get close\u2026<\/p>\r\n<!-- \/wp:paragraph -->\r\n\r\n\r\n<!-- wp:paragraph -->\r\n<p>The iShares 20+ Year Treasury Bond Fund (TLT) fell to as low as $108.12 in mid-June. That\u2019s not far above our $100-per-share downside target. And importantly, it was still a significant move lower\u2026<\/p>\r\n<!-- \/wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>TLT traded for roughly $150 per share in December. So from peak to trough, it fell roughly 30% in around six months. That\u2019s a huge move \u2013 especially for a supposedly safe bond fund.<\/p>\r\n<!-- \/wp:paragraph -->\r\n\r\n<p style=\"font-weight: 400;\"><span style=\"font-style: inherit; font-weight: inherit;\">\u00a0<\/span><\/p>\r\n<p><a href=\"https:\/\/insights.chaikinanalytics.com\/the-most-telegraphed-recession-in-history\/\/\">Continue Reading &gt;&gt;\u00a0<\/a><\/p>",
    //           "post_title": "The Most Telegraphed Recession in History",
    //           "post_excerpt": "",
    //           "post_status": "publish",
    //           "comment_status": "closed",
    //           "ping_status": "closed",
    //           "post_password": "",
    //           "post_name": "sp-500-index-grinds-higher-as-tech-rally-continues-while-energy-rebounds-2-3-3-2-2-2-2-2-2-2-2-2-2-3-2-2-2-6-3-2-2-2-3-2-3-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-2-3-2-2-4-3-2-2-3-2-2-2-2-2-3-2-41",
    //           "to_ping": "",
    //           "pinged": "",
    //           "post_modified": "2022-07-22 14:57:02",
    //           "post_modified_gmt": "2022-07-22 18:57:02",
    //           "post_content_filtered": "",
    //           "post_parent": 0,
    //           "guid": "https:\/\/app.chaikinanalytics.com\/insights\/?p=29963",
    //           "menu_order": 0,
    //           "post_type": "post",
    //           "post_mime_type": "",
    //           "comment_count": "0",
    //           "filter": "raw"
    //         },
    //         {
    //           "ID": 29961,
    //           "post_author": "11",
    //           "post_date": "2022-07-21 13:49:55",
    //           "post_date_gmt": "2022-07-21 17:49:55",
    //           "post_content": "<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Pete Carmasino dives into the latest round of earnings reports in today\u2019s <em>Insights<\/em> video. As he explains, most companies have beaten estimates. However, the follow-through has been lackluster. Get all the details in the video.\r\n\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<p style=\"font-weight: 400;\"><span style=\"font-style: inherit; font-weight: inherit;\">\u00a0<\/span><\/p>\r\n<a href=\"https:\/\/insights.chaikinanalytics.com\/earnings-season-is-underwhelming-investors\/\">Continue Reading &gt;&gt;\u00a0<\/a>",
    //           "post_title": "Earnings Season Is Underwhelming Investors",
    //           "post_excerpt": "",
    //           "post_status": "publish",
    //           "comment_status": "closed",
    //           "ping_status": "closed",
    //           "post_password": "",
    //           "post_name": "sp-500-index-grinds-higher-as-tech-rally-continues-while-energy-rebounds-2-3-3-2-2-2-2-2-2-2-2-2-2-3-2-2-2-6-3-2-2-2-3-2-3-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-2-3-2-2-4-3-2-2-3-2-2-2-2-2-3-2-40",
    //           "to_ping": "",
    //           "pinged": "",
    //           "post_modified": "2022-07-21 13:49:55",
    //           "post_modified_gmt": "2022-07-21 17:49:55",
    //           "post_content_filtered": "",
    //           "post_parent": 0,
    //           "guid": "https:\/\/app.chaikinanalytics.com\/insights\/?p=29961",
    //           "menu_order": 0,
    //           "post_type": "post",
    //           "post_mime_type": "",
    //           "comment_count": "0",
    //           "filter": "raw"
    //         },
    //         {
    //           "ID": 29951,
    //           "post_author": "11",
    //           "post_date": "2022-07-20 17:02:44",
    //           "post_date_gmt": "2022-07-20 21:02:44",
    //           "post_content": "<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Sentiment is driven by the markets. But importantly, sentiment doesn\u2019t have the power to change the markets \u2013 at least not the way we would like.\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">The markets received better-than-expected news from video-streaming pioneer Netflix (NFLX). That\u2019s providing a relief for investors in the company\u2019s stock, as well as other tech-related companies. Netflix is up about 4.8% as we go to press.\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Meanwhile, capitulation is still on the minds of market participants. Equity allocations remain extremely low. They\u2019re at a level not seen since April 2009.\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Negative earnings revisions are picking up steam as well. Bank of America lowered its expected 2023 cumulative earnings for the S&P 500 Index down to $200.\r\n<\/li>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Finally, it\u2019s Wednesday. That means we\u2019ll review sector rotation today.\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<p style=\"font-weight: 400;\"><span style=\"font-style: inherit; font-weight: inherit;\">\u00a0<\/span><\/p>\r\n<a href=\"https:\/\/insights.chaikinanalytics.com\/markets-change-sentiment-not-the-other-way-around\/\">Continue Reading &gt;&gt;\u00a0<\/a>",
    //           "post_title": "Markets Change Sentiment... Not the Other Way Around",
    //           "post_excerpt": "",
    //           "post_status": "publish",
    //           "comment_status": "closed",
    //           "ping_status": "closed",
    //           "post_password": "",
    //           "post_name": "sp-500-index-grinds-higher-as-tech-rally-continues-while-energy-rebounds-2-3-3-2-2-2-2-2-2-2-2-2-2-3-2-2-2-6-3-2-2-2-3-2-3-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-2-3-2-2-4-3-2-2-3-2-2-2-2-2-3-2-39",
    //           "to_ping": "",
    //           "pinged": "",
    //           "post_modified": "2022-07-20 17:02:44",
    //           "post_modified_gmt": "2022-07-20 21:02:44",
    //           "post_content_filtered": "",
    //           "post_parent": 0,
    //           "guid": "https:\/\/app.chaikinanalytics.com\/insights\/?p=29951",
    //           "menu_order": 0,
    //           "post_type": "post",
    //           "post_mime_type": "",
    //           "comment_count": "0",
    //           "filter": "raw"
    //         },
    //         {
    //           "ID": 29948,
    //           "post_author": "11",
    //           "post_date": "2022-07-19 13:16:15",
    //           "post_date_gmt": "2022-07-19 17:16:15",
    //           "post_content": "<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"list-style-type: none;\">\r\n<ul>\r\n \t<li style=\"font-weight: 400;\" aria-level=\"1\">Apple (AAPL) caused alarm bells to ring through the markets yesterday. In short, the mega-cap tech giant announced a hiring freeze. Apple also said it won\u2019t fill any open positions created from employees leaving.\r\n<\/li>\r\n\t<li style=\"font-weight: 400;\" aria-level=\"1\">This is a major announcement. Apple typically increases its headcount every year. But with this statement, the company left any reductions to its headcount \u2013 think layoffs \u2013 to the imaginations of investors. We\u2019ll discuss this in today\u2019s note.\r\n<\/li>\r\n\t<li style=\"font-weight: 400;\" aria-level=\"1\">The final hour of trading yesterday turned into a freefall. Importantly, 3,900 \u2013 the resistance level we\u2019ve called out in our recent notes \u2013 held as the markets rolled over. But one thing is clear\u2026 Uncertainty breeds more uncertainty, and it also creates fear.\r\n<\/li>\r\n\t<li style=\"font-weight: 400;\" aria-level=\"1\">Last week\u2019s announcement that the Consumer Price Index climbed 9.1% year over year in June doesn\u2019t help, either. And raising interest rates is destructive to demand and overall market morale. So again\u2026 that breeds more uncertainty (and fear).\r\n<\/li>\r\n\t<li style=\"font-weight: 400;\" aria-level=\"1\">Meanwhile, video-streaming pioneer Netflix (NFLX) will kick off the technology sector\u2019s quarterly earnings period tonight. We\u2019ll see if it leads to even more uncertainty.\r\n<\/li>\r\n\t<li style=\"font-weight: 400;\" aria-level=\"1\">Before we dive into today\u2019s note, make sure you watch yesterday\u2019s video to catch up on the key index levels and other indicators. Let\u2019s get started\u2026\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<\/li>\r\n<\/ul>\r\n<p style=\"font-weight: 400;\"><span style=\"font-style: inherit; font-weight: inherit;\">\u00a0<\/span><\/p>\r\n<a href=\"https:\/\/insights.chaikinanalytics.com\/uncertainty-breeds-more-uncertainty-and-creates-fear\/\">Continue Reading &gt;&gt;\u00a0<\/a>",
    //           "post_title": "Uncertainty Breeds (More) Uncertainty... and Creates Fear",
    //           "post_excerpt": "",
    //           "post_status": "publish",
    //           "comment_status": "closed",
    //           "ping_status": "closed",
    //           "post_password": "",
    //           "post_name": "sp-500-index-grinds-higher-as-tech-rally-continues-while-energy-rebounds-2-3-3-2-2-2-2-2-2-2-2-2-2-3-2-2-2-6-3-2-2-2-3-2-3-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-2-3-2-2-4-3-2-2-3-2-2-2-2-2-3-2-38",
    //           "to_ping": "",
    //           "pinged": "",
    //           "post_modified": "2022-07-19 13:16:15",
    //           "post_modified_gmt": "2022-07-19 17:16:15",
    //           "post_content_filtered": "",
    //           "post_parent": 0,
    //           "guid": "https:\/\/app.chaikinanalytics.com\/insights\/?p=29948",
    //           "menu_order": 0,
    //           "post_type": "post",
    //           "post_mime_type": "",
    //           "comment_count": "0",
    //           "filter": "raw"
    //         }
    //       ]
    //     },
    //     "2": {
    //       "67": [
    //         {
    //           "ID": 30550,
    //           "post_author": "11",
    //           "post_date": "2023-02-15 08:45:29",
    //           "post_date_gmt": "2023-02-15 13:45:29",
    //           "post_content": "<p>&nbsp;<\/p>\r\n<!-- wp:paragraph -->\r\n<p>Don't fall for the cute Super Bowl ad...<\/p>\r\n<p><!-- \/wp:paragraph --><\/p><br>\r\n<p><!-- wp:paragraph -->CrowdStrike (CRWD) is a major player in the corporate cybersecurity space. And during Sunday's game, the company marketed its services to more than 110 million people.<\/p>\r\n<p><!-- \/wp:paragraph --><\/p><br>\r\n<p><!-- wp:paragraph -->The ad was a play on the classic Trojan horse story...<\/p>\r\n<p><!-- \/wp:paragraph --><\/p><br>\r\n<p><!-- wp:paragraph -->It started with a gigantic wooden horse at the gates of a castle or fort. And several men up on the wall seem excited to receive the horse.<\/p>\r\n<p><!-- \/wp:paragraph --><\/p>\r\n<p><!-- wp:paragraph --><\/p>\r\n<p><!-- \/wp:paragraph --><\/p>\r\n<p><!-- wp:paragraph --><\/p>\r\n<p style=\"font-weight: 400;\"><span style=\"font-style: inherit; font-weight: inherit;\">\u00a0<\/span><\/p>\r\n<p><a href=\"https:\/\/chaikinpowerfeed.com\/this-super-bowl-stock-is-toxic-waste\/\">Continue Reading &gt;&gt;\u00a0<\/a><\/p>",
    //           "post_title": "This Super Bowl Stock Is 'Toxic Waste'",
    //           "post_excerpt": "",
    //           "post_status": "publish",
    //           "comment_status": "closed",
    //           "ping_status": "closed",
    //           "post_password": "",
    //           "post_name": "this-super-bowl-stock-is-toxic-waste",
    //           "to_ping": "",
    //           "pinged": "",
    //           "post_modified": "2023-02-14 13:33:12",
    //           "post_modified_gmt": "2023-02-14 18:33:12",
    //           "post_content_filtered": "",
    //           "post_parent": 0,
    //           "guid": "https:\/\/app.chaikinanalytics.com\/insights\/?p=30550",
    //           "menu_order": 0,
    //           "post_type": "post",
    //           "post_mime_type": "",
    //           "comment_count": "0",
    //           "filter": "raw"
    //         },
    //         {
    //           "ID": 30545,
    //           "post_author": "11",
    //           "post_date": "2023-02-14 08:45:51",
    //           "post_date_gmt": "2023-02-14 13:45:51",
    //           "post_content": "<p>&nbsp;<\/p>\r\n<!-- wp:paragraph -->\r\n<p>Perhaps you read the <i>Chaikin PowerFeed<\/i> on our website every morning...<\/p>\r\n<p><!-- \/wp:paragraph --><\/p>\r\n<p><!-- wp:paragraph -->That makes sense. After all, that's why we keep a full archive.<\/p>\r\n<p><!-- \/wp:paragraph --><\/p>\r\n<p><!-- wp:paragraph -->But if you don't know already, we also send the PowerFeed by e-mail. And if you're one of the folks who hasn't signed up to get these e-mails, you're missing out on a key feature...<\/p>\r\n<p><!-- \/wp:paragraph -->You see, the bottom of every <i>PowerFeed<\/i> e-mail includes a snapshot from the Power Gauge.<\/p>\r\n<p><!-- wp:paragraph --><\/p>\r\n<p><!-- \/wp:paragraph --><\/p>\r\n<p><!-- wp:paragraph --><\/p>\r\n<p><!-- \/wp:paragraph --><\/p>\r\n<p><!-- wp:paragraph --><\/p>\r\n<p style=\"font-weight: 400;\"><span style=\"font-style: inherit; font-weight: inherit;\">\u00a0<\/span><\/p>\r\n<p><a href=\"https:\/\/chaikinpowerfeed.com\/i-just-shared-this-with-my-team-and-you-should-see-it-too\">Continue Reading &gt;&gt;\u00a0<\/a><\/p>",
    //           "post_title": "I Just Shared This With My Team... And You Should See It, Too",
    //           "post_excerpt": "",
    //           "post_status": "publish",
    //           "comment_status": "closed",
    //           "ping_status": "closed",
    //           "post_password": "",
    //           "post_name": "sp-500-index-grinds-higher-as-tech-rally-continues-while-energy-rebounds-2-3-3-2-2-2-2-2-2-2-2-2-2-3-2-2-2-6-3-2-2-2-3-2-3-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-2-3-2-2-4-3-2-2-3-2-2-2-2-2-3-2-138",
    //           "to_ping": "",
    //           "pinged": "",
    //           "post_modified": "2023-02-14 13:32:27",
    //           "post_modified_gmt": "2023-02-14 18:32:27",
    //           "post_content_filtered": "",
    //           "post_parent": 0,
    //           "guid": "https:\/\/app.chaikinanalytics.com\/insights\/?p=30545",
    //           "menu_order": 0,
    //           "post_type": "post",
    //           "post_mime_type": "",
    //           "comment_count": "0",
    //           "filter": "raw"
    //         },
    //         {
    //           "ID": 30540,
    //           "post_author": "11",
    //           "post_date": "2023-02-13 08:45:03",
    //           "post_date_gmt": "2023-02-13 13:45:03",
    //           "post_content": "<p>&nbsp;<\/p>\r\n<!-- wp:paragraph -->\r\n<p>I've asked myself the same question over and over in recent months...<\/p>\r\n<p><!-- \/wp:paragraph --><\/p><br>\r\n<p><!-- wp:paragraph -->What the heck are they waiting for?<\/p>\r\n<p><!-- \/wp:paragraph --><\/p><br>\r\n<p><!-- wp:paragraph -->I'm talking about Wall Street analysts.<\/p>\r\n<p><!-- \/wp:paragraph --><\/p><br>\r\n<p><!-- wp:paragraph -->I like to pick on these analysts because... well, they kind of make it easy. How on Earth are these folks behind the curve so often on great fundamental stocks?<\/p>\r\n<p><!-- \/wp:paragraph --><\/p>\r\n<p><!-- wp:paragraph --><\/p>\r\n<p><!-- \/wp:paragraph --><\/p>\r\n<p><!-- wp:paragraph --><\/p>\r\n<p style=\"font-weight: 400;\"><span style=\"font-style: inherit; font-weight: inherit;\">\u00a0<\/span><\/p>\r\n<p><a href=\"https:\/\/chaikinpowerfeed.com\/how-to-profit-when-wall-street-is-late-to-the-party\">Continue Reading &gt;&gt;\u00a0<\/a><\/p>",
    //           "post_title": "How to Profit When Wall Street Is Late to the Party",
    //           "post_excerpt": "",
    //           "post_status": "publish",
    //           "comment_status": "closed",
    //           "ping_status": "closed",
    //           "post_password": "",
    //           "post_name": "sp-500-index-grinds-higher-as-tech-rally-continues-while-energy-rebounds-2-3-3-2-2-2-2-2-2-2-2-2-2-3-2-2-2-6-3-2-2-2-3-2-3-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-2-3-2-2-4-3-2-2-3-2-2-2-2-2-3-2-137",
    //           "to_ping": "",
    //           "pinged": "",
    //           "post_modified": "2023-02-10 15:31:28",
    //           "post_modified_gmt": "2023-02-10 20:31:28",
    //           "post_content_filtered": "",
    //           "post_parent": 0,
    //           "guid": "https:\/\/app.chaikinanalytics.com\/insights\/?p=30540",
    //           "menu_order": 0,
    //           "post_type": "post",
    //           "post_mime_type": "",
    //           "comment_count": "0",
    //           "filter": "raw"
    //         },
    //         {
    //           "ID": 30537,
    //           "post_author": "11",
    //           "post_date": "2023-02-10 08:45:58",
    //           "post_date_gmt": "2023-02-10 13:45:58",
    //           "post_content": "<p>&nbsp;<\/p>\r\n<!-- wp:paragraph -->\r\n<p>Longtime Chaikin PowerFeed readers know I'm a data junkie...<\/p>\r\n<p><!-- \/wp:paragraph --><\/p><br>\r\n<p><!-- wp:paragraph -->I can't help myself.<\/p>\r\n<p><!-- \/wp:paragraph --><\/p><br>\r\n<p><!-- wp:paragraph -->It all started back in the mid-1960s. Back then, I had just started as a broker. And one day, a colleague introduced me to George Chestnutt's financial writing...<\/p>\r\n<p><!-- \/wp:paragraph --><\/p><br>\r\n<p><!-- wp:paragraph -->Chestnutt's work gave me access to the best data available at the time. And before long, my life's work revolved around collecting, parsing, and using data to help investors succeed.<\/p>\r\n<p><!-- \/wp:paragraph --><\/p>\r\n<p><!-- wp:paragraph --><\/p>\r\n<p><!-- \/wp:paragraph --><\/p>\r\n<p><!-- wp:paragraph --><\/p>\r\n<p style=\"font-weight: 400;\"><span style=\"font-style: inherit; font-weight: inherit;\">\u00a0<\/span><\/p>\r\n<p><a href=\"https:\/\/chaikinpowerfeed.com\/avoid-these-27-portfolio-landmines-today\">Continue Reading &gt;&gt;\u00a0<\/a><\/p>",
    //           "post_title": "Avoid These 27 'Portfolio Landmines' Today",
    //           "post_excerpt": "",
    //           "post_status": "publish",
    //           "comment_status": "closed",
    //           "ping_status": "closed",
    //           "post_password": "",
    //           "post_name": "sp-500-index-grinds-higher-as-tech-rally-continues-while-energy-rebounds-2-3-3-2-2-2-2-2-2-2-2-2-2-3-2-2-2-6-3-2-2-2-3-2-3-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-2-3-2-2-4-3-2-2-3-2-2-2-2-2-3-2-136",
    //           "to_ping": "",
    //           "pinged": "",
    //           "post_modified": "2023-02-09 14:32:11",
    //           "post_modified_gmt": "2023-02-09 19:32:11",
    //           "post_content_filtered": "",
    //           "post_parent": 0,
    //           "guid": "https:\/\/app.chaikinanalytics.com\/insights\/?p=30537",
    //           "menu_order": 0,
    //           "post_type": "post",
    //           "post_mime_type": "",
    //           "comment_count": "0",
    //           "filter": "raw"
    //         },
    //         {
    //           "ID": 30531,
    //           "post_author": "11",
    //           "post_date": "2023-02-09 08:45:44",
    //           "post_date_gmt": "2023-02-09 13:45:44",
    //           "post_content": "<p>&nbsp;<\/p>\r\n<!-- wp:paragraph -->\r\n<p>Cathie Wood is at it again...<\/p>\r\n<p><!-- \/wp:paragraph --><\/p><br>\r\n<p><!-- wp:paragraph -->Bloomberg interviewed the ARK Investment Management founder and CEO last Friday. And during the conversation, she declared...<\/p>\r\n<p><!-- \/wp:paragraph --><\/p><br>\r\n<p><!-- wp:paragraph -->We are the new Nasdaq.<\/p>\r\n<p><!-- \/wp:paragraph --><\/p><br>\r\n<p><!-- wp:paragraph -->It's a bold statement. It implies that the industry's most trusted tech-tracking index is dead.<\/p>\r\n<p><!-- \/wp:paragraph --><\/p>\r\n<p><!-- wp:paragraph --><\/p>\r\n<p><!-- \/wp:paragraph --><\/p>\r\n<p><!-- wp:paragraph --><\/p>\r\n<p style=\"font-weight: 400;\"><span style=\"font-style: inherit; font-weight: inherit;\">\u00a0<\/span><\/p>\r\n<p><a href=\"https:\/\/chaikinpowerfeed.com\/cathie-wood-declares-the-nasdaq-dead-and-she-isnt-wrong\/\">Continue Reading &gt;&gt;\u00a0<\/a><\/p>",
    //           "post_title": "Cathie Wood Declares the Nasdaq Dead... And She Isn't Wrong",
    //           "post_excerpt": "",
    //           "post_status": "publish",
    //           "comment_status": "closed",
    //           "ping_status": "closed",
    //           "post_password": "",
    //           "post_name": "sp-500-index-grinds-higher-as-tech-rally-continues-while-energy-rebounds-2-3-3-2-2-2-2-2-2-2-2-2-2-3-2-2-2-6-3-2-2-2-3-2-3-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-2-3-2-2-4-3-2-2-3-2-2-2-2-2-3-2-135",
    //           "to_ping": "",
    //           "pinged": "",
    //           "post_modified": "2023-02-08 16:49:44",
    //           "post_modified_gmt": "2023-02-08 21:49:44",
    //           "post_content_filtered": "",
    //           "post_parent": 0,
    //           "guid": "https:\/\/app.chaikinanalytics.com\/insights\/?p=30531",
    //           "menu_order": 0,
    //           "post_type": "post",
    //           "post_mime_type": "",
    //           "comment_count": "0",
    //           "filter": "raw"
    //         },
    //         {
    //           "ID": 30529,
    //           "post_author": "11",
    //           "post_date": "2023-02-08 08:45:48",
    //           "post_date_gmt": "2023-02-08 13:45:48",
    //           "post_content": "<p>&nbsp;<\/p>\r\n<!-- wp:paragraph -->\r\n<p>Most Americans don't understand how the housing market works...<\/p>\r\n<p><!-- \/wp:paragraph --><\/p><br>\r\n<p><!-- wp:paragraph -->For example, personal-finance website NerdWallet recently released the results of a survey of potential homebuyers. It showed that many folks think we're in uncharted territory...<\/p>\r\n<p><!-- \/wp:paragraph --><\/p><br>\r\n<p><!-- wp:paragraph -->Two-thirds of the more than 2,000 survey respondents said they expect an imminent crash. And more than 60% of those folks believe current mortgage rates are \"unprecedented.\"<\/p>\r\n<p><!-- \/wp:paragraph --><\/p><br>\r\n<p><!-- wp:paragraph -->That's far from the case, though...<\/p>\r\n<p><!-- \/wp:paragraph --><\/p>\r\n<p><!-- wp:paragraph --><\/p>\r\n<p><!-- \/wp:paragraph --><\/p>\r\n<p><!-- wp:paragraph --><\/p>\r\n<p style=\"font-weight: 400;\"><span style=\"font-style: inherit; font-weight: inherit;\">\u00a0<\/span><\/p>\r\n<p><a href=\"https:\/\/chaikinpowerfeed.com\/28-million-americans-plan-to-get-a-100000-discount\/\">Continue Reading &gt;&gt;\u00a0<\/a><\/p>",
    //           "post_title": "28 Million Americans Plan to Get a $100,000 Discount",
    //           "post_excerpt": "",
    //           "post_status": "publish",
    //           "comment_status": "closed",
    //           "ping_status": "closed",
    //           "post_password": "",
    //           "post_name": "sp-500-index-grinds-higher-as-tech-rally-continues-while-energy-rebounds-2-3-3-2-2-2-2-2-2-2-2-2-2-3-2-2-2-6-3-2-2-2-3-2-3-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-2-3-2-2-4-3-2-2-3-2-2-2-2-2-3-2-134",
    //           "to_ping": "",
    //           "pinged": "",
    //           "post_modified": "2023-02-08 07:23:10",
    //           "post_modified_gmt": "2023-02-08 12:23:10",
    //           "post_content_filtered": "",
    //           "post_parent": 0,
    //           "guid": "https:\/\/app.chaikinanalytics.com\/insights\/?p=30529",
    //           "menu_order": 0,
    //           "post_type": "post",
    //           "post_mime_type": "",
    //           "comment_count": "0",
    //           "filter": "raw"
    //         },
    //         {
    //           "ID": 30526,
    //           "post_author": "11",
    //           "post_date": "2023-02-07 08:45:02",
    //           "post_date_gmt": "2023-02-07 13:45:02",
    //           "post_content": "<p>&nbsp;<\/p>\r\n<!-- wp:paragraph -->\r\n<p>The \"risk on\" trade is back so far this year...<\/p>\r\n<p><!-- \/wp:paragraph --><\/p><br>\r\n<p><!-- wp:paragraph -->Even after pulling back slightly over the past couple days, the S&P 500 Index is still up around 7% in 2023. And the tech-heavy Nasdaq Composite Index is up roughly 14%.<\/p>\r\n<p><!-- \/wp:paragraph --><\/p><br>\r\n<p><!-- wp:paragraph -->Being too \"bearish\" and sitting on the sidelines as stocks rally will hurt your portfolio.<\/p>\r\n<p><!-- \/wp:paragraph --><\/p><br>\r\n<p><!-- wp:paragraph -->But that's only half the story...<\/p>\r\n<p><!-- \/wp:paragraph --><\/p>\r\n<p><!-- wp:paragraph --><\/p>\r\n<p><!-- \/wp:paragraph --><\/p>\r\n<p><!-- wp:paragraph --><\/p>\r\n<p style=\"font-weight: 400;\"><span style=\"font-style: inherit; font-weight: inherit;\">\u00a0<\/span><\/p>\r\n<p><a href=\"https:\/\/chaikinpowerfeed.com\/use-the-power-gauge-to-crush-a-hedge-fund\/\">Continue Reading &gt;&gt;\u00a0<\/a><\/p>",
    //           "post_title": "Use the Power Gauge to Crush a Hedge Fund",
    //           "post_excerpt": "",
    //           "post_status": "publish",
    //           "comment_status": "closed",
    //           "ping_status": "closed",
    //           "post_password": "",
    //           "post_name": "sp-500-index-grinds-higher-as-tech-rally-continues-while-energy-rebounds-2-3-3-2-2-2-2-2-2-2-2-2-2-3-2-2-2-6-3-2-2-2-3-2-3-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-2-3-2-2-4-3-2-2-3-2-2-2-2-2-3-2-133",
    //           "to_ping": "",
    //           "pinged": "",
    //           "post_modified": "2023-02-06 15:02:28",
    //           "post_modified_gmt": "2023-02-06 20:02:28",
    //           "post_content_filtered": "",
    //           "post_parent": 0,
    //           "guid": "https:\/\/app.chaikinanalytics.com\/insights\/?p=30526",
    //           "menu_order": 0,
    //           "post_type": "post",
    //           "post_mime_type": "",
    //           "comment_count": "0",
    //           "filter": "raw"
    //         },
    //         {
    //           "ID": 30524,
    //           "post_author": "11",
    //           "post_date": "2023-02-06 08:45:22",
    //           "post_date_gmt": "2023-02-06 13:45:22",
    //           "post_content": "<p>&nbsp;<\/p>\r\n<!-- wp:paragraph -->\r\n<p>The Power Gauge just turned \"very bullish\" on Chinese Internet companies...<\/p>\r\n<p><!-- \/wp:paragraph --><\/p><br>\r\n<p><!-- wp:paragraph -->Specifically, last Thursday, our research led to a buying opportunity in the KraneShares CSI China Internet Fund (KWEB).<\/p>\r\n<p><!-- \/wp:paragraph --><\/p><br>\r\n<p><!-- wp:paragraph -->KWEB has already doubled off its October bottom. And momentum is now in its favor.<\/p>\r\n<p><!-- \/wp:paragraph --><\/p><br>\r\n<p><!-- wp:paragraph -->But today, I want to focus on another part of the China story...<\/p>\r\n<p><!-- \/wp:paragraph --><\/p>\r\n<p><!-- wp:paragraph --><\/p>\r\n<p><!-- \/wp:paragraph --><\/p>\r\n<p><!-- wp:paragraph --><\/p>\r\n<p style=\"font-weight: 400;\"><span style=\"font-style: inherit; font-weight: inherit;\">\u00a0<\/span><\/p>\r\n<p><a href=\"https:\/\/chaikinpowerfeed.com\/earn-about-6-per-month-on-chinese-volatility\/\">Continue Reading &gt;&gt;\u00a0<\/a><\/p>",
    //           "post_title": "Earn About 6% per Month on Chinese Volatility",
    //           "post_excerpt": "",
    //           "post_status": "publish",
    //           "comment_status": "closed",
    //           "ping_status": "closed",
    //           "post_password": "",
    //           "post_name": "sp-500-index-grinds-higher-as-tech-rally-continues-while-energy-rebounds-2-3-3-2-2-2-2-2-2-2-2-2-2-3-2-2-2-6-3-2-2-2-3-2-3-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-2-3-2-2-4-3-2-2-3-2-2-2-2-2-3-2-132",
    //           "to_ping": "",
    //           "pinged": "",
    //           "post_modified": "2023-02-06 07:41:46",
    //           "post_modified_gmt": "2023-02-06 12:41:46",
    //           "post_content_filtered": "",
    //           "post_parent": 0,
    //           "guid": "https:\/\/app.chaikinanalytics.com\/insights\/?p=30524",
    //           "menu_order": 0,
    //           "post_type": "post",
    //           "post_mime_type": "",
    //           "comment_count": "0",
    //           "filter": "raw"
    //         },
    //         {
    //           "ID": 30522,
    //           "post_author": "11",
    //           "post_date": "2023-02-03 08:45:47",
    //           "post_date_gmt": "2023-02-03 13:45:47",
    //           "post_content": "<p>&nbsp;<\/p>\r\n<!-- wp:paragraph -->\r\n<p>An important change is developing in the stock market right now...<\/p>\r\n<p><!-- \/wp:paragraph --><\/p><br>\r\n<p><!-- wp:paragraph -->The Power Gauge is starting to signal newfound \"bullish\" opportunities.<\/p>\r\n<p><!-- \/wp:paragraph --><\/p><br>\r\n<p><!-- wp:paragraph -->But there's a problem...<\/p>\r\n<p><!-- \/wp:paragraph --><\/p><br>\r\n<p><!-- wp:paragraph -->A lot of these opportunities appear to include what many investors would consider \"dealbreakers.\" One of these apparent dealbreakers is a company with falling earnings.<\/p>\r\n<p><!-- \/wp:paragraph --><\/p>\r\n<p><!-- wp:paragraph --><\/p>\r\n<p><!-- \/wp:paragraph --><\/p>\r\n<p><!-- wp:paragraph --><\/p>\r\n<p style=\"font-weight: 400;\"><span style=\"font-style: inherit; font-weight: inherit;\">\u00a0<\/span><\/p>\r\n<p><a href=\"https:\/\/chaikinpowerfeed.com\/earnings-are-heading-lower-but-this-stock-is-strong\/\">Continue Reading &gt;&gt;\u00a0<\/a><\/p>",
    //           "post_title": "Earnings Are Heading Lower... but This Stock Is Strong",
    //           "post_excerpt": "",
    //           "post_status": "publish",
    //           "comment_status": "closed",
    //           "ping_status": "closed",
    //           "post_password": "",
    //           "post_name": "sp-500-index-grinds-higher-as-tech-rally-continues-while-energy-rebounds-2-3-3-2-2-2-2-2-2-2-2-2-2-3-2-2-2-6-3-2-2-2-3-2-3-2-2-2-2-2-2-2-2-2-2-2-2-2-3-2-2-2-2-2-2-2-2-3-2-2-4-3-2-2-3-2-2-2-2-2-3-2-131",
    //           "to_ping": "",
    //           "pinged": "",
    //           "post_modified": "2023-02-03 07:35:18",
    //           "post_modified_gmt": "2023-02-03 12:35:18",
    //           "post_content_filtered": "",
    //           "post_parent": 0,
    //           "guid": "https:\/\/app.chaikinanalytics.com\/insights\/?p=30522",
    //           "menu_order": 0,
    //           "post_type": "post",
    //           "post_mime_type": "",
    //           "comment_count": "0",
    //           "filter": "raw"
    //         }
    //       ]
    //     }
    //   }
   
      let getPowerFeedInsightDataObject: GetPowerFeedInsightDataObject[] = [];
      getPowerFeedInsightDataObject = resp[2][67].sort((a, b) =>
        a.post_date > b.post_date ? -1 : 1
      );
      const latestPowerFeedData: GetPowerFeedInsightDataObject =
        resp[2][67].sort((a, b) => (a.post_date > b.post_date ? -1 : 1))[0];

      this.powerFeedData = this.preparePowerFeedData(latestPowerFeedData);
      
      this.getIssuesAndUpdatesData(resp[0][2]);
 });
  }

  /**
   * getIssuesAndUpdatesData gets the issues and updates data from the API
   */
  private getIssuesAndUpdatesData(data): void {
    const pubCSV =
      this.appDataService.productPreference?.productCodes?.join(',');
    this.api.getIssuesAndUpdates(pubCSV).subscribe((resp: any) => {
      const issuesAndUpdatesDataList1 = data.map(
        (item: Record<string, string>) => this.prepareMarketInsightData(item))
      ;

      this.issuesAndUpdatesDataList = resp.data.map(
        (item: Record<string, string>) => this.prepareIssuesAndUpdatesData(item)
      );
     
     
      this.issuesAndUpdatesDataList =   ((this.issuesAndUpdatesDataList.concat(issuesAndUpdatesDataList1)).sort((a, b) =>
        new Date(a.publicationDate) > new Date(b.publicationDate) ? -1 : 1
      )).slice(0, 6);
    //  this.issuesAndUpdatesDataList = this.issuesAndUpdatesDataList.slice(0,5)
     
    });
  }



  ngOnInit(): void {
    this.userAccess = this.appDataService.userRoleMappingModel;
    this.getPowerFeedInsightData();
   
  }

  /**
   * getPublicationFullName returns the fullname of the publication based on the code passed
   * @param publicationCode {string} publication type code
   * @returns {string} publication full name
   */
  public getPublicationFullName(publicationCode: string): string {
    const publicationsFullName = {
      CPGI: 'Power Gauge Investor',
      CTAC: 'PowerTactics',
      CPGR: 'Power Gauge Report',
      CPTR:'PowerTrader'
    };

    return publicationsFullName[publicationCode.toUpperCase()];
  }

  public parseTitle(title: string): string {
    return title?.replace(/[^a-zA-Z ]/g, '');
  }

  public convertStringToHTML(rawHtmlString: string): any {
    const parser = new DOMParser();
    const doc = parser.parseFromString(rawHtmlString, 'text/html');
    return doc.body.innerHTML || '';
  }

  readFullArticleLink(): void {
    window.open('https://chaikinpowerfeed.com/', '_blank');
  }


  prepareMarketInsightData(responseIssuesAndUpdatesData): IIssuesAndUpdatesData {
    return {
      publicationName: "Market Insights",
      publicationDate: this.formatDate(responseIssuesAndUpdatesData.post_date),
      publicationTitle: responseIssuesAndUpdatesData.post_title,
      publicationLink: this.editPostLink(responseIssuesAndUpdatesData.post_content),
    };
  }

editPostLink(postLink):any{
 let linkIndex = postLink.lastIndexOf("<a href")+9;
  if (linkIndex != -1) {
   let linkIndexEnd = postLink.indexOf("Continue Reading")-2;
    let post_link = postLink.substring(linkIndex, linkIndexEnd);
   
    return post_link
  
  }
}

}
