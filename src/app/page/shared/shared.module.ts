import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatNativeDateModule } from '@angular/material/core';
import { MatStepperModule } from '@angular/material/stepper';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';

import { PgrOverviewComponent } from './components/power-gauge-rating/power-gauge-rating.component';
import { ProductsComponent } from './components/products/products.component';
import { QuickDescriptionComponent } from './components/quick-description-popup/quick-discription.component';
import { LogoutComponent } from './components/logout-popup/logout-popup.component';
import { SearchComponent } from './components/search/search.component';

import { CreateListDialogComponent } from './components/create-list-dialog/create-list-dialog.component';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';
import { InfoDialogComponent } from './components/info-dialog/info-dialog.component';
import { EditListDialogComponent } from './components/edit-list-dialog/edit-list-dialog.component';
import { AddTickersToListComponent } from './components/add-tickers-to-list/add-tickers-to-list.component';
import { DuplicatewpDialogComponent } from './components/duplicatewp-dialog/duplicatewp-dialog.component';
import { DisclaimerComponent } from './components/disclaimer/disclaimer.component';
import { FooterComponent } from './components/footer/footer.component';
import { ClickOutsideModule } from 'ng-click-outside';

import { ChecklistComponent } from './panels/checklist/checklist.component';
import { PubPanelComponent } from './panels/pub-panel/pub-panel.component';
import { MarketsPanelComponent } from './panels/markets-panel/markets-panel.component';
// PgrOverviewComponent,
//     ProductsComponent,
//     QuickDescriptionComponent,
//     LogoutComponent,
//     MarketQuickViewBarComponent,

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDialogModule,
    MatDividerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatStepperModule,
    MatListModule,
    MatButtonToggleModule,
    MatSelectModule,
    MatTooltipModule,
    ClickOutsideModule
  ],
  declarations: [
    PgrOverviewComponent,
    ChecklistComponent,
    ProductsComponent,
    LogoutComponent,
    QuickDescriptionComponent,
    FooterComponent,
    DisclaimerComponent,
    CreateListDialogComponent,
    ConfirmDialogComponent,
    InfoDialogComponent,
    EditListDialogComponent,
    AddTickersToListComponent,
    DuplicatewpDialogComponent,
    SearchComponent,
    PubPanelComponent,
    MarketsPanelComponent,
  ],
  exports: [
    PgrOverviewComponent,
    ChecklistComponent,
    ProductsComponent,
    LogoutComponent,
    QuickDescriptionComponent,
    FooterComponent,
    DisclaimerComponent,
    CreateListDialogComponent,
    ConfirmDialogComponent,
    InfoDialogComponent,
    EditListDialogComponent,
    AddTickersToListComponent,
    DuplicatewpDialogComponent,
    SearchComponent,
    PubPanelComponent,
    MarketsPanelComponent
  ],
  entryComponents: [
  ]
})
export class SharedModule { }