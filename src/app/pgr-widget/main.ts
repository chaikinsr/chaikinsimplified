import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import { environment } from 'src/environments/environment';
import { WidgetModule } from './widget.module';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(WidgetModule);
