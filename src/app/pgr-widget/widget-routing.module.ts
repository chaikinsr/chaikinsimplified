import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { WidgetComponent } from "./widget.component";



const routes: Routes = [
    { 
        path: 'widget',
        component: WidgetComponent
    }
];

@NgModule({
    imports: [
        // [RouterModule.forRoot(routes,{useHash: true})]
        RouterModule.forRoot(routes, {
        scrollPositionRestoration: 'enabled',
        anchorScrolling: 'enabled',
        onSameUrlNavigation: 'reload', useHash: true,
        relativeLinkResolution: 'legacy'
    },),
      ],
      exports: [RouterModule],
    providers: []
})
export class WidgetRoutingModule { }