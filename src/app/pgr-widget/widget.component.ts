import {
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { ContextSummaryDataModel, HeaderDataModel } from "src/app/core/data-models/app-model";
import { ContextSummaryRootObject, EarningsContextSummary, ExpertOpnionsContextSummary, FinancialContextSummary, LoginDetailModel, PgrContextSummary, PriceVolumeContextSummary, SymbolDataRootObject } from "src/app/core/data-models/service-model";
import { CookieService } from 'ngx-cookie-service';
import { APIService } from "src/app/core/http-services/api.service";
import { DataService } from "src/app/core/services/data.service";
import { LocalStorage, SharedUtil } from 'src/app/core/services/utilily/utility';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import {Location} from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-widget',
    templateUrl: './widget.component.html',
    styleUrls: ['./widget.component.scss'],
})

export class WidgetComponent implements OnInit, OnChanges {

    headerDataModel: HeaderDataModel = new HeaderDataModel();
   // @Input() pgrContextSummary: PgrContextSummary = new PgrContextSummary();
  pgrContextSummary: PgrContextSummary = new PgrContextSummary();
  financialContextSummary = new FinancialContextSummary();
  earningsContextSummary = new EarningsContextSummary();
  priceVolumeContextSummary = new PriceVolumeContextSummary();
  expertOpnionsContextSummary = new ExpertOpnionsContextSummary();
    reportDate: string;
    userDetail: LoginDetailModel;
    product_id: any;
    symbol:string ;
    constructor(
        private api: APIService,
        private dataService: DataService,private actRoute: ActivatedRoute,
        private location: Location,
        ) {
            
           
            if (!this.userDetail) {
                this.userDetail = this.getUserDetail();
              }
    }
    ngOnChanges(): void {
    }
    ngOnInit(): void {
setTimeout(() => {
  this.product_id =  this.actRoute.queryParams.subscribe(res => {
  Object.keys(res).length > 0? this.symbol = res['symbol'].toUpperCase() :  this.symbol = 'META';
          // Object.keys(res).length > 0 ? this.location.go('/widget?symbol=' + res['symbol'].toUpperCase() ) :this.location.go('/widget?symbol=' + this.symbol );
          this.getSymbolData();
          this.getPgrDataAndContextSummary();
        });
}, 1000);

      //  this.getSymbolData();
        // this.reportDate = moment()
        // .tz('America/New_York')
        // .format('MM/DD/YY HH:mmA z');
      this.reportDate = moment().format('MM/DD/YY HH:mmA z');
    //  this.getSymbolData();
    }

    getUserDetail(): LoginDetailModel {
        const userDetail: LoginDetailModel = LocalStorage.getItem('userDetail');
        return userDetail;
      }

    getSymbolData(): void {
        let symbol = LocalStorage.getItem('mainSymbol');
        let components = 'metaInfo,EPSData,pgr';
        let token = "0PP43177DE04O173F082F97BAS7745Q2"
        const httpGetSymbolData$ = this.api.getSymbolDataForWidget(this.symbol, components, token );
        httpGetSymbolData$
          .pipe(
            map((resp) => {
              const symbolData = resp as SymbolDataRootObject;
              return this.prepareHeaderData(symbolData);
            })
          )
          .subscribe((symbolData: HeaderDataModel) => {
            this.headerDataModel = Object.assign({}, symbolData);
          //  this.companyInformation();
          });
      }
    
      prepareHeaderData(data: SymbolDataRootObject): HeaderDataModel {
        let headerData = new HeaderDataModel();
        headerData.reportDate = 'As of ' + this.reportDate;
        headerData.generalDate = 'Rating as of ' + this.reportDate.split(' ')[0];
        if (data.status === 'success') {
          let metainfo = data.metaInfo[0];
          let epsData = data.EPSData;
          let pgr = data.pgr;
          headerData.pgrRating = metainfo.PGR;
          headerData.rawPgrRating = metainfo.raw_PGR;
          headerData.pgr = SharedUtil.getPgrRating(
            headerData.pgrRating,
            headerData.rawPgrRating
          );
          headerData.ratingType = SharedUtil.getPgrNameWithoutNeutralValues(
            headerData.pgr
          );
          headerData.ratingTypeColorLabel =
            SharedUtil.getColorForRating(headerData.ratingType) + '-label';
          headerData.ratingTypeColorBg =
            SharedUtil.getColorForRating(headerData.ratingType) + '-bg';
          headerData.symbol = metainfo.symbol;
          headerData.name = metainfo.name;
          headerData.isEtf = metainfo.is_etf_symbol;
          headerData.last = metainfo.Last;
          headerData.listRating = metainfo.listRating;
          // isNaN(headerData.listRating)
          headerData.industryColor = (!metainfo.industry_name)
            ? 'grey'
            : headerData.listRating > 50
              ? 'green'
              : 'red';
          let lastFormattedString = SharedUtil.formatNumber(headerData.last);
          headerData.lastFormattedString = SharedUtil.appendSymbol(
            lastFormattedString,
            '$'
          );
          headerData.change = metainfo.Change;
          headerData.changeFormattedString = SharedUtil.formatNumber(
            headerData.change
          );
          headerData.percentageChange = metainfo['Percentage '];
          let percentageChangeFormattedString = SharedUtil.formatNumber(
            headerData.percentageChange
          );
          headerData.percentageChangeFormattedString = SharedUtil.appendSymbol(
            percentageChangeFormattedString,
            '%'
          );
          if (percentageChangeFormattedString !== 'NA') {
            if (headerData.percentageChange > 0) {
              headerData.percentageChangeFormattedString =
                '(+' + headerData.percentageChangeFormattedString + ')';
              headerData.percentageChangeColor = 'green';
            } else if (headerData.percentageChange < 0) {
              headerData.percentageChangeFormattedString =
                '(-' + headerData.percentageChangeFormattedString + ')';
              headerData.percentageChangeColor = 'red';
            } else {
              headerData.percentageChangeFormattedString =
                '(' + headerData.percentageChangeFormattedString + ')';
              headerData.percentageChangeColor = 'grey';
            }
          }
          headerData.pgrImageSrc = SharedUtil.getPgrArcWithoutNeutralValue(
            headerData.pgrRating,
            headerData.rawPgrRating,
            headerData.isEtf
          );
          headerData.pgrImageSrcGradient = SharedUtil.getPgrGradientArcWithoutNeutralValue(   
            headerData.pgrRating,
            headerData.rawPgrRating,
            headerData.isEtf
          );
    
          const powerBarArr = pgr[6]['power_bar'];
          headerData.red = +powerBarArr[0];
          headerData.yellow = +powerBarArr[1];
          headerData.green = +powerBarArr[2];
          this.setPowerBarWidth(
            headerData.red,
            headerData.yellow,
            headerData.green,
            headerData
          );
    
          headerData.industry = (metainfo.industry_name) ? metainfo.industry_name : "None";
          headerData.financialRating = pgr[1]['Financials'][0]['Value'];
          headerData.earningRating = pgr[2]['Earnings'][0]['Value'];
          headerData.technicalRating = pgr[3]['Technicals'][0]['Value'];
          headerData.expertRating = pgr[4]['Experts'][0]['Value'];
    
          let financial = pgr[1]['Financials'];
          headerData.financialDebtToEquity = financial[1]['LT Debt to Equity'];
          headerData.financialPriceToBook = financial[2]['Price to Book'];
          headerData.financialReturnOnEquity = financial[3]['Return on Equity'];
          headerData.financialPriceToSales = financial[4]['Price to Sales'];
          headerData.financialFreeCashFlow = financial[5]['Free Cash Flow'];
    
          let earning = pgr[2]['Earnings'];
          headerData.earningGrowth = earning[1]['Earnings Growth'];
          headerData.earningSurprise = earning[2]['Earnings Surprise'];
          headerData.earningTrend = earning[3]['Earnings Trend'];
          headerData.earningProfitToEarning = earning[4]['Projected P/E'];
          headerData.earningConsistency = earning[5]['Earnings Consistency'];
    
          let technical = pgr[3]['Technicals'];
          headerData.technicalRelativeStrengthVsMarket =
            technical[1]['Rel Strength vs Market'];
          headerData.technicalChaikinMoneyFlow = technical[2]['Chaikin Money Flow'];
          headerData.technicalPriceStrength = technical[3]['Price Strength'];
          headerData.technicalPriceTrendRoc = technical[4]['Price Trend ROC'];
          headerData.technicalVolumeTrend = technical[5]['Volume Trend'];
    
          let expert = pgr[4]['Experts'];
          headerData.expertEstimateTrend = expert[1]['Estimate Trend'];
          headerData.expertShortInterest = expert[2]['Short Interest'];
          headerData.expertInsiderActivity = expert[3]['Insider Activity'];
          headerData.expertRatingTrend = expert[4]['Analyst Rating Trend'];
          headerData.expertIndustrialRelStrength =
            expert[5]['Industry Rel Strength'];
    
          headerData.lastReportDate = data.EPSData.last_report_date;
          headerData.nextReportDate = data.EPSData.next_report_date;
          headerData.lastQuarterConsensusEstimate =
            data.EPSData.last_quarter_consensus_estimate;
          headerData.epsDiffDescription = data.EPSData.eps_diff_description;
          headerData.currentQuarter = data.EPSData.current_quarter;
          headerData.currentQuarterConsensusEstimate =
            data.EPSData.current_quarter_consensus_estimate;
          headerData.earningsReportInfo = data.EPSData.earnings_report_info;
          headerData.preReportInfo = data.EPSData.pre_report_info;
          headerData.estimateRevisionInfo = data.EPSData.estimate_revision_info;
        }
        return headerData;
      }

      setPowerBarWidth(red, yellow, green, headerData): void {
        let bearishPerCountStyle = 0;
        let neutralPerCountStyle = 0;
        let bullishPerCountStyle = 0;
    
        const totalCount = red + yellow + green;
        const bearishPer = Math.round(
          totalCount === 0 ? 0 : (red / totalCount) * 100
        );
        const neutralPer = Math.round(
          totalCount === 0 ? 0 : (yellow / totalCount) * 100
        );
        const bullishPer = Math.round(
          totalCount === 0 ? 0 : (green / totalCount) * 100
        );
    
        neutralPerCountStyle = 7;
        const bearishCountPer =
          bearishPer === 0 ? 0 : (bearishPer / (bearishPer + bullishPer)) * 70;
        const bullishCountPer =
          bullishPer === 0 ? 0 : (bullishPer / (bearishPer + bullishPer)) * 70;
        if (bearishCountPer === 0 && bullishCountPer === 0) {
          bearishPerCountStyle = 35;
          bullishPerCountStyle = 35;
        } else if (20 - bearishCountPer > 0) {
          bearishPerCountStyle = 20;
          bullishPerCountStyle = bullishCountPer - (20 - bearishCountPer);
        } else if (20 - bullishCountPer > 0) {
          bearishPerCountStyle = bearishCountPer - (20 - bullishCountPer);
          bullishPerCountStyle = 20;
        } else {
          bearishPerCountStyle = bearishCountPer;
          bullishPerCountStyle = bullishCountPer;
        }
        headerData.bearishPerCountStyle = bearishPerCountStyle;
        headerData.neutralPerCountStyle = neutralPerCountStyle;
        headerData.bullishPerCountStyle = bullishPerCountStyle;
      }

      getPgrDataAndContextSummary(): void {
        let token = "FXC43177DE04OQSX3082F97BAS77489Z"
       // let symbol = LocalStorage.getItem('mainSymbol');
        let industry = null;
        const httpGetContextSummaryData$ = this.api.getContextSummaryDataForWidget(
          this.symbol,
          industry,token
        );
        httpGetContextSummaryData$
          .pipe(
            map((resp) => {
              const summaryData = resp as ContextSummaryRootObject;
              return this.prepareContextSummaryData(summaryData);
            })
          )
          .subscribe((summaryData: ContextSummaryDataModel) => {
            this.pgrContextSummary = Object.assign(
              {},
              summaryData.pgrContextSummary
            );
            this.financialContextSummary = Object.assign(
              {},
              summaryData.financialContextSummary
            );
            this.earningsContextSummary = Object.assign(
              {},
              summaryData.earningsContextSummary
            );
            this.priceVolumeContextSummary = Object.assign(
              {},
              summaryData.priceVolumeContextSummary
            );
            this.expertOpnionsContextSummary = Object.assign(
              {},
              summaryData.expertOpnionsContextSummary
            );
          });
      }
    
      prepareContextSummaryData(
          data: ContextSummaryRootObject
        ): ContextSummaryDataModel {
        let contextSummaryDataMadel = new ContextSummaryDataModel();
        contextSummaryDataMadel.pgrContextSummary = data.pgrContextSummary[0];
        contextSummaryDataMadel.pgrContextSummary.mainSentence =
          contextSummaryDataMadel.pgrContextSummary.mainSentence.replace(
            /<TRADEMARK>/g,
            ''
          );
        contextSummaryDataMadel.financialContextSummary =
          data.financialContextSummary[0];
        contextSummaryDataMadel.financialRatingBgColorClass =
          SharedUtil.getColorForRating(
            contextSummaryDataMadel.financialContextSummary.status
          ) + '-bg';
        contextSummaryDataMadel.financialRatingLableColorClass =
          SharedUtil.getColorForRating(
            contextSummaryDataMadel.financialContextSummary.status
          ) + '-label';
        contextSummaryDataMadel.financialContextSummary.financialRatingBgColorClass =
          contextSummaryDataMadel.financialRatingBgColorClass;
        contextSummaryDataMadel.financialContextSummary.financialRatingLableColorClass =
          contextSummaryDataMadel.financialRatingLableColorClass;
    
        contextSummaryDataMadel.earningsContextSummary =
          data.earningsContextSummary[0];
        contextSummaryDataMadel.earningRatingBgColorClass =
          SharedUtil.getColorForRating(
            contextSummaryDataMadel.earningsContextSummary.status
          ) + '-bg';
        contextSummaryDataMadel.earningRatingLableColorClass =
          SharedUtil.getColorForRating(
            contextSummaryDataMadel.earningsContextSummary.status
          ) + '-label';
        contextSummaryDataMadel.earningsContextSummary.earningRatingBgColorClass =
          contextSummaryDataMadel.earningRatingBgColorClass;
        contextSummaryDataMadel.earningsContextSummary.earningRatingLableColorClass =
          contextSummaryDataMadel.earningRatingLableColorClass;
    
        contextSummaryDataMadel.priceVolumeContextSummary =
          data.priceVolumeContextSummary[0];
        contextSummaryDataMadel.priceVolumeRatingBgColorClass =
          SharedUtil.getColorForRating(
            contextSummaryDataMadel.priceVolumeContextSummary.status
          ) + '-bg';
        contextSummaryDataMadel.priceVolumeRatingLableColorClass =
          SharedUtil.getColorForRating(
            contextSummaryDataMadel.priceVolumeContextSummary.status
          ) + '-label';
        contextSummaryDataMadel.priceVolumeContextSummary.priceVolumeRatingBgColorClass =
          contextSummaryDataMadel.priceVolumeRatingBgColorClass;
        contextSummaryDataMadel.priceVolumeContextSummary.priceVolumeRatingLableColorClass =
          contextSummaryDataMadel.priceVolumeRatingLableColorClass;
    
        contextSummaryDataMadel.expertOpnionsContextSummary =
          data.expertOpnionsContextSummary[0];
        contextSummaryDataMadel.expertRatingBgColorClass =
          SharedUtil.getColorForRating(
            contextSummaryDataMadel.expertOpnionsContextSummary.status
          ) + '-bg';
        contextSummaryDataMadel.expertRatingLableColorClass =
          SharedUtil.getColorForRating(
            contextSummaryDataMadel.expertOpnionsContextSummary.status
          ) + '-label';
        contextSummaryDataMadel.expertOpnionsContextSummary.expertRatingBgColorClass =
          contextSummaryDataMadel.expertRatingBgColorClass;
        contextSummaryDataMadel.expertOpnionsContextSummary.expertRatingLableColorClass =
          contextSummaryDataMadel.expertRatingLableColorClass;
    
        return contextSummaryDataMadel;
      }
    
}