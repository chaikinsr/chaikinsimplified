import { Injectable } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { environment } from 'src/environments/environment';
import { ProductAccessModel, UserRoleMappingModel } from '../core/data-models/app-model';
import { AuthorizationModel, LoginDetailModel, ProductCodesAndPreferenceObject } from '../core/data-models/service-model';
import { APIService } from '../core/http-services/api.service';
import { AuthService } from '../core/http-services/auth.service';
import { AppDataService } from '../core/services/app-data.service';
import { DataService } from '../core/services/data.service';
import { LocalStorage, SharedUtil } from '../core/services/utilily/utility';
import * as _ from 'lodash';

@Injectable({ providedIn: 'root' })
export class AdvancedSearchResolver implements Resolve<any> {
    constructor(
        private apiService: APIService,
        private authService: AuthService,
        private router: Router,
        private cookieService: CookieService,
        private dataService: DataService,
        private route: ActivatedRoute,
        public appDataService: AppDataService,
           ) { }
           resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
               if(this.appDataService.userRoleMappingModel.userPermission.advancedSearch){
                this.appDataService.previousRoutingUrl = route.routeConfig.path;
                return true;
               }else{
                window.location.replace(window.location.protocol + "//" + window.location.host + this.appDataService.previousRoutingUrl);
                   return false;
               }
        }
        
}