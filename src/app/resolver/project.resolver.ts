import { Injectable } from '@angular/core';
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  Resolve,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { environment } from 'src/environments/environment';
import {
  ProductAccessModel,
  UserRoleMappingModel,
} from '../core/data-models/app-model';
import {
  AuthorizationModel,
  LoginDetailModel,
  ProductCodesAndPreferenceObject,
} from '../core/data-models/service-model';
import { APIService } from '../core/http-services/api.service';
import { AuthService } from '../core/http-services/auth.service';
import { AppDataService } from '../core/services/app-data.service';
import { DataService } from '../core/services/data.service';
import { LocalStorage, SharedUtil } from '../core/services/utilily/utility';
import * as _ from 'lodash';

@Injectable({ providedIn: 'root' })
export class ProjectResolver implements Resolve<any> {
  env: string = environment.env;
  productsSubscriptionList = [];
  showPopOnCAMobile = false;
  productAccessModelObject = new ProductAccessModel();
  httpAuthorization$: any;
  constructor(
    private apiService: APIService,
    private authService: AuthService,
    private router: Router,
    private cookieService: CookieService,
    private dataService: DataService,
    private route: ActivatedRoute,
    public appDataService: AppDataService
  ) {}

  //    resolve(): Observable<any> {
  //     this.getProductPreferenceResponse();
  //         return  this.apiService.getProductCodesAndPreference( this.appDataService.cookiesData.token)
  //   }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    return new Promise((resolve, reject) => {
      this.apiService
        .getProductCodesAndPreference(this.appDataService.cookiesData.token)
        .subscribe((response) => {
          //  let productCodesAndPreferenceObject = new ProductCodesAndPreferenceObject();
          if (response['status'] === 'true') {
            let productCodesAndPreferenceObject =
              new ProductCodesAndPreferenceObject();
            productCodesAndPreferenceObject =
              response as unknown as ProductCodesAndPreferenceObject;
            this.appDataService.productPreference =
              productCodesAndPreferenceObject as ProductCodesAndPreferenceObject;
            this.appDataService.previousRoutingUrl =
              route['_routerState']['url'];
            let role: string;
            if (this.env != 'local') {
              role = this.cookieService.get('role')
                ? this.cookieService.get('role')
                : this.cookieService.get('pgr-role');
            } else {
              role = 'Power Pulse';
            }
            //   role = this.cookieService.get('role') ? this.cookieService.get('role') : this.cookieService.get('pgr-role');
            let productCode = SharedUtil.getProductCode(role);
            this.onMobileLogin();
            this.giveAccessToUser(productCode, response['productCodes']);
            this.productsSubscriptionList = response['productCodes'];
            this.unlockUserSubscriptions(response['productPreference']);
          } else {
            if (this.env != 'local') {
              // this.deleteAllCookies();
              window.location.replace(
                window.location.protocol +
                  '//' +
                  window.location.host +
                  '/login/'
              );
            }
          }
          resolve(response);
        });
    });
  }

  // if the user has access of both CPGR and CPP then user can access both dashboard and report or
  // if the user has only CPGR pubcode then user can access only report. For CPP and CPGR user functionality
  //works same on both device (Mobile'M' and desktop 'U')

  giveAccessToUser(productCode, pubCodeArray): any {
    let device = this.showPopOnCAMobile ? 'M' : 'U';
    let finalPubCodeText: string;
    let pubCode;
    if (productCode === 'CHAP') {
      pubCode = [];
      pubCode.push(productCode);
    } else {
      pubCode = _.filter(pubCodeArray, function (o) {
        return productCode === o;
      });
    }

    const pubCodeForClst = _.filter(pubCodeArray, function (o) {
      return 'CLST' === o;
    });
    const pubCodeForCPP = _.filter(pubCodeArray, function (o) {
      return 'CPP' === o;
    });
    const pubCodeForCPGR = _.filter(pubCodeArray, function (o) {
      return 'CPGR' === o;
    });
    if (
      (pubCode[0] === 'CPGR' &&
        pubCode[0] !== 'CPP' &&
        pubCodeForClst.length &&
        !pubCodeForCPP.length) ||
      (pubCode[0] !== 'CPGR' &&
        pubCode[0] === 'CPP' &&
        pubCodeForClst.length &&
        !pubCodeForCPGR.length)
    ) {
      device = 'U';
      finalPubCodeText = pubCodeForClst
        ? pubCode[0] + '_' + pubCodeForClst[0]
        : pubCode[0];
    } else if (
      (pubCode[0] === 'CPGR' &&
        pubCodeForCPP.length &&
        pubCodeForClst.length) ||
      (pubCode[0] === 'CPP' && pubCodeForCPGR.length && pubCodeForClst.length)
    ) {
      device = 'U';
      finalPubCodeText = 'CPGR_CPP_CLST';
    } else if (
      (pubCode[0] === 'CPGR' && pubCodeForCPP.length) ||
      (pubCode[0] === 'CPP' && pubCodeForCPGR.length)
    ) {
      device = 'U';
      finalPubCodeText = 'CPGR_CPP';
    } else if (pubCode[0] === 'CPGR' && pubCode[0] === 'CPP') {
      device = 'U';
      finalPubCodeText = pubCode[0];
    } else if (
      (pubCode[0] === 'CPGR' || pubCode[0] === 'CPP') &&
      !pubCodeForClst.length
    ) {
      device = 'U';
      finalPubCodeText = pubCode[0];
    } else {
      finalPubCodeText = pubCode[0];
    }
    let userRole = new UserRoleMappingModel(finalPubCodeText, device);
    this.appDataService.userRoleMappingModel = userRole;
    if (!userRole) {
      window.location.replace(
        window.location.protocol + '//' + window.location.host + '/login/'
      );
    }
  }

  getProductPreferenceResponse(): any {
    this.apiService
      .getProductCodesAndPreference(this.appDataService.cookiesData.token)
      .subscribe((response) => {
        //  let productCodesAndPreferenceObject = new ProductCodesAndPreferenceObject();
        if (response['status'] === 'true') {
          this.productsSubscriptionList = response['productCodes'];
          let productPreference = response['productPreference'];
          this.unlockUserSubscriptions(productPreference);
          //  this.uid = response['uid'];
          // this.apiService.updateProductPreferance('NA',this.uid).subscribe((response)=>{
          //   console.log(response)
          // })
        } else {
          if (this.env != 'local') {
            // this.deleteAllCookies();
            window.location.replace(
              window.location.protocol + '//' + window.location.host + '/login/'
            );
          }
        }
        return this.productsSubscriptionList;
        //  this.emitProductListData(this.productsList)
      });
  }

  unlockUserSubscriptions(productPreference): void {
    const productsListModel = SharedUtil.getProductListObject();
    const countAccess = [];
    const object = productsListModel;
    // tslint:disable-next-line: forin
    for (const key in object) {
      this.productAccessModelObject.totalProductAccessArray = [];
      this.productAccessModelObject.productsSubscriptionList = [];
      if (Object.prototype.hasOwnProperty.call(object, key)) {
        const element = object[key];
        // element.subscriptionCode;
        this.productsSubscriptionList.forEach((productCode) => {
          if (productCode === 'CPP' || productCode === 'CPGR') {
            this.productAccessModelObject.productsSubscriptionList.push(
              productCode
            );
          }
          this.prepareProductAccessObject(productCode);
          const index = element.subscriptionCode.indexOf(productCode);
          // tslint:disable-next-line: triple-equals
          if (element.name == productPreference) {
            element.productPreference = true;
          }
          if (index > -1) {
            countAccess.push(element.name);
            element.productAccess = true;
          }
          if (productCode === 'CPP') {
            countAccess.push('Power Gauge Report');
          }
        });
      }
    }
    //this.productAccessModelObject.productsSubscriptionList =  this.productsSubscriptionList;
    // this.productAccessModelObject.totalProductAccessArray.push(countAccess);
    // this.appDataService.productAccessModel = this.productAccessModelObject;
    this.appDataService.productsListModel = productsListModel;
  }

  prepareProductAccessObject(value): void {
    switch (value) {
      case 'CPP':
        this.productAccessModelObject.dashboard = true;
        this.productAccessModelObject.report = true;
        break;
      case 'CPGR':
        this.productAccessModelObject.report = true;
        break;
      case 'CLST':
        this.productAccessModelObject.checklist = true;
        break;
      case 'CPGI':
      case 'CHAP':
        //  if(this.showPopOnCAMobile && (this.cookieService.get('role') === 'Power Gauge Analytics' || this.cookieService.get('role') === 'Chaikin Analytics')){
        //  if((this.cookieService.get('role') === 'Power Gauge Analytics' || this.cookieService.get('role') === 'Chaikin Analytics')){
        this.productAccessModelObject.dashboard = true;
        this.productAccessModelObject.report = true;
        this.productAccessModelObject.checklist = true;
        this.productAccessModelObject.powerGaugeAnalytics = true;
        // }
        break;
      default:
        break;
    }
  }

  onMobileLogin(): void {
    let _isNotMobile = (function () {
      let check = false;
      (function (a) {
        if (
          /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(
            a
          ) ||
          /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
            a.substr(0, 4)
          )
        )
          check = true;
      })(navigator.userAgent || navigator.vendor);
      return !check;
    })();

    if (!_isNotMobile) {
      this.showPopOnCAMobile = true;
    } else {
      //  this.cookieService.set('role', 'Chaikin Analytics', { path: '/', domain: '.chaikinanalytics.com' });
      // window.location.replace(window.location.protocol + "//" + window.location.host + "/");
      this.showPopOnCAMobile = false;
    }
  }
}
