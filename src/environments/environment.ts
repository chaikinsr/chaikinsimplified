// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  APIUrl: '/CPTRestSecure/app',
  email: '',
  password: '',
  token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImhhcmRlZXAuZGVvbEBwYXhjZWwubmV0Iiwic25haWQiOiIiLCJjdXN0b21lck51bWJlciI6IlNBQzAwMjMxMDM2NDQiLCJjb250YWN0aWQiOiIiLCJzdWJzY3JpcHRpb25zIjoiQ0hBUCxDTFNULENQR0ksQ1BQLENQR1IsQ1BXIiwiaWF0IjoxNjQ3NDMzNTQwLCJleHAiOjE2NDc1MTk5NDB9.lqZehnv824FOvm_18L1yNHKghidwfAAfgw5b5rcVfDo',
  env: 'dev'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
